<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ListRequestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('list_requests')->delete();


        $list_requests=[
            [
                'label'=>"کاشت مو",
                'name'=>"hair-transplantation",
                'token'=>Str::random()
            ]

        ];


        DB::table('list_requests')->insert($list_requests);
    }
}
