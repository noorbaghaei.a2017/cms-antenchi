<?php
//14ce36d3
return [

    'version'=>10.1,
    'paginate'=>25,
    'prefix-admin'=>'dashboard',
    'prefix-member'=>'my-desk',
    'prefix-page'=>'page',
    'icon'=>[
        'approved'=>'fa fa-check',
        'add'=>' fa fa-plus ',
        'role'=>' fa fa-user ',
        'detail'=>' fa fa-eye ',
        'delete'=>' fa fa-trash-o ',
        'edit'=>' fa fa-edit ',
        'gallery'=>' fa fa-image ',
        'questions'=>' fa fa-question',
        'categories'=>'fa fa-folder-open-o',
        'leaders'=>'fa fa-mortar-board',
        'advertising'=>'ion-ios-book',
    ],
    'flag'=>[
        'fa'=>'img/flag/iran.png',
        'en'=>'img/flag/united-states-of-america.png',
        'de'=>'img/flag/germany.png',
        'ru'=>'img/flag/russia.png',
        'ar'=>'img/flag/saudi-arabia.png',
        'tr'=>'img/flag/turkey.png',
        'sv'=>'img/flag/sweden.png',
    ],
    'all-lang'=>[
        'fa',
        'en',
        'de'

    ],
    'extra-lang'=>[
        'en',
        'fa'

    ],
    'country'=>[
        'iran'=>'ir',
        'arabic'=>'ar',
        'german'=>'de',
        'russia'=>'ru',
        'saudi-arabia'=>'sa',

    ],
    'notification'=>[
        'sms','call','email'
    ],
    'points'=>[
        'identifier'=>2,
    ],
    'education'=>[
        'name'=>[
            'diploma','associate_degree','bachelor','ma','doctor'
            ],
        'kind'=>[
            'part_time','full_time','working_hours','project'
        ],
        'salary'=>[
            '1000000-3000000','3000000-5000000','5000000-8000000','8000000-15000000'
        ],
        'position'=>[
            'manager'
        ],
        'experience'=>[
            '1','1-3','3-5'
        ],
        'guild'=>[
            'productive'
        ],
    ],

    'seo'=>[
        'robots'=>[
            'index','follow','noindex','nofollow','noarchive','nosnippet','all','none','noimageindex','notranslate'
        ]
    ],
    'collection-image'=>'images',
    'collection-video'=>'videos',
    'collection-images'=>'gallery',
    'collection-download'=>'download',
    'cms.collection-cv'=>'cv',
];
