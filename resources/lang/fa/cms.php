<?php

return [
    'lang'=>[
        'fa'=>'فارسی',
        'de'=>'آلمانی',
        'en'=>'انگلیسی',
        'ar'=>'عربی',

    ],
    'country_name'=>[
        'Iran'=>'ایران',
        'Germany'=>'آلمان',

    ],
    'state_name'=>[
        'Tehran'=>'تهران',
        'Hamedan'=>'همدان',

    ],
    'city_nam'=>[
        'Tehran'=>'تهران',
        'Hamedan'=>'همدان',
    ],

    'option'=>'گزینه ها',
    'add'=>'افزودن',
    'all'=>'همه',
    'main'=>'اصلی',
    'bussiness'=>'کسب و کار',
    'filter'=>'فیلتر',
    'share'=>'اشتراک ',
    'save'=>'ذخیره',
    'ad'=>'آگهی',
    'job'=>'شغل',
    'jobs'=>'شغل ها',
    'enjoy'=>'لذت ببرید',
    'law'=>'قوانین',
    'properties'=>'خصوصیت ها',
    'agree'=>'قبول دارم',
    'agree_law'=>' حریم شخصی ',
    'banner'=>'بنر',
    'slider'=>'اسلایدر',
    'video'=>'فیلم',
    'max-pixel'=>'حداکثر پیکسل',
    'max-size'=>'حداکثر اندازه',
    'header-media'=>'گالری بنر',
    'contacts'=>'اطلاعات تماس',
    'open'=>'باز',
    'close'=>'بسته',
    'location'=>'موقعیت',
    'download'=>'دانلود',
    'all_category'=>'همه دسته بندی ها',
    'home'=>'صفحه اصلی',
    'what'=>'چی',
    'where'=>'کجا ( به لاتین نوشته شود )',
    'sorry'=>'متاسفم',
    'more'=>'بیشتر',
    'captcha'=>'عبارت امنیتی را وارد کنید',
    'banner'=>'بنر',
    'reset'=>'بازیابی ',
    'pattern'=>'الگو',
    'order_by'=>'مرتب سازی',
    'slide'=>'اسلاید',
    'you-dont-have-job'=>'شما هیچ شغلی ندارید',
    'approved'=>'تایید',
    'pending'=>'در انتظار تایید',
    'error'=>'خطای سیستمی  ',
    'ads'=>'آگهی ها',
    'quality'=>'کیفیت',
    'branches'=>'شعبه ها',
    'show-galleries'=>'نمایش گالری تصاویر',
    'favorites-list'=>'لیست علاقمتدی ها',
    'client-cart'=>'سبد خرید',
    'client-messages'=>'پیام ها',
    'show-notifications'=>'پیغام ها',
    'client-bussiness'=>'کسب و کار ها',
    'edit-bussiness'=>'ویرایش کسب و کار ها',
    'show-comments'=>'دیدکاه ها',
    'show-times'=>'ساعت عا',
    'show-menus'=>'منو ها',
    'show-attributes'=>'ویژگی ها',
    'client-favorites'=>'لیست علاقمندی ها',
    'sub-category'=>'زیر دسته بندی',
    'favorites'=>'علاقمندی ها',
    'empty'=>'بدون امکانات ',
    'time'=>' زمان  ',
    'phone-2'=>'تلفن ۲',
    'sure_apporved'=>'آیا از تایید اطمینان دارید',
    'insights'=>'خلاصه کسب و کار ها',
    'social_medias'=>'شبکه های اجتماعی',
    'header_medias'=>'مدیریت بنر',
    'proposals'=>'پیشنهادات',
    'verify'=>'در حال بررسی هویت',
    'search-in-mobile'=>'در موبایل جستجو میکنید؟',
    'text-search-in-mobile'=>'شما به راحتی میتوانید در موبایل جستجو کنید',
    'find-what-you-need'=>' دنبال چه چیزی هستی',
    'invalid-foreign-postal-code'=>'کد پستی معتبر نیست',
    'edit-profile'=>'ویرایش پروفایل',
    'change-password'=>'تغییر پسورد',
    'edit'=>'ویرایش',
    'reserve'=>'رزو ها',
    'comment'=>'دیدگا ها',
    'delete'=>'حذف',
    'features'=>'ویژگی ها',
    'gallery'=>'گالری',
    'questions'=>'سوالات متداول',
    'detail'=>'جزییات',
    'create_date'=>'تاریخ ثبت',
    'update_date'=>'تاریخ بروز رسانی',
    'thumbnail'=>'تصویر شاخص',
    'title'=>'عنوان',
    'states'=>'استان ',
    'cities'=>'شهر ',
    'video'=>'ویدئو',
    'offers-box'=>'صندوق پیشنهادات',
    'text'=>'متن',
    'doctors-letter-of-appreciation'=>'تقدیر نامه پزشکی',
    'force'=>'انتشار',
    'city'=>'شهر',
    'twitter'=>'تویتر',
    'footer'=>'پایین صفحه',
    'facebook'=>'فیسبوک',
    'google'=>'گوگل اکانت',
    'skill_advertising'=>'مهارت',
    'linkdin'=>'لینکدین',
    'duration'=>'مدت',
    'excerpt'=>'خلاصه',
    'refer_link'=>'لینک معرف',
    'short_link'=>'لینک کوتاه',
    'order'=>'سفارشات',
    'my_bussiness'=>'کسب و کار های من',
    'agree_nut'=>'مقررات استفاده از سایت',
    'queuing-system'=>'سیستم نوبت دهی',
    'register-turns'=>'ثبت نوبت',
    'slug'=>'نامک',
    'reason-for-referral'=>'علت مراجعه',
    'possibilities'=>'امکانات',
    'sure_delete'=>' آیا از حذف  مطمئن هستید؟',
    'first_name'=>'نام',
    'last_name'=>'نام خانوادگی',
    'inventory'=>'موجودی',
    'code'=>'کد',
    'instagram'=>'اینستاگرام',
    'special_service'=>'خدمات خاص',
    'Businesses'=>'کسب و کار ها ',
    'more_of_business'=>'برخی از کسب و کارها ',
    'whatsapp'=>'واتساپ',
    'send'=>'ارسال',
    'necessary'=>'الزامی',
    'contact-us'=>'تماس با ما',
    'menu'=>'منو',
    'menus'=>'منو ها',
    'user-panel'=>'پنل کاربری',
    'galleries'=>'گالری تصاویر',
    'complete-account'=>'تکمیل حساب کاربری',
    'return-website'=>'بازگشت به سایت',
    'confirm-after-login'=>'درخواست شما با موفقیت ثبت شد در صورت تکمیل اطلاعات کاربری خود به قسمت ویرایش اطلاعات بروید در غیر اینصورت میتوانید مجدد به سایت بازگردید.',
    'send-code'=>'ارسال کد',
    'articles'=>'مقالات',
    'informations'=>'اخبار',
    'events'=>'رویداد ها',
    'read_more'=>'ادامه مطلب',
    'message'=>'متن',
    'follow_us'=>'ما را دنبال کنید',
    'last_article'=>'آخرین مقالات',
    'next_article'=>'مقاله بعدی',
    'previous_article'=>'مقاله قبلی',
    'productive'=>'تولیدی',
    'mobile'=>'موبایل',
    'email'=>'ایمیل',
    'logo'=>'لوگو',
    'note'=>'توجه',
    'login'=>'ورود',
    'register'=>'ثبت نام',
    'get_in_touch_with_us'=>'با ما در ارتباط باشید ',
    'send_message'=>'ارسال پیام ',
    'about_author'=>'درباره نویسنده',
    'by'=>'توسط',
    'recent_posts'=>'پست های اخیر',
    'sms'=>'اس ام اس',
    'next'=>'بعدی',
    'kind'=>'نوع',
    'shop'=>'فروشگاه',
    'manufacturer'=>'شرکت تولید کننده',
    'vendor-company'=>'شرکت فروشنده',
    'previous'=>'قبلی',
    'call'=>'تماس',
    'notifications'=>'هشدار ها',
    'countries'=>'کشور ها',
    'label'=>'برچسب',
    'create'=>'ایجاد',
    'seo'=>'سئو',
    'seo-services'=>'خدمات سئو',
    'keyword'=>'کلمات کلیدی',
    'description'=>'متن توضیحات',
    'create_article'=>'ایجاد مقاله',
    'create_information'=>'ایجاد خبر',
    'last_information'=>'آخرین اخبار',
    'tags'=>'برچسب ها',
    'company-type'=>'نوع شرکت',
    'company-name'=>' نام شرکت',
    'private-equity'=>'سهامی خاص',
    'product-name'=>'نام محصول',
    'registration-number'=>' شماره ثبت',
    'operating-license-number'=>' شماره پروانه بهره برداری',
    'economic-code'=>'کد اقتصادی',
    'does-the-product-you-produce-or-supply-have-iran-code'=>'آیا محصول تولیدی یا مورد تامین شما دارای ایران کد می باشد ',
    'do-you-use-the-services-of-other-companies-or-factories'=>' آیا از خدمات سایر شرکتها یا کارخانجات استفاده می کنید ',
    'explain-if-you-use-the-services-of-other-companies'=>' اگر از خدمات سایر شرکتها استفاده می کنید توضیح دهید.',
    'does-your-product-have-standard-name-it'=>'آیا محصول تولیدی شما دارای استاندارد است؟ نام ببرید',
    'is-your-production-method-below-certain-standard-name-it'=>'آیا روش تولید شما تحت استاندارد خاصی است؟ نام ببرید',
    'have-you-obtained-certificates-of-quality-management-environment-etc-explain'=>'آیا گواهینامه های مدیریت کیفیت، محیط زیست و... را کسب کرده اید؟ توضیح دهید',
    'do-you-have-unit-called-research-and-development'=>' آیا واحدی به نام تحقیق و توسعه دارید؟',
    'is-the-quality-control-work-of-the-product-or-production-service-done-in-your-collection'=>' آیا کار کنترل کیفیت محصول یا خدمات تولیدی در مجموعه شما انجام می شود؟',
    'number-of-staff-below-the-diploma-set-but-with-specialization'=>'تعداد پرسنل زیر فوق دیپلم مجموعه ولی با تخصص',
    'number-of-staff-above-the-diploma-set'=>'تعداد پرسنل فوق دیپلم مجموعه',
    'number-of-bachelor-staff'=>' تعداد پرسنل لیسانس مجموعه',
    'number-of-postgraduate-staff'=>' تعداد پرسنل فوق لیسانس مجموعه',
    'number-of-staff-with-doctoral-education-in-the-complex'=>'تعداد پرسنل با تحصیلات دکترا در مجموعه',
    'no'=>'نه',
    'is-there-codified-training-system-for-the-unit-staff'=>' آیا سیستم آموزشی مدون برای پرسنل آن واحد وجود دارد؟',
    'number-of-third-party-inspector-certification-during-the-past-year'=>'تعداد گواهی تائید بازرس ثالث در طی یک سال گذشته',
    'do-you-have-history-of-exporting-product-or-service-during-the-last-5-years'=>' آیا طی 5 سال گذشته دارای سابقه صادرات محصول یا خدمت هستید؟',
    'export-amount-during-the-last-5-years'=>'مقدار صادرات طی 5 سال گذشته',
    'do-you-have-the-facilities-to-transport-the-goods-to-the-place-of-the-buyer-company'=>'آیا دارای امکانات حمل کالا به محل شرکت خریدار هستید؟',
    'do-you-have-distribution-network-if-yes-please-name-the-distribution-locations'=>'آیا شبکه توزیع کالا دارید؟ در صورت مثبت بودن جواب، لطفا مکانهای توزیع را نام ببرید.',
    'do-you-have-equipment-for-packing-goods'=>'آیا دارای تجهیزات بسته بندی کالا هستید',
    'are-you-member-of-research-institute-research-associations-trade-unions-unions-etc-name-it'=>' آیا عضو موسسه پژوهشی، تحقیقاتی، انجمن های علمی، صنفی، اتحادیه ،... می باشید؟ نام ببرید',
    'how-many-days-after-the-purchased-or-repaired-goods-stay-in-your-warehouse-do-you-receive-storage-costs-and-how-much'=>'بعد از چه تعداد روز از ماندن کالای خریداری شده یا تعمیر شده در انبار خود، هزینه انبارداری دریافت می کنید؟ و چه مقدار؟',
    'licensee-name'=>'نام صاحب جواز کسب',
    'supplier-form'=>'فرم تامین کنندگان',
    'national-code-of-the-licensee'=>'کد ملی صاحب جواز کسب',
    'the-address-of-central-office'=>' آدرس دفتر مرکزی',
    'telephone-central-office'=>' تلفن دفتر مرکزی',
    'head-office-fax'=>'فکس دفتر مرکزی',
    'factory-address'=>'آدرس کارخانه',
    'do-you-have-a-stock-for-the-products-or-products-you-supply'=>' آیا برای تولیدات یا محصولاتی که تامین می کنید انبار دارید؟',
    'warehouse-capacity'=>'ظرفیت انبارها',
    'number-of-warehouses'=>' تعداد انبارها',
    'annual-production-capacity'=>'ظرفیت تولید سالانه',
    'production-volume-of-the-last-three-years'=>'حجم تولید سه سال گذشته',
    'the-average-annual-turnover-of-the-current-year'=>'حجم پول در گردش میانگین سالانه سال جاری',
    'money-in-circulation-Annual-average-of-the-last-5-years'=>'حجم پول در گردش میانگین سالانه 5 سال گذشته',
    'if-you-are-a-manufacturer-select-the-status-of-the-factory-or-workshop'=>'چنانچه تولید کننده هستید، وضعیت کارخانه یا کارگاه را انتخاب نمایید ',
    'the-factory-or-workshop-you-own'=>'کارخانه یا کارگاه تحت تملک شما ',
    'factory-or-workshop-under-your-official-rent'=>'کارخانه یا کارگاه تحت اجاره ی رسمی شما',
    'does-the-product-offered-by-you-have-a-warranty-guarantee-and-after-sales-service'=>'آیا محصول تولیدی/خدمت و یا کالای ارائه شده از طرف شما دارای گارانتی، وارانتی و خدمات پس از فروش می باشد؟',
    'do-you-offer-technical-advice-in-your-field-of-work'=>'آیا مشاوره فنی در زمینه کاری خود ارائه می دهید؟',
    'is-your-technical-advice-free-if-you-do-not-buy'=>'آیا مشاوره فنی شما در صورت عدم خرید، رایگان است؟',
    'is-your-technical-advice-free-to-buy'=>'آیا مشاوره فنی شما در صورت خرید، رایگان است؟',
    'number-of-successful-contracts-in-the-past-year-for-the-goods-under-evaluation-with-the-cement-steel-oil-and-petrochemical-industries-mines'=>'تعداد قراردادهای (یا فاکتور فروش) موفق در یک سال گذشته برای کالای مورد ارزیابی با صنایع سیمان، فولاد، نفت و پتورشیمی، معادن( به تفکیک هر شرکت تعداد قرارداد یا فاکتور را بنویسید) بغیر از اسپندار',
    'number-of-successful-contracts-in-the-past-year-with-companies-under-espandar-for-the-product-under-evaluation'=>'تعداد قراردادهای (یا فاکتور فروش) موفق در یک سال گذشته با شرکتهای زیر مجموعه اسپندار برای کالای مورد ارزیابی ( به تفکیک هر شرکت تعداد قرارداد یا فاکتور را بنویسید)',
    'factory-phone'=>'تلفن کارخانه',
    'factory-fax'=>'فکس کارخانه',
    'company-email'=>'ایمیل شرکت',
    'company-website'=>'وب سایت شرکت',
    'company-capital'=>' سرمایه شرکت',
    'names-of-board-members'=>'نام اعضای هیئت مدیره',
    'are-you-a-representative-if-yes-what-kind-of-representation'=>'آیا نماینده هستید؟ اگر بلی، چه نوع نمایندگی',
    'is-there-a-variety-of-brands-in-the-goods-you-supply'=>' آیا در کالاهای مورد تامین شما تنوع برند هم وجود دارد؟',
    'do-you-use-the-services-of-insurance-companies-for-employees-products-devices-and-buildings-explain'=>'آیا از خدمات شرکتهای بیمه جهت کارکنان، محصولات، دستگاه ها و ابنیه استفاده می کنید؟ توضیح دهید',
    'national-id'=>'شناسه ملی',
    'business-license-number'=>'شماره جواز کسب',
    'date-of-establishment'=>'تاریخ تاسیس',
    'with-limited-responsibility'=>'با مسئولیت محدود ',
    'other-cases'=>'سایر موارد',
    'field-of-activity'=>'زمینه فعایت',
    'production'=>'تولیدی',
    'sales'=>'فروش',
    'type-of-ownership'=>'نوع مالکیت',
    'governmental'=>'دولتی',
    'dashboard'=>'داشبورد ',
    'fast_access'=>'دسترسی سریع',
    'href'=>'آدرس',
    'version'=>'نسخه',
    'content'=>'محتوا',
    'advertisings'=>'آگهی ها',
    'products'=>'محصولات',
    'portfolios'=>'portfolios',
    'clients'=>'clients',
    'permissions'=>'permissions',
    'invalid-data'=>'اطلاعات مطابقت ندارد',
    'hello'=>'سلام',
    'your-welcome'=>'خوش آمدید',
    'best-advertisings'=>'best advertisings',
    'copy'=>'تمامی حقوق برای «امین نوربقایی  » محفوظ است. ',
    'profile'=>'پروفایل ',
    'subscribe-newsletter'=>'عضویت در خبرنامه',
    'enter-your-email'=>'ایمیل خود را وارد کنید',
    'find-advertisings'=>'بهترین کسب و کار ها رو پیدا کن',
    'msg_error_server'=>'درخواست شما به سرور نا موفق بود',
    'logout'=>'خروج',
    'post-an-ad'=>'ثبت تبلیغ',
    'sub_category'=>'زیر دسته بندی',
    'create_bussiness'=>'کسب و کارها',
    'add_bussiness'=>'اضافه کردن کسب کار',
    'extended'=>'تمدید شد',
    'change-image'=>'تغییر عکس',
    'position'=>'موقعیت',
    'looking-for'=>'در جستجوی',
    'staff'=>'کارمندان',
    'forget_password'=>'بازیابی رمز عبور',
    'enter-your-verify-code'=>'کد تایید را وارد کنید',
    'please-check-your-email-for-verify-account-right-now'=>'لطفا آدرس الکترونیکی خود را چک کنید',
    'verify-code'=>'تایید',
    'invalid-code'=>'کد نامعتبر است',
    'invalid-email'=>'ایمیل نامعتبر است',
    'social-media'=>'شبکه های اجتماعی',
    'return'=>'برگشت',
    'website'=>'سایت',
    'symbol'=>'نماد',
    'store_category'=>'انبار',
    'currency'=>'واحد پول',
    'select'=>'انتخاب',
    'setting'=>'تنظیمات',
    'messages'=>'پیام ها',
    'manager'=>'مدیریت',
    'token'=>'access token',
    'domain'=>'دامنه',
    'update'=>'بروز رسانی',
    'sub-menu'=>'زیر منو',
    'print'=>'چاپ',
    'icon'=>'آیکون',
    'manual-address'=>'آدرس دستی',
    'parent'=>'والد',
    'self'=>'خودم',
    'address'=>'آدرس',
    'subject'=>'موضوع',
    'answer'=>'جواب',
    'name'=>'نام',
    'customer'=>'مشتری',
    'sign-date'=>'تاریخ رزرو',
    'full_name'=>'نام و نام خانوادگی',
    'role'=>'نقش',
    'username'=>'نام کاربری',
    'password'=>'کلمه عبور',
    'info'=>'اطلاعات',
    'record'=>'رکورد',
    'awards'=>'جوایز',
    'query'=>'محدودیت',
    'discount-services'=>'خدمات تخفیف',
    'discount'=>'تخفیف',
    'amount'=>'مقدار تخفیف',
    'percentage'=>'درصد تخفیف',
    'students'=>'دانشجویان',
    'count_student'=>'تعداد دانشجویان',
    'score_student'=>'درج نمره',
    'score'=>'نمره',
    'rate'=>'امتیاز',
    'info-another'=>'اطلاعات بیشتر',
    'company'=>'شرکت',
    'company-another'=>'اطلاعات شرکت',
    'about-me'=>'درباره من',
    'about-us'=>'درباره ما',
    'services'=>'خدمات',
    'working-hours'=>'ساعات کاری',
    'relate-website'=>'سایت های مرتبط',
    'holiday'=>'تعطیل',
    'to'=>'تا',
    'slogan'=>'شعار',
    'no-role'=>'بدون نقش',
    'accessory'=>'دسترسی',
    'new-password'=>'کلمه عبور جدید',
    'current-password'=>'کلمه عبور فعلی',
    'price'=>'قیمت',
    'period'=>'دوره زمانی',
    'attributes'=>'ویژگی ها',
    'terminal'=>'ترمینال',
    'status'=>'وضعیت',
    'google-map-longitude'=>'طول نقضه گوگل',
    'google-map-latitude'=>'عرض نقشه گوگل',
    'google-map-link'=>'آدرس نقشه گوگل',
    'view'=>' بازدید',
    'like'=>' لایک',
    'address_two'=>'آدرس دوم',
    'star'=>'رتبه',
    'count_child'=>'تعداد زیر مجموعه',
    'phone'=>'تلفن',
    'manufacturer_data'=>'ایجاد کننده',
    'author'=>'نویسنده',
    'text-top-dashboard'=>'یا به اندازه آرزوهایتان تلاش کنید، یا به اندازه تلاشتان آرزو.',
    'publisher'=>'ناشر',
    'canonical'=>'لینک مرجع',
    'robots'=>'موتور جستجو',
    'android'=>'برنامه نویس موبایل',
    'senior-developer'=>'برنامه نویس ارشد',
    'database-analyst'=>'تحلیلگر دیتابیس',
    'market-finder'=>'بازار یاب',
    'the-secretary'=>'منشی',
    'accountants'=>'حسابدار',
    'graphic designer'=>'گرافیست',
    'janitor'=>'آبدار چی',
    'professor'=>'استاد',
    'human-resources-manager'=>'مدیر منابع انسانی',
    'it-expert'=>'کارشناس IT',
    'officer'=>'مسئول دفتر',
    'members'=>'کارمندان',
    'financial-manager'=>'مدیر مالی',
    'project-manager'=>'مدیر پروژه',
    'contractors'=>'پیمانکار',
    'security-expert'=>'کارشناس امنیت',
    'inspector'=>'بازرس',
    'execute-management'=>'مدیریت اجرایی',
    'educational-assistant'=>'معاونت آموزشی',
    'master-of-education'=>'کارشناس ارشد آموزش',
    'education-expert'=>'کارشناس آموزش',
    'educational-supervisor'=>'سوپروایزر آموزش',
    'support'=>'پشتیبان',
    'support-supervisor'=>'سوپروایزر , پشتیبان',
    'supervisor'=>'سوپروایزر',
    'internal-manager'=>'مدیر داخلی',
    'technical-and-engineering-deputy'=>'معاونت فنی و مهندسی',
    'the-coach'=>'مربی',
    'agent'=>'نماینده',
    'graphic-designer'=>'گرافیست',
    'search'=>'جستجو',
    'copy-right'=>'متن کپی رایت',
    'title-website'=>'نام سایت',
    'country'=>'کشور',
    'category'=>'دسته بندی',
    'file'=>'فایل',
    'download-file'=>'فایل دانلود',
    'number_limit_special'=>'محدودیت تعداد ویژه',
    'ability'=>'توانایی',
    'columns'=>'ستون ها',
    'column'=>'ستون',
    'categories'=>'دسته بندی ها',
    'fax'=>'فکس',
    'Iran'=>'ایران',
    'arabic'=>'عربستان',
    'time_limit'=>'محدوددیت زمان',
    'number_limit'=>'محدودیت تعداد',
    'work_experience'=>'سابقه کار',
    'number_employees'=>'تعداد نیرو ها',
    'job_position'=>'موقعیت شغلی',
    'postal_code'=>'کد پستی',
    'guild'=>'سنف',
    'plan'=>'پلن',
    'Germany'=>'آلمان',
    'time_start'=>'ساعت شروع',
    'date_start'=>'تاریخ شروع',
    'time_end'=>'ساعت پایان',
    'date_end'=>'تاریخ پایان',
    'week'=>'روز های هفته',
    'week-services'=>'مشخص کردن زمان های هفته',
    'date'=>'تاریخ و ساعت',
    'date-services'=>'مشخص کردن تاریخ',
    'sign-date-services'=>'مشخص کردن تاریخ رزرو',
    'saturday'=>'شنیه',
    'sunday'=>'یکشنبه',
    'monday'=>'دوشنبه',
    'flag'=>'پرچم',
    'language'=>'زبان',
    'chat-room'=>'اتاق گفتگو',
    'tuesday'=>'سه شنبه',
    'wednesday'=>'چهار شنبه',
    'thursday'=>'پنج شنبه',
    'friday'=>'جمعه',
    'event_place'=>'محل برگزاری',
    'capacity'=>'ظرفیت',
    'templates'=>'قالب ها',
    'total_hour'=>'تعداد کل ساعات',
    'leader'=>'سر گروه',
    'leaders'=>'سر گروه ها',
    'prerequisites'=>'پیش نیاز ها',
    'employer'=>'کارفرما',
    'seeker'=>'کارجو',
    'branch'=>'شعبه',
    'welcome'=>'خوش آمدید',
    'welcome_text'=>'متن خوش آمد گویی',
    'tanks_text'=>'ممنون بخاطره استقاده از وب ابلیکیشن ما!',
    'select_address'=>'انتخاب آدرس',
    'holiday-today'=>'مناسب های امروز ',
    'top-menu'=>'منو بالا',
    'bottom-menu'=>'منو پایین',
    'show_template'=>'مشاهده قالب',
    'show_profile'=>'مشاهده قالب',
    'active'=>'فعال',
    'title_fa'=>'عنوان فارسی',
    'title_en'=>'عنوان انگلیسی',
    'privacy'=>'حریم شخصی',
    'imprint'=>'مقررات استفاده از سایت',
    'help'=>'راهنمایی',
    'msg_success_server'=>'اطلاعات با موفقعیت  بررسی شد',
    'notification_email'=>'اعلان های ایمیل',
    'save_count'=>'ذخیره شده',
    'translate_english'=>'ترجمه انگلیسی',
    'translate_persion'=>'ترجمه فارسی',
    'Hinten'=>'Hinten',
    'Vorne'=>'Vorne',
    'Oben'=>'Oben',
    'options'=>'گزینه ها',
    'ok'=>'باشه',
    'public'=>'عمومی',
    'private'=>'خصوصی',
    'access'=>'دسترسی',
    'identity_card'=>'شماره ملی',
    'inactive'=>'غیر فعال',
    'gender'=>'جنسیت',
    'loss'=>'مدل ربزش',
    'yes'=>'آری',
    'cancel'=>'انصراف',
    'color'=>'رنگ',
    'fall_time'=>'زمان ریزش',
    'transplantation'=>'کاشت',
    'feeling'=>'احساس',
    'execution_time'=>'زمان اجرا',
    'age'=>'سن',
    'salary'=>'میزان حقوق',
    'count_member'=>'تعداد کارمندان',
    'male'=>'مرد',
    'female'=>'خانم',
    'city_birthday'=>'محل تولد',
    'diploma'=>'دیپلم',
    'bachelor'=>'لیسانس',
    'associate_degree'=>'فوق دیپلم',
    'ma'=>'فوق لیسانس',
    'coming_soon'=>' به زودی',
    'doctor'=>'دکتر',
    'min_salary'=>'حداقل حقوق',
    'max_salary'=>'حداکثر حقوق',
    'full_time'=>'تمام وقت',
    'order_id'=>'شماره سفارش ',
    'part_time'=>'نیمه وقت',
    'working_hours'=>'دور کاری',
    'project'=>'پروژه ای',
    'kind_advertising'=>'نوع آگهی',
    'ios_app'=>'برنامه ios',
    'product_property'=>'خصوصیات محصول',
    'product_attribute'=>'ویژگی محصول',
    'product_property_services'=>'تنظیمات خصوصیات محصولات',
    'product_attribute_services'=>'تنظیمات ویژگی محصولات',
    'android_app'=>'برنامه اندروید',
    'special'=>'ویژه',
    'normal'=>'معمولی',
    'areas'=>'ناحیه',
    'educations'=>'تحصیلات',
    'add_student'=>'اضافه کردن دانشجو ',
    'new_password'=>'پسورد جدید',
    'current_password'=>'پسورد فعلی',
    'empty_password'=>' اگر میخواهید تغییر ندهید، خالی رها کنید',
    'title.required'=>' فیلد عنوان الزامیست',
    'parent.required'=>' فیلد والد الزامیست',
    'size.required'=>' فیلد اندازه الزامیست',
    'list.required'=>' فیلد لیست متو الزامیست',
    'order.required'=>' فیلد الویت الزامیست',
    'symbol.required'=>' فیلد نماد الزامیست',
    'text.required'=>' فیلد نماد الزامیست',
    'category.required'=>' فیلد دسته بندی الزامیست',
    'excerpt.required'=>' فیلد خلاصه الزامیست',
    'answer.required'=>' فیلد جواب الزامیست',
    'status.required'=>' فیلد وضعیت الزامیست',
];
