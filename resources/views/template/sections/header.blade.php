
<!DOCTYPE HTML>
<html lang="{{LaravelLocalization::getCurrentLocale()}}">
    <head>
        <!--=============== basic  ===============-->
        <meta charset="UTF-8">
        {!! SEO::generate() !!}

            @yield('seo')
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!--=============== css  ===============-->	
        <link type="text/css" rel="stylesheet" href="{{asset('template/css/reset.css')}}">
        
       
        <link type="text/css" rel="stylesheet" href="{{asset('template/css/color.css')}}">

        <link type="text/css" rel="stylesheet" href="{{asset('template/css/shop.css')}}">
      
@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")
            <link type="text/css" rel="stylesheet" href="{{asset('template/css/rtl-plugins.css')}}">
            <link type="text/css" rel="stylesheet" href="{{asset('template/css/style.css')}}">
            <link type="text/css" rel="stylesheet" href="{{asset('template/css/plugin.css')}}">
            <link type="text/css" rel="stylesheet" href="{{asset('template/css/dashboard-style.css')}}">
            
            @else
            <link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-plugins.css')}}">
            <link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-style.css')}}">
            <link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-plugin.css')}}">
            <link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-dashboard-style.css')}}">

          
            @endif


        <!-- Date Picker CSS -->
        <!-- <link rel="stylesheet" href="{{asset('template/css/date-picker.css')}}"> -->
        <!-- persian picker CSS -->
        <!-- <link rel="stylesheet" href="src/jquery.md.bootstrap.datetimepicker.style.css" /> -->

        <!--=============== favicons ===============-->
          <!-- Favicon -->
    @if(!$setting->Hasmedia('logo'))
        <link rel="shortcut icon" href="{{asset('img/no-img.gif')}}">
        <link rel="apple-touch-icon" href="{{asset('img/no-img.gif')}}">
    @else
        <link rel="shortcut icon" href="{{$setting->getFirstMediaUrl('logo')}}">
        <link rel="apple-touch-icon" href="{{$setting->getFirstMediaUrl('logo')}}">
    @endif
    <!-- Stylesheets -->

    @yield('heads')

    </head>

    <body>
        <!--loader-->
        <div class="loader-wrap">
            <div class="loader-inner">
                <div class="loader-inner-cirle"></div>
            </div>
        </div>
        <!--loader end-->
        <!-- main start  -->
        <div id="main">
            <!-- header -->
            <header class="main-header">
                <!-- logo-->
                <a href="{{route('front.website')}}" class="logo-holder">
                @if(!$setting->Hasmedia('logo'))
                    <img src="{{asset('img/no-img.gif')}}" alt="">
                    @else
                    <img src="{{$setting->getFirstMediaUrl('logo')}}" alt="">

                    @endif
                </a>
                <!-- logo end-->
              
                <!-- header opt --> 
                @if(auth('client')->check())
                <a title="{{__('cms.add_bussiness')}}" href="{{route('client.create.bussiness')}}" class="add-list color-bg"><span><i class="fal fa-layer-plus"></i></span></a>
                
               
               <div class="header-user-menu">
                    <div class="header-user-name">
                        <span>
                        
                        @if(!$client->Hasmedia('images'))
                        <img src="{{asset('template/images/user-icon-2.jpg')}}" alt="">
                        @else

                        <img src="{{$client->getFirstMediaUrl('images')}}" alt="">
                       
                        @endif
                        </span>
                       
                    </div>
                    <ul>
                        <li><a href="{{route('client.dashboard')}}"> {{__('cms.dashboard')}}  </a></li>
                        <li><a href="{{route('client.edit')}}"> {{__('cms.edit')}}  </a></li>
                        <li><a href="{{route('client.create.bussiness')}}"> {{__('cms.jobs')}}  </a></li>
                        <li><a href="{{route('client.show.cart')}}"> {{__('cms.order')}}  </a></li>
                        <li><a href="{{route('client.logout.panel')}}">{{__('cms.logout')}} </a></li>
                   
                    </ul>
                </div>
               @else
               <div class="show-reg-form modal-open avatar-img" data-srcav="{{asset('template/images/avatar/3.jpg')}}"><i class="fal fa-user"></i>{{__('cms.login')}}</div>


               @endif
               
                <!-- header opt end--> 

                

                <!-- lang-wrap-->
                <div class="lang-wrap">
                    <div class="show-lang"><span><strong>{{LaravelLocalization::getCurrentLocale()}}</strong></span></div>
                    <ul class="lang-tooltip  no-list-style">
                                 @foreach(config('cms.all-lang') as $language)
                                    @if(LaravelLocalization::getCurrentLocale()===$language)
                                    <li><a href="#" class="current-lan" data-lantext="{{$language}}">{{__('cms.lang.'.$language)}}</a></li>
                        
                                      @else
                                      <li><a href="{{setLangFront(LaravelLocalization::getCurrentLocale(),$language)}}" data-lantext="Fr">{{__('cms.lang.'.$language)}}</a></li>
     
                                     @endif
                                @endforeach
                      
                       
                    </ul>
                </div>
                <!-- lang-wrap end-->  





                

                <!-- nav-button-wrap--> 
                <div class="nav-button-wrap color-bg">
                    <div class="nav-button">
                        <span></span><span></span><span></span>
                    </div>
                </div>
                <!-- nav-button-wrap end-->


                <!--  navigation --> 
                <div class="nav-holder main-menu" >
                    <nav>
                        <ul class="no-list-style" >
                       
                        @foreach ($top_menus as  $menu)

                       

                        @if($menu->pattern=="category")
  
          
                                    <li style="position:unset;"><a href="{{$menu->href}}">{!! convert_lang($menu,LaravelLocalization::getCurrentLocale(),'symbol') !!} <i class="fa fa-caret-down"></i></a>

                                        <!--third  level  -->
                                        <ul style="width:100%;">
                                        @foreach ($all_category_bussiness_top as $category)

                                        @if (childCountCategory($category->id))


           

                                            <li class="child-category-mobile" style="width:32%;"><a href="{{route('search.page.category.bussiness',['filter'=>0,'category'=>$category->id])}}">{!! convert_lang($category,LaravelLocalization::getCurrentLocale(),'title') !!}<i class="fa fa-caret-down"></i></a>

                                                                <!--third  level  -->
                                                <ul class="mega_menu_category" style="max-width:831px;top: 100%;left: 0 !important;right: 0 !important;width: 100%;">
                                                    @foreach (childCategoryTop($category->id,$child_active_category) as $child)
                                                    <li class="child-category-mobile" style="width:32%;"><a href="{{route('search.page.bussiness',['filter'=>0,'category'=>$child->id])}}">{!! convert_lang($child,LaravelLocalization::getCurrentLocale(),'title') !!}</a></li>
                                                    @endforeach
                                                     <li class="btn_more_menu"><a  href="{{route('search.show.category.bussiness',['category'=>$category->id])}}" >{{__('cms.more')}}</a></li>

                                                </ul>
                                                <!--third  level end-->
                                           
                                        
                                       
                                        @else

                                      

                                            <li class="child-category-mobile" style="width:32%;"><a href="{{route('search.page.bussiness',['filter'=>0,'category'=>$category->id])}}">{!! convert_lang($category,LaravelLocalization::getCurrentLocale(),'title') !!}</a>

                                      
                                        @endif
                                        </li>
                                            @endforeach
                                            <li class="btn_more_menu"><a  href="{{route('search.show.category.bussiness',['category'=>0])}}" >{{__('cms.more')}}</a></li>

                                       
                                        </ul>
                                        <!--third  level end-->

                                      

                                    </li>
                                   
                              

                      
                       

                        @elseif($menu->pattern=="search")
                        <li><a href="{{route('search.page.bussiness',['filter'=>1])}}">{!! convert_lang($menu,LaravelLocalization::getCurrentLocale(),'symbol') !!}

                        @else
                       
                        <li><a href="{{$menu->href}}">{!! convert_lang($menu,LaravelLocalization::getCurrentLocale(),'symbol') !!}
                            @if(count($menu->childs)>0)
                            <i class="fa fa-caret-down"></i>
                            @endif
                            </a>
                        
                        
                        @if(count($menu->childs) > 0)
                       
                       
                                <!--third  level  -->
                                <ul>
                                @foreach ($menu->childs as  $child_menu)

                                    <li><a href="{{$child_menu->href}}">{!! convert_lang($child_menu,LaravelLocalization::getCurrentLocale(),'symbol') !!}</a></li>
                                @endforeach

                                </ul>
                                <!--third  level end-->
                       

                        @endif
                        </li>

                        @endif

                        @endforeach
                   
                        </ul>
                    </nav>
                </div>
                <!-- navigation  end -->










               
                
            </header>
            <!-- header end-->
            <!-- wrapper-->
            <div id="wrapper">





