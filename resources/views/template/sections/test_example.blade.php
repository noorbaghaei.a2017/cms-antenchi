if($law=="true"){

$item=[
    'email'=>!is_null($email) ? $email : "",
    'password'=>!is_null($password) ? $password : "",
    'first'=>!is_null($first) ? $first : "",
    'last'=>!is_null($last) ? $last : "",
    'law'=>1

];


}
else{
$item=[
    'email'=>!is_null($email) ? $email : "",
    'password'=>!is_null($password) ? $password : "",
    'first'=>!is_null($first) ? $first : "",
    'last'=>!is_null($last) ? $last : ""

];


}



$validator = Validator::make($item, [
'email'=>'unique:clients|required|email',
'password'=>'required|min:8',
'law'=>'required',
'first'=>'required',
'last'=>'required'
]);

if($validator->fails()){

$errors=$validator->errors();

$data=[
'result'=>false,
'errors'=>$errors,
'request'=>$item
];
return response()->json(array('msg'=>'ok','data'=>$data),200);

}
else{
        $code=5044;
    $client=Client::create([
        'email'=>$email,
        'first_name'=>$first,
        'last_name'=>$last,
        'password'=>Hash::make($password),
        'code'=>$code,
        'law'=>1,
        'ip'=>showIp(),
        'code_expire'=>now()->addMinutes(10),
        'role'=>ClientRole::whereTitle('user')->firstOrFail()->id,
        'token'=>tokenGenerateSimple(),
    ]);

    $client->myCode()->create([
        'code'=>generateCode()
    ]);

    $client->update([
        'is_active'=>2,
        'expire'=>now()

    ]);

    $client->info()->create();

    $client->analyzer()->create();

    $client->company()->create();

    $client->seo()->create();


    $client->wallet()->create([
        'score'=>500
    ]);

    $client->cart()->create([
        'client'=>$client->id,
        'mobile'=>""
    ]);


    if($client){
        auth('client')->loginUsingId($client->id);
        $data=[
            'result'=>true,
        ];
        return response()->json(array('msg'=>'ok','data'=>$data),200);
    }

    else{

        $data=[
            'result'=>false,
        ];
        return response()->json(array('msg'=>'ok','data'=>$data),200);
    }

}
$data=[
'result'=>false,
'errors'=>'error'
];
return response()->json(array('msg'=>'ok','data'=>$data),200);













function Register() {

if($("input#law").is(':checked')){

    law=true;
}
else{
   law=false;
}

msg_error_server="{{__('cms.msg_error_server')}}";
email=$('input#email_r').val( ); 
$password=$('input#password_r').val( ); 
$first=$('input#first_name_r').val( ); 
$last=$('input#last_name_r').val( ); 

$.ajax({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
},
type:'POST',
contentType:'application/json; charset=utf-8',
url:'/ajax/register/client/'+email+'/'+$password+"/"+$first+'/'+$last+'/'+law,
data: {email:email} ,
beforeSend:function(){
    $('#btn-register').hide();
},
success: function (response) {
    console.log(response.data);
    if(response.data.result){
      
     

        window.location.href = '/user/panel/dashboard'
    }
    else {
       
        $("#result-ajax").empty();
        jQuery.each(response.data.errors, function(index, itemData) {
            $("#result-ajax").append("<span>"+itemData+"</span><br>");
                });
            

           
    }

},
error: function (xhr,ajaxOptions,thrownError) {
    
    $("#result-ajax").empty()
            .append("<span>"+msg_error_server+"</span>");
},
complete:function(){
    $('#btn-register').show();
}


});

}











if($("input#law").is(':checked')){

law=true;
}
else{
law=false;
}

msg_error_server="{{__('cms.msg_error_server')}}";
email=$('input#email_r').val( ); 
password=$('input#password_r').val( ); 
first=$('input#first_name_r').val( ); 
last=$('input#last_name_r').val( ); 




$("#result-ajax").empty()
                            .append("<span>"+msg_error_server+"</span>");
















                                                                             <!-- reviews-comments-item -->  
                                                                             <div class="reviews-comments-item">
                                                   
                                                   <div class="review-comments-avatar">
                                                   
                                                       <img src="{{asset('template/images/user-icon-2.jpg')}}" alt=""> 
                                                   </div>
                                                   <div class="reviews-comments-item-text fl-wrap">
                                                   <div class="listing-rating " >
                                                   @for ($i = 1; $i <= (int)$comment->rate; $i++) 
                                           <i data-content="{{$i}}"  class="fa fa-star " ></i>
                                               @endfor
                                               @for ($j = 0; $j < 5-(int)$comment->rate; ++$j) 
                                           <i data-content="{{$i++}}"  class="fa fa-star gray " ></i>
                                               @endfor
                                                       
                                                   </div>
                                                   <br>
                                                       <div class="reviews-comments-header fl-wrap">
                                                           <h4><a href="#"> {{$comment->title}}</a></h4>
                                                         
                                                       </div>
                                                      {!! $comment->text !!}
                                                       
                                                       <div class="reviews-comments-item-footer fl-wrap">
                                                           <div class="reviews-comments-item-date"><span><i class="far fa-calendar-check"></i>{{$comment->created_at->ago()}}  </span></div>
                                                       </div>
                                                   </div>
                                               </div>
                                               <!--reviews-comments-item end--> 






                                               <a href="{{setLangJob(LaravelLocalization::getCurrentLocale(),'fa',route('client.job.language.show',['lang'=>'fa','token'=>$job->token]))}}"><img src="{{asset('img/flag/iran.png')}}" style="width:30px;{{checkLangTranslate('job','en',$job->id) ? '' : 'filter: grayscale(100%);' }}"></a>
                                                <a href="{{setLangJob(LaravelLocalization::getCurrentLocale(),'en',route('client.job.language.show',['lang'=>'en','token'=>$job->token]))}}"><img src="{{asset('img/flag/united-kingdom.png')}}" style="width:30px;{{checkLangTranslate('job','en',$job->id) ? '' : 'filter: grayscale(100%);' }}"></a>
                                            