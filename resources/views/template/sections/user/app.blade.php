@include('template.sections.user.header')


<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            @yield('content')
        </div>
    </div>
</div>

@include('template.sections.user.footer')
