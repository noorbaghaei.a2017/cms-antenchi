  <!--  dashboard-menu-->
  <div class="col-md-3">
                                <div class="mob-nav-content-btn color2-bg init-dsmen fl-wrap"><i class="fal fa-bars"></i> {{__('cms.menu')}} </div>
                                <div class="clearfix"></div>
                                <div class="fixed-bar fl-wrap" id="dash_menu">
                                    <div class="user-profile-menu-wrap fl-wrap block_box">
                                        <!-- user-profile-menu-->
                                        <div class="user-profile-menu">
                                            <h3>{{__('cms.main')}}</h3>
                                            <ul class="no-list-style">
                                                <li><a href="{{route('client.dashboard')}}" class="{{\Route::current()->getName()=='client.dashboard' ? 'user-profile-act' : ''}}"><i class="fal fa-chart-line"></i>{{__('cms.dashboard')}}</a></li>
                                                <li><a href="{{route('client.edit')}}" class="{{\Route::current()->getName()=='client.edit' ? 'user-profile-act' : ''}}"><i class="fal fa-user-edit"></i> {{__('cms.edit-profile')}} </a></li>
                                                <li><a href="{{route('client.show.change.password')}}" class="{{\Route::current()->getName()=='client.show.change.password' ? 'user-profile-act' : ''}}"><i class="fal fa-key"></i> {{__('cms.change-password')}} </a></li>
                                                <li><a href=" {{route('client.favorites.job')}}" class="{{\Route::current()->getName()=='client.favorites.job' ? 'user-profile-act' : ''}}"><i class="far fa-heart"></i> {{__('cms.favorites')}} </a></li>
                                                <li><a href=" {{route('client.show.cart')}}" class="{{\Route::current()->getName()=='client.show.cart' ? 'user-profile-act' : ''}}"><i class="far fa-shopping-cart"></i> {{__('cms.order')}} </a></li>
                                                <li><a href="{{route('client.messages')}}" class="{{\Route::current()->getName()=='client.messages' ? 'user-profile-act' : ''}}">
                                                    <i class="fal fa-comment"></i>
                                                    @if(clientChatUnreadCount($client->id) > 0)
                                                        <span>{{clientChatUnreadCount($client->id)}}</span>
                                                    @endif

                                                     {{__('cms.support')}} 
                                                    </a>
                                                </li>
                                                <li><a href="{{route('client.show.notifications')}}" class="{{\Route::current()->getName()=='client.show.notifications' ? 'user-profile-act' : ''}}"><i class="fal fa-rss"></i> {{__('cms.notifications')}} </a></li>
                                            
                                                <li><a href="{{route('client.bussiness')}}" class="{{\Route::current()->getName()=='client.bussiness' ? 'user-profile-act' : ''}}"><i class="fal fa-th-list"></i> {{__('cms.my_bussiness')}}  </a></li>

                                            </ul>
                                        </div>
                                        <!-- user-profile-menu end-->
                                        <!-- user-profile-menu-->
                                       
                                        <!-- user-profile-menu end-->                                        
                                        <button class="logout_btn color2-bg">{{__('cms.logout')}} <i class="fas fa-sign-out"></i></button>
                                    </div>
                                </div>
                                <a class="back-tofilters color2-bg custom-scroll-link fl-wrap" href="#dash_menu">{{__('cms.return')}}<i class="fas fa-caret-up"></i></a>
                            </div>