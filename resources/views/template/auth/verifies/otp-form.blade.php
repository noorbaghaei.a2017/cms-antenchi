<!DOCTYPE html>
<html lang="fa">

<head>
    <meta charset="utf-8" />
    @if(!$setting->Hasmedia('logo'))
        <link rel="shortcut icon" href="{{asset('img/no-img.gif')}}">
        <link rel="apple-touch-icon" href="{{asset('img/no-img.gif')}}">
    @else
        <link rel="shortcut icon" href="{{$setting->getFirstMediaUrl('logo')}}">
        <link rel="apple-touch-icon" href="{{$setting->getFirstMediaUrl('logo')}}">
    @endif

    <link href="{{asset('template/user/css/bootstrap-rtl.css')}}" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport' />
    <title>{{__(__('cms.login'))}}   </title>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" href="{{asset('template/fonts/font-awesome/css/font-awesome.min.css')}}" />

    <link href="{{asset('template/user/css/login.css')}}" rel="stylesheet" />
</head>

<body>

<div class="container">
    <div class="info">
        <h1>{{__('cms.welcome')}} </h1>
    </div>
</div>
<div class="form">
    <div>
        <a href="{{route('front.website')}}">

            @if(!$setting->Hasmedia('logo'))
                <img src="{{asset('img/no-img.gif')}}" style="width: 150px;"/>
            @else
                <img src="{{$setting->getFirstMediaUrl('logo')}}" style="width:150px;"/>
            @endif
                <div class="hidden">
                    <span id="code" data-client="{{$data['client']->token}}"></span>
                </div>
        </a>

        <span class="invalid-feedback text-center" role="alert" style="display: block;padding: 10px">
                                        <strong id="result"></strong>
                                    </span>
        <div class="message-light">
            برای شماره همراه {{$data['mobile']}} کد تایید ارسال گردید

        </div>


    </div>

    <form action="{{ route('client.otp') }}" class="login-form" method="GET">
        @csrf
        <input id="code" name="code" type="text" placeholder="{{__('cms.code')}}" autocomplete="off"/>
        <button id="ajaxStart" onclick="getCode()">{{__('cms.login')}}</button>
    </form>
</div>



</body>
<!--   Core JS Files   -->
<script src="{{asset('template/js/jquery-3.6.0.min.js')}}" type="text/javascript"></script>

<script src="{{asset('template/user/js/login.js')}}" type="text/javascript"></script>


<script>


    function getCode() {

        $user=$('span#code').data('client');
        $code=$('input#code').val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type:'POST',
            contentType:'application/json; charset=utf-8',
            url:'/ajax/verify/'+$user+"/"+$code,
            data: { field1:$user,field2:$code} ,
            beforeSend: function(){
                $("#ajaxStart").attr("disabled", true);
            },
            success: function (response) {
                $("#container-ajaxStart button").css("display", 'none');
                $("#container-ajaxStart").append("<span>عملیات با موفقیت انحام شد در  حال اتصال به پنل کاربری شما لطفا منتطر باشید</span>");
                window.location.href = '/user/panel/welcome'
            },
            error: function (response) {
                $('#result').text('کد نامعتبر است');
            },
            complete:function(data){
                $("#ajaxStart").attr("disabled", false);

            }

        });

    }
    $(document).ready(function () {
        $("#code").keypress(function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });


    });
</script>


</html>

