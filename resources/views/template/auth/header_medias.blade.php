@extends('template.app')


@section('content')




  <!-- content-->
  <div class="content">

  @include('template.auth.header-welcome')


                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                        <div class="container">
                            @include('template.auth.menu')
                            <!-- dashboard content-->
                            <div class="col-md-9">

                            @include('template.alert.error')
                            @include('template.alert.success')

                                <!-- profile-edit-container--> 
                                <div class="profile-edit-container fl-wrap block_box">
                                    <div class="custom-form">
                                        <form action="{{route('client.change.status.banner.bussiness',['bussiness'=>$item->token])}}" method="GET">
                                        <div class="row"  >
                                        <div class="col-md-6">
                                       
                                           
                                             <select  class="chosen-select no-search-select"  type="text" name="banner_status">

                                             
                                                 
                                             <option value="1" {{$item->banner_status==1 ? 'selected' : ''}}>{{__('cms.banner')}}</option>
                                             <option value="2" {{$item->banner_status==2 ? 'selected' : ''}}>{{__('cms.slide')}}</option>
                                             
                                           


                                             </select>                                                
                                             </div>
                                             <div class="col-md-6">
                                             <button style="margin-top: 5px;width: 100%;" type="submit" class="logout_btn color2-bg">{{__('cms.update')}} <i class="fas fa-sign-out"></i></button>

                                                </div>
                                        </div>
                                        </form>
                                        @if($item->banner_status==1)
                                        <div class="row" id="banner-show">
                                            <!--col --> 
                                            <div class="col-md-12">
                           
                                                <form class="add-list-media-wrap" method="POST" action="{{route('client.remove.banner.bussiness',['bussiness'=>$item->token])}}">
                                                   @csrf
                                                    <div class="add-list-media-wrap">
                                                    @if(!$item->hasMedia('banner'))
                                                        <div class="fuzone" style="cursor:unset;background-image:url({{asset('template/images/banner_test.jpeg')}});background-size:cover;">
                                                         @else
                                                         <div class="fuzone" style="background-image:url({{$item->getFirstMediaUrl('banner')}});background-size:cover;">


                                                         @endif
                                                        </div>
                                                    </div>
                                                    @if($item->hasMedia('banner'))
                                                    <button  type="submit" class="logout_btn color2-bg">{{__('cms.delete')}} <i class="fas fa-trash"></i></button>
                                                    @endif
                                                </form>
                                            </div>
                                            <!--col end--> 
                                           
                                                                                          
                                        </div>
                                      
                                        @elseif($item->banner_status==2)
                                        <div class="row" id="slide-show">
                            @foreach ($slides as  $slide)
                                             <!--col --> 
                                             <div class="col-md-4">
                                               
                                               <form class="add-list-media-wrap" method="POST" action="{{route('client.remove.gallery.slide.bussiness')}}">
                                                   @csrf
                                                   <div>
                                                   <div>
                                                        <input hidden name="translate" value="{{$slide->id}}">
                                                   </div>
                                                   </div>
                                                   <div class="add-list-media-wrap">
                                                   <div  class="fuzone"  style="background-image:url(/media/{{$slide->id}}/{{$slide->file_name}});background-size:cover;">
    
                                                       </div>
                                                   </div>
                                                   <button type="submit" class="logout_btn color2-bg">{{__('cms.delete')}} <i class="fas fa-trash"></i></button>

                                               </form>
                                           </div>
                                           <!--col end--> 
                                        @endforeach
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <!-- profile-edit-container end-->
                            
                           

                                @if($item->banner_status==1)
                                        <!-- profile-edit-container--> 
                                <div class="profile-edit-container fl-wrap block_box" id="banner-store">
                                    <div class="custom-form">
                                        <div class="row">
                                            <!--col --> 
                                            <div class="col-md-12">
                                                                 
                                            <div>
                        <p style="color:red">image-pixel : 1920px * 1024px</p>
                        <p style="color:red">image-size : 3MB</p>
                        </div>
                                                <div class="add-list-media-wrap">
                                                    <form method="POST" action="{{route('client.store.banner.bussiness',['bussiness'=>$item->token])}}" class="add-list-media-wrap" enctype="multipart/form-data">
                                                       @csrf
                                                        <div id="bg-banner" class="fuzone">
                                                            <div id="text-image-banner" class="fu-text">
                                                                <span><i class="fal fa-image"></i>  {{__('cms.add')}}</span>
                                                            </div>
                                                                <input type="file" name="banner" class="upload" onchange="loadBanner(event)">
                                                               

                                                        </div>
                                                        <button type="submit" class="logout_btn color2-bg">{{__('cms.save')}} <i class="fas fa-sign-out"></i></button>

                                                    </form>
                                                </div>
                                            </div>
                                            <!--col end--> 
                                           
                                                                                          
                                        </div>
                                    </div>
                                </div>
                                <!-- profile-edit-container end-->
                               

                                @elseif($item->banner_status==2)
                              <!-- profile-edit-container--> 
                              <div class="profile-edit-container fl-wrap block_box" id="slide-store">
                                    <div class="custom-form">
                                        <div class="row">
                                            <!--col --> 
                                            <div class="col-md-12">

                                            <div>
                        <p style="color:red">image-size : 1920px * 1024px</p>
                        </div>
                                               
                                                <div class="add-list-media-wrap">
                                                    <form method="POST" action="{{route('client.store.gallery.slide.bussiness',['bussiness'=>$item->token])}}" class="add-list-media-wrap" enctype="multipart/form-data">
                                                       @csrf
                                                    
                                                        <div id="bg" class="fuzone">
                                                            <div id="text-image" class="fu-text">
                                                                <span><i class="fal fa-image"></i>  {{__('cms.add')}}</span>
                                                            </div>
                                                                <input type="file" name="gallery" class="upload" onchange="loadFile(event)">
                                                       

                                                        </div>

                                                        <button type="submit" class="logout_btn color2-bg">{{__('cms.save')}} <i class="fas fa-sign-out"></i></button>

                                                    </form>
                                                </div>
                                            </div>
                                            <!--col end--> 
                                           
                                                                                          
                                        </div>
                                    </div>
                                </div>
                                <!-- profile-edit-container end-->
                                @endif

                                @if($item->banner_status==2)
                                 <!-- profile-edit-container--> 
                                 <div class="profile-edit-container fl-wrap block_box">
                                    <div class="custom-form">
                                        <div class="row">

                                        @foreach ($medias as  $media)
                                             <!--col --> 
                                             <div class="col-md-4">
                                               
                                               <form class="add-list-media-wrap" method="POST" action="{{route('client.remove.gallery.bussiness')}}">
                                                   @csrf
                                                   <div>
                                                   <div>
                                                        <input hidden name="translate" value="{{$media->id}}">
                                                   </div>
                                                   </div>
                                                   <div class="add-list-media-wrap">
                                                   <div  class="fuzone"  style="background-image:url(/media/{{$media->id}}/{{$media->file_name}});background-size:cover;">
    
                                                       </div>
                                                   </div>
                                                   <button type="submit" class="logout_btn color2-bg">{{__('cms.delete')}} <i class="fas fa-trash"></i></button>

                                               </form>
                                           </div>
                                           <!--col end--> 
                                        @endforeach
                                           

                                            

                                           
                                           
                                                                                          
                                        </div>
                                    </div>
                                </div>
                                <!-- profile-edit-container end-->
                                @endif

                       
                              
                            </div>
                            <!-- dashboard content end-->
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->


@endsection


@section('heads')

@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<link type="text/css" rel="stylesheet" href="{{asset('template/css/dashboard-style.css')}}">
@else

<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-dashboard-style.css')}}">
@endif
@endsection



@section('scripts')


<script>



function loadBanner(e) {
	 image = $('#bg-banner');
     text = $('#text-image-banner');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:cover');
    
};

function loadFile(e) {
	 image = $('#bg');
     text = $('#text-image');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:cover');
    
};

            function loadCity() {



                    $code=$("#postal_code").val();
                   
               
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type:'POST',
                    contentType:'application/json; charset=utf-8',
                    url:'/ajax/load/city/postalcode/'+$code,
                    data: { field1:$code} ,
                    beforeSend:function(){
                    
                },
                    success: function (response) {
                       if(response.data.status){

                           console.log(response.data);
                           $('#city').empty();
                           $('.nice-select .list').empty();
                           $.each(response.data.result, function(index, key) {
                         
                            $('#city').append("<option value='"+key.fields.plz_name+"-"+key.fields.krs_name+"'>"+key.fields.plz_name+"-"+key.fields.krs_name+"</option>");
                            });
                            $.each(response.data.result, function(index, key) {
                               
                                $('.nice-select .list').append("<li data-value='"+key.fields.plz_name+"-"+key.fields.krs_name+"' class='option'>"+key.fields.plz_name+"-"+key.fields.krs_name+"</li>");
                            });
                              

                              

                       }
                       else{
                        console.log('no');
                       }
                    },
                    error: function (xhr,ajaxOptions,thrownError) {
                    
                    console.log(xhr,ajaxOptions,thrownError);
                        // $("#result-ajax").empty()
                        //     .append("<span>"+msg+"</span>");
                    },
                    complete:function(){
                   
                }

                });

           }

</script>


@endsection




