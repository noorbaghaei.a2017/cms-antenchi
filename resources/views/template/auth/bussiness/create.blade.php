
@extends('template.app')


@section('content')




  <!-- content-->
  <div class="content">
  <input id="lang_form" type="text" value="{{LaravelLocalization::getCurrentLocale()}}" hidden>
                <!--  section  -->
                <section class="parallax-section dashboard-header-sec gradient-bg" data-scrollax-parent="true">
                        <div class="container">
                          
                            <div class="dashboard-header_conatiner fl-wrap dashboard-header_title">
                                <h1> {{__('cms.your-welcome')}}  : <span>{{$client->full_name}}</span></h1>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                      

                        <div class="dashboard-header fl-wrap">
                            <div class="container">
                                <div class="dashboard-header_conatiner fl-wrap" style="margin-bottom:40px;">
                                    <div class="dashboard-header-avatar">
                                    @if(!$client->Hasmedia('images'))
                        <img src="{{asset('template/images/user-icon-2.jpg')}}" alt="">
                        @else

                        <img src="{{$client->getFirstMediaUrl('images')}}" alt="">
                       
                        @endif
                                        
                                        <a href="{{route('client.edit')}}" class="color-bg edit-prof_btn"><i class="fal fa-edit"></i></a>
                                    </div>
                                    <div class="dashboard-header-stats-wrap">
                                        <div class="dashboard-header-stats">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                 
                                                  
                                                  
                                                  
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    <!--  dashboard-header-stats-wrap end -->
                                    <a href="{{route('client.create.bussiness')}}" class="add_new-dashboard">{{__('cms.bussiness')}}   <i class="fal fa-layer-plus"></i></a>

                                </div>
                            </div>
                        </div>
                        <div class="gradient-bg-figure" style="right:-30px;top:10px;"></div>
                        <div class="gradient-bg-figure" style="left:-20px;bottom:30px;"></div>
                        <div class="circle-wrap" style="left:120px;bottom:120px;" data-scrollax="properties: { translateY: '-200px' }">
                            <div class="circle_bg-bal circle_bg-bal_small"></div>
                        </div>
                        <div class="circle-wrap" style="right:420px;bottom:-70px;" data-scrollax="properties: { translateY: '150px' }">
                            <div class="circle_bg-bal circle_bg-bal_big"></div>
                        </div>
                        <div class="circle-wrap" style="left:420px;top:-70px;" data-scrollax="properties: { translateY: '100px' }">
                            <div class="circle_bg-bal circle_bg-bal_big"></div>
                        </div>
                        <div class="circle-wrap" style="left:40%;bottom:-70px;"  >
                            <div class="circle_bg-bal circle_bg-bal_middle"></div>
                        </div>
                        <div class="circle-wrap" style="right:40%;top:-10px;"  >
                            <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
                        </div>
                        <div class="circle-wrap" style="right:55%;top:90px;"  >
                            <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
                        </div>
                    </section>
                    <!--  section  end-->
                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                        <div class="container">
                            @include('template.auth.menu')
                            <!-- dashboard content-->
                            <div class="col-md-9">
  
                            <form action="{{route('client.store.bussiness',['client'=>$client])}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="dashboard-title  dt-inbox fl-wrap">

<h3>{{__('cms.info')}}</h3>
</div>
                                <!-- profile-edit-container--> 
                                <div class="profile-edit-container fl-wrap block_box">

                                @include('template.alert.error')
                                <div style="margin-bottom:10px"></div>
                                    <div class="custom-form">

                                        <div class="row">
                                                <div class="col-sm-4">
                                                <label>{{__('cms.title')}} <i class="fal fa-user"></i></label>
                                                <input type="text" name="title" class="{{$errors->has('title') ? 'error-input' : ''}}" placeholder="{{__('cms.title')}}" value="{{old('title')}}" autocomplete="off"/>                                                
                                            </div>
                                            <div class="col-sm-4">
                                                <label>{{__('cms.title_en')}} <i class="fal fa-user"></i></label>
                                                <input type="text" name="title_en" class="{{$errors->has('title_en') ? 'error-input' : ''}}" placeholder="{{__('cms.title_en')}}" value="{{old('title_en')}}" autocomplete="off"/>                                                
                                            </div>
                                            <div class="col-sm-4">
                                                <label>{{__('cms.title_fa')}} <i class="fal fa-user"></i></label>
                                                <input type="text" name="title_fa" class="{{$errors->has('title_fa') ? 'error-input' : ''}}" placeholder="{{__('cms.title_fa')}}" value="{{old('title_fa')}}" autocomplete="off"/>                                                
                                            </div>
                                            <div class="col-sm-12">
                                                <label>{{__('cms.excerpt')}}  <i class="fal fa-text"></i></label>
                                                <input type="text" name="excerpt" class="{{$errors->has('excerpt') ? 'error-input' : ''}}" placeholder="{{__('cms.excerpt')}}" value="{{old('excerpt')}}" autocomplete="off"/>                                                
                                            </div>
                                            <div class="col-sm-12" style="margin-bottom:15px">
                                                <label>{{__('cms.text')}}  <i class="fal fa-text"></i></label>
                                                <textarea type="text" name="text" class="{{$errors->has('text') ? 'error-input' : ''}}" placeholder="{{__('cms.text')}}" >
                                                {{old('text')}}
                                                </textarea>                                                
                                            </div>
                                            

                                          
                                            <div class="col-sm-6">
                                             <label> {{__('cms.category')}}   </label>
                                             <select  class="chosen-select no-search-select {{$errors->has('category') ? 'error-input' : ''}}" type="text" name="category" onchange="loadSubCategory(event)">

                                             @foreach ($all_parent_category_bussiness as $category)
                                                 
                                             <option value="{{$category->id}}" {{$category->id==old('category') ? 'selected' : ''}}>{!! convert_lang($category,LaravelLocalization::getCurrentLocale(),'title') !!}</option>
                                             
                                             @endforeach


                                             </select>                                                
                                             </div>
                                             <div class="col-sm-6" >
                                             <label> {{__('cms.sub_category')}}   </label>
                                             <select id="sub_category"  class="chosen-select no-search-select " type="text" name="sub_category" >

               @if(old('sub_category', null) != null)
                                           @foreach ($all_category_bussiness->where('parent',old('category', null)) as $sub_list)
                                                 
                                                 <option value="{{$sub_list->id}}" {{$sub_list->id==old('sub_category') ? 'selected' : ''}}>{!! convert_lang($sub_list,LaravelLocalization::getCurrentLocale(),'title') !!}</option>
                                                 
                                                 @endforeach


                                                 @else

                                                 @foreach ($all_category_bussiness->where('parent',$all_parent_category_bussiness->first()->id) as $sub_list)
                                                 @if ($loop->first)
                                                 <option value="{{$sub_list->id}}" selected>{!! convert_lang($sub_list,LaravelLocalization::getCurrentLocale(),'title') !!}</option>

                                                @else

                                                <option value="{{$sub_list->id}}" >{!! convert_lang($sub_list,LaravelLocalization::getCurrentLocale(),'title') !!}</option>


                                                @endif
  
                                                 
                                                 @endforeach




                                          @endif


                                             </select>                                                
                                             </div>

                                           


   


                                            
</div>

</div>

</div>


<div class="dashboard-title  dt-inbox fl-wrap">

<h3>{{__('cms.thumbnail')}} </h3>
</div>

   <!-- profile-edit-container--> 
   <div class="profile-edit-container fl-wrap block_box">


<div style="margin-bottom:10px"></div>
    <div class="custom-form">

    <div class="info-image">
            <p style="color:red">{{__('cms.max-pixel')}} : 468px * 376px</p>
            <p style="color:red">{{__('cms.max-size')}} : 3MB</p>
            </div>
                                        <div class="listsearch-input-item fl-wrap">
                                           
                                               
                                                <div id="bg" class="fuzone">
                   <div id="text-image" class="fu-text">
                       <span><i class="fal fa-image"></i>  {{__('cms.add')}}</span>
                   </div>
                       <input type="file" name="image" class="upload" onchange="loadFile(event)">
                      

               </div>
                                              
                                            
                                        </div>
</div>
</div>                                       




<div class="dashboard-title  dt-inbox fl-wrap">

<h3>{{__('cms.location')}} / {{__('cms.contacts')}}</h3>
</div>
                                <!-- profile-edit-container--> 
                                <div class="profile-edit-container fl-wrap block_box">

                          
                                <div style="margin-bottom:10px"></div>
                                    <div class="custom-form">


                                          

                                    <div class="row">
                                         
                                            <div class="col-sm-6">
                                                <label> {{__('cms.address')}} <i class="fas fa-map-marker"></i>  </label>
                                                <input type="text" name="address" class="{{$errors->has('address') ? 'error-input' : ''}}" placeholder="{{__('cms.address')}}" value="{{old('address')}}" autocomplete="off"/>                                                
                                            </div>
                                            <div class="col-sm-6">
                                                <label> {{__('cms.address_two')}} <i class="fas fa-map-marker"></i>  </label>
                                                <input type="text" name="address_two" class="{{$errors->has('address_two') ? 'error-input' : ''}}" placeholder="{{__('cms.address_two')}}" value="{{old('address_two')}}" autocomplete="off"/>                                                
                                            </div>
                                         </div>

                                         <div class="row">
                                            <div class="col-sm-6">
                                                <label> {{__('cms.postal_code')}} <i class="far fa-globe"></i>  </label>
                                                @if(!is_null(mostJobBot(auth('client')->user()->id)))
                                                <input id="postal_code" onkeyup="loadCity(event)" class="{{$errors->has('postal_code') ? 'error-input' : ''}}" type="text" name="postal_code" placeholder="{{__('cms.postal_code')}}" value="{{getJob(mostJobBot(auth('client')->user()->id)->botable_id)->postal_code}}" autocomplete="off"/>                                                

                                                @else
                                                <input id="postal_code" onkeyup="loadCity(event)" class="{{$errors->has('postal_code') ? 'error-input' : ''}}" type="text" name="postal_code" placeholder="{{__('cms.postal_code')}}" value="{{old('postal_code')}}" autocomplete="off"/>                                                


                                                @endif
                                            </div>
                                        
                                         <div class="col-sm-6">
                                         <input hidden id="longitude" name="longitude" type="text" value="{{old('longitude')}}">
                                         <input hidden id="latitude" name="latitude" type="text" value="{{old('latitude')}}">
                                             <label> {{__('cms.city')}}   </label>
                                           
                                             <select id="city" class="chosen-select no-search-select {{$errors->has('city') ? 'error-input' : ''}}" type="text" name="city" >

                                             @if(!empty(old('city')))
                                             <option value="{{old('city')}}" selected>{{old('city')}}</option>
                                             
                                             @else

                                             @if(!is_null(mostJobBot(auth('client')->user()->id)))

                                             <option value="{{getJob(mostJobBot(auth('client')->user()->id)->botable_id)->city}}" selected>{{getJob(mostJobBot(auth('client')->user()->id)->botable_id)->city}}</option>

                                             @else
                                             <option value="" selected></option>

                                             @endif
                                             @endif

                                             </select>                                                
                                         </div>
                                        
                                     </div>
  
                                       <div class="row">
                                                <div class="col-md-6">
                                                    <label>Longitude (Drag marker on the map)<i class="fal fa-long-arrow-alt-right"></i>  </label>
                                                    <input type="text" placeholder="Map Longitude"  id="long" value=""/>                                                
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Latitude (Drag marker on the map) <i class="fal fa-long-arrow-alt-down"></i> </label>
                                                    <input type="text" placeholder="Map Latitude"  id="lat" value=""/>                                            
                                                </div>
                                        </div>
                                            <div class="map-container">
                                                <div id="singleMap" class="drag-map" data-latitude="40.7427837" data-longitude="-73.11445617675781"></div>
                                            </div>

                                       
                                            <div class="row">
                                                <div style="margin-bottom:15px"></div>
                                                <div class="col-sm-6">
                                                        <label>{{__('cms.email')}}  <i class="fal fa-user"></i></label>
                                                        <input type="text" name="email" class="{{$errors->has('email') ? 'error-input' : ''}}"  placeholder="{{__('cms.email')}}" value="{{old('email')}}" autocomplete="off"/>                                                
                                                    </div>
                                                
                                                    <div class="col-sm-6">
                                                        <label>{{__('cms.phone')}}  <i class="fal fa-user"></i></label>
                                                        <input type="text" name="phone" class="{{$errors->has('phone') ? 'error-input' : ''}}"  placeholder="{{__('cms.phone')}}" value="{{old('phone')}}" autocomplete="off"/>                                                
                                                    </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label>{{__('cms.phone-2')}}  <i class="fal fa-user"></i></label>
                                                    <input type="text" name="phone_2" class="{{$errors->has('phone_2') ? 'error-input' : ''}}"  placeholder="{{__('cms.phone')}}" value="{{old('phone_2')}}" autocomplete="off"/>                                                
                                                </div>
                                                <div class="col-sm-6">
                                                    <label>{{__('cms.mobile')}}  <i class="fal fa-user"></i></label>
                                                    <input type="text" name="mobile" class="{{$errors->has('mobile') ? 'error-input' : ''}}"  placeholder="{{__('cms.mobile')}}" value="{{old('mobile')}}" autocomplete="off"/>                                                
                                                </div>
                                           
                                         
                                            </div>
                                    </div>
                                   
                                </div>
                                <!-- profile-edit-container end--> 


                                <div class="dashboard-title  dt-inbox fl-wrap">

<h3>{{__('cms.attributes')}}</h3>
</div>


                                                                 <!-- profile-edit-container--> 
                                                                 <div class="profile-edit-container fl-wrap block_box">
                                  
                                   
                                         <!-- act-widget--> 
                                         <div class="act-widget fl-wrap">
                                         @foreach ($all_attributes as $value)
                                            <div class="act-widget-header">
                                                <h4>{{$value->title}}</h4>
                                                <div class="onoffswitch">
                                                    <input type="checkbox" name="attributes_item[]" class="onoffswitch-checkbox" value="{{$value->id}}" id="{{$value->id}}" >
                                                    <label class="onoffswitch-label" for="{{$value->id}}">
                                                    <span class="onoffswitch-inner"></span>
                                                    <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            @endforeach
                                          
                                       
                                        <!-- act-widget end--> 
                                    
                                       
                                                                                 
                                      
                                    </div>
                                </div>
                                <!-- profile-edit-container end-->  











                                
<div class="dashboard-title  dt-inbox fl-wrap">

<h3>{{__('cms.time')}}</h3>
</div>

   <!--  section  -->
   <div class="profile-edit-container fl-wrap block_box">
                           
                           <div class="custom-form">
                              
                              
                                   
                                           <label> {{__('cms.monday')}} <i class="fal fa-clock"></i></label>
                                           <input type="text" name="monday" placeholder="for example : 9AM-5PM" value=""/>
                                           <label>{{__('cms.tuesday')}}<i class="fal fa-clock"></i>  </label>
                                           <input type="text" name="tuesday" placeholder="for example : 9AM-5PM" value=""/>
                                           <label> {{__('cms.wednesday')}} <i class="fal fa-clock"></i>  </label>
                                           <input type="text" name="wednesday" placeholder="for example : 9AM-5PM" value=""/>
                                           <label> {{__('cms.thursday')}} <i class="fal fa-clock"></i>  </label>
                                           <input type="text" name="thursday" placeholder="for example : 9AM-5PM" value=""/>
                                           <label> {{__('cms.friday')}} <i class="fal fa-clock"></i>  </label>
                                           <input type="text" name="friday" placeholder="for example : 9AM-5PM" value=""/>
                                           <label> {{__('cms.saturday')}} <i class="fal fa-clock"></i>  </label>
                                           <input type="text" name="saturday" placeholder="for example : 9AM-5PM" value=""/>
                                           <label> {{__('cms.sunday')}} <i class="fal fa-clock"></i>  </label>
                                           <input type="text" name="sunday" placeholder="for example : 9AM-5PM" value=""/>
                                          
      
                               
                               <!-- dashboard content end-->
                           </div>
   </div>
                       <!--  section  end-->



                       


                       <div class="dashboard-title  dt-inbox fl-wrap">

<h3>{{__('cms.header-media')}}</h3>
</div>

    <!-- profile-edit-container--> 
    <div class="profile-edit-container fl-wrap block_box">
          
    <div class="custom-form">
                                        <div class="row">
                                            <!--col --> 
                                            <div class="col-md-4">
                                                <div class="add-list-media-header" style="margin-bottom:20px">
                                                    <label class="radio inline"> 
                                                    <input type="radio" onchange="changeStatusBanner(event)" data-info="banner_info"  name="status_banner" value="banner"  checked>
                                                    <span>{{__('cms.banner')}}</span> 
                                                    </label>
                                                </div>


                                            </div>
                                            <!--col end--> 
                                            
                                            <!--col --> 
                                            <div class="col-md-4">
                                                <div class="add-list-media-header" style="margin-bottom:20px">
                                                    <label class="radio inline"> 
                                                    <input type="radio" name="status_banner" data-info="slide_info" onchange="changeStatusBanner(event)" value="slide">
                                                    <span>{{__('cms.slider')}}</span> 
                                                    </label>
                                                </div>

                                                
                                            </div>
                                            <!--col end--> 
                                            <!--col --> 
                                            <div class="col-md-4">
                                                <div class="add-list-media-header" style="margin-bottom:20px">
                                                    <label class="radio inline"> 
                                                    <input type="radio" name="status_banner" data-info="video_info" onchange="changeStatusBanner(event)" value="video">
                                                    <span>{{__('cms.video')}}</span> 
                                                    </label>
                                                </div>
                                                
                                            </div>
                                            <!--col end-->  
                                            
                                            
                                        </div>

                                    </div>

                                    <!-- start module banner -->
                                    <div class="custom-form" id="banner_info" >

                                            <div class="row">
                                                <div class="col-md-12">
                                              

                                                
                                                <div class="add-list-media-wrap">
                                                    <div class="add-list-media-wrap">
                                                        <div class="listsearch-input-item fl-wrap">
                                                           
                                                        <div>
                                            
                                                        <div class="info-image">
                                                            <p style="color:red">{{__('cms.max-pixel')}} : 1920px * 1024px</p>
                                                            <p style="color:red">{{__('cms.max-size')}} : 3MB</p>
                                                        </div>
                        </div>
                                               <div class="add-list-media-wrap">
                                                    
                                                       <div id="banner" class="fuzone">
                                                           <div id="text-banner" class="fu-text">
                                                               <span><i class="fal fa-image"></i>  {{__('cms.add')}}</span>
                                                           </div>
                                                               <input type="file" name="banner" class="upload" onchange="loadBanner(event)" checked>
                                                              

                                                       </div>

                                                 
                                               </div>

                                                           
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                </div>
                                            </div>

                                            <!-- end module banner -->


                                            <!-- start module slide -->
                                           <div class="custom-form" id="slide_info" style="display:none;">

                                                <div class="row">
                                                    <!-- start item list  -->
                                                    <div class="col-md-4">


                                                    <div class="add-list-media-wrap">
                                                    <div class="listsearch-input-item fl-wrap">
                                                   
                                        <div>
                                            <br>
                                            <div class="info-image">
                                                            <p style="color:red">{{__('cms.max-pixel')}} : 1920px * 1024px</p>
                                                            <p style="color:red">{{__('cms.max-size')}} : 3MB</p>
                                                        </div>
                        </div>
                                               <div class="add-list-media-wrap">
                                                    
                                                       <div id="slide_1" class="fuzone">
                                                           <div id="text-slide-1" class="fu-text">
                                                               <span><i class="fal fa-image"></i>  {{__('cms.add')}}</span>
                                                           </div>
                                                               <input type="file" name="slides[]" class="upload" onchange="loadSlide1(event)">
                                                              

                                                       </div>

                                                 
                                               </div>
                                          
                                                    </div>
                                                </div>

                                                    </div>
                                                      <!-- end item list  -->
                                                       <!-- start item list  -->
                                                    <div class="col-md-4">


<div class="add-list-media-wrap">
<div class="listsearch-input-item fl-wrap">

<div>
<br>
<div class="info-image">
        <p style="color:red">{{__('cms.max-pixel')}} : 1920px * 1024px</p>
        <p style="color:red">{{__('cms.max-size')}} : 3MB</p>
    </div>
</div>
<div class="add-list-media-wrap">

   <div id="slide_2" class="fuzone">
       <div id="text-slide-2" class="fu-text">
           <span><i class="fal fa-image"></i>  {{__('cms.add')}}</span>
       </div>
           <input type="file" name="slides[]" class="upload" onchange="loadSlide2(event)">
          

   </div>


</div>

</div>
</div>

</div>
  <!-- end item list  -->
   <!-- start item list  -->
   <div class="col-md-4">


<div class="add-list-media-wrap">
<div class="listsearch-input-item fl-wrap">

<div>
<br>
<div class="info-image">
        <p style="color:red">{{__('cms.max-pixel')}} : 1920px * 1024px</p>
        <p style="color:red">{{__('cms.max-size')}} : 3MB</p>
    </div>
</div>
<div class="add-list-media-wrap">

   <div id="slide_3" class="fuzone">
       <div id="text-slide-3" class="fu-text">
           <span><i class="fal fa-image"></i>  {{__('cms.add')}}</span>
       </div>
           <input type="file" name="slides[]" class="upload" onchange="loadSlide3(event)">
          

   </div>


</div>

</div>
</div>

</div>
  <!-- end item list  -->
                                                </div>
                                            </div>

                                        <!-- end module slide -->


                                                             <!-- start module video -->
                                                             <div class="custom-form" id="video_info" style="display:none;">

                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                        <div class="add-list-media-wrap">
                                                                            <label>Youtube  <i class="fab fa-youtube"></i></label>
                                                                            <input type="text" placeholder="https://www.youtube.com/" value=""/>   
                                                                       </div>

                                                                        </div>
                                                                    </div>
                                                            </div>

                                                            <!-- end module video -->
                                    

</div>






<div class="dashboard-title  dt-inbox fl-wrap">

<h3>{{__('cms.galleries')}}</h3>
</div>

    <!-- profile-edit-container--> 
    <div class="profile-edit-container fl-wrap block_box">
          
      <!-- start module slide -->
      <div class="custom-form"  >

<div class="row">
    <!-- start item list  -->
    <div class="col-md-4">


    <div class="add-list-media-wrap">
    <div class="listsearch-input-item fl-wrap">
   
<div>
<br>
<div class="info-image">
            <p style="color:red">{{__('cms.max-pixel')}} : 1920px * 1024px</p>
            <p style="color:red">{{__('cms.max-size')}} : 3MB</p>
        </div>
</div>
<div class="add-list-media-wrap">
    
       <div id="photo_1" class="fuzone">
           <div id="text-photo-1" class="fu-text">
               <span><i class="fal fa-image"></i>  {{__('cms.add')}}</span>
           </div>
               <input type="file" name="photos[]" class="upload" onchange="loadPhoto1(event)">
              

       </div>

 
</div>

    </div>
</div>

    </div>
      <!-- end item list  -->
       <!-- start item list  -->
    <div class="col-md-4">


<div class="add-list-media-wrap">
<div class="listsearch-input-item fl-wrap">

<div>
<br>
<div class="info-image">
<p style="color:red">{{__('cms.max-pixel')}} : 1920px * 1024px</p>
<p style="color:red">{{__('cms.max-size')}} : 3MB</p>
</div>
</div>
<div class="add-list-media-wrap">

<div id="photo_2" class="fuzone">
<div id="text-photo-2" class="fu-text">
<span><i class="fal fa-image"></i>  {{__('cms.add')}}</span>
</div>
<input type="file" name="photos[]" class="upload" onchange="loadPhoto2(event)">


</div>


</div>

</div>
</div>

</div>
<!-- end item list  -->
<!-- start item list  -->
<div class="col-md-4">


<div class="add-list-media-wrap">
<div class="listsearch-input-item fl-wrap">

<div>
<br>
<div class="info-image">
<p style="color:red">{{__('cms.max-pixel')}} : 1920px * 1024px</p>
<p style="color:red">{{__('cms.max-size')}} : 3MB</p>
</div>
</div>
<div class="add-list-media-wrap">

<div id="photo_3" class="fuzone">
<div id="text-photo-3" class="fu-text">
<span><i class="fal fa-image"></i>  {{__('cms.add')}}</span>
</div>
<input type="file" name="photos[]" class="upload" onchange="loadPhoto3(event)">


</div>


</div>

</div>
</div>

</div>
<!-- end item list  -->
</div>
</div>


    </div>


 <!-- end profile-edit-container--> 



                       <div class="dashboard-title  dt-inbox fl-wrap">

<h3>{{__('cms.social-media')}}</h3>
</div>


            <!-- profile-edit-container--> 
                 <div class="profile-edit-container fl-wrap block_box">
                                  
                     <div class="custom-form">
                              <label>Website <i class="fab fa-facebook"></i></label>
                                        <input type="text" name="website" placeholder="https://www.google.com/" value=""/>
                                        <label>Facebook <i class="fab fa-facebook"></i></label>
                                        <input type="text" name="facebook" placeholder="https://www.facebook.com/" value=""/>
                                        <label>Twitter<i class="fab fa-twitter"></i>  </label>
                                        <input type="text" name="twitter" placeholder="https://twitter.com/" value=""/>
                                        <label>whatsapp<i class="fab fa-vk"></i>  </label>
                                        <input type="text" name="whatsapp" placeholder="https://whatsapp.com" value=""/>
                                        <label> Instagram <i class="fab fa-instagram"></i>  </label>
                                        <input type="text" name="instagram" placeholder="https://www.instagram.com/" value=""/>
                                    </div>                    


</div>




                               
                              
<button type="submit" class="logout_btn color2-bg">{{__('cms.save')}} <i class="fas fa-sign-out"></i></button>

                                </form>                                  
                            </div>
                            <!-- dashboard content end-->
                        </div>
                    </section>

                    

                      
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->


@endsection


@section('heads')

@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<link type="text/css" rel="stylesheet" href="{{asset('template/css/dashboard-style.css')}}">
@else

<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-dashboard-style.css')}}">
@endif
@endsection



@section('scripts')


<script>

$('#showCheckoutHistory').change(function() {
        if ($(this).prop('checked')) {
            alert("You have elected to show your checkout history."); //checked
        }
        else {
            alert("You have elected to turn off checkout history."); //not checked
        }
    });

    function changeStatusBanner(e) {

        $('#banner_info').hide();
        $('#slide_info').hide();
        $('#video_info').hide();
      $target="#"+e.target.dataset.info;
      $($target).show();
	 
};

function loadFile(e) {
	 image = $('#bg');
     text = $('#text-image');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:cover');
    
};

function loadSlide1(e) {
	 image = $('#slide_1');
     text = $('#text-slide-1');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:cover');
    
};
function loadSlide2(e) {
	 image = $('#slide_2');
     text = $('#text-slide-2');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:cover');
    
};
function loadSlide3(e) {
	 image = $('#slide_3');
     text = $('#text-slide-3');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:cover');
    
};



function loadPhoto1(e) {
	 image = $('#photo_1');
     text = $('#text-photo-1');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:cover');
    
};
function loadPhoto2(e) {
	 image = $('#photo_2');
     text = $('#text-photo-2');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:cover');
    
};
function loadPhoto3(e) {
	 image = $('#photo_3');
     text = $('#text-photo-3');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:cover');
    
};

function loadBanner(e) {
	 image = $('#banner');
     text = $('#text-banner');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:cover');
    
};



function loadSubCategory(e) {


     
$category= e.target.value;
$lang=$("#lang_form").val();




$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    contentType:'application/json; charset=utf-8',
    url:'/ajax/load/sub/category/bussiness/'+$category+"/"+$lang,
    data: { field1:$category,field2:$lang} ,
    beforeSend:function(){
    
},
beforeSend:function(){
    $('#sub_category + .nice-select').attr('style','opacity:0');
},

success: function (response) {


   if(response.data.status){

       console.log(response.data);

       $('#sub_category').empty();
     
       $('#sub_category + .nice-select .list').empty();

       $('#sub_category + .nice-select > span.current').text('');

       $('#sub_category + .nice-select .list').empty();
       
     
     //start for each for load select input 

       $.each(response.data.result, function(key, index) {
   
                        if(key==0){
                                $('#sub_category').append("<option value='"+index.id+"'   selected>"+index.translate+"</option>");
                                $('#sub_category + .nice-select > span.current').text(index.translate);
                            }
                            else{
                                $('#sub_category').append("<option value='"+index.id+"'   >"+index.translate+"</option>");
                                $('#sub_category + .nice-select  ul.list ').append("<li class='option'  data-value='"+index.id+"'   >"+index.translate+"</li>"); 
                            }
         
 
        });

       }


  //end for each for load select input 

   
},
error: function (xhr,ajaxOptions,thrownError) {

console.log(xhr,ajaxOptions,thrownError);

    $("#sub_category").empty();
    $('#sub_category + .nice-select .list').empty();
    $('#sub_category + .nice-select > span.current').text('');
    $('#sub_category + .nice-select').attr('style','opacity:0');

  
},
complete:function(){
    $('#sub_category + .nice-select').attr('style','opacity:1');
}

});
}

function loadCity(e) {

var myInput = document.getElementById("postal_code");
     


if(myInput.value.length==5){



console.log($('#latitude').val());
          

    $code=$("#postal_code").val();
   

$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    contentType:'application/json; charset=utf-8',
    url:'/ajax/load/city/postalcode/'+$code,
    data: { field1:$code} ,
    beforeSend:function(){
        $('#city + .nice-select').attr('style','opacity:0');
},
    success: function (response) {
       if(response.data.status){

           console.log(response.data);
           $('#city').empty();
           $('#city + .nice-select .list').empty();
           $('#city + .nice-select > span.current').text('');
         
            $('#city').append("<option value='"+response.data.result[0].fields.plz_name+"' selected>"+response.data.result[0].fields.plz_name+"</option>");
           
           
            $('#city + .nice-select .list').empty();
                $('#city + .nice-select .list').append("<li data-value='"+response.data.result[0].fields.plz_name+"' class='option  selected focus'>"+response.data.result[0].fields.plz_name+"</li>");
           
                $('#city + .nice-select > span.current').text(response.data.result[0].fields.plz_name);

              

       }
       else{
        console.log('no');
       }
    },
    error: function (xhr,ajaxOptions,thrownError) {
    
    console.log(xhr,ajaxOptions,thrownError);
    $('#city + .nice-select > span.current').text('');
    $('#city + .nice-select').attr('style','opacity:0');

    },
    complete:function(){
        $('#city + .nice-select').attr('style','opacity:1');
}

});

}
}

</script>


@endsection




