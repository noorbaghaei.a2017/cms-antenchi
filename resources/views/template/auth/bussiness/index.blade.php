@extends('template.app')


@section('content')




  <!-- content-->
  <div class="content">
                <!--  section  -->
                <section class="parallax-section dashboard-header-sec gradient-bg" data-scrollax-parent="true">
                        <div class="container">
                           
                            <div class="dashboard-header_conatiner fl-wrap dashboard-header_title">
                                <h1> {{__('cms.your-welcome')}}  : <span>{{$client->full_name}}</span></h1>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="dashboard-header fl-wrap">
                            <div class="container">
                                <div class="dashboard-header_conatiner fl-wrap" style="margin-bottom:40px;">
                                    <div class="dashboard-header-avatar">
                                    @if(!$client->Hasmedia('images'))
                        <img src="{{asset('template/images/user-icon-2.jpg')}}" alt="">
                        @else

                        <img src="{{$client->getFirstMediaUrl('images')}}" alt="">
                       
                        @endif
                                        
                                        <a href="{{route('client.edit')}}" class="color-bg edit-prof_btn"><i class="fal fa-edit"></i></a>
                                    </div>
                                    <div class="dashboard-header-stats-wrap">
                                        <div class="dashboard-header-stats">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                 
                                                  
                                                  
                                                  
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    <!--  dashboard-header-stats-wrap end -->
                                    <a href="{{route('client.create.bussiness')}}" class="add_new-dashboard">{{__('cms.bussiness')}}   <i class="fal fa-layer-plus"></i></a>

                                </div>
                            </div>
                        </div>
                        <div class="gradient-bg-figure" style="right:-30px;top:10px;"></div>
                        <div class="gradient-bg-figure" style="left:-20px;bottom:30px;"></div>
                        <div class="circle-wrap" style="left:120px;bottom:120px;" data-scrollax="properties: { translateY: '-200px' }">
                            <div class="circle_bg-bal circle_bg-bal_small"></div>
                        </div>
                        <div class="circle-wrap" style="right:420px;bottom:-70px;" data-scrollax="properties: { translateY: '150px' }">
                            <div class="circle_bg-bal circle_bg-bal_big"></div>
                        </div>
                        <div class="circle-wrap" style="left:420px;top:-70px;" data-scrollax="properties: { translateY: '100px' }">
                            <div class="circle_bg-bal circle_bg-bal_big"></div>
                        </div>
                        <div class="circle-wrap" style="left:40%;bottom:-70px;"  >
                            <div class="circle_bg-bal circle_bg-bal_middle"></div>
                        </div>
                        <div class="circle-wrap" style="right:40%;top:-10px;"  >
                            <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
                        </div>
                        <div class="circle-wrap" style="right:55%;top:90px;"  >
                            <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
                        </div>
                    </section>
                    <!--  section  end-->
                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                        <div class="container">
                            @include('template.auth.menu')
                            <!-- dashboard content-->
                            <div class="col-md-9">
                             
                           
                             <!-- dashboard-list-box--> 
                                 <div class="dashboard-list-box  fl-wrap">
                                 @foreach ($client->jobs as $job)
                                     
                                 <!-- dashboard-list -->    
                                 <div class="dashboard-list jobs fl-wrap">
                                        <div class="dashboard-message">
                                           
                                            <div class="booking-list-contr">
                                          
                                                     <a href="{{route('client.show.edit.bussiness',['bussiness'=>$job->token])}}" class="color-bg tolt" data-microtip-position="left" data-tooltip="{{__('cms.edit')}}"><i class="fal fa-edit"></i></a>
                                                    <a href="{{route('client.show.comments',['bussiness'=>$job->token])}}" class="color-bg tolt" data-microtip-position="left" data-tooltip="{{__('cms.comment')}}"><i class="fal fa-envelope"></i></a>
                                                    <a href="{{route('client.show.galleries',['bussiness'=>$job->token])}}" class="color-bg tolt" data-microtip-position="left" data-tooltip="{{__('cms.galleries')}}"><i class="fal fa-image"></i></a>
                                                    <a href="{{route('client.show.times',['bussiness'=>$job->token])}}" class="color-bg tolt" data-microtip-position="left" data-tooltip="{{__('cms.time')}}"><i class="fal fa-clock"></i></a>
                                                    <a href="{{route('client.show.menus',['bussiness'=>$job->token])}}" class="color-bg tolt" data-microtip-position="left" data-tooltip="{{__('cms.menus')}}"><i class="fal fa-bars"></i></a>
                                                    <a href="{{route('client.show.attributes',['bussiness'=>$job->token])}}" class="color-bg tolt" data-microtip-position="left" data-tooltip="{{__('cms.attributes')}}"><i class="fal fa-info"></i></a>
                                                    <a href="{{route('client.show.social_medias',['bussiness'=>$job->token])}}" class="color-bg tolt" data-microtip-position="left" data-tooltip="{{__('cms.social_medias')}}"><i class="fa fa-users"></i></a>
                                                    <a href="{{route('client.show.header_medias',['bussiness'=>$job->token])}}" class="color-bg tolt" data-microtip-position="left" data-tooltip="{{__('cms.header_medias')}}"><i class="fa fa-file-image"></i></a>

                                            </div>
                                            
                                            <div class="dashboard-message-text">
                                            <div class="option_button">
                                            
                                                <a class="translate_buttom btn btn-primary"  style="background: #4DB7FE !important;"  href="{{setLangJob(LaravelLocalization::getCurrentLocale(),'fa',route('client.job.language.show',['lang'=>'fa','token'=>$job->token]))}}">{{__('cms.translate_persion')}}</a>
                                                <a class="translate_buttom btn btn-primary"   style="background: #4DB7FE !important;"  href="{{setLangJob(LaravelLocalization::getCurrentLocale(),'en',route('client.job.language.show',['lang'=>'en','token'=>$job->token]))}}">{{__('cms.translate_english')}}</a>
                                            
                                            </div>
                                            <br>
                                                            @if(!$job->Hasmedia('images'))
                                                                   <img  src="{{asset('template/images/no-image.jpeg')}}" alt="title" title="title" >
                                                               @else
                                                               <img  src="{{$job->getFirstMediaUrl('images')}}" alt="title" title="title" >
                                                               @endif
                                               
                                                <h4>
                                                    <a href="{{route('bussiness.single',['bussiness'=>$job->slug])}}">{{convert_lang($job,LaravelLocalization::getCurrentLocale(),'title')}} -  </a><span>{{__('cms.'.$job->ShowStatus)}}</span>
                                                </h4>
                                                <div class="geodir-category-location clearfix"><a href="#"> {{$job->city}} </a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- dashboard-list end-->   
                                 @endforeach
                                     
                                                                           
                                </div>
                                <!-- dashboard-list-box end--> 
                           
                            </div>
                            <!-- dashboard content end-->
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->


@endsection


@section('heads')

@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<link type="text/css" rel="stylesheet" href="{{asset('template/css/dashboard-style.css')}}">
@else

<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-dashboard-style.css')}}">
@endif
@endsection



@section('scripts')


<script>

            function loadCity() {



                    $code=$("#postal_code").val();
                   
               
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type:'POST',
                    contentType:'application/json; charset=utf-8',
                    url:'/ajax/load/city/postalcode/'+$code,
                    data: { field1:$code} ,
                    beforeSend:function(){
                    
                },
                    success: function (response) {
                       if(response.data.status){

                           console.log(response.data);
                           $('#city').empty();
                           $('.nice-select .list').empty();
                           $.each(response.data.result, function(index, key) {
                         
                            $('#city').append("<option value='"+key.fields.plz_name+"-"+key.fields.krs_name+"'>"+key.fields.plz_name+"-"+key.fields.krs_name+"</option>");
                            });
                            $.each(response.data.result, function(index, key) {
                               
                                $('.nice-select .list').append("<li data-value='"+key.fields.plz_name+"-"+key.fields.krs_name+"' class='option'>"+key.fields.plz_name+"-"+key.fields.krs_name+"</li>");
                            });
                              

                              

                       }
                       else{
                        console.log('no');
                       }
                    },
                    error: function (xhr,ajaxOptions,thrownError) {
                    
                    console.log(xhr,ajaxOptions,thrownError);
                        // $("#result-ajax").empty()
                        //     .append("<span>"+msg+"</span>");
                    },
                    complete:function(){
                   
                }

                });

           }

</script>


@endsection




