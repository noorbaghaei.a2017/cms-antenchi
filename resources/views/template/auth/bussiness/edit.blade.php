@extends('template.app')


@section('content')




  <!-- content-->
  <div class="content">
  <input id="lang_form" type="text" value="{{LaravelLocalization::getCurrentLocale()}}" hidden>

                <!--  section  -->
                <section class="parallax-section dashboard-header-sec gradient-bg" data-scrollax-parent="true">
                        <div class="container">
                          
                            <div class="dashboard-header_conatiner fl-wrap dashboard-header_title">
                                <h1> {{__('cms.your-welcome')}}  : <span>{{$client->full_name}}</span></h1>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                       
                        <div class="dashboard-header fl-wrap">
                            <div class="container">
                                <div class="dashboard-header_conatiner fl-wrap" style="margin-bottom:40px;">
                                    <div class="dashboard-header-avatar">
                                    @if(!$client->Hasmedia('images'))
                        <img src="{{asset('template/images/user-icon-2.jpg')}}" alt="">
                        @else

                        <img src="{{$client->getFirstMediaUrl('images')}}" alt="">
                       
                        @endif
                                        
                                        <a href="{{route('client.edit')}}" class="color-bg edit-prof_btn"><i class="fal fa-edit"></i></a>
                                    </div>
                                    <div class="dashboard-header-stats-wrap">
                                        <div class="dashboard-header-stats">
                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                 
                                                  
                                                  
                                                  
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    <!--  dashboard-header-stats-wrap end -->
                                    <a href="{{route('client.create.bussiness')}}" class="add_new-dashboard">{{__('cms.bussiness')}}   <i class="fal fa-layer-plus"></i></a>

                                </div>
                            </div>
                        </div>
                        <div class="gradient-bg-figure" style="right:-30px;top:10px;"></div>
                        <div class="gradient-bg-figure" style="left:-20px;bottom:30px;"></div>
                        <div class="circle-wrap" style="left:120px;bottom:120px;" data-scrollax="properties: { translateY: '-200px' }">
                            <div class="circle_bg-bal circle_bg-bal_small"></div>
                        </div>
                        <div class="circle-wrap" style="right:420px;bottom:-70px;" data-scrollax="properties: { translateY: '150px' }">
                            <div class="circle_bg-bal circle_bg-bal_big"></div>
                        </div>
                        <div class="circle-wrap" style="left:420px;top:-70px;" data-scrollax="properties: { translateY: '100px' }">
                            <div class="circle_bg-bal circle_bg-bal_big"></div>
                        </div>
                        <div class="circle-wrap" style="left:40%;bottom:-70px;"  >
                            <div class="circle_bg-bal circle_bg-bal_middle"></div>
                        </div>
                        <div class="circle-wrap" style="right:40%;top:-10px;"  >
                            <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
                        </div>
                        <div class="circle-wrap" style="right:55%;top:90px;"  >
                            <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
                        </div>
                    </section>
                    <!--  section  end-->
                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                        <div class="container">
                            @include('template.auth.menu')
                            <!-- dashboard content-->
                            <div class="col-md-9">
                             
                            <form action="{{route('client.update.bussiness',['bussiness'=>$item->token])}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    @method('PATCH')
                                  
                                <!-- profile-edit-container--> 
                                <div class="profile-edit-container fl-wrap block_box">
                                    <div class="dashboard-title  dt-inbox fl-wrap">

<h3>{{__('cms.info')}}</h3>
</div>
                                @include('template.alert.error')
                                <div style="margin-bottom:10px"></div>
                                    <div class="custom-form">

                                    <div class="row">
                                                <div class="col-sm-12">
                                                <label>{{__('cms.title')}} <i class="fal fa-user"></i></label>
                                                <input type="text" name="title" class="{{$errors->has('title') ? 'error-input' : ''}}" placeholder="{{__('cms.title')}}" value="{{old('title') ? old('title') : $item->title}}" autocomplete="off"/>                                                
                                            </div>
                                           
                                            <div class="col-sm-12">
                                                <label>{{__('cms.excerpt')}}  <i class="fal fa-user"></i></label>
                                                <input type="text" name="excerpt" class="{{$errors->has('excerpt') ? 'error-input' : ''}}"  placeholder="{{__('cms.excerpt')}}" value="{{old('excerpt') ? old('excerpt') : $item->excerpt}}" autocomplete="off"/>                                                
                                            </div>
                                            <div class="col-sm-12" style="margin-bottom:15px">
                                                <label>{{__('cms.text')}}  <i class="fal fa-text"></i></label>
                                                <textarea type="text" name="text" class="{{$errors->has('text') ? 'error-input' : ''}}" placeholder="{{__('cms.text')}}">
                                                {{old('text') ? old('text') : $item->text}}
                                                </textarea>                                                
                                            </div>

                                    <div class="col-sm-6">
                                             <label> {{__('cms.category')}} <i class="far fa-globe"></i>  </label>
                                             <select  class="chosen-select no-search-select {{$errors->has('category') ? 'error-input' : ''}}" type="text" name="category" onchange="loadSubCategory(event)">
                                             @if(showServiceCategory($item->service)->parent==0)
                                             @foreach ($all_parent_category_bussiness as $category)

                                            
                                             <option value="{{$category->id}}" {{$item->service==$category->id ? "selected" : ""}}>{!! convert_lang($category,LaravelLocalization::getCurrentLocale(),'title') !!}</option>

                                            

                                             @endforeach
                                             @else
                                            
                                            

                                                 @foreach ($all_parent_category_bussiness as $category)
                                                 <option value="{{$category->id}}" {{showServiceCategory($item->service)->parent==$category->id ? "selected" : ""}}>{!! convert_lang($category,LaravelLocalization::getCurrentLocale(),'title') !!}</option>
                                                 @endforeach

                                                 
                                                

                                             @endif


                                             </select>                                                
                                             </div>

                                             <div class="col-sm-6" >
                                             <label> {{__('cms.sub_category')}} <i class="far fa-globe"></i>  </label>
                                             <select id="sub_category"  class="chosen-select no-search-select " type="text" name="sub_category" >

                                           
                                             

                                             @foreach($all_category_bussiness->where('parent',showServiceCategory($item->service)->parent) as $sub_category)
                                             
                                             <option value="{{$sub_category->id}}" {{$item->service==$sub_category->id ? "selected" : ""}}>{!! convert_lang($sub_category,LaravelLocalization::getCurrentLocale(),'title') !!}</option>
                                             

                                             @endforeach

                                            


                                             </select>                                                
                                             </div>


                                          
                                           
</div>

</div>
</div>


<div class="dashboard-title  dt-inbox fl-wrap">

<h3>{{__('cms.thumbnail')}} </h3>
</div>

   <!-- profile-edit-container--> 
   <div class="profile-edit-container fl-wrap block_box">


    <div style="margin-bottom:10px"></div>
    <div class="custom-form">

            <div class="row">

<!--col --> 
<div class="col-md-12">
<div>
<br>
<div class="info-image">
                        <p style="color:red">{{__('cms.max-pixel')}} : 468px * 376px</p>
                        <p style="color:red">{{__('cms.max-size')}} : 3MB</p>
                        </div>
                        </div>
       <div class="add-list-media-wrap">
             
              @if(!$item->Hasmedia('images'))
                      <div id="bg" class="fuzone">
                   <div id="text-image" class="fu-text">
                       <span><i class="fal fa-image"></i>  {{__('cms.add')}}</span>
                   </div>
                       <input type="file" name="image" class="upload" onchange="loadFile(event)">
                      

               </div>
                       
                        @else

                        <div id="bg" class="fuzone" style="background:url({{$item->getFirstMediaUrl('images')}});background-size:cover">
                   <div id="text-image" class="fu-text" style="display:none">
                       <span><i class="fal fa-image"></i>  {{__('cms.add')}}</span>
                   </div>
                       <input type="file" name="image" class="upload" onchange="loadFile(event)">
                      

               </div>
                       
                       
                        @endif
               

         
       </div>
   </div>
   <!--col end--> 

            </div>                                  
                                            
                                      
    </div>
</div>                                       






                           <!-- profile-edit-container--> 
                           <div class="profile-edit-container fl-wrap block_box">
                                    <div class="dashboard-title  dt-inbox fl-wrap">

<h3>{{__('cms.location')}} / {{__('cms.contacts')}}</h3>
</div>
                              
                                <div style="margin-bottom:10px"></div>
                                    <div class="custom-form">
                                    <div class="row">

                                           <div style="margin-bottom:15px"></div>
                                           
                                           <div class="col-sm-6">
                                                <label>{{__('cms.email')}}  <i class="fal fa-user"></i></label>
                                                <input type="text" name="email" class="{{$errors->has('email') ? 'error-input' : ''}}"  placeholder="{{__('cms.email')}}" value="{{old('email') ? old('email') : $item->email}}" autocomplete="off"/>                                                
                                            </div>
                                            <div class="col-sm-6">
                                                <label>{{__('cms.phone')}}  <i class="fal fa-user"></i></label>
                                                <input type="text" name="phone" class="{{$errors->has('phone') ? 'error-input' : ''}}"  placeholder="{{__('cms.phone')}}" value="{{old('phone') ? old('phone') : $item->phone}}" autocomplete="off"/>                                                
                                            </div>
                                            <div class="col-sm-6">
                                                <label>{{__('cms.phone-2')}}  <i class="fal fa-user"></i></label>
                                                <input type="text" name="phone_2" class="{{$errors->has('phone-2') ? 'error-input' : ''}}"  placeholder="{{__('cms.phone')}}" value="{{old('phone_2') ? old('phone_2') : $item->phone_2}}" autocomplete="off"/>                                                
                                            </div>
                                            <div class="col-sm-6">
                                                <label>{{__('cms.mobile')}}  <i class="fal fa-user"></i></label>
                                                <input type="text" name="mobile" class="{{$errors->has('mobile') ? 'error-input' : ''}}"  placeholder="{{__('cms.mobile')}}" value="{{old('mobile') ? old('mobile') : $item->mobile}}" autocomplete="off"/>                                                
                                            </div>
                                           
                                         
                                    </div>

                                    <div class="row">
                                         
                                         <div class="col-sm-6">
                                             <label> {{__('cms.address')}} <i class="fas fa-map-marker"></i>  </label>
                                             <input type="text" name="address" class="{{$errors->has('address') ? 'error-input' : ''}}" placeholder="{{__('cms.address')}}" value="{{old('address') ? old('address') : $item->address}}" autocomplete="off"/>                                                
                                         </div>
                                         <div class="col-sm-6">
                                             <label> {{__('cms.address_two')}} <i class="fas fa-map-marker"></i>  </label>
                                             <input type="text" name="address_two" class="{{$errors->has('address_two') ? 'error-input' : ''}}" placeholder="{{__('cms.address_two')}}" value="{{old('address_two') ? old('address_two') : $item->address_two}}"  autocomplete="off"/>                                                
                                         </div>
                                         <div class="col-sm-6">
                                             <label> {{__('cms.postal_code')}} <i class="far fa-globe"></i>  </label>
                                             <input id="postal_code" class="{{$errors->has('postal_code') ? 'error-input' : ''}}" onkeyup="loadCity(event)" type="text" name="postal_code" placeholder="{{__('cms.postal_code')}}" value="{{old('postal_code') ? old('postal_code') : $item->postal_code}}" autocomplete="off"/>                                                
                                         </div>
                                         <div class="col-sm-6">
                                         <input id="longitude" hidden name="longitude" type="text" value="{{$item->longitude}}">
                                         <input id="latitude" hidden name="latitude" type="text" value="{{$item->latitude}}">
                                             <label> {{__('cms.city')}}  </label>
                                             <select id="city" class="chosen-select no-search-select {{$errors->has('city') ? 'error-input' : ''}}" type="text" name="city">
                                             @if(!empty(old('city')))
                                             <option value="{{old('city')}}" selected>{{old('city')}}</option>
                                             
                                             @else
                                             <option value="{{$item->city}}" selected>{{$item->city}}</option>
                                                @endif
                                             </select>                                                
                                         </div>
                                        



                                         <div class="row">
                                            <div class="col-md-6">
                                                <label>Longitude (Drag marker on the map)<i class="fal fa-long-arrow-alt-right"></i>  </label>
                                                <input type="text" placeholder="Map Longitude"  id="long" value=""/>                                                
                                            </div>
                                            <div class="col-md-6">
                                                <label>Latitude (Drag marker on the map) <i class="fal fa-long-arrow-alt-down"></i> </label>
                                                <input type="text" placeholder="Map Latitude"  id="lat" value=""/>                                            
                                            </div>
                                        </div>
                                        <div class="map-container">
                                            <div id="singleMap" class="drag-map" data-latitude="40.7427837" data-longitude="-73.11445617675781"></div>
                                        </div>
                                       
                                        
                                     </div>
                                       


                                    </div>

                                </div>
                                <!-- profile-edit-container end--> 




<button type="submit" class="logout_btn color2-bg">{{__('cms.save')}} <i class="fas fa-sign-out"></i></button>


                                </form>     
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->


@endsection


@section('heads')

@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<link type="text/css" rel="stylesheet" href="{{asset('template/css/dashboard-style.css')}}">
@else

<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-dashboard-style.css')}}">
@endif
@endsection



@section('scripts')


<script>


function loadFile(e) {
	 image = $('#bg');
     text = $('#text-image');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:cover');
    
};


function loadSlide(e) {
	 image = $('#slide');
     text = $('#text-slide');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:cover');
    
};

function loadBanner(e) {
	 image = $('#banner');
     text = $('#text-banner');
    text.hide();
    
    image.attr('style','background-image:url("'+URL.createObjectURL(event.target.files[0])+'");background-size:cover');
    
};



function loadSubCategory(e) {


     
$category= e.target.value;
$lang=$("#lang_form").val();




$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    contentType:'application/json; charset=utf-8',
    url:'/ajax/load/sub/category/bussiness/'+$category+"/"+$lang,
    data: { field1:$category,field2:$lang} ,
    beforeSend:function(){
    
},
beforeSend:function(){
    $('#sub_category + .nice-select').attr('style','opacity:0');
},

success: function (response) {


   if(response.data.status){

       console.log(response.data);

       $('#sub_category').empty();
     
       $('#sub_category + .nice-select .list').empty();

       $('#sub_category + .nice-select > span.current').text('');

       $('#sub_category + .nice-select .list').empty();
       
     
     //start for each for load select input 

       $.each(response.data.result, function(key, index) {
          
       
            
           if(index.translates.length!='0'){

            $.each(index.translates, function(k, i) {

                    
                 if(i.lang==$lang){
                        if(key==0){
                                $('#sub_category').append("<option value='"+index.id+"'   selected>"+i.title+"</option>");
                                $('#sub_category + .nice-select > span.current').text(i.title);
                            }
                            else{
                                $('#sub_category').append("<option value='"+index.id+"'   >"+i.title+"</option>");
                                $('#sub_category + .nice-select  ul.list ').append("<li class='option'  data-value='"+index.id+"'   >"+i.title+"</li>"); 
                            }
                       
                    }
                 
            });
               

           }
           else {

                        if(key==0){
                                $('#sub_category').append("<option value='"+index.id+"'   selected>"+index.title+"</option>");
                                $('#sub_category + .nice-select > span.current').text(index.title);
                            }
                            else{
                                $('#sub_category').append("<option value='"+index.id+"'   >"+index.title+"</option>");
                                $('#sub_category + .nice-select  ul.list ').append("<li class='option'  data-value='"+index.id+"'   >"+index.title+"</li>"); 
                            }

           }
          
          
        });


  //end for each for load select input 

   }
},
error: function (xhr,ajaxOptions,thrownError) {

console.log(xhr,ajaxOptions,thrownError);

    $("#sub_category").empty();
    $('#sub_category + .nice-select .list').empty();
    $('#sub_category + .nice-select > span.current').text('');
    $('#sub_category + .nice-select').attr('style','opacity:0');

  
},
complete:function(){
    $('#sub_category + .nice-select').attr('style','opacity:1');
}

});
}

function loadCity(e) {

var myInput = document.getElementById("postal_code");
     


if(myInput.value.length==5){

    $code=$("#postal_code").val();
   

$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    contentType:'application/json; charset=utf-8',
    url:'/ajax/load/city/postalcode/'+$code,
    data: { field1:$code} ,
    beforeSend:function(){
        $('#city + .nice-select').attr('style','opacity:0');
},
    success: function (response) {
       if(response.data.status){

           console.log(response.data);
           $('#city').empty();
           $('#city + .nice-select .list').empty();
           $('#city + .nice-select > span.current').text('');
           
         
            $('#city').append("<option value='"+response.data.result[0].fields.plz_name+"' selected>"+response.data.result[0].fields.plz_name+"</option>");
           
           
            $('#city + .nice-select .list').empty();
                $('#city + .nice-select .list').append("<li data-value='"+response.data.result[0].fields.plz_name+"' class='option  selected focus'>"+response.data.result[0].fields.plz_name+"</li>");
           
                $('#city + .nice-select > span.current').text(response.data.result[0].fields.plz_name);

              

              

       }
       else{
        console.log('no');
       }
    },
    error: function (xhr,ajaxOptions,thrownError) {
    
    console.log(xhr,ajaxOptions,thrownError);
    $('#city + .nice-select > span.current').text('');
    $('#city + .nice-select').attr('style','opacity:0');

    },
    complete:function(){
        $('#city + .nice-select').attr('style','opacity:1');
}

});

}
}

</script>


@endsection




