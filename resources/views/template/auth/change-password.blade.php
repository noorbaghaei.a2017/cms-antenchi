@extends('template.app')


@section('content')



 <!-- content-->
 <div class="content">
 @include('template.auth.header-welcome')
                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                  
                        <div class="container">
                          
                          @include('template.auth.menu')
                            <!-- dashboard-menu  end-->
                            <!-- dashboard content-->
                            <div class="col-md-9">
                            
        
                                <form action="{{route('client.change.password')}}" method="POST">
                               
                                    @csrf
                                    @method('PATCH')
                                    <!-- profile-edit-container--> 
                                <div class="profile-edit-container fl-wrap block_box">
                                @include('core::layout.alert-danger')
                                <div style="margin-bottom:10px"></div>
                            
                                    <div class="custom-form">
                                        <div class="pass-input-wrap fl-wrap">
                                            <label>{{__('cms.current-password')}}  </label>
                                            <input type="password" class="pass-input" name="current_password" placeholder="" value=""/>
                                            <span class="eye"><i class="far fa-eye" aria-hidden="true"></i> </span>
                                        </div>
                                        <div class="pass-input-wrap fl-wrap">
                                            <label>{{__('cms.new-password')}}  </label>
                                            <input type="password" class="pass-input" name="new_password" placeholder="" value=""/>
                                            <span class="eye"><i class="far fa-eye" aria-hidden="true"></i> </span>
                                        </div>
                                       
                                        <button class="btn    color2-bg  float-btn"> {{__('cms.update')}}<i class="fal fa-save"></i></button>
                                    </div>
                                </div>
                                <!-- profile-edit-container end--> 
                                </form>                                   
                            </div>
                            <!-- dashboard content end-->
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->

@endsection  


@section('heads')

@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<link type="text/css" rel="stylesheet" href="{{asset('template/css/dashboard-style.css')}}">
@else

<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-dashboard-style.css')}}">
@endif

@endsection