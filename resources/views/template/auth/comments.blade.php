@extends('template.app')


@section('content')

 <!-- content-->
 <div class="content">
 
 @include('template.auth.header-welcome')

                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                        <div class="container">
                        @include('template.auth.menu')
                             <!-- dashboard content-->
                             <div class="col-md-9">
                               
                                <!-- profile-edit-container--> 
                                <div class="profile-edit-container fl-wrap block_box">
                                   

@foreach($items as $item)
                                    <!-- reviews-comments-item -->  
                                    <div class="reviews-comments-item">
                                        <div class="review-comments-avatar">
                                            <img src="{{asset('template/images/user-icon-2.jpg')}}" alt=""> 
                                           
                                           @if($item->secret==0)

                                           <h4>{{showClient($item->client)->full_name}}</h4>

                                            @else
                                            <h4>{{__('cms.unknown')}}</h4>

                                           @endif

                                        </div>
                                        <div class="reviews-comments-item-text fl-wrap">
                                            <div class="reviews-comments-header fl-wrap">
                                                <h4><a href="#">{{$item->title}} </a></h4>
                                               
                                            </div>
                                    <p>
                                    {{$item->text}}
                                    </p>
                                <div class="reviews-comments-item-footer fl-wrap">
                                                <div class="reviews-comments-item-date"><span><i class="far fa-calendar-check"></i>  {{$item->created_at->ago()}}</span></div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <!--reviews-comments-item end--> 

                                    @endforeach
                                   
                                                                                
                                </div>
                               
                            </div>
                            <!-- dashboard content end-->
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->



@endsection


@section('heads')


@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<link type="text/css" rel="stylesheet" href="{{asset('template/css/dashboard-style.css')}}">
@else

<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-dashboard-style.css')}}">
@endif

@endsection


