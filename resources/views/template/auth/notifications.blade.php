@extends('template.app')


@section('content')

 <!-- content-->
 <div class="content">
 
 @include('template.auth.header-welcome')

                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                        <div class="container">
                        @include('template.auth.menu')
                           <!-- dashboard content-->
                           
                               
                                    <div class="col-md-9">
                                       
                                        <!-- dashboard-list-box--> 
                                        <div class="dashboard-list-box mar-dash-list fl-wrap">
                                          
                                            <!-- dashboard-list end-->    
                                            <div class="dashboard-list fl-wrap">
                                                <div class="dashboard-message">
                                                    <span class="new-dashboard-item"><i class="fal fa-times"></i></span>
                                                    <div class="dashboard-message-text">
                                                       
                                                        <p>  text notification </p>
                                                    </div>
                                                    <div class="dashboard-message-time"><i class="fal fa-calendar-week"></i> 2 min ago</div>
                                                </div>
                                            </div>
                                            <!-- dashboard-list end-->  
                                                                              
                                        </div>
                                        <!-- dashboard-list-box end--> 
                                      
                                    </div>
                                   
                             
                           
                            <!-- dashboard content end-->
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->



@endsection


@section('heads')


@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<link type="text/css" rel="stylesheet" href="{{asset('template/css/dashboard-style.css')}}">
@else

<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-dashboard-style.css')}}">
@endif

@endsection


