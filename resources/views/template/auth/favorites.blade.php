@extends('template.app')


@section('content')


 <!-- content-->
 <div class="content">

 @include('template.auth.header-welcome')

                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                        <div class="container">
                        @include('template.auth.menu')
                            <!-- dashboard content-->
                            <div class="col-md-9">

                            @include('template.alert.success')

                            <div style="margin-bottom:50px"></div>
                              
                                <!-- list-single-facts -->                               
                                <div class="list-single-facts fl-wrap">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <!-- inline-facts -->
                                            <div class="inline-facts-wrap gradient-bg ">
                                                <div class="inline-facts">
                                                    <i class="fal fa-chart-bar"></i>
                                                    <div class="milestone-counter">
                                                        <div class="stats animaper">
                                                            <div class="num" data-content="0" data-num="{{count($client->jobs)}}">0</div>
                                                        </div>
                                                    </div>
                                                    <h6>{{__('cms.job')}} </h6>
                                                </div>
                                                <div class="stat-wave">
                                                    <svg viewbox="0 0 100 25">
                                                        <path fill="#fff" d="M0 30 V12 Q30 17 55 2 T100 11 V30z" />
                                                    </svg>
                                                </div>
                                            </div>
                                            <!-- inline-facts end -->
                                        </div>
                                        <div class="col-md-4">
                                            <!-- inline-facts  -->
                                            <div class="inline-facts-wrap gradient-bg ">
                                                <div class="inline-facts">
                                                    <i class="fal fa-comments-alt"></i>
                                                    <div class="milestone-counter">
                                                        <div class="stats animaper">
                                                            <div class="num" data-content="0" data-num="0">0</div>
                                                        </div>
                                                    </div>
                                                    <h6>{{__('cms.advertisings')}} </h6>
                                                </div>
                                                <div class="stat-wave">
                                                    <svg viewbox="0 0 100 25">
                                                        <path fill="#fff" d="M0 30 V12 Q30 6 55 12 T100 11 V30z" />
                                                    </svg>
                                                </div>
                                            </div>
                                            <!-- inline-facts end -->
                                        </div>
                                        <div class="col-md-4">
                                            <!-- inline-facts  -->
                                            <div class="inline-facts-wrap gradient-bg ">
                                                <div class="inline-facts">
                                                    <i class="fal fa-envelope-open-dollar"></i>
                                                    <div class="milestone-counter">
                                                        <div class="stats animaper">
                                                            <div class="num" data-content="0" data-num="0">0</div>
                                                        </div>
                                                    </div>
                                                    <h6>{{__('cms.view')}} </h6>
                                                </div>
                                                <div class="stat-wave">
                                                    <svg viewbox="0 0 100 25">
                                                        <path fill="#fff" d="M0 30 V12 Q30 12 55 5 T100 11 V30z" />
                                                    </svg>
                                                </div>
                                            </div>
                                            <!-- inline-facts end -->  
                                        </div>
                                    </div>
                                </div>
                                <!-- list-single-facts end -->                                          
                              
                               
                               @if(count($client->jobs) < 1)
                                <!-- dashboard-list-box--> 
                                <div class="dashboard-list-box  fl-wrap">


                                    <!-- dashboard-list end-->    
                                    <div class="dashboard-list fl-wrap">
                                        <div class="dashboard-message">
                                        <span class="new-dashboard-item"><a style="color:#fdfdfd;" href="{{route('client.create.bussiness')}}"><i class="fal fa-plus"></i></a></span>
                                            <div class="dashboard-message-text">
                                                <i class="far fa-exclamation-triangle red-bg"></i> 
                                                <p> {{__('cms.you-dont-have-job')}} </p>
                                            </div>
                                            <div class="dashboard-message-time"></div>
                                        </div>
                                    </div>
                                    <!-- dashboard-list end-->    
                                                                      
                                   
                                                                        
                                </div>
                                <!-- dashboard-list-box end--> 

                              @endif  



                            <!-- dashboard-list-box--> 
                            <div class="dashboard-list-box  fl-wrap">
                                 @foreach ($items as $job)
                                     
                                 <!-- dashboard-list -->    
                                 <div class="dashboard-list fl-wrap">
                                        <div class="dashboard-message">
                                      
                                        <div class="booking-list-contr">
                                           
                                                    
                                                    <a href="{{route('client.favorite.destroy.bussiness',['token'=>$job->token])}}" class="color-bg tolt" data-microtip-position="left" data-tooltip="{{__('cms.delete')}}"><i class="fal fa-trash"></i></a>
                                                  
    
                                                  
                                                  
                                            </div>
                                       
                                            <div class="dashboard-message-text">
                                           
                                                            @if(!$job->Hasmedia('images'))
                                                                   <img  src="{{asset('img/no-img.gif')}}" alt="title" title="title" >
                                                               @else
                                                               <img  src="{{$job->getFirstMediaUrl('images')}}" alt="title" title="title" >
                                                               @endif
                                               
                                                <h4>
                                                    <a href="{{route('bussiness.single',['bussiness'=>$job->slug])}}">{{$job->title}} </a>
                                                </h4>
                                                <div class="geodir-category-location clearfix"><a href="#"> {{$job->city}} </a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- dashboard-list end-->   
                                 @endforeach
                                     
                                                                           
                                </div>
                                <!-- dashboard-list-box end--> 
                           


                            </div>
                            <!-- dashboard content end-->
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->

@endsection
