@extends('template.app')


@section('content')




  <!-- content-->
  <div class="content">
  @include('template.auth.header-welcome')
                    <!--  section  -->
                    <section class="gray-bg main-dashboard-sec" id="sec1">
                        <div class="container">
                            @include('template.auth.menu')
                            <!-- dashboard content-->
                            <div class="col-md-9">
                            @include('template.alert.error')
                            @include('template.alert.success')
                                       <!-- profile-edit-container--> 
                                <div class="profile-edit-container fl-wrap block_box">
                                    <form method="post" action="{{route('job.change.social.medias',['token'=>$item->token])}}" class="custom-form">
                                    @csrf
                                
                                                <div class="custom-form">
                                                                    <label>Website <i class="fab fa-facebook"></i></label>
                                                                    <input type="text" name="website" placeholder="https://www.google.com/" value="{{$item->info->website}}"/>
                                                                    <label>Facebook <i class="fab fa-facebook"></i></label>
                                                                    <input type="text" name="facebook" placeholder="https://www.facebook.com/" value="{{$item->info->facebook}}"/>
                                                                    <label>Twitter<i class="fab fa-twitter"></i>  </label>
                                                                    <input type="text" name="twitter" placeholder="https://twitter.com/" value="{{$item->info->twitter}}"/>
                                                                    <label>whatsapp<i class="fab fa-vk"></i>  </label>
                                                                    <input type="text" name="whatsapp" placeholder="https://whatsapp.com" value="{{$item->info->whatsapp}}"/>
                                                                    <label> Instagram <i class="fab fa-instagram"></i>  </label>
                                                                    <input type="text" name="instagram" placeholder="https://www.instagram.com/" value="{{$item->info->instagram}}"/>
                                                    </div>                    
             

                                    
                                       
                                                                                 
                                                    <button type="submit" class="logout_btn color2-bg">{{__('cms.save')}} <i class="fas fa-sign-out"></i></button>

                                    </form>
                                </div>
                                <!-- profile-edit-container end-->                  
                            </div>
                            <!-- dashboard content end-->
                        </div>
                    </section>
                    <!--  section  end-->
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->


@endsection


@section('heads')

@if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

<link type="text/css" rel="stylesheet" href="{{asset('template/css/dashboard-style.css')}}">
@else

<link type="text/css" rel="stylesheet" href="{{asset('template/css/ltr-dashboard-style.css')}}">
@endif
@endsection



@section('scripts')


<script>

            function loadCity() {



                    $code=$("#postal_code").val();
                   
               
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type:'POST',
                    contentType:'application/json; charset=utf-8',
                    url:'/ajax/load/city/postalcode/'+$code,
                    data: { field1:$code} ,
                    beforeSend:function(){
                    
                },
                    success: function (response) {
                       if(response.data.status){

                           console.log(response.data);
                           $('#city').empty();
                           $('.nice-select .list').empty();
                           $.each(response.data.result, function(index, key) {
                         
                            $('#city').append("<option value='"+key.fields.plz_name+"-"+key.fields.krs_name+"'>"+key.fields.plz_name+"-"+key.fields.krs_name+"</option>");
                            });
                            $.each(response.data.result, function(index, key) {
                               
                                $('.nice-select .list').append("<li data-value='"+key.fields.plz_name+"-"+key.fields.krs_name+"' class='option'>"+key.fields.plz_name+"-"+key.fields.krs_name+"</li>");
                            });
                              

                              

                       }
                       else{
                        console.log('no');
                       }
                    },
                    error: function (xhr,ajaxOptions,thrownError) {
                    
                    console.log(xhr,ajaxOptions,thrownError);
                        // $("#result-ajax").empty()
                        //     .append("<span>"+msg+"</span>");
                    },
                    complete:function(){
                   
                }

                });

           }

</script>


@endsection




