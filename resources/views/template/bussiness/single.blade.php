@extends('template.app')

@section('content')

       <!-- content-->
       <div class="content">
           @if($item->banner_status==1)
                    <section class="listing-hero-section hidden-section" data-scrollax-parent="true" id="sec1">
                        <div class="bg-parallax-wrap">

                        @if(!$item->hasMedia('banner'))

                        
                                @if(showServiceCategory($item->service)->hasMedia('images'))
                                    <div class="bg par-elem "  data-bg="{{showServiceCategory($item->service)->getFirstMediaUrl('images')}}" data-scrollax="properties: { translateY: '30%' }" style="background-size: contain"></div>
                                @else
                                <div class="bg par-elem "  data-bg="{{asset('template/images/banner_test.jpeg')}}" data-scrollax="properties: { translateY: '30%' }" style="background-size: contain"></div>

                                @endif
                       
                        @else
                        <div class="bg par-elem "  data-bg="{{$item->getFirstMediaUrl('banner')}}" data-scrollax="properties: { translateY: '30%' }" background-size: contain></div>


                        @endif    

                            <div class="overlay"></div>
                        </div>
                        <div class="container">
                            <div class="list-single-header-item  fl-wrap">
                                <div class="row">
                                    <div class="col-md-9">
                                    <input id="inner_text" type="text" hidden data-item="{{$item->token}}">
                                        <h1>  {{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}} </h1>
                                        <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i> {{$item->city}} </a> <a href="#"> <i class="fal fa-phone"></i>{{$item->phone}}</a> <a href="#"><i class="fal fa-envelope"></i>{{$item->email}}</a></div>
                                    </div>
                                    <div class="col-md-3">
                                        <a class="fl-wrap list-single-header-column custom-scroll-link " >
                                            <div class="listing-rating-count-wrap single-list-count">
                                                <div id="show-rate" class="review-score">{{round($rate,1)}}</div>
                                                <div class="listing-rating " id="list-rate">
                                               
                        
                                               @for ($i = 1; $i <= (int)$rate; $i++) 
                                                <i data-content="{{$i}}"  class="fa fa-star" ></i>
                                                    @endfor
                                                    @for ($j = 0; $j < 5-(int)$rate; ++$j) 
                                                <i data-content="{{$i++}}"  class="fa fa-star gray" ></i>
                                                    @endfor
                                             
                                                   
                                                </div>
                                                <br>                                                
                                                <div class="reviews-count">{{__('cms.rate')}} </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="list-single-header_bottom fl-wrap">
                                <a class="listing-item-category-wrap" href="#">
                                    <div class="listing-item-category  red-bg"><i class="{{showServiceCategory($item->service)->icon}}"></i></div>
                                    <span>{{convert_lang(showServiceCategory($item->service),LaravelLocalization::getCurrentLocale(),'title')}} </span>
                                </a>
                                <div class="list-single-stats">
                                    <ul class="no-list-style">
                                        <li><span class="viewed-counter"><i class="fas fa-eye"></i> {{__('cms.view')}} -  {{$item->analyzer->view}} </span></li>
                                        <li><span class="bookmark-counter"><i class="fas fa-heart"></i> {{__('cms.comment')}} -  {{count($comments)}} </span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                    @elseif($item->banner_status==2)
  <!-- listing-carousel-wrap -->
  <section class="listing-hero-section hidden-section" data-scrollax-parent="true" id="sec1">
                        <div class="bg-parallax-wrap">
                            <!--ms-container-->
                            <div class="slideshow-container" data-scrollax="properties: { translateY: '300px' }" >
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">

                                    @if(count($slides) > 0)

                                @foreach ($slides as $slide)
                                
                                       
                                        <!--ms_item-->
                                        <div class="swiper-slide">
                                            <div class="ms-item_fs fl-wrap full-height">
                                                <div class="bg" data-bg="/media/{{$slide->id}}/{{$slide->file_name}}"></div>
                                                <div class="overlay"></div>
                                            </div>
                                        </div>
                                        <!--ms_item end-->
                                
                                    @endforeach
                                    
                                    @else
                                        <!--ms_item-->
                                        <div class="swiper-slide">
                                            <div class="ms-item_fs fl-wrap full-height">
                                                <div class="bg" data-bg="{{asset('template/images/banner_test.jpeg')}}"></div>
                                                <div class="overlay"></div>
                                            </div>
                                        </div>
                                        <!--ms_item end-->
                                        <!--ms_item-->
                                        <div class="swiper-slide">
                                            <div class="ms-item_fs fl-wrap full-height">
                                                <div class="bg" data-bg="{{asset('template/images/banner_test_1.jpg')}}"></div>
                                                <div class="overlay"></div>
                                            </div>
                                        </div>
                                        <!--ms_item end-->
                                        <!--ms_item-->
                                        <div class="swiper-slide">
                                            <div class="ms-item_fs fl-wrap full-height">
                                                <div class="bg" data-bg="{{asset('template/images/banner_test_2.jpg')}}"></div>
                                                <div class="overlay"></div>
                                            </div>
                                        </div>
                                        <!--ms_item end-->

                                     @endif   
                                       
                                    </div>
                                </div>
                            </div>
                            <!--ms-container end-->
                            <div class="overlay"></div>
                        </div>
                        <div class="slide-progress-wrap">
                            <div class="slide-progress"></div>
                        </div>
                        <div class="container">
                            <div class="list-single-header-item  fl-wrap">
                                <div class="row">
                                    <div class="col-md-9">
                                    <input id="inner_text" type="text" hidden data-item="{{$item->token}}">
                                        <h1>  {{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}} </h1>
                                        <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i> {{$item->city}} </a> <a href="#"> <i class="fal fa-phone"></i>{{$item->phone}}</a> <a href="#"><i class="fal fa-envelope"></i>{{$item->email}}</a></div>
                                    </div>
                                    <div class="col-md-3">
                                        <a class="fl-wrap list-single-header-column custom-scroll-link " >
                                            <div class="listing-rating-count-wrap single-list-count">
                                                <div id="show-rate" class="review-score">{{round($rate,1)}}</div>
                                                <div class="listing-rating " id="list-rate">
                                               
                        
                                               @for ($i = 1; $i <= (int)$rate; $i++) 
                                                <i data-content="{{$i}}"  class="fa fa-star" ></i>
                                                    @endfor
                                                    @for ($j = 0; $j < 5-(int)$rate; ++$j) 
                                                <i data-content="{{$i++}}"  class="fa fa-star gray" ></i>
                                                    @endfor
                                             
                                                   
                                                </div>
                                                <br>                                                
                                                <div class="reviews-count">{{__('cms.rate')}} </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="list-single-header_bottom fl-wrap">
                                <a class="listing-item-category-wrap" href="#">
                                    <div class="listing-item-category  red-bg"><i class="{{showServiceCategory($item->service)->icon}}"></i></div>
                                    <span>{{convert_lang(showServiceCategory($item->service),LaravelLocalization::getCurrentLocale(),'title')}} </span>
                                </a>
                                <div class="list-single-stats">
                                    <ul class="no-list-style">
                                        <li><span class="viewed-counter"><i class="fas fa-eye"></i> {{__('cms.view')}} -  {{$item->analyzer->view}} </span></li>
                                        <li><span class="bookmark-counter"><i class="fas fa-heart"></i> {{__('cms.comment')}} -  {{count($comments)}} </span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- listing-carousel-wrap end--> 
                    @endif


                    <!-- scroll-nav-wrapper--> 
                    <div class="scroll-nav-wrapper fl-wrap">
                        <div class="container">
                            <nav class="scroll-nav scroll-init">
                                <ul class="no-list-style">
                                    <li><a href="#sec2"><i class="fal fa-info"></i>{{__('cms.detail')}}</a></li>
                                    @if(count($medias) > 0)   
                                    <li><a href="#sec3"><i class="fal fa-image"></i>{{__('cms.gallery')}}</a></li>
                                   @endif
                                    @if(count($item->menus) > 0)
                                    <li><a href="#sec4"><i class="fal fa-utensils"></i>{{__('cms.menu')}}</a></li>
                                    @endif
                                    @if(count($item->comments) > 0)
                                    <li><a href="#sec5"><i class="fal fa-comments-alt"></i> {{__('cms.comment')}}</a></li>
                                    @endif
                                </ul>
                            </nav>
                            <div class="scroll-nav-wrapper-opt">
                            @if(auth('client')->check())
                                            @if(!checkJob($item->id,auth('client')->user()->id) && !checkFavorite($item->id,auth('client')->user()->id))
                                <a href="{{route('client.store.favorite.job',['token'=>$item->token])}}" class="scroll-nav-wrapper-opt-btn"> <i class="fas fa-heart"></i> {{__('cms.save')}} </a>
                                @endif
                                @endif
                                <!-- <a href="#" class="scroll-nav-wrapper-opt-btn showshare"> <i class="fas fa-share"></i> {{__('cms.share')}} </a>
                                <div class="share-holder hid-share">
                                    <div class="share-container  isShare">

                                    

                                    </div>
                                </div> -->
                             
                            </div>
                        </div>
                    </div>
                    <!-- scroll-nav-wrapper end--> 
                    <section class="gray-bg no-top-padding">
                        <div class="container">
                          
                            <div class="clearfix"></div>
                            <div class="row">
                                <!-- list-single-main-wrapper-col -->
                                <div class="col-md-8">
                                @include('template.alert.success')
                                @include('template.alert.error')
                                    <!-- list-single-main-wrapper -->
                                    <div class="list-single-main-wrapper fl-wrap" id="sec2">
                                       
                                       @if(!is_null(trim($item->text)))
                                        <!-- list-single-main-item --> 
                                        <div class="list-single-main-item fl-wrap block_box">
                                            <div class="list-single-main-item-title">
                                                <h3>{{__('cms.description')}}</h3>
                                            </div>
                                            <div class="list-single-main-item_content fl-wrap">
                                            {{convert_lang($item,LaravelLocalization::getCurrentLocale(),'text')}}
                                            
                                            </div>
                                        </div>
                                        <!-- list-single-main-item end -->
                                        @endif 

                                     
                                          <!-- list-single-main-item --> 
                                          <div class="list-single-main-item fl-wrap block_box">
                                            <div class="list-single-main-item-title">
                                                <h3>{{__('cms.attributes')}}  </h3>
                                            </div>
                                            <div class="list-single-main-item_content fl-wrap">
                                                <div class="listing-features fl-wrap">
                                                    <ul class="no-list-style">
                                                     @foreach ($item->attributes as $attr)
                                                        <li><a href="#"><i class="{{getAttribute($attr->attribute)->icon}}"></i> {{convert_lang(getAttribute($attr->attribute),LaravelLocalization::getCurrentLocale(),'title')}}</a></li>
                                                      @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- list-single-main-item end --> 
                                      

                                        @if(count($medias) > 0)                                    
                                        <!-- list-single-main-item-->   
                                        <div class="list-single-main-item fl-wrap block_box" id="sec3">
                                            <div class="list-single-main-item-title">
                                                <h3>{{__('cms.gallery')}}</h3>
                                            </div>
                                            <div class="list-single-main-item_content fl-wrap">
                                                <div class="single-carousel-wrap fl-wrap lightgallery">
                                                    <div class="sc-next sc-btn color2-bg"><i class="fas fa-caret-right gallery"></i></div>
                                                    <div class="sc-prev sc-btn color2-bg"><i class="fas fa-caret-left gallery"></i></div>
                                                    <div class="single-carousel fl-wrap full-height">
                                                        <div class="swiper-container">
                                                            <div class="swiper-wrapper">

                                                            @foreach ($medias as  $media)
                                                                <!-- swiper-slide-->   
                                                                <div class="swiper-slide">
                                                                    <div class="box-item">
                                                                        <img  src="/media/{{$media->id}}/{{$media->file_name}}"   alt="">
                                                                        <a href="/media/{{$media->id}}/{{$media->file_name}}" class="gal-link popup-image"><i class="fa fa-search gallery"  ></i></a>
                                                                    </div>
                                                                </div>
                                                                <!-- swiper-slide end-->   
                                                               @endforeach                                                                 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- list-single-main-item end --> 

                                        @endif         
                                        <!-- list-single-facts -->                               
                                        <div class="list-single-facts fl-wrap">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <!-- inline-facts -->
                                                    <div class="inline-facts-wrap gradient-bg ">
                                                        <div class="inline-facts">
                                                            <i class="fal fa-smile-plus"></i>
                                                            <div class="milestone-counter">
                                                                <div class="stats animaper">
                                                                    <div class="num" data-content="0" data-num="{{$item->analyzer->view}}">0</div>
                                                                </div>
                                                            </div>
                                                            <h6>   {{__('cms.view')}} </h6>
                                                        </div>
                                                        <div class="stat-wave">
                                                            <svg viewbox="0 0 100 25">
                                                                <path fill="#fff" d="M0 30 V12 Q30 17 55 2 T100 11 V30z" />
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <!-- inline-facts end -->
                                                </div>
                                                <div class="col-md-4">
                                                    <!-- inline-facts  -->
                                                    <div class="inline-facts-wrap gradient-bg ">
                                                        <div class="inline-facts">
                                                            <i class="fal fa-users"></i>
                                                            <div class="milestone-counter">
                                                                <div class="stats animaper">
                                                                    <div class="num" data-content="0" data-num="{{count($comments)}}">0</div>
                                                                </div>
                                                            </div>
                                                            <h6>  {{__('cms.comment')}} </h6>
                                                        </div>
                                                        <div class="stat-wave">
                                                            <svg viewbox="0 0 100 25">
                                                                <path fill="#fff" d="M0 30 V12 Q30 6 55 12 T100 11 V30z" />
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <!-- inline-facts end -->
                                                </div>
                                                <div class="col-md-4">
                                                    <!-- inline-facts  -->
                                                    <div class="inline-facts-wrap gradient-bg ">
                                                        <div class="inline-facts">
                                                            <i class="fal fa-award"></i>
                                                            <div class="milestone-counter">
                                                                <div class="stats animaper">
                                                                    <div class="num" data-content="0" data-num="{{$item->favorites->count()}}">{{$item->favorites->count()}}</div>
                                                                </div>
                                                            </div>
                                                            <h6>{{__('cms.save_count')}}  </h6>
                                                        </div>
                                                        <div class="stat-wave">
                                                            <svg viewbox="0 0 100 25">
                                                                <path fill="#fff" d="M0 30 V12 Q30 12 55 5 T100 11 V30z" />
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <!-- inline-facts end -->  
                                                </div>
                                            </div>
                                        </div>
                                        <!-- list-single-facts end -->   

                                    @if(count($item->menus) > 0)
                                            <!-- list-single-main-item-->   
                                        <div class="list-single-main-item fl-wrap block_box" id="sec4">
                                            <div class="list-single-main-item-title">
                                                <h3>{{__('cms.menu')}} </h3>
                                            </div>
                                            <div class="list-single-main-item_content fl-wrap">
                                                <div class="menu-filters">
                                                    <a href="#" class="gallery-filter  menu-filters-active" data-filter="*">{{__('cms.all')}}</a>
                                                   @foreach ($menus as $menu)
                                                   <a href="#" class="gallery-filter" data-filter=".{{$menu->id}}">{{convert_lang($menu,LaravelLocalization::getCurrentLocale(),'title')}} </a>
                                                  
                                                   @endforeach
                                                   
                                                </div>
                                                <div class="restor-menu-widget fl-wrap">

                                                @foreach ($item->menus as $value)
                                                        <!--restmenu-item-->  
                                                        <div class="restmenu-item {{$value->menu}}">
                                                        @if(!$value->Hasmedia('images'))
                                                        <a href="{{asset('template/images/no-image.jpg')}}" class="restmenu-item-img image-popup"> 
                                                       
                                                        <img src="{{asset('template/images/no-image.jpg')}}" alt="">
                                                        </a>
                                                               @else
                                                               <a href="{{$value->getFirstMediaUrl('images')}}" class="restmenu-item-img image-popup"> 
                                                             
                                                        <img src="{{$value->getFirstMediaUrl('images')}}" alt="">
                                                        </a>
                                                               @endif     
                                                      


                                                        <div class="restmenu-item-det">
                                                            <div class="restmenu-item-det-header fl-wrap">
                                                                <h4>  {{convert_lang($value,LaravelLocalization::getCurrentLocale(),'title')}} </h4>
                                                                <div class="restmenu-item-det-price">€ {{$value->price}} </div>
                                                            </div>
                                                            <p>{{convert_lang($value,LaravelLocalization::getCurrentLocale(),'excerpt')}}</p>
                                                            <div class="restmenu-item-det-price">{{convert_lang(getMenuJob($value->menu),LaravelLocalization::getCurrentLocale(),'title')}} </div>
                                                        </div>
                                                    </div>
                                                    <!--restmenu-item end-->  
                                                @endforeach
                                                
                                                                                                   
                                                </div>
                                                <!-- <a href="#" class="btn color2-bg   float-btn">{{__('cms.download')}} PDF<i class="fal fa-file-pdf"></i></a> -->
                                            </div>
                                        </div>
                                        <!-- list-single-main-item end --> 
                                        
                                       @endif
                                        

                                       @if(count($comments) > 0)
                                        <!-- list-single-main-item -->   
                                        <div class="list-single-main-item fl-wrap block_box" id="sec5">
                                            <div class="list-single-main-item-title">
                                                <h3> {{__('cms.comment')}} </h3>
                                            </div>
                                                                                 
                                            <div class="list-single-main-item_content fl-wrap">
                                                <div class="reviews-comments-wrap">

                                              
                                                @foreach ($comments as $comment)
                                                    
                                               
                                                <div class="reviews-comments-item">
                                                        <div class="review-comments-avatar">
                                                        @if($comment->secret==0)
  
                                                              
                                                        @if(!is_null($comment->client))
                                                            @if(getInfoClient($comment->client)->Hasmedia('images'))
                                                            <img src="{{getInfoClient($comment->client)->getFirstMediaUrl('images')}}" alt="">
                                                            
                                                            @else
                                                            <img src="{{asset('template/images/user-icon-2.jpg')}}" alt=""> 
                                                            @endif
                                                        @endif

                                                        @else
                                                        <img src="{{asset('template/images/user-icon-2.jpg')}}" alt=""> 

                                                        @endif
                                                        
                                                        </div>
                                                        <div class="reviews-comments-item-text fl-wrap">
                                                            <div class="reviews-comments-header fl-wrap">
                                                                @if($comment->secret==0)

                                                              
                                                                    @if(!is_null($comment->client))
                                                                    <h4><a href="#">{{getInfoClient($comment->client)->first_name}} </a></h4>
                                                                    @endif
                                                                @endif
                                                              
                                                                <div class="review-score-user">
                                                                    <span class="review-score-user_item">{{round($comment->rate,1)}}</span>
                                                                    <div class="listing-rating card-popup-rainingvis" data-starrating2="{{intval($comment->rate)}}"></div>
                                                                </div>
                                                            </div>
                                                        <p>
                                                        {{ $comment->text }}
</p>
                                                            <div class="reviews-comments-item-footer fl-wrap">
                                                                <div class="reviews-comments-item-date"><span><i class="far fa-calendar-check"></i> {{$comment->created_at->ago()}} </span></div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    @endforeach                                                               
                                                </div>
                                            </div>
                                        </div>
                                        <!-- list-single-main-item end -->
                                        @endif    






                                            @if(auth('client')->check())

                                               @if(!checkJob($item->id,auth('client')->user()->id))

                                          <!-- list-single-main-item -->   
                                        <div class="list-single-main-item fl-wrap block_box" id="sec6">
                                            <div class="list-single-main-item-title fl-wrap">
                                                <h3> {{__('cms.comment')}}</h3>
                                            </div>

             

                                            <!-- Add Review Box -->
                                            <div id="add-review" class="add-review-box">
                                                <!-- Review Comment -->
                                                <form action="{{route('client.send.bussiness.comment',['bussiness'=>$item->token])}}" method="POST"  class="add-comment  custom-form"  >
                                                  
                                                  @csrf


                                                    <fieldset>
                                                       
                                                        <div class="list-single-main-item_content fl-wrap">
                                                            <div class="row">
                                                            @if(auth('client')->check())
                                                            <div class="col-md-12" style="margin-bottom:10px;">

                                                               
                                                                <i data-content="1" class="fa fa-star gray send_rate"></i> 
                                                                <i data-content="2" class="fa fa-star gray send_rate"></i> 
                                                                <i data-content="3" class="fa fa-star gray send_rate"></i> 
                                                                <i data-content="4" class="fa fa-star gray send_rate"></i> 
                                                                <i data-content="5" class="fa fa-star gray send_rate"></i>

                                                                <input id="set_rate" hidden type="text" name="rate" >

                                                            </div>

                                                               <div class="col-md-6">

                                                                                    <label><i class="fal fa-user"></i></label>
                                                                    <select  class="chosen-select no-search-select" type="text" name="secret">

                                                                                                <option value="1" >hidden</option>
                                                                                                <option value="0" selected>show</option>


                                                                    </select>    
                                                                   
                                                                   
                                                                    
                                                                </div>
                                                              @else
                                                  
                                                              <div class="col-md-6">
                                                                    <label><i class="fal fa-user"></i></label>
                                                                    <input type="text" name="email" class="{{$errors->has('email') ? 'error-input' : ''}}" placeholder="{{__('cms.email')}} *" value="{{old('email')}}" autocompelete="false"/>
                                                                </div>
                                                               
                                                                @endif
                                                                <div class="col-md-6">
                                                                    <label><i class="fal fa-user"></i></label>
                                                                    <input type="text" name="title" class="{{$errors->has('title') ? 'error-input' : ''}}" placeholder="{{__('cms.title')}} *" value="{{old('title')}}" autocompelete="false"/>
                                                                </div>
                                                               
                                                                
                                                                
                                                            </div>
                                                            <textarea name="text" cols="40" class="{{$errors->has('text') ? 'error-input' : ''}}" rows="3" placeholder="{{__('cms.message')}} :">{{old('text')}}</textarea>
                                                           
                                                            <div class="row">
                                                                    <div class="col-md-4"></div>
                                                                    <div class="form-group col-md-4">
                                                                        <div class="captcha">
                                                                        <span>{!! captcha_img() !!}</span>
                                                                        <button type="button" class="btn btn-success"><i class="fa fa-sync" id="refresh" onclick="changeCaptcha(event)"></i></button>
                                                                        </div>
                                                                    </div>
                                                                 </div>
                                                                <div class="row">
                                                                        <div class="col-md-4"></div>
                                                                        <div class="form-group col-md-4">
                                                                        <input id="captcha" type="text" class="form-control" placeholder="{{__('cms.captcha')}}" name="captcha">
                                                                         </div>
                                                                 </div>
                                                           
                                                            <div class="clearfix"></div>

                                                           
                                                    
                                                           
                                                        
                                                            <button type="submit" class="btn  color2-bg  float-btn">{{__('cms.send')}} <i class="fal fa-paper-plane"></i></button>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                            <!-- Add Review Box / End -->
                                        </div>
                                        <!-- list-single-main-item end --> 

                                                @else



                                                @endif

                                                @else

                                                <!-- list-single-main-item -->   
                                        <div class="list-single-main-item fl-wrap block_box" id="sec6">
                                            <div class="list-single-main-item-title fl-wrap">
                                                <h3> {{__('cms.comment')}}</h3>
                                            </div>

                                          

                                            <!-- Add Review Box -->
                                            <div id="add-review" class="add-review-box">
                                                <!-- Review Comment -->
                                                <form action="{{route('client.send.bussiness.comment',['bussiness'=>$item->token])}}" method="POST"  class="add-comment  custom-form"  >
                                                  
                                                  @csrf


                                                    <fieldset>
                                                       
                                                        <div class="list-single-main-item_content fl-wrap">
                                                            <div class="row">
                                                            @if(auth('client')->check())
                                                            <div class="col-md-12" style="margin-bottom:10px;">

                                                            <i data-content="1" class="fa fa-star gray send_rate"></i> 
                                                                <i data-content="2" class="fa fa-star gray send_rate"></i> 
                                                                <i data-content="3" class="fa fa-star gray send_rate"></i> 
                                                                <i data-content="4" class="fa fa-star gray send_rate"></i> 
                                                                <i data-content="5" class="fa fa-star gray send_rate"></i>
<input id="set_rate" hidden type="text" name="rate" >

</div>
                                                           
                                                               <div class="col-md-6">

                                                               <label><i class="fal fa-user"></i></label>
                                             <select  class="chosen-select no-search-select" type="text" name="secret">

                                                                        <option value="1" >hidden</option>
                                                                        <option value="0" selected>show</option>


                                             </select>    
       
                                                                </div>
                                                                @else
                                                                <div class="col-md-6">
                                                                    <label><i class="fal fa-user"></i></label>
                                                                    <input type="text" name="email" class="{{$errors->has('email') ? 'error-input' : ''}}" placeholder="{{__('cms.email')}} *" value="{{old('email')}}" autocompelete="false"/>
                                                                </div>
                                                                @endif
                                                                <div class="col-md-6">
                                                                    <label><i class="fal fa-user"></i></label>
                                                                    <input type="text" name="title" class="{{$errors->has('title') ? 'error-input' : ''}}" placeholder="{{__('cms.title')}} *" value="{{old('title')}}"/>
                                                                </div>
                                                                
                                                            </div>
                                                            <textarea name="text" cols="40" rows="3" class="{{$errors->has('text') ? 'error-input' : ''}}" placeholder="{{__('cms.message')}} :">{{old('text')}}</textarea>
 
                                                           

                                                                <div class="row">
                                                                    <div class="col-md-4"></div>
                                                                    <div class="form-group col-md-4">
                                                                        <div class="captcha">
                                                                        <span>{!! captcha_img() !!}</span>
                                                                        <button type="button" class="btn btn-success"><i class="fa fa-sync" id="refresh" onclick="changeCaptcha(event)"></i></button>
                                                                        </div>
                                                                    </div>
                                                                 </div>
                                                                <div class="row">
                                                                        <div class="col-md-4"></div>
                                                                        <div class="form-group col-md-4">
                                                                        <input id="captcha" type="text" class="form-control" placeholder="{{__('cms.captcha')}}" name="captcha">
                                                                         </div>
                                                                 </div>



                                                            <div class="clearfix"></div>

                                                           
                                                    
                                                           
                                                        
                                                            <button type="submit" class="btn  color2-bg  float-btn">{{__('cms.send')}} <i class="fal fa-paper-plane"></i></button>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                            <!-- Add Review Box / End -->
                                        </div>
                                        <!-- list-single-main-item end --> 

                                            @endif

                                    </div>
                                </div>
                                <!-- list-single-main-wrapper-col end -->
                                <div class="col-md-4">
                                @if(!is_null($item->time))
                                <!-- list-single-sidebar -->
                                
                                    <!--box-widget-item -->
                                    <div class="box-widget-item fl-wrap block_box">
                                        <div class="box-widget-item-header">
                                            <h3> {{__('cms.time')}}</h3>
                                        </div>
                                        <div class="box-widget opening-hours fl-wrap">
                                            <div class="box-widget-content">
                                                <ul class="no-list-style">
                                                  
                                                    <li class="mon "><span class="opening-hours-day">{{__('cms.monday')}} </span><span class="opening-hours-time">{{($item->time->monday) ? $item->time->monday : __('cms.close')}}</span></li>
                                                    <li class="tue "><span class="opening-hours-day">{{__('cms.tuesday')}} </span><span class="opening-hours-time ">{{($item->time->tuesday) ? $item->time->tuesday : __('cms.close')}}</span></li>
                                                    <li class="wed"><span class="opening-hours-day">{{__('cms.wednesday')}} </span><span class="opening-hours-time ">{{($item->time->wednesday) ? $item->time->wednesday : __('cms.close')}}</span></li>
                                                    <li class="thu"><span class="opening-hours-day">{{__('cms.thursday')}} </span><span class="opening-hours-time">{{($item->time->thursday) ? $item->time->thursday : __('cms.close')}}</span></li>
                                                    <li class="fri "><span class="opening-hours-day">{{__('cms.friday')}} </span><span class="opening-hours-time ">{{($item->time->friday) ? $item->time->friday : __('cms.close')}}</span></li>
                                                    <li class="sat "><span class="opening-hours-day">{{__('cms.saturday')}} </span><span class="opening-hours-time ">{{($item->time->saturday) ? $item->time->saturday : __('cms.close')}}</span></li>
                                                    <li class="sun "><span class="opening-hours-day">{{__('cms.sunday')}} </span><span class="opening-hours-time ">{{($item->time->sunday) ? $item->time->sunday : __('cms.close')}}</span></li>
                                                    


                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!--box-widget-item end -->  

                                    @endif 
                                   
                                                                                                  
                                    <!--box-widget-item -->                                       
                                    <div class="box-widget-item fl-wrap block_box">
                                        <div class="box-widget-item-header">
                                            <h3>{{__('cms.location')}} / {{__('cms.call')}}  </h3>
                                        </div>
                                        <div class="box-widget">
                                            <div class="map-container">

                                            @if(!$item->Hasmedia('images'))
                                                                                        <img  src="{{asset('template/images/no-image.jpg')}}" alt="title" title="title"  width="100%">
                                                                                    @else
                                                                                    <img  src="{{$item->getFirstMediaUrl('images')}}" alt="title" title="title"  width="100%">
                                                                                    @endif
                                            </div>
                                            <div class="box-widget-content bwc-nopad">
                                                <div class="list-author-widget-contacts list-item-widget-contacts bwc-padside">
                                                    <ul class="no-list-style">
                                                        
                                                        <li><span><i class="fal fa-map-marker"></i> {{__('cms.city')}} :</span> <a href="#">{{$item->city}}</a></li>
                                                        <li><span><i class="fal fa-map-marker"></i> {{__('cms.address')}} :</span> <a target='_blank' href="https://maps.google.com/?q={{$item->address}}">{{$item->address}}</a></li>

                                                        <li><span><i class="fal fa-phone"></i> {{__('cms.phone')}} :</span> <a href="tel:{{$item->phone}}">{{$item->phone}}</a></li>
                                                        <li><span><i class="fal fa-envelope"></i> {{__('cms.email')}} :</span> <a href="mailto:{{$item->email}}">{{$item->email}}</a></li>

                                                    </ul>
                                                </div>
                                                <div class="list-widget-social bottom-bcw-box  fl-wrap">
                                                    <ul class="no-list-style">
                                                        <li><a href="#" target="_blank" ><i class="fab fa-facebook-f"></i></a></li>
                                                        <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                                        <li><a href="#" target="_blank" ><i class="fab fa-vk"></i></a></li>
                                                        <li><a href="#" target="_blank" ><i class="fab fa-instagram"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--box-widget-item end -->  
                                                               
                                  
@if(count($all_bussiness->where('service',$item->service)->where('postal_code',$item->postal_code)->where('id','!=',$item->id)) > 0)

                                    <!--box-widget-item -->
                                    <div class="box-widget-item fl-wrap block_box">
                                        <div class="box-widget-item-header">
                                            <h3> {{__('cms.jobs')}}  </h3>
                                        </div>
                                        <div class="box-widget  fl-wrap">
                                            <div class="box-widget-content">
                                                <!--widget-posts-->
                                                <div class="widget-posts  fl-wrap">
                                                    <ul class="no-list-style">

                                                    @foreach ($all_bussiness->where('service',$item->service)->where('postal_code',$item->postal_code)->where('id','!=',$item->id) as $bussiness )
                                                        <li>
                                                            <div class="widget-posts-img">
                                                            <a href="{{route('bussiness.single',['bussiness'=>$bussiness->slug])}}" class="geodir-category-img-wrap fl-wrap">
                                                                @if(!$bussiness->Hasmedia('images'))
                                                                                        <img  src="{{asset('template/images/no-image.jpg')}}" alt="title" title="title" >
                                                                                    @else
                                                                                    <img  src="{{$bussiness->getFirstMediaUrl('images')}}" alt="title" title="title" >
                                                                                    @endif
                                                                </a>
                                                            </div>
                                                            <div class="widget-posts-descr">
                                                                <h4><a href="{{route('bussiness.single',['bussiness'=>$bussiness->slug])}}">{{$bussiness->title}} </a></h4>
                                                                <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i> {{$bussiness->city}} </a></div>
                                                                <div class="widget-posts-descr-link"><a href="#" >{{showServiceCategory($bussiness->service)->title}} </a>  </div>
                                                                <div class="widget-posts-descr-score">4.1</div>
                                                            </div>
                                                        </li>

                                                    @endforeach
                                                     
                                                    </ul>
                                                </div>
                                                <!-- widget-posts end-->
                                            </div>
                                        </div>
                                    </div>
                                    <!--box-widget-item end --> 
                                    
                                    @endif
                                      
                                </div>
                                <!-- list-single-sidebar end -->                                
                            </div>
                        </div>
                    </section>
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->

                




@endsection



@section('scripts')


<script>


$(".send_rate").click(function(e){
    $(".send_rate").removeClass('yellow');
    $(".send_rate").removeClass('gray');

    $(e.target).addClass('yellow');
    $(e.target).prevAll().addClass('yellow');
    $("#set_rate").attr('value',$(e.target).attr('data-content'))
   console.log(e.target);
});

function setRate(e) {

var  token = $('#inner_text').data('item');
var  rate = $(e.target).data('content');
     

$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    contentType:'application/json; charset=utf-8',
    url:'/ajax/set/rate/'+token+'/'+rate,
    data: { field1:token,field2:rate} ,
    beforeSend:function(){
        $(e.target).attr('style','color:green !important');
},
    success: function (response) {
       

           console.log(response.data);
           $("#list-rate").empty();
           for (i = 1; i <= Math.floor(response.data.rate); i++) {
            $("#list-rate").append(" <i data-content='"+i+"'  class='fa fa-star' onclick='setRate(event)'></i>");
             }
             for (j = 1; j <= 5-(Math.floor(response.data.rate)); j++) {
            $("#list-rate").append(" <i data-content='"+(i++)+"'  class='fa fa-star gray' onclick='setRate(event)'></i>");
             }
         
           $("#show-rate").text(Math.round(response.data.rate * 10) / 10);

          
           
              
    },
    error: function (xhr,ajaxOptions,thrownError) {
        
    console.log(xhr,ajaxOptions,thrownError);
      
    },
    complete:function(){
        $(e.target).attr('style','');
}

});


}

function changeCaptcha(e) {

    $.ajax({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
     type:'GET',
     contentType:'application/json; charset=utf-8',
     url:'/ajax/refreshcaptcha',
     success:function(data){
       
       
        $(".captcha span").html(data.captcha);
     },
     error: function (xhr,ajaxOptions,thrownError) {
       
        console.log(xhr,ajaxOptions,thrownError);
          
        },
  });

}

</script>



@endsection

@section('heads')

<style>
.yellow{
    color:#FACC39 !important;
}
.gray{
    color:gray !important;
}


</style>

@endsection



