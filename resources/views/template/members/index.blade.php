@extends('template.app')

@section('content')




<!-- Page Breadcrumb -->
<section class="page-breadcrumb">
    <div class="image-layer" style="background-image:url({{asset('template/images/background/1.png')}})"></div>
    <div class="container">
        <div class="clearfix">
            <div class="pull-right">

            </div>
            <div class="pull-left">

            </div>
        </div>
    </div>
</section>
<!-- End Page Breadcrumb -->

<!-- Doctors Page Section -->
<section class="doctors-page-section">
    <div class="container">

        <!--MixitUp Galery-->
        <div class="mixitup-gallery">


            <div class="filter-list row box-member">

                @foreach($items as $item)
                <!-- Team Block -->
                <div class="team-block all {{$item->role}} mix traumatology dental pediatric col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="image">
                            @if(!$item->Hasmedia('images'))
                                <img src="{{asset('img/no-img.gif')}}" alt="" />
                            @else
                                <img src="{{$item->getFirstMediaUrl('images')}}" alt="" />
                            @endif
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <ul class="team-social-box">
                                        <li class="facebook"><a href="#" class="icon icon-instagram"></a><span
                                                class="social-name">اینستاکرام</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="lower-content">
                            <h3><a href="{{route('members.single',['member'=>$item->id])}}">{{$item->full_name}}</a></h3>
                            <p class="designation">{{$item->role_name}}</p>
                        </div>
                    </div>
                </div>
                    @endforeach


            </div>

        </div>

    </div>
</section>
<!-- End Doctors Page Section -->

@endsection
