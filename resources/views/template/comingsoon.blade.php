<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Coming Soon!</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400" rel="stylesheet">
        <!-- Styles -->
        <link rel="stylesheet" href="{{asset('css/comingsoon.css')}}">
    </head>
    <body>
    <div id="particles-js"></div>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md " >
                    Coming Soon
                </div>
                <!-- <div class="title m-b-md" id="clock"></div> -->
               
               
            </div>
        </div>
    </body>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="{{asset('/js/jquery.countdown.min.js')}}"></script>
    <script src="{{asset('/js/particles.min.js')}}"></script>
    <script src="{{asset('/js/comingsoon.js')}}"></script>
    
    <script>
  
    </script>
</html>


@foreach ($all_bussiness as $bussiness )
                               
                               <!--  gallery-item-->
                               <div class="gallery-item events cat-{{str_replace(' ', '_', $bussiness->city)}}">
                                   <!-- listing-item  -->
                                   <div class="listing-item">
                                       <article class="geodir-category-listing fl-wrap">
                                           <div class="geodir-category-img">
                                       
                                               <a href="{{route('bussiness.single',['bussiness'=>$bussiness->slug])}}" class="geodir-category-img-wrap fl-wrap">
                                                                   @if(!$bussiness->Hasmedia('images'))
                                                                       <img  src="{{asset('img/no-img.gif')}}" alt="title" title="title" >
                                                                   @else
                                                                   <img  src="{{$bussiness->getFirstMediaUrl('images')}}" alt="title" title="title" >
                                                                   @endif
                                           
                                              
                                               </a>
       
                                               <div class="geodir-category-opt">
                                                   <div class="listing-rating-count-wrap">
                                                      
                                                       <br>
                                                       <div class="reviews-count">{{count($bussiness->comments)}} {{__('cms.comment')}}</div>
                                                   </div>
                                               </div>
                                           </div>
                                           <div class="geodir-category-content fl-wrap title-sin_item">
                                               <div class="geodir-category-content-title fl-wrap">
                                                   <div class="geodir-category-content-title-item">
                                                       <h3 class="title-sin_map"><a href="{{route('bussiness.single',['bussiness'=>$bussiness->slug])}}"> {{convert_lang($bussiness,LaravelLocalization::getCurrentLocale(),'title')}}</a></h3>
                                                   </div>
                                               </div>

                                               <div class="geodir-category-text fl-wrap">
                                              <p class="small-text">  {{convert_lang($bussiness,LaravelLocalization::getCurrentLocale(),'excerpt')}}</p>
                                              <div class="facilities-list fl-wrap">
                                                  <div class="facilities-list-title">{{__('cms.attributes')}} : </div>
                                                  <ul class="no-list-style">
                                                  @foreach ($bussiness->attributes as $attr)

                                                  <li class="tolt"  data-microtip-position="top" data-tooltip=" {{getAttribute($attr->attribute)->title}}"><i class="{{getAttribute($attr->attribute)->icon}}"></i></li>

                                                       @endforeach
                                                   </ul>
                                              </div>
                                          </div>

                                               <div class="geodir-category-footer fl-wrap">
                                                   <a class="listing-item-category-wrap">
                                                       <div class="listing-item-category-theme category-box"><i class="{{showServiceCategory($bussiness->service)->icon}}"></i></div>
                                                       <span> {{convert_lang(showServiceCategory($bussiness->service),LaravelLocalization::getCurrentLocale(),'title')}}</span>
                                                   </a>
                                                   <div class="geodir-opt-list">
                                                       <ul class="no-list-style">
                                                           <li><a href="#" class="show_gcc"><i class="fal fa-envelope"></i><span class="geodir-opt-tooltip"> {{__('cms.call')}}</span></a></li>
                                                           <li><a target='_blank' href="https://maps.google.com/?q={{$bussiness->address}}" class="single-map-item" ><i class="fal fa-map-marker-alt"></i><span class="geodir-opt-tooltip"> {{__('cms.location')}} </span> </a></li>

                                                           @if($bussiness->Hasmedia(config("cms.collection-images")))
                                                           <li>
                                                           
                                                               <div class="dynamic-gal gdop-list-link" data-dynamicPath='[
                                                               @foreach($bussiness->getMedia(config("cms.collection-images")) as $media)
                                                               {"src": "/media/{{$media->id}}/{{$media->file_name}}"},
                                                               @endforeach
                                                                ]'>
                         
                                                               <i class="fal fa-search-plus"></i><span class="geodir-opt-tooltip"> {{__('cms.gallery')}} </span></div>
                                                           </li>
                                                           @endif
                                                       </ul>
                                                   </div>
                                                   
                                                   <div class="geodir-category_contacts">
                                                       <div class="close_gcc"><i class="fal fa-times-circle"></i></div>
                                                       <ul class="no-list-style">
                                                           <li><span><i class="fal fa-phone"></i> {{__('cms.call')}} : </span><a href="#">{{$bussiness->phone}}</a></li>
                                                           <li><span><i class="fal fa-envelope"></i> {{__('cms.email')}} : </span><a href="#">{{$bussiness->email}}</a></li>
                                                       </ul>
                                                   </div>
                                               </div>
                                           </div>
                                       </article>
                                   </div>
                                   <!-- listing-item end -->                              
                               </div>
                               <!-- gallery-item  end-->

                           @endforeach 
