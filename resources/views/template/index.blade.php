﻿@extends('template.app')

@section('content')

  <!-- content-->
  <div class="content">




  
                    <!--section  -->
                    <section class="hero-section"   data-scrollax-parent="true">
                        <div class="bg-tabs-wrap">
                            <div class="bg-parallax-wrap" data-scrollax="properties: { translateY: '200px' }">


                                @if(isset($single_slider))

                                    @if($single_slider->Hasmedia('images'))

                                    <div class="bg bg_tabs"  data-bg="{{$single_slider->getFirstMediaUrl('images')}}"></div>
                               
                                    @endif

@else

<div class="bg bg_tabs"  data-bg="{{asset('template/images/bg-one.png')}}"></div>
                               

                              

                               @endif 
                                <div class="overlay op7"></div>
                            </div>
                        </div>
                        <div class="container small-container">
                            <div class="intro-item fl-wrap">
                            

                                <div class="bubbles">
                                    <h1>{{__('cms.find-what-you-need')}}</h1>
                                </div>
                            </div>
                            <!-- main-search-input-tabs-->
                            <div class="main-search-input-tabs  tabs-act fl-wrap">
                                <ul class="tabs-menu change_bg no-list-style">
                                    <li class="current"><a href="#tab-inpt1" data-bgtab="{{asset('template/images/bg/hero/1.jpg')}}"> {{__('cms.services')}} </a></li>
                                </ul>
                                <form action="{{route('search.page.bussiness',['filter'=>1])}}" method="GET">
                               
                                <!--tabs -->                       
                                <div class="tabs-container fl-wrap  ">
                                    <!--tab -->
                                    <div class="tab">
                                        <div id="tab-inpt1" class="tab-content first-tab">
                                            <div class="main-search-input-wrap fl-wrap">
                                                <div class="main-search-input fl-wrap">
                                                     
                                                     <div class="main-search-input-item " >
                                                        <label><i class="fal fa-info"></i></label>
                                                        @if(auth('client')->check() && !is_null(mostJobBot(auth('client')->user()->id)))
   
                                                        <input id="what" type="text"  onkeyup="loadCategory(event)"  placeholder=" {{__('cms.what')}} " value="{{showServiceCategory(getJob(mostJobBot(auth('client')->user()->id)->botable_id)->service)->title}}" autocomplete="off"/>
                                                        <input id="secret_what"  name="was" value="{{showServiceCategory(getJob(mostJobBot(auth('client')->user()->id)->botable_id)->service)->title}}" hidden>
                                                   @else
                                                   <input id="what" type="text"  onkeyup="loadCategory(event)"  placeholder=" {{__('cms.what')}} " value="" autocomplete="off"/>

                                                   <input id="secret_what"  name="was" value="" hidden>
                                                        @endif

                                                     
                                                        <div id="result-was">

                                                            <ul id="item-was">
                                                          
                                                             
                                                            </ul>
                                                            <img id="item-was-loader" style="width:100%" hidden src="{{asset('template/images/preloader-text.gif')}}">

                                                             
                                                        </div>

                                                    </div>
                                                    <div class="main-search-input-item " id="location-search">
                                                    <span class="btn-gps"><i class="fal fa-map-marker-alt"></i></span>
                                                        <label><i class="fal fa-map-marker-alt"></i></label>
                                                        @if(auth('client')->check() && !is_null(mostJobBot(auth('client')->user()->id)))
   
                                                        <input id="wo" type="text" onkeyup="loadInfo(event)"  placeholder=" {{__('cms.where')}} " value="{{getJob(mostJobBot(auth('client')->user()->id)->botable_id)->city}}" autocomplete="off"/>
                                                        <input id="secret_wo"  name="wo" value="{{getJob(mostJobBot(auth('client')->user()->id)->botable_id)->city}}" hidden>
                                                        @else
                                                        <input id="wo" type="text" onkeyup="loadInfo(event)"  placeholder=" {{__('cms.where')}} " value="" autocomplete="off"/>
                                                        <input id="secret_wo"  name="wo" value="" hidden>

                                                        @endif
                                                        <div id="result-wo">

                                                                <ul id="item-wo">

                                                                
                                                                </ul>
                                                                <img id="item-wo-loader" style="width:100%" hidden src="{{asset('template/images/preloader-text.gif')}}">

                                                                
                                                                </div>
                                                   
                                                    </div>
                                                   
                                                   
                                                   <input hidden id="longitude" name="longitude" type="text" value="">
                                                   <input hidden id="latitude" name="latitude" type="text" value="">  
                                                   
                                                    <button type="submit" class="main-search-button color2-bg" >{{__('cms.search')}} <i class="far fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--tab end-->
                                                             
                                                           
                                                                  
                                </div>
                                <!--tabs end-->
                                </form>
                            </div>
                            <!-- main-search-input-tabs end-->
                            <div class="hero-categories fl-wrap">
                                <ul class="no-list-style">

                                @foreach ($more_view_category_bussiness as $category_bussiness)
                                <li><a href="{{route('search.page.bussiness',['filter'=>1,'category'=>$category_bussiness->id])}}"><i class="{{$category_bussiness->icon}}"></i><span> {{convert_lang($category_bussiness,LaravelLocalization::getCurrentLocale(),'title')}}</span></a></li>
                               
                                @endforeach
                               
                                </ul>
                            </div>
                        </div>
                        <div class="header-sec-link">
                            <a href="#sec1" class="custom-scroll-link"><i class="fal fa-angle-double-down"></i></a> 
                        </div>
                    </section>
                    <!--section end-->
                    <!--section  -->
                    <section class="slw-sec" id="sec1">
                        <div class="section-title">
                            <h2>{{__('cms.proposals')}}</h2>
                            <div class="section-subtitle">{{__('cms.proposals')}}</div>
                        
                            <span class="section-separator"></span>
                        </div>
                        <div class="listing-slider-wrap fl-wrap">
                            <div class="listing-slider fl-wrap">
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">

                                    @foreach ($all_bussiness as $bussiness )
                                          <!--  swiper-slide  -->
                                          <div class="swiper-slide">
                                            <div class="listing-slider-item fl-wrap">
                                                <!-- listing-item  -->
                                                <div class="listing-item listing_carditem">
                                                    <article class="geodir-category-listing fl-wrap">
                                                        <div class="geodir-category-img">
                                                     
                                                            <a href="{{route('bussiness.single',['bussiness'=>$bussiness->slug])}}" class="geodir-category-img-wrap fl-wrap">
                                                            @if(!$bussiness->Hasmedia('images'))
                                                                        <img class="ads"  src="{{asset('template/images/no-image.jpg')}}" alt="title" title="title" >
                                                                    @else
                                                                    <img  class="ads" src="{{$bussiness->getFirstMediaUrl('images')}}" alt="title" title="title" >
                                                                    @endif
                                                            </a>
                                                            <div class="geodir-category-opt">
                                                                <div class="geodir-category-opt_title">
                                                                    <h4><a href="{{route('bussiness.single',['bussiness'=>$bussiness->slug])}}"> {{convert_lang($bussiness,LaravelLocalization::getCurrentLocale(),'title')}}</a></h4>
                                                                    <div class="geodir-category-location"><a href="#"><i class="fas fa-map-marker-alt"></i>  {{$bussiness->city}}</a></div>
                                                                </div>
                                                                <div class="listing-rating-count-wrap">
                                                                  
                                                                    <br>
                                                                    <div class="reviews-count">{{count($bussiness->comments)}} {{__('cms.comment')}}</div>
                                                                </div>
                                                                <div class="listing_carditem_footer fl-wrap">
                                                                    <a class="listing-item-category-wrap" href="#">
                                                                        <div class="listing-item-category-theme category-box"><i class="{{showServiceCategory($bussiness->service)->icon}}"></i></div>
                                                                        <span style="margin-bottom:7px;">{{convert_lang(showServiceCategory($bussiness->service),LaravelLocalization::getCurrentLocale(),'title')}}</span>
                                                                    </a>
                                                                   
                                                                  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </article>
                                                </div>
                                                <!-- listing-item end -->                                                   
                                            </div>
                                        </div>
                                        <!--  swiper-slide end  -->  
                                    @endforeach
                                      
                                                                
                                    </div>
                                </div>
                                <div class="listing-carousel-button listing-carousel-button-next2"><i class="fas fa-caret-right"></i></div>
                                <div class="listing-carousel-button listing-carousel-button-prev2"><i class="fas fa-caret-left"></i></div>
                            </div>
                            <div class="tc-pagination_wrap">
                                <div class="tc-pagination2"></div>
                            </div>
                        </div>
                    </section>
                    <!--section end-->
                    <div class="sec-circle fl-wrap"></div>
                 
                    <!--section end-->
                    <section class="parallax-section small-par" data-scrollax-parent="true">
                        <div class="bg par-elem "  data-bg="{{asset('template/images/bg/22.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay  op7"></div>
                        <div class="container">
                            <div class=" single-facts single-facts_2 fl-wrap">
                               
                            </div>
                        </div>
                    </section>
                    <!--section end--> 
                    <!--section  -->
                    <section>
                        <div class="container big-container">
                            <div class="section-title">
                                <h2><span>  {{__('cms.insights')}}</span></h2>
                                <div class="section-subtitle">{{__('cms.insights')}}</div>
                
                                <span class="section-separator"></span>
                            </div>
                            <div class="listing-filters gallery-filters fl-wrap">
                                <a href="#" class="gallery-filter  gallery-filter-active" data-filter="*">{{__('cms.all')}}   </a>
                               @foreach ($all_city_bussiness as $city)
                               <a href="#" class="gallery-filter" data-filter=".cat-{{str_replace(' ', '_', $city)}}"> {{$city}} </a>
                               @endforeach
                            

                            </div>
                            <div class="grid-item-holder gallery-items fl-wrap">

                            @foreach ($all_bussiness as $bussiness )
                               
                               
                                 <!--  gallery-item-->
                                 <div class="gallery-item  events cat-{{str_replace(' ', '_', $bussiness->city)}}">
                                    <!-- listing-item  -->
                                    <div class="listing-item">
                                        <article class="geodir-category-listing fl-wrap">
                                            <div class="geodir-category-img">
                                                <a href="{{route('bussiness.single',['bussiness'=>$bussiness->slug])}}" class="geodir-category-img-wrap fl-wrap">
                                                                   @if(!$bussiness->Hasmedia('images'))
                                                                       <img  src="{{asset('template/images/no-image.jpg')}}" alt="title" title="title" >
                                                                   @else
                                                                   <img  src="{{$bussiness->getFirstMediaUrl('images')}}" alt="title" title="title" >
                                                                   @endif
                                           
                                              
                                               </a>
                                               <div class="geodir_status_date gsd_open"><i class="fal fa-lock-open"></i>{{__('cms.open')}}</div>

                                               <div class="geodir-category-opt">
                                                    <div class="listing-rating-count-wrap">
                                                        <div class="review-score">{{round($bussiness->rate_count,1)}}</div>
                                                        <div class="listing-rating card-popup-rainingvis" data-starrating2="{{round($bussiness->rate_count,1)}}"></div>
                                                        <br>
                                                        <div class="reviews-count">{{count($bussiness->comments)}} {{__('cms.comment')}} </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="geodir-category-content fl-wrap title-sin_item">
                                                <div class="geodir-category-content-title fl-wrap">
                                                    <div class="geodir-category-content-title-item">
                                                       <h3 class="title-sin_map"><a href="{{route('bussiness.single',['bussiness'=>$bussiness->slug])}}"> {{convert_lang($bussiness,LaravelLocalization::getCurrentLocale(),'title')}}</a></h3>
                                                   </div>
                                                </div>
                                                <div class="geodir-category-text fl-wrap">
                                                <p class="small-text">  {{convert_lang($bussiness,LaravelLocalization::getCurrentLocale(),'excerpt')}}</p>

                                                    <div class="facilities-list fl-wrap">
                                                    <div class="facilities-list-title">{{__('cms.attributes')}} : </div>
                                                        <ul class="no-list-style">
                                                
                                                    @if(count($bussiness->attributes) > 0)
                                                        @foreach ($bussiness->attributes as $attr)
                                                        <li class="tolt"  data-microtip-position="top" data-tooltip=" {{convert_lang(getAttribute($attr->attribute),LaravelLocalization::getCurrentLocale(),'title')}}"><i class="{{getAttribute($attr->attribute)->icon}}"></i></li>

                                                      @endforeach
                                                      @else
                                                      <li ><i style="color:#999;cursor:unset;">{{__('cms.empty')}}</i></li>


                                                      @endif
                                                      

                                                         
                                                           
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="geodir-category-footer fl-wrap">
                                                <a class="listing-item-category-wrap">
                                                       <div class="listing-item-category-theme category-box"><i class="{{showServiceCategory($bussiness->service)->icon}}"></i></div>
                                                       <span> {{convert_lang(showServiceCategory($bussiness->service),LaravelLocalization::getCurrentLocale(),'title')}}</span>
                                                   </a>
                                                    <div class="geodir-opt-list">
                                                    <ul class="no-list-style">
                                                           <li><a href="#" class="show_gcc"><i class="fal fa-envelope"></i><span class="geodir-opt-tooltip"> {{__('cms.call')}}</span></a></li>
                                                           <li><a target='_blank' href="https://maps.google.com/?q={{$bussiness->address}}" class="single-map-item" ><i class="fal fa-map-marker-alt"></i><span class="geodir-opt-tooltip"> {{__('cms.location')}} </span> </a></li>

                                                           @if($bussiness->Hasmedia(config("cms.collection-images")))
                                                           <li>
                                                           
                                                               <div class="dynamic-gal gdop-list-link" data-dynamicPath='[
                                                               @foreach($bussiness->getMedia(config("cms.collection-images")) as $media)
                                                               {"src": "/media/{{$media->id}}/{{$media->file_name}}"},
                                                               @endforeach
                                                                ]'>
                         
                                                               <i class="fal fa-search-plus"></i><span class="geodir-opt-tooltip"> {{__('cms.gallery')}} </span></div>
                                                           </li>
                                                           @endif
                                                       </ul>
                                                    </div>
                                                   
                                                    <div class="geodir-category_contacts">
                                                       <div class="close_gcc"><i class="fal fa-times-circle"></i></div>
                                                       <ul class="no-list-style">
                                                           <li><span><i class="fal fa-phone"></i> {{__('cms.call')}} : </span><a href="#">{{$bussiness->phone}}</a></li>
                                                           <li><span><i class="fal fa-envelope"></i> {{__('cms.email')}} : </span><a href="#">{{$bussiness->email}}</a></li>
                                                       </ul>
                                                   </div>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                    <!-- listing-item end -->                              
                                </div>
                                <!-- gallery-item  end-->

                            @endforeach    
                               
                                                            
                                                                                                                                                     
                            </div>
                            <a href="{{route('bussiness')}}" class="btn  dec_btn  color2-bg">{{__('cms.read_more')}} <i class="fal fa-arrow-alt-right"></i></a>
                        </div>
                    </section>
                    <!--section end-->                                        					
                    <!--section  -->
                    <section class="parallax-section" data-scrollax-parent="true">
                        <div class="bg par-elem "  data-bg="{{asset('template/images/bg/11.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay op7"></div>
                        <!--container-->
                       
                    </section>
                    <!--section end-->
                    <!--section  -->
                    <section      data-scrollax-parent="true">
                        <div class="container">
                            <div class="section-title">
 
                            </div>
                            <div class="process-wrap fl-wrap">
                                <ul class="no-list-style">
                                    <li>
                                        <div class="process-item">
                                            <span class="process-count">1 </span>
                                            <div class="time-line-icon"><i class="fa fa-user"></i></div>
                                            <h4>  {{__('cms.register')}}    </h4>
                                        </div>
                                        <span class="pr-dec"></span>
                                    </li>
                                   
                                    <li>
                                        <div class="process-item">
                                            <span class="process-count">2</span>
                                            <div class="time-line-icon"><i class="fal fa-layer-plus"></i></div>
                                            <h4>  {{__('cms.job')}}  </h4>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="process-item">
                                            <span class="process-count">3</span>
                                            <div class="time-line-icon"><i class="fa fa-smile"></i></div>
                                            <h4>  {{__('cms.enjoy')}}  </h4>
                                        </div>
                                        <span class="pr-inc"></span>
                                    </li>
                                </ul>
                                <div class="process-end"><i class="fal fa-check"></i></div>
                            </div>
                        </div>
                    </section>
                    <!--section end-->

                     <!--section  -->
                     <section class="gradient-bg hidden-section" data-scrollax-parent="true">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="colomn-text  pad-top-column-text fl-wrap">
                                        <div class="colomn-text-title">
        
                                            <h3>{{__('cms.search-in-mobile')}}</h3>
                                            <p>{{__('cms.text-search-in-mobile')}}</p>
                                           
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="collage-image">
                                    @if(LaravelLocalization::getCurrentLocale()=="fa" || LaravelLocalization::getCurrentLocale()=="ar")

                                        <img src="{{asset('template/images/api.png')}}" class="main-collage-image" alt="">                               
                                        @else
                                        <img src="{{asset('template/images/ltr-api.png')}}" class="main-collage-image" alt="">                               

                                        @endif
                                        <div class="images-collage-title color2-bg icdec"> 
                                        @if(!$setting->Hasmedia('logo'))

                                        <img src="{{asset('img/no-img.gif')}}"   alt="{{$setting->name}}">
  
                                        @else
                                        <img src="{{$setting->getFirstMediaUrl('logo')}}"   alt="{{$setting->name}}">
                                        
                                        @endif
                                            
                                        </div>
                                        <div class="images-collage_icon green-bg" style="right:-20px; top:120px;"><i class="fal fa-thumbs-up"></i></div>
                                        <div class="collage-image-min cim_1"><img src="{{asset('template/images/api/1.jpg')}}" alt=""></div>
                                        <div class="collage-image-min cim_2"><img src="{{asset('template/images/api/1.jpg')}}" alt=""></div>
                                        <div class="collage-image-btn green-bg">Booking now</div>
                                        <div class="collage-image-input">Search <i class="fa fa-search"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gradient-bg-figure" style="right:-30px;top:10px;"></div>
                        <div class="gradient-bg-figure" style="left:-20px;bottom:30px;"></div>
                        <div class="circle-wrap" style="left:270px;top:120px;" data-scrollax="properties: { translateY: '-200px' }">
                            <div class="circle_bg-bal circle_bg-bal_small"></div>
                        </div>
                        <div class="circle-wrap" style="right:420px;bottom:-70px;" data-scrollax="properties: { translateY: '150px' }">
                            <div class="circle_bg-bal circle_bg-bal_big"></div>
                        </div>
                        <div class="circle-wrap" style="left:420px;top:-70px;" data-scrollax="properties: { translateY: '100px' }">
                            <div class="circle_bg-bal circle_bg-bal_big"></div>
                        </div>
                        <div class="circle-wrap" style="left:40%;bottom:-70px;"  >
                            <div class="circle_bg-bal circle_bg-bal_middle"></div>
                        </div>
                        <div class="circle-wrap" style="right:40%;top:-10px;"  >
                            <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
                        </div>
                        <div class="circle-wrap" style="right:55%;top:90px;"  >
                            <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
                        </div>
                    </section>
                    <!--section end-->  
                    
                    <br>
                    <br>
                    
                            
                  
                </div>
                <!--content end-->



@endsection


@section('scripts')


<script>



 $(".btn-gps").click(function(e){

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
                position => {
                  
                      $("#wo").attr('placeholder','use current location');
                      $("#longitude").attr('value',position.coords.longitude);
                      $("#latitude").attr('value',position.coords.latitude);
                    
                },
                error => {
                    $("#wo").attr('placeholder','can not load gps location');
                    $("#longitude").attr('value','');
                      $("#latitude").attr('value','');
                     
                    console.log(error);
                }, {
                enableHighAccuracy: true
                , timeout: 5000});
        }


});

// function showPosition(position) {
//   console.log(position.coords);
//   $("#wo").attr('value',position.coords.latitude);
// }
// function error(error) {
//   console.log(error);
// }



            function loadInfo(e) {

                var myInput = document.getElementById("wo");
                     


                if(myInput.value.length!=0){

                    $info=$("#wo").val();
                   
               
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type:'POST',
                    contentType:'application/json; charset=utf-8',
                    url:'/ajax/load/city/info/'+$info,
                    data: { field1:$info} ,
                    beforeSend:function(){
                        $("#item-wo li").hide();
                         $("#item-wo-loader").show();
                },
                    success: function (response) {
                      

                        console.log($info);
                      console.log(response.data);
                        
                        $("#item-wo").empty();
                            jQuery.each(response.data, function(index, value){
                                $("#item-wo").append(
                                    "<li onclick='changeInfo(event)' data-secret="+value+" style='cursor:pointer'>"+value+"</li>"
                                );
                            
                            });
                      

                     
                    },
                    error: function (xhr,ajaxOptions,thrownError) {
                        $("#item-wo").empty();
                        $("#item-wo li").show();
                         $("#item-wo-loader").hide();
                    console.log(xhr,ajaxOptions,thrownError);
                      
                    },
                    complete:function(){
                        $("#item-wo li").show();
                         $("#item-wo-loader").hide();
                }

                });

}
           }








function changeCategory(e) {
        $data=e.target.textContent;
        $current_title=e.target.dataset.secret;
        $("#what").val($data);
        $("#secret_what").val($current_title);
        
    $("#item-was").empty();
}

function changeInfo(e) {
        $data=e.target.textContent;
        $current_title=e.target.dataset.secret;
        $("#wo").val($data);
        $("#secret_wo").val($current_title);
      
    $("#item-wo").empty();
}





           function loadCategory(e) {

var myInput = document.getElementById("what");
     


if(myInput.value.length!=0){

    $title=$("#what").val();
    $lang=$('html').attr('lang');
   

$.ajax({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type:'POST',
    contentType:'application/json; charset=utf-8',
    url:'ajax/load/category/job/'+$title+"/"+$lang,
    data: { field1:$title,field2:$lang} ,
    beforeSend:function(){
        $("#item-was li").hide();
        $("#item-was-loader").show();
       
},
    success: function (response) {
       if(response.data.status){
           console.log(response.data);

        $("#item-was").empty();
           jQuery.each(response.data.result, function(index, value){
               $("#item-was").append(
                   "<li onclick='changeCategory(event)' style='cursor:pointer' data-secret="+value.title+" data-id="+value.id+">"+value.translate+"</li>"
               );
           
        });
           
       }
       else{
        $("#item-was").empty();
       
       }
    },
    error: function (xhr,ajaxOptions,thrownError) {
        $("#item-was").empty();
    console.log(xhr,ajaxOptions,thrownError);
      
    },
    complete:function(){
       
        $("#item-was-loader").hide();
        $("#item-was li").show();
}

});

}

else{
    $("#item-was").empty();  
}
}



</script>


@endsection






