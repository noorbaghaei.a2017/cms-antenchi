
@extends('template.app')

@section('content')


  <!-- content-->
  <div class="content">
                    <!--  section  -->
                    <section class="parallax-section single-par" data-scrollax-parent="true">
                        <div class="bg par-elem "  data-bg="{{asset('template/images/bg/11.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay op7"></div>
                        <div class="container">
                            <div class="section-title center-align big-title">
                                <h2><span> {{__('cms.contact-us')}} </span></h2>
                                <span class="section-separator"></span>
                            </div>
                        </div>
                        <div class="header-sec-link">
                            <a href="#sec1" class="custom-scroll-link"><i class="fal fa-angle-double-down"></i></a> 
                        </div>
                    </section>
                    <!--  section  end-->               
                    <!--  section  -->
                    <section   id="sec1" data-scrollax-parent="true">
                        <div class="container">
                            <!--about-wrap -->
                            <div class="about-wrap">
                                <div class="row">
                                    <div class="col-md-4">
                                     
                                        <!--box-widget-item -->                                       
                                        <div class="box-widget-item fl-wrap block_box">
                                            <div class="box-widget">
                                                <div class="box-widget-content bwc-nopad">
                                                    <div class="list-author-widget-contacts list-item-widget-contacts bwc-padside">
                                                        <ul class="no-list-style">
                                                            <li><span><i class="fal fa-map-marker"></i> {{__('cms.address')}} :</span> <a href="#singleMap" class="custom-scroll-link">{{convert_lang($setting,LaravelLocalization::getCurrentLocale(),'address')}}</a></li>
                                                            <li><span><i class="fal fa-phone"></i> {{__('cms.mobile')}} :</span> <a href="tel::{{isset(json_decode($setting->phone,true)[0]) ? json_decode($setting->phone,true)[0] : ""}}">{{isset(json_decode($setting->phone,true)[0]) ? json_decode($setting->phone,true)[0] : ""}}</a></li>
                                                            <li><span><i class="fal fa-envelope"></i> {{__('cms.email')}} :</span> <a href="mailto::{{$setting->email}}">{{$setting->email}}</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="list-widget-social bottom-bcw-box  fl-wrap">
                                                        <ul class="no-list-style">

                                                            @if(!is_null($setting->info->telegram))
                                                            <li><a href="https://telegram.me/{{$setting->info->telegram}}" target="_blank" ><i class="fab fa-telegram"></i></a></li>
                                                           @endif
                                                           @if(!is_null($setting->info->instagram))
                                                            <li><a href="https://www.instagram.com/{{$setting->info->instagram}}" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                                           @endif

                                                           @if(!is_null($setting->info->whatsapp))
                                                            <li><a href="https://wa.me/{{$setting->info->whatsapp}}" target="_blank" ><i class="fab fa-whatsapp"></i></a></li>
                                                           @endif

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--box-widget-item end -->  
                                        <!--box-widget-item -->
                                        <div class="box-widget-item fl-wrap" style="margin-top:20px;">
                                            <div class="banner-wdget fl-wrap">
                                                <div class="overlay op4"></div>
                                                <div class="bg"  data-bg="{{asset('template/images/bg/18.jpg')}}"></div>
                                                <div class="banner-wdget-content fl-wrap">
                                                    <a href="#" class="color-bg">{{__('cms.read_more')}} </a>
                                                </div>
                                            </div>
                                        </div>
                                        <!--box-widget-item end -->                                            
                                    </div>
                                    <div class="col-md-8">
                                    @include('template.alert.error')
                                                @include('template.alert.success')
                                        <div class="ab_text">
                               
<p>

text
</p>                                            <div id="contact-form">
                                               
                                                <form method="get"  class="custom-form" action="{{route('send.idea')}}">
                                                    <fieldset>
                                                        <label><i class="fal fa-user"></i></label>
                                                        <input type="text" name="full_name" id="name" class="{{$errors->has('full_name') ? 'error-input' : ''}}" placeholder=" {{__('cms.name')}} *" value="" autocomplete="off"/>
                                                        <div class="clearfix"></div>
                                                  
                                                        <label><i class="fal fa-envelope"></i>  </label>
                                                        <input type="text"  name="email" id="email" class="{{$errors->has('email') ? 'error-input' : ''}}" placeholder="{{__('cms.email')}} *" value="" autocomplete="off"/>
                                                        <div class="clearfix"></div>


                                                        <label><i class="fal fa-book"></i>  </label>
                                                        <input type="text"  name="subject" class="{{$errors->has('subject') ? 'error-input' : ''}}"  placeholder="{{__('cms.subject')}} *" value="" autocomplete="off"/>
                                                   <div class="clearfix"></div>

                                                     <label><i class="fal fa-text"></i>  </label>
                                                        <textarea name="message" class="{{$errors->has('email') ? 'error-input' : ''}}"  cols="40" rows="3" placeholder=" {{__('cms.message')}}:"></textarea>
                                                        <div class="clearfix"></div>

                                                    </fieldset>
                                                    <button class="btn float-btn color2-bg" id="submit">{{__('cms.send')}} <i class="fal fa-paper-plane"></i></button>
                                                </form>
                                            </div>
                                            <!-- contact form  end--> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- about-wrap end  --> 
                        </div>
                    </section>
                    <section class="no-padding-section">
                        <div class="map-container">

                            {!! $setting->google_map !!}
                            
                        </div>
                    </section>
                </div>
                <!--content end-->





@endsection




