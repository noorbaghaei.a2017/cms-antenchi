@extends('template.app')

@section('content')


                <!-- content-->
                <div class="content">

                  

<div class="container big-container" style="text-align:left;">
                 

               
              

                 <div style="margin:15px 0">
<h1 style="color: #697891;">INHALTSVERZEICHNIS</h1>
</div>

                 <ul>


<li ><a href="#Gegenstand-dieser-Datenschutzinformation"">Gegenstand dieser Datenschutzinformation</a></li>
<li ><a href="#Verantwortlicher-für-die-Datenverarbeitung">Verantwortlicher für die Datenverarbeitung</a></li>
<li ><a href="#Webhosting">Webhosting</a></li>
<li ><a href="#Umfang-der-Datenverarbeitung">Umfang der Datenverarbeitung</a></li>
<li ><a href="#Einwilligung-in-E-Mail-Newsletter">Einwilligung in E-Mail Newsletter</a></li>
<li ><a href="#Einsatz-von-Cookies">Einsatz von Cookies</a></li>
<li ><a href="#Analyse-&-Tracking">Analyse & Tracking</a></li>
<li ><a href="#Google-Maps">Google-Maps</a></li>
<li ><a href="#Ihre-Rechte-als-Betroffener">Ihre Rechte als Betroffener</a></li>
<li ><a href="#Beschwerderecht">Beschwerderecht</a></li>
</ul>

              



                 <section>
                 <div class="section-title" >
                 <h2 style="text-align:left;" id="Gegenstand-dieser-Datenschutzinformation">Gegenstand-dieser-Datenschutzinformation</h2>
                  </div>
                  <p>
                  Diese Datenschutzinformation informiert Sie darüber, welche personenbezogene Daten wir bei Ihrem Besuch unserer Online-Portals „INIAZ“ erheben und wie wir diese verarbeiten. Darüber hinaus informieren wir Sie über die Ihnen zustehenden Rechte.


                  </p>
                  <br>
                  <p>
                  Personenbezogene Daten sind alle Daten, die auf Sie persönlich beziehbar sind, wie z.B. Name, Anschrift, E-Mail-Adresse, hochgeladene Inhalte oder Ihr Nutzerverhalten. 
                  </p>
                  <br>

                  <p>
                  Diese Datenschutzinformation ist aktuell gültig. Durch die Weiterentwicklung unseres Portals oder aufgrund geänderter gesetzlicher beziehungsweise behördlicher Vorgaben kann es notwendig werden, diese Datenschutzinformation zu ändern. Die jeweils aktuelle Fassung kann von Ihnen jederzeit auf unserer der Webseite abgerufen und ausgedruckt werden.

                  </p>
                  <br>

                  </section>

                  <section>

                  <div class="section-title" >
                  <h2 style="text-align:left;" id="Verantwortlicher-für-die-Datenverarbeitung">Verantwortlicher-für-die-Datenverarbeitung</h2>
                  </div>
                  <p>

                  Verantwortlich im Sinne von Art. 4 Nr. 7 der Datenschutz-Grundverordnung (DSGVO) und zugleich Diensteanbieter im Sinne des Telemediengesetzes (TMG) ist:

                  </p>

                  <br>

                  <p>
                  Alireza Asadi
Friedrich-Ebert-Platz 2
51373 Leverkusen
Deutschland
Tel: […]
E-Mail: info@[...].de 


                  </p>
                  <br>


<p>
Fragen oder Anmerkungen zu dieser Datenschutzerklärung richten Sie bitte an folgende E-Mail-Adresse: info@[...].de


</p>
<br>




                 </section>

                 <section>
                 <div class="section-title" >
                  <h2 style="text-align:left;" id="Webhosting">Webhosting</h2>
                  </div>

                  <p>

Unsere Website wird bei einem Webhosting-Unternehmen („Dienstleister“) gespeichert. Unser Dienstleister verarbeitet Daten, die beim Besuch unserer Webseite und der Nutzung von Anwendungen entstehen.

</p>

<br>
                  <p>

                  Wir haben mit unserem Dienstleister einen Auftragsverarbeitungsvertrag geschlossen. Dieser Vertrag verpflichtet unseren Dienstleister, die Daten nach unserer Weisung zu verarbeiten und nicht zu eigenen Zwecken zu verwenden.

                  </p>

</section>

<section>
<div class="section-title" >
                  <h2 style="text-align:left;" id="Umfang-der-Datenverarbeitung">Umfang-der-Datenverarbeitung</h2>
                  </div>
                  <p>
                  Der Umfang der Datenverarbeitung unterscheidet sich danach, ob Sie unsere Webseite nur zum Abruf von Informationen nutzen („informatorische Nutzung“) oder ob Sie zusätzlich Einträge in das Geschäftsverzeichnis machen, Anzeigen schalten oder Bewertungen abgeben.


                  </p>
                  <br>

                  <p>
                  Bei der informatorischen Nutzung erheben wir Daten, die uns Ihr Internetbrowser automatisch übermittelt, wie etwa Datum und Uhrzeit, Browsertyp, Browser-Einstellung, Betriebssystem, zuletzt besuchte Webseite, übertragene Datenmenge und Zugriffsstatus sowie Ihre IP-Adresse. Die Daten werden gelöscht, sobald sie für die Erreichung des Zwecks ihrer Erhebung nicht mehr erforderlich sind. Bei der Bereitstellung der Website ist dies der Fall, wenn die jeweilige Sitzung beendet ist. Die Protokolldateien werden maximal bis zu 24 Stunden direkt und ausschließlich für Administratoren zugänglich aufbewahrt. Die IP-Adresse wird nur für die Dauer Ihres Besuchs gespeichert. Darüber hinaus speichern wir die Daten zu Zwecken der Protokollierung ausschließlich in anonymisierter Form, indem wir die IP-Adresse so kürzen, dass eine Zuordnung nicht mehr möglich ist. Die Datenverarbeitung bei einer informatorischen Nutzung ist zur Wahrung unserer berechtigten Interessen an einer optimalen Darstellung unserer Webseite erforderlich und erfolgt daher auf der Rechtsgrundlage des Art. 6 Abs. 1 Satz 1 lit. f) DSGVO.
                  </p>

<br>
                  <p>
                  Wenn Sie Funktionen des Online-Portals nutzen, ist die Angabe persönlicher Daten erforderlich, wie Name, Anschrift, E-Mail-Adresse und Telefonnummer. Notwendige Pflichtangaben sind besonders gekennzeichnet, alle weiteren Angaben sind freiwillig. Sie haben darüber hinaus die Möglichkeit, ein Benutzerkonto anzulegen, in dem Ihre Daten für eine spätere Nutzung gespeichert werden. Sie können diese Daten und das Benutzerkonto jederzeit löschen. Rechtsgrundlage für die Datenverarbeitung ist Art. 6 Abs.1 lit. b) DSGVO. Die Verarbeitung Ihrer Daten ist für die Erfüllung des Nutzungsvertrags oder zur Durchführung vorvertraglicher Maßnahmen erforderlich.

                  </p>


<br>
                  <p>
                  Sie können uns über das auf unserer Seite bereitgestellte Kontaktformular Nachrichten übermitteln. Hierbei fragen wir Ihren Namen und Ihre E-Mail-Adresse ab. Die Datenverarbeitung erfolgt bei nicht-registrierten Nutzern auf Grundlage von Art. 6 Abs. 1 S. 1 lit. b DSGVO, da sie zur Durchführung vorvertraglicher Maßnahmen erforderlich ist und bei registrierten Nutzern auf Grundlage von Art. 6 Abs. 1 S. 1 lit. f DSGVO, da wir ein berechtigtes Interesse daran haben, auf Anfragen unserer Nutzern reagieren zu  können und so einen funktionierenden Service zu gewährleisten. Die für die Benutzung des Kontaktformulars von uns erhobenen personenbezogenen Daten werden nach Erledigung der von Ihnen gestellten Anfrage gelöscht, es sei denn, dass wir diese wegen der Art Ihrer Anfrage vorhalten müssen.

                  </p>


</section>
                  <section>
                  <div class="section-title" >
                  <h2 style="text-align:left;" id="Einwilligung-in-E-Mail-Newsletter">Einwilligung-in-E-Mail-Newsletter</h2>
                  </div>
                  <p>

                  Wenn Sie sich in unseren Newsletter eintragen, übersenden wir Ihnen regelmäßig per E-Mail aktuelle Informationen zu unserem Online-Portal. Für Ihre Anmeldung bei unserem E-Mail-Newsletter benötigen wir neben Ihrer Einwilligung lediglich Ihren Namen zur persönlichen Ansprache und Ihre E-Mail-Adresse, an die der Newsletter versendet werden soll.

                  </p>


<br>
                  <p>

                  Für die Anmeldung zu unserem E-Mail-Newsletter verwenden wir aus Sicherheitsgründen das so genannte Double-Opt-In-Verfahren: Hierbei erhalten Sie nach Ihrer Eintragung zu unserem Newsletter eine Aktivierungs-E-Mail an Ihre angegebene E-Mail-Adresse. Erst wenn Sie durch Anklicken eines dort enthaltenen Links Ihre Anmeldung bestätigt haben, erhalten Sie in der Folge den gewünschten E-Mail-Newsletter. Wenn Sie Ihre Anmeldung nicht binnen der mitgeteilten Frist nach Erhalt der Aktivierungs-E-Mail bestätigen, wird Ihre Newsletter-Anmeldung aus Sicherheitsgründen automatisch gelöscht. Wenn Sie keine E-Mail-Newsletter mehr von uns erhalten möchten, können Sie sich jederzeit von unserem Newsletter abmelden, indem Sie entweder auf den am Ende jeder Newsletter-E-Mail enthaltenen Abmeldelink klicken oder uns eine formlose E-Mail zusenden.

                  </p>


<br>
                  <p>
                  Die Datenverarbeitung bei einer Bestellung unseres Newsletters erfolgt auf der Rechtsgrundlage des Art. 6 Abs. 1 Satz 1 lit. a) DSGVO in Verbindung mit Ihrer Einwilligung in den Erhalt des Newsletters. Ihre Einwilligung speichern wir zu Nachweis- und Dokumentationszwecken bis zu drei Jahre zum Jahresende nach dem letzten Newsletter-Versand an Sie oder Ihrer Erklärung eines Widerrufs. 

                  </p>



</section>
<section>
<div class="section-title" >
                  <h2 style="text-align:left;" id="Einsatz-von-Cookies">Einsatz-von-Cookies</h2>
                  </div>
                  <p>

                  Im Rahmen unserer Webseite nutzen wir die sog. Cookies. Cookies sind kleine Textdateien, die von unserem Webserver an Ihren Browser gesandt und von diesem auf Ihrem Endgerät für einen späteren Abruf vorgehalten werden. Cookies werden von uns genutzt, um Ihnen ein optimales Nutzungserlebnis unserer Webseite zu ermöglichen. Des Weiteren setzen wir Cookies zur Analyse des Nutzerverhaltens sowie für personalisiertes Marketing ein.

                  </p>

                  <br>

                  <p>
                  Ob Cookies gesetzt und abgerufen werden können, können Sie beim Besuch unserer Webseite durch Einstellungen im sog. Consent-Banner selbst bestimmen. Weiterhin können Sie in Ihrem Browser Einstellungen vornehmen, zum Beispiel das Speichern von Cookies gänzlich deaktivieren, es auf bestimmte Webseiten beschränken oder Ihren Browser so konfigurieren, dass sie automatisch benachrichtigt werden, sobald ein Cookie gesetzt werden soll. Sie können auch einzelne Cookies blockieren oder löschen. Dies kann aus technischen Gründen jedoch dazu führen, dass einige Funktionen unserer Webseite beeinträchtigt sind und nicht mehr vollständig funktionieren.

                  </p>


<br>
                  <p>
                  Rechtsgrundlage für den Einsatz von Cookies ist Ihre Einwilligung gem. Art. 6 Abs. 1 lit a) DSGVO beim erstmaligen Aufruf unserer Webseite. Sollten Sie der Verwendung von Cookies zugestimmt haben, können Sie diese Einwilligung jederzeit widerrufen. Cookies werden dann gelöscht.

                  </p>


</section>

<section>
<div class="section-title">
                  <h2 style="text-align:left;" id="Analyse-&-Tracking">Analyse-&-Tracking</h2>
                  </div>
                  <p>
                  Wir nutzen Analyse & Tracking Tools, um unser Angebot zu verbessern und unsere Werbung an die richtigen Zielgruppen zu adressieren. Rechtsgrundlage für die Nutzung der Tools ist Ihre Einwilligung über das Consent-Banner Tool beim erstmaligen Besuch unserer Website. Rechtsgrundlage ist dementsprechend Art. 6 Abs. 1 Sat1 lit a) DSGVO.


                  </p>

                  <br>

                  <p>
                  Wir setzen Tracking Tools von Facebook ein. Die Tools ermöglichen es uns, bestimmten Gruppen von pseudonymisierten Besuchern unserer Webseite, die auch Facebook nutzen, individuell abgestimmte und interessenbezogene Werbung auf Facebook anzeigen zu lassen.

                  </p>


                  <br>

                  <p>
                  Wir nutzen Google-Analytics als Website Tracking-Tool, um das Nutzerverhalten von Besuchern unserer Webseite auszuwerten und dadurch das Nutzungserlebnis auf unserer Webseite zu verbessern. Dabei werden Daten an Google gesendet, ausgewertet und uns in Form von Berichten zur Verfügung gestellt.

                  </p>


</section>

<section>
<div class="section-title" >
                  <h2 style="text-align:left;" id="Google-Maps">Google-Maps</h2>
                  </div>
                  <p>
                  Wir nutzen in unserem Portal den Kartendienst „Google-Maps“ ein, um ihnen die Standorte von Geschäften und Wegbeschreibungen anzuzeigen. Bei der Nutzung von Google-Maps werden Daten an Google übertragen und von Google ausgewertet.

                  </p>


<br>
                  <p>
                  Der Einsatz von Google-Maps beruht auf unserem berechtigten Interesse, Nutzern die Anschriften und Wege zu Geschäften bzw. anderen Nutzern zu zeigen, da dies einen wesentlichen Mehrwert unserer Online-Portals darstellt. Rechtsgrundlage für die Verwendung von Google-Maps ist somit Art. 6 Abs. 1 lit f) DSGVO.

                  </p>

                  </section>

                  <section>

                  <div class="section-title" >
                  <h2 style="text-align:left;" id="Ihre-Rechte-als-Betroffener">Ihre-Rechte-als-Betroffener</h2>
                 </div>
                  <p>

                  Sie haben das Recht, jederzeit Auskunft darüber zu verlangen, ob und welche Daten wir von Ihnen zu welchem Zweck verarbeiten und an wen und auf welcher Grundlage sie weitergegeben werden. Das beinhaltet auch Ihr Recht auf Aushändigung von Kopien. Bei großem Datenbestand dürfen Nutzer gebeten werden, die Auskunft auf bestimmte Datenarten zu konkretisieren. Die Auskunft muss unverzüglich erfolgen und darf in keinem Fall länger als ein Monat dauern.

                  </p>

                  <br>

                  <p>
                  Sie haben das Recht, die Sie betreffenden personenbezogenen Daten in einem strukturierten, gängigen und maschinenlesbaren Format zu erhalten oder die Übermittlung an einen anderen Verantwortlichen zu verlangen (Art. 20 DSGVO).

                  </p>

                  <br>

                  <p>
                  Auf Anfrage werden wir Sie gern informieren, ob und welche personenbezogenen Daten zu Ihrer Person gespeichert sind (Art. 15 DSGVO), insbesondere über die Verarbeitungszwecke, die Kategorie der personenbezogenen Daten, die Kategorien von Empfängern, gegenüber denen Ihre Daten offengelegt wurden oder werden, die geplante Speicherdauer, das Bestehen eines Rechts auf Berichtigung, Löschung, Einschränkung der Verarbeitung oder Widerspruch, das Bestehen eines Beschwerderechts, die Herkunft ihrer Daten, sofern diese nicht bei uns erhoben wurden, sowie über das Bestehen einer automatisierten Entscheidungsfindung einschließlich Profiling.

                  </p>

                  <br>

                  <p>
                  Ihnen steht zudem das Recht zu, etwaig unrichtig erhobene personenbezogene Daten berichtigen oder unvollständig erhobene Daten vervollständigen zu lassen (Art. 16 DSGVO).

                  </p>

                  <br>

                  <p>
Ferner haben Sie das Recht, von uns die Einschränkung der Verarbeitung Ihrer Daten zu verlangen, sofern die gesetzlichen Voraussetzungen hierfür vorliegen (Art. 18 DSGVO).

                  </p>
                  <br>
                  <p>
                  Darüber haben Sie das sogenannte „Recht auf Vergessenwerden“, d.h. Sie können von uns die Löschung Ihrer personenbezogenen Daten verlangen, sofern hierfür die gesetzlichen Voraussetzungen vorliegen (Art. 17 DSGVO).
                  </p>

                  <br>

                  <p>
                  Unabhängig davon werden Ihre personenbezogenen Daten automatisch von uns gelöscht, wenn der Zweck der Datenerhebung weggefallen oder die Datenverarbeitung unrechtmäßig erfolgt ist.

                  </p>

                  <br>

                  <p>
                  Gemäß Art. 7 Abs. 3 DSGVO haben Sie das Recht, Ihre einmal erteilte Einwilligung jederzeit gegenüber uns zu widerrufen. Dies hat zur Folge, dass wir die Datenverarbeitung, die auf dieser Einwilligung beruhte, für die Zukunft nicht mehr fortführen dürfen.

                  </p>

                  <br>

                  <p>
                  Sie haben zudem das Recht, jederzeit gegen die Verarbeitung Ihrer personenbezogenen Daten Widerspruch zu erheben, sofern ein Widerspruchsrecht gesetzlich vorgesehen ist. Im Falle eines wirksamen Widerrufs werden Ihre personenbezogenen Daten ebenfalls automatisch durch uns gelöscht (Art. 21 DSGVO).

                  </p>

                  <br>

                  <p>
                  Möchten Sie von Ihrem Widerrufs- oder Widerspruchsrecht Gebrauch machen, genügt eine Mitteilung per E-Mail.

                  </p>
                  <br>

                  </section>

                  <section>
                  <div class="section-title" >
                  <h2 style="text-align:left;" id="Beschwerderecht">Beschwerderecht</h2>
                  </div>
                  <p>
                  Bei Verstößen gegen die datenschutzrechtlichen Vorschriften haben Sie gem. Art. 77 DSGVO die Möglichkeit, Beschwerde einer Aufsichtsbehörde Ihrer Wahl zu erheben. 

                  </p>

                  <br>

                  <p>

                  Die für uns zuständige Aufsichtsbehörde ist der Landesbeauftragte für Datenschutz.

                  </p>

</section>
                </div>
                <!--content end-->




@endsection
