@extends('template.app')

@section('content')

  <!-- content-->
  <div class="content">
                    <!--  section  -->
                    <section class="parallax-section single-par" data-scrollax-parent="true">
                        <div class="bg par-elem "  data-bg="{{asset('template/images/bg/35.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay op7"></div>
                        <div class="container">
                            <div class="section-title center-align big-title">
                                
                            </div>
                        </div>
                        <div class="header-sec-link">
                            <a href="#sec1" class="custom-scroll-link"><i class="fal fa-angle-double-down"></i></a> 
                        </div>
                    </section>
                    <!--  section  end-->
                    <section class="gray-bg small-padding no-top-padding-sec" id="sec1">
                        <div class="container">
                        

                           
<div class="breadcrumbs inline-breadcrumbs fl-wrap block-breadcrumbs">
<a href="{{route('front.website')}}">{{__('cms.home')}}</a>

</div>

                            <!-- list-main-wrap-header-->
                            <div class="list-main-wrap-header fl-wrap   block_box no-vis-shadow no-bg-header fixed-listing-header">
                                <!-- list-main-wrap-title-->
                               
                                <!-- list-main-wrap-title end-->
                                <!-- list-main-wrap-opt-->
                                <div class="list-main-wrap-opt">
                                    
                                    <!-- price-opt-->
                                    <div class="grid-opt">
                                        <ul class="no-list-style">
                                            <li class="grid-opt_act"><span class="two-col-grid act-grid-opt tolt" data-microtip-position="bottom" data-tooltip="نمایش شبکه ای"><i class="fal fa-th"></i></span></li>
                                            <li class="grid-opt_act"><span class="one-col-grid tolt" data-microtip-position="bottom" data-tooltip="نمایش لیستی"><i class="fal fa-list"></i></span></li>
                                        </ul>
                                    </div>
                                    <!-- price-opt end-->
                                </div>
                                <!-- list-main-wrap-opt end-->                    
                                <a class="custom-scroll-link back-to-filters clbtg" href="#lisfw"><i class="fal fa-search"></i></a>
                            </div>
                            <!-- list-main-wrap-header end-->                      
                            <div class="mob-nav-content-btn  color2-bg show-list-wrap-search ntm fl-wrap"><i class="fal fa-filter"></i>  {{__('cms.filter')}}</div>
                            <div class="fl-wrap">
                                <!-- listsearch-input-wrap-->
                                <div class="listsearch-input-wrap lws_mobile fl-wrap tabs-act inline-lsiw" id="lisfw">
                                    <div class="listsearch-input-wrap_contrl fl-wrap">
                                        <ul class="tabs-menu fl-wrap no-list-style">
                                            <li class="current" style="width:100%"><a href="#category-search"> <i class="fal fa-image"></i>  {{__('cms.sub-category')}} </a></li>
                                        </ul>
                                    </div>
                                  
                                    <!--tabs -->                       
                                    <div class="tabs-container fl-wrap">
                                    <form action="{{route('search.page.bussiness',['filter'=>1])}}" method="GET">

                                       
                                        <!--tab --> 
        
                                        <div class="tab">
                                            <div id="category-search" class="tab-content first-tab">
                                                <!-- category-carousel-wrap -->  
                                                <div class="category-carousel-wrap fl-wrap">
                                                    <div class="category-carousel fl-wrap full-height">
                                                        <div class="swiper-container">
                                                            <div class="swiper-wrapper">

                                                            @foreach ($sub_categories as $sub)
                                                                
                                                             <!-- category-carousel-item -->  
                                                             <div class="swiper-slide">
                                                                    <a href="{{route('search.page.bussiness',['filter'=>1,'category'=>$sub->id])}}" class="category-carousel-item fl-wrap full-height" >
                                                                        <img src="{{asset('template/images/all/12.jpg')}}" alt="">
                                                                        <div class="category-carousel-item-icon red-bg"><i class="fal fa-cheeseburger"></i></div>
                                                                        <div class="category-carousel-item-container">
                                                                            <div class="category-carousel-item-title">{!! convert_lang($sub,LaravelLocalization::getCurrentLocale(),'title') !!}</div>
                                                                            <div class="category-carousel-item-counter">{{$sub->user_services()->count()}} {{__('cms.job')}}</div>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                                <!-- category-carousel-item end -->  
                                                              
                                                            @endforeach
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- category-carousel-wrap end-->  
                                                </div>
                                                <div class="catcar-scrollbar fl-wrap">
                                                    <div class="hs_init"></div>
                                                    <div class="cc-contorl">
                                                        <div class="cc-contrl-item cc-next"><i class="fal fa-angle-left"></i></div>
                                                        <div class="cc-contrl-item cc-prev"><i class="fal fa-angle-right"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--tab end-->
                                    </div>
                                    <!--tabs end-->
                                </div>
                                <!-- listsearch-input-wrap end-->                                
                                <!-- listing-item-container -->
                                <div class="listing-item-container init-grid-items fl-wrap nocolumn-lic three-columns-grid">
                                   
             

                                     @foreach ($items as $item )

<!-- listing-item  -->
<div class="listing-item">
    <article class="geodir-category-listing fl-wrap">
        <div class="geodir-category-img">
            <a href="{{route('bussiness.single',['bussiness'=>$item->slug])}}" class="geodir-category-img-wrap fl-wrap">
            @if(!$item->Hasmedia('images'))
                                    <img  src="{{asset('img/no-img.gif')}}" alt="title" title="title" >
                                @else
                                <img  src="{{$item->getFirstMediaUrl('images')}}" alt="title" title="title" >
                                @endif
            </a>
           
            <div class="geodir-category-opt">
                <div class="listing-rating-count-wrap">
                  
                  <br>
                    <div class="reviews-count">{{count($item->comments)}} {{__('cms.comment')}}</div>
                </div>
            </div>
        </div>
        <div class="geodir-category-content fl-wrap title-sin_item">
            <div class="geodir-category-content-title fl-wrap">
                <div class="geodir-category-content-title-item">
                    <h3 class="title-sin_map"><a href="{{route('bussiness.single',['bussiness'=>$item->slug])}}">{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}} </a></h3>
                    <div class="geodir-category-location fl-wrap"><a href="#" ><i class="fas fa-map-marker-alt"></i> {{$item->city}}</a></div>
                </div>
            </div>
                                          <div class="geodir-category-text fl-wrap">
                                               <p class="small-text"> {{$item->title}}</p>
                                               <div class="facilities-list fl-wrap">
                                                   <div class="facilities-list-title">{{__('cms.attributes')}} : </div>
                                                   <ul class="no-list-style">
                                                   @foreach ($item->attributes as $attr)

                                                   <li class="tolt"  data-microtip-position="top" data-tooltip=" {{getAttribute($attr->attribute)->title}}"><i class="{{getAttribute($attr->attribute)->icon}}"></i></li>

                                                        @endforeach
                                                    </ul>
                                               </div>
                                           </div>
            <div class="geodir-category-footer fl-wrap">
                <a class="listing-item-category-wrap">
                    <div class="listing-item-category category-box"><i class="fal fa-cheeseburger"></i></div>
                    <span> {{convert_lang(showServiceCategory($item->service),LaravelLocalization::getCurrentLocale(),'title')}}</span>
                </a>
                <div class="geodir-opt-list">
                <ul class="no-list-style">
                                                           <li><a href="#" class="show_gcc"><i class="fal fa-envelope"></i><span class="geodir-opt-tooltip"> {{__('cms.call')}}</span></a></li>
                                                           <li><a target='_blank' href="https://maps.google.com/?q={{$item->address}}" class="single-map-item" ><i class="fal fa-map-marker-alt"></i><span class="geodir-opt-tooltip"> {{__('cms.location')}} </span> </a></li>

                                                           @if($item->Hasmedia(config("cms.collection-images")))
                                                           <li>
                                                           
                                                               <div class="dynamic-gal gdop-list-link" data-dynamicPath='[
                                                               @foreach($item->getMedia(config("cms.collection-images")) as $media)
                                                               {"src": "/media/{{$media->id}}/{{$media->file_name}}"},
                                                               @endforeach
                                                                ]'>
                         
                                                               <i class="fal fa-search-plus"></i><span class="geodir-opt-tooltip"> {{__('cms.gallery')}} </span></div>
                                                           </li>
                                                           @endif
                                                       </ul>
                </div>
               
                <div class="geodir-category_contacts">
                    <div class="close_gcc"><i class="fal fa-times-circle"></i></div>
                    <ul class="no-list-style">
                        <li><span><i class="fal fa-phone"></i> {{__('cms.call')}} : </span><a href="#">{{$item->phone}}</a></li>
                        <li><span><i class="fal fa-envelope"></i> {{__('cms.email')}} : </span><a href="#">{{$item->email}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </article>
</div>
<!-- listing-item end --> 


@endforeach     

                                                                                       
                                    <!-- <div class="pagination fwmpag">
                                        <a href="#" class="prevposts-link"><i class="fas fa-caret-right"></i><span>قبلی</span></a>
                                        <a href="#">1</a>
                                        <a href="#" class="current-page">2</a>
                                        <a href="#">3</a>
                                        <a href="#">...</a>
                                        <a href="#">7</a>
                                        <a href="#" class="nextposts-link"><span>بعدی</span><i class="fas fa-caret-left"></i></a>
                                    </div> -->
                                </div>
                                <!-- listing-item-container end -->
                            </div>
                        </div>
                    </section>
                    <div class="limit-box fl-wrap"></div>
                </div>
                <!--content end-->


@endsection








