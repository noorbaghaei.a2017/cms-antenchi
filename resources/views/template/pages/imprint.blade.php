@extends('template.app')

@section('content')


                <!-- content-->
                <div class="content">

                <div class="container big-container" style="text-align:left;">
                 

               
              

                 <div style="margin:15px 0">
<h1 style="color: #697891;">INHALTSVERZEICHNIS</h1>
</div>

                 <ul>


<li ><a href="#Anwendungsbereich">Anwendungsbereich</a></li>
<li ><a href="#Geschäftsverzeichnis">Geschäftsverzeichnis</a></li>
<li ><a href="#Kleinanzeigen">Kleinanzeigen</a></li>
<li ><a href="#Bewertungen">Bewertungen</a></li>
<li ><a href="#Nutzungsgebühren">Nutzungsgebühren</a></li>
<li ><a href="#Laufzeit">Laufzeit</a></li>
<li ><a href="#Nutzungsrechte">Nutzungsrechte</a></li>
<li ><a href="#Alternative Streitbeilegung">Alternative Streitbeilegung</a></li>
<li ><a href="#Schlussbestimmungen">Schlussbestimmungen</a></li>

</ul>




<section>
                 <div class="section-title" >
                 <h2 style="text-align:left;" id="Anwendungsbereich">Anwendungsbereich</h2>
                  </div>
                  <p>
                  ASADI & ROSTAMIAN betreiben unter www.iniaz.de ein Online-Portal für die persische Community. Das Portal umfasst ein Geschäftsverzeichnis und Kleinanzeigen.


                  </p>
                  <br>
                  <p>
                  Das Portal wird von ASADI & ROSTAMIAN zu privaten, nicht kommerziellen Zwecken betrieben. Die Nutzung des Portals ist nur unter Einhaltung dieser Nutzungsbedingen gestattet.
                  </p>
                  <br>

                  
                  <br>

                  </section>


                  <section>
                 <div class="section-title" >
                 <h2 style="text-align:left;" id="Geschäftsverzeichnis">Geschäftsverzeichnis</h2>
                  </div>
                  <p>
                  Das Geschäftsverzeichnis beinhaltet Einträge von Unternehmen, die von persischen Inhabern betrieben werden oder die persische Produkte anbieten.

                  </p>
                  <br>
                  <p>
                  Geschäftsinhaber können sich mit einem Benutzerkonto registrieren und ihr Unternehmen in das Geschäftsverzeichnis eintragen. Der Eintrag muss für eine bestimmte Kategorie erfolgen und die vorgesehenen Angaben enthalten. Die Angaben müssen vollständig, wahr und aktuell sein.
                  </p>
                  <br>

                  <p>
                  ASADI & ROSTAMIAN behalten sich eine Prüfung und Freischaltung des Benutzerkontos bzw. des Eintrags nach freiem Ermessen vor. Es besteht kein Anspruch eines Geschäftsinhabers auf einen Eintrag seines Unternehmens in das Geschäftsverzeichnis.
                  </p>
                  <br>

                  <p>
                  ASADI & ROSTAMIAN haften gegenüber Nutzern nicht für die Richtigkeit der Einträge im Geschäftsverzeichnis. Des Weiteren haften ASADI & ROSTAMIAN nicht für die Bonität der eingetragenen Unternehmen.
                  </p>
                  <br>

                  
                  <br>

                  </section>


                  <section>
                 <div class="section-title" >
                 <h2 style="text-align:left;" id="Kleinanzeigen">Kleinanzeigen</h2>
                  </div>
                  <p>
                  Mit Kleinanzeigen können Nutzer Angebote im Online-Portal inserieren. Die Schaltung von Kleinanzeigen erfordert die Registrierung mit einem Benutzerkonto. Das Durchsuchen von Anzeigen ist ohne Registrierung möglich. 


                  </p>
                  <br>
                  <p>
                  Kleinanzeigen müssen die geltenden Gesetze einhalten. ASADI & ROSTAMIAN  prüfen Kleinanzeigen vor der Veröffentlichung und entscheiden nach freiem Ermessen, ob eine Kleinanzeige veröffentlicht wird. Ein Anspruch des Nutzers auf Veröffentlichung besteht nicht.      
                              </p>
                  <br>

                  
                  <br>

                  <p>
                  ASADI & ROSTAMIAN  behalten sich vor, die Zahl der Anzeigen je Nutzer zu beschränken. Die Schaltung von Duplikat-Anzeigen ist untersagt und führt zur Löschung des Benutzerkontos.                
                  </p>
                    <br>

                  
                  <br>

                  <p>
                  Kleinanzeigen müssen aktuelle Kontaktdaten enthalten, sodass interessierte Nutzer Kontakt zu dem inserierenden Nutzer aufnehmen können.

                  
                  <br>

                  <p>
                  ASADI & ROSTAMIAN  haften gegenüber Nutzern nicht für die Richtigkeit der Angaben in Kleinanzeigen und sind nicht Anbieter der inserierten Angebote. Verträge über inserierte Angebote kommen ausschließlich zwischen den Nutzern zustande.


                  </p>

                  <br>

                  </section>




                  <section>
                 <div class="section-title" >
                 <h2 style="text-align:left;" id="Bewertungen">Bewertungen</h2>
                  </div>
                  <p>
                  Nutzer können die im Geschäftsverzeichnis eingetragene Unternehmen bewerten. Mit dem Eintrag in das Geschäftsverzeichnis erklärt sich der Geschäftsinhaber damit einverstanden, von anderen Nutzern bewertet zu werden.


                  </p>
                  <br>
                  <p>
                  Bewertungen dürfen nur auf Grund persönlicher Erfahrungen des Nutzers mit dem bewerteten Unternehmen abgegeben werden. Bewertungen müssen sachlich und objektiv sein.                  
                   </p>
                
                  <br>

                  <p>
                  Wenn eine Bewertung gegen die vorgenannten Grundsätze verstößt, behalten sich ASADI & ROSTAMIAN vor, die entsprechende Bewertung zu entfernen und den Nutzer zu sperren.

                  </p>

                  
                  <br>

                  </section>


                  <section>
                 <div class="section-title" >
                 <h2 style="text-align:left;" id="Nutzungsgebühren">Nutzungsgebühren</h2>
                  </div>
                  <p>
                  Einträge in das Geschäftsverzeichnis und die Schaltung von Anzeigen sind kostenlos. Es besteht kein Anspruch der Nutzer auf eine dauerhafte kostenlose Nutzung. ASADI & ROSTAMIAN sind jederzeit berechtigt, kostenpflichtige Angebote einzuführen. 

                  </p>
                  <br>
                

                  
                  <br>

                  </section>


                  <section>
                 <div class="section-title" >
                 <h2 style="text-align:left;" id="Laufzeit">Laufzeit</h2>
                  </div>
                  <p>
                  Die Nutzung des Portals erfolgt auf unbestimmte Zeit. ASADI & ROSTAMIAN sind jederzeit berechtigt, den Betrieb des Portals einzustellen.


                  </p>
                  <br>
             

                  
                  <br>

                  </section>


                  <section>
                 <div class="section-title" >
                 <h2 style="text-align:left;" id="Nutzungsrechte">Nutzungsrechte</h2>
                  </div>
                  <p>
                  Mit der Eintragung seines Unternehmens räumt der Geschäftsinhaber ASADI & ROSTAMIAN ein Nutzungsrecht für die Veröffentlichung des Eintrags ein. 


                  </p>
                  <br>
                  <p>
                  Das Geschäftsverzeichnis ist eine urheberrechtlich geschützte Datenbank. Das Kopieren der Datenbankeinträge ist nicht erlaubt und begründet einen Anspruch von ASADI & ROSTAMIAN auf Schadensersatz.             
                       </p>
                

                  
                  <br>

                  <p>
                  Mit Schaltung einer Anzeige räumt der Inserent ASADI & ROSTAMIAN ein Nutzungsrecht für die Veröffentlichung der Anzeige ein. ASADI & ROSTAMIAN sind berechtigt, Anzeigen auch auf anderen Internetseiten zu veröffentlichen, um die Reichweite zu erhöhen.
                  </p>

                  
                  <br>

                  </section>


                  <section>
                 <div class="section-title" >
                 <h2 style="text-align:left;" id="Alternative Streitbeilegung">Alternative Streitbeilegung</h2>
                  </div>
                  <p>
                  Die Europäische Kommission stellt eine Plattform für die außergerichtliche Online-Streitbeilegung bereit, die unter http://ec.europa.eu/consumers/odr abrufbar ist.


                  </p>
                  <br>
                  <p>
                  Zur Teilnahme an einem Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle sind ASADI & ROSTAMIAN nicht verpflichtet und nicht bereit.                 
                   </p>
                 

                  
                  <br>

                  </section>


                  <section>
                 <div class="section-title" >
                 <h2 style="text-align:left;" id="Schlussbestimmungen">Schlussbestimmungen</h2>
                  </div>
                  <p>
                  Auf Rechtsbeziehungen zwischen ASADI & ROSTAMIAN und dem Nutzer findet ausschließlich das Recht der Bundesrepublik Deutschland unter Ausschluss der Bestimmungen des UN-Kaufrechts (CISG) und des Internationalen Privatrechts Anwendung.


                  </p>
                  <br>
                 

                  </section>



</div>
                  
                </div>
                <!--content end-->




@endsection
