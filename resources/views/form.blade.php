



@if(LaravelLocalization::getCurrentLocale()==="fa" || LaravelLocalization::getCurrentLocale()==="ar")
    <h1 style="text-align: right"> {{__('cms.supplier-form')}}</h1>


    <div class="text-right">
        <img src="{{asset(config('cms.flag.'.LaravelLocalization::getCurrentLocale()))}}" width="30">

    </div>
<ul style="text-align: right;direction: rtl">

    @else
        <h1 style="text-align: left"> {{__('cms.supplier-form')}}</h1>


        <div class="text-left">
            <img src="{{asset(config('cms.flag.'.LaravelLocalization::getCurrentLocale()))}}" width="30">

        </div>
        <ul style="text-align: left;direction: ltr">
    @endif

    @if(isset($kind))
        <li>{{__('cms.kind')}} : {{  $kind }}</li>
    @endif

    @if(isset($company_name))
        <li>{{__('cms.company-name')}} : {{  $company_name }}</li>
    @endif

        @if(isset($company_type))
            <li>{{__('cms.company-type')}} : {{  $company_type }}</li>
        @endif

        @if(isset($field_of_activity))
            <li>{{__('cms.field-of-activity')}} : {{  $field_of_activity }}</li>
        @endif

        @if(isset($type_of_ownership))
            <li>{{__('cms.type-of-ownership')}} : {{  $type_of_ownership }}</li>
        @endif

        @if(isset($product_name))
            <li>{{__('cms.product-name')}} : {{  $product_name }}</li>
        @endif


        @if(isset($registration_number))
            <li>{{__('cms.registration-number')}} : {{  $registration_number }}</li>
        @endif


        @if(isset($operating_license_number))
            <li>{{__('cms.operating-license-number')}} : {{  $operating_license_number }}</li>
        @endif

        @if(isset($economic_code))
            <li>{{__('cms.economic-code')}} : {{  $economic_code }}</li>
        @endif

            @if(isset($national_id))
                <li>{{__('cms.national-id')}} : {{  $national_id }}</li>
            @endif

        @if(isset($business_license_number))
            <li>{{__('cms.business-license-number')}} : {{  $business_license_number }}</li>
        @endif


        @if(isset($date_of_establishment))
            <li>{{__('cms.date-of-establishment')}} : {{  $date_of_establishment }}</li>
        @endif

        @if(isset($licensee_name))
            <li>{{__('cms.licensee-name')}} : {{  $licensee_name }}</li>
        @endif

        @if(isset($national_code_of_the_licensee))
            <li>{{__('cms.national-code-of-the-licensee')}} : {{  $national_code_of_the_licensee }}</li>
        @endif

        @if(isset($the_address_of_central_office))
            <li>{{__('cms.telephone-central-office')}} : {{  $the_address_of_central_office }}</li>
        @endif

        @if(isset($head_office_fax))
            <li>{{__('cms.head-office-fax')}} : {{  $head_office_fax }}</li>
        @endif


        @if(isset($factory_address))
            <li>{{__('cms.factory-address')}} : {{  $factory_address }}</li>
        @endif

        @if(isset($factory_phone))
            <li>{{__('cms.factory-phone')}} : {{  $factory_phone }}</li>
        @endif

        @if(isset($factory_fax))
            <li>{{__('cms.factory-fax')}} : {{  $factory_fax }}</li>
        @endif

        @if(isset($company_email))
            <li>{{__('cms.company-email')}} : {{  $company_email }}</li>
        @endif

        @if(isset($company_website))
            <li>{{__('cms.company-website')}}  : {{  $company_website }}</li>
        @endif

        @if(isset($company_capital))
            <li>{{__('cms.company-capital')}} : {{  $company_capital }}</li>
        @endif
        @if(isset($names_of_board_members))
            <li>{{__('cms.names-of-board-members')}} : {{  $names_of_board_members }}</li>
        @endif
        @if(isset($are_you_a_representative_if_yes_what_kind_of_representation))
            <li>{{__('cms.are-you-a-representative-if-yes-what-kind-of-representation')}} : {{  $are_you_a_representative_if_yes_what_kind_of_representation }}</li>
        @endif
        @if(isset($is_there_a_variety_of_brands_in_the_goods_you_supply))
            <li>{{__('cms.is-there-a-variety-of-brands-in-the-goods-you-supply')}} : {{  $is_there_a_variety_of_brands_in_the_goods_you_supply }}</li>
        @endif

        @if(isset($do_you_use_the_services_of_insurance_companies_for_employees_products_devices_and_buildings_explain))
            <li>{{__('cms.do-you-use-the-services-of-insurance-companies-for-employees-products-devices-and-buildings-explain')}} : {{  $do_you_use_the_services_of_insurance_companies_for_employees_products_devices_and_buildings_explain }}</li>
        @endif

        @if(isset($do_you_have_a_stock_for_the_products_or_products_you_supply))
            <li>{{__('cms.do-you-have-a-stock-for-the-products-or-products-you-supply')}} : {{  $do_you_have_a_stock_for_the_products_or_products_you_supply }}</li>
        @endif

        @if(isset($warehouse_capacity))
            <li>{{__('cms.warehouse-capacity')}} : {{  $warehouse_capacity }}</li>
        @endif

        @if(isset($number_of_warehouses))
            <li>{{__('cms.number-of-warehouses')}} : {{  $number_of_warehouses }}</li>
        @endif

        @if(isset($annual_production_capacity))
            <li>{{__('cms.annual-production-capacity')}} : {{  $annual_production_capacity }}</li>
        @endif

        @if(isset($production_volume_of_the_last_three_years))
            <li>{{__('cms.production-volume-of-the-last-three-years')}} : {{  $production_volume_of_the_last_three_years }}</li>
        @endif





        @if(isset($the_average_annual_turnover_of_the_current_year))
            <li>{{__('cms.the-average-annual-turnover-of-the-current-year')}} : {{  $the_average_annual_turnover_of_the_current_year }}</li>
        @endif

        @if(isset($money_in_circulation_Annual_average_of_the_last_5_years))
            <li>{{__('cms.money-in-circulation-Annual-average-of-the-last-5-years')}} : {{  $money_in_circulation_Annual_average_of_the_last_5_years }}</li>
        @endif
        @if(isset($if_you_are_a_manufacturer_select_the_status_of_the_factory_or_workshop))
            <li>{{__('cms.if-you-are-a-manufacturer-select-the-status-of-the-factory-or-workshop')}} : {{  $if_you_are_a_manufacturer_select_the_status_of_the_factory_or_workshop }}</li>
        @endif
        @if(isset($does_the_product_offered_by_you_have_a_warranty_guarantee_and_after_sales_service))
            <li>{{__('cms.does-the-product-offered-by-you-have-a-warranty-guarantee-and-after-sales-service')}} : {{  $does_the_product_offered_by_you_have_a_warranty_guarantee_and_after_sales_service }}</li>
        @endif


        @if(isset($do_you_offer_technical_advice_in_your_field_of_work))
            <li>{{__('cms.do-you-offer-technical-advice-in-your-field-of-work')}} : {{  $do_you_offer_technical_advice_in_your_field_of_work }}</li>
        @endif
        @if(isset($is_your_technical_advice_free_if_you_do_not_buy))
            <li>{{__('cms.is-your-technical-advice-free-if-you-do-not-buy')}} : {{  $is_your_technical_advice_free_if_you_do_not_buy }}</li>
        @endif


        @if(isset($is_your_technical_advice_free_to_buy))
            <li>{{__('cms.is-your-technical-advice-free-to-buy')}} : {{  $is_your_technical_advice_free_to_buy }}</li>
        @endif


        @if(isset($number_of_successful_contracts_in_the_past_year_for_the_goods_under_evaluation_with_the_cement_steel_oil_and_petrochemical_industries_mines))
            <li>{{__('cms.number-of-successful-contracts-in-the-past-year-for-the-goods-under-evaluation-with-the-cement-steel-oil-and-petrochemical-industries-mines')}} : {{  $number_of_successful_contracts_in_the_past_year_for_the_goods_under_evaluation_with_the_cement_steel_oil_and_petrochemical_industries_mines }}</li>
        @endif
        @if(isset($number_of_successful_contracts_in_the_past_year_with_companies_under_espandar_for_the_product_under_evaluation))
            <li>{{__('cms.number-of-successful-contracts-in-the-past-year-with-companies-under-espandar-for-the-product-under-evaluation')}} : {{  $number_of_successful_contracts_in_the_past_year_with_companies_under_espandar_for_the_product_under_evaluation }}</li>
        @endif

        @if(isset($do_you_use_the_services_of_other_companies_or_factories))
            <li>{{__('cms.do-you-use-the-services-of-other-companies-or-factories')}} : {{  $do_you_use_the_services_of_other_companies_or_factories }}</li>
        @endif
        @if(isset($does_the_product_you_produce_or_supply_have_iran_code))
            <li>{{__('cms.does-the-product-you-produce-or-supply-have-iran-code')}} : {{  $does_the_product_you_produce_or_supply_have_iran_code }}</li>
        @endif
        @if(isset($explain_if_you_use_the_services_of_other_companies))
            <li>{{__('cms.explain-if-you-use-the-services-of-other-companies')}} : {{  $explain_if_you_use_the_services_of_other_companies }}</li>
        @endif


        @if(isset($does_your_product_have_standard_name_it))
            <li>{{__('cms.does-your-product-have-standard-name-it')}} : {{  $does_your_product_have_standard_name_it }}</li>
        @endif
        @if(isset($is_your_production_method_below_certain_standard_name_it))
            <li>{{__('cms.is-your-production-method-below-certain-standard-name-it')}} : {{  $is_your_production_method_below_certain_standard_name_it }}</li>
        @endif




        @if(isset($have_you_obtained_certificates_of_quality_management_environment_etc_explain))
            <li>{{__('cms.have-you-obtained-certificates-of-quality-management-environment-etc-explain')}} : {{  $have_you_obtained_certificates_of_quality_management_environment_etc_explain }}</li>
        @endif

        @if(isset($do_you_have_unit_called_research_and_development))
            <li>{{__('cms.do-you-have-unit-called-research-and-development')}} : {{  $do_you_have_unit_called_research_and_development }}</li>
        @endif
        @if(isset($is_the_quality_control_work_of_the_product_or_production_service_done_in_your_collection))
            <li>{{__('cms.is-the-quality-control-work-of-the-product-or-production-service-done-in-your-collection')}} : {{  $is_the_quality_control_work_of_the_product_or_production_service_done_in_your_collection }}</li>
        @endif
        @if(isset($number_of_staff_below_the_diploma_set_but_with_specialization))
            <li>{{__('cms.number-of-staff-below-the-diploma-set-but-with-specialization')}} : {{  $number_of_staff_below_the_diploma_set_but_with_specialization }}</li>
        @endif


        @if(isset($is_there_codified_training_system_for_the_unit_staff))
            <li>{{__('cms.is-there-codified-training-system-for-the-unit-staff')}} : {{  $is_there_codified_training_system_for_the_unit_staff }}</li>
        @endif

        @if(isset($number_of_third_party_inspector_certification_during_the_past_year))
            <li>{{__('cms.number-of-third-party-inspector-certification-during-the-past-year')}} : {{  $number_of_third_party_inspector_certification_during_the_past_year }}</li>
        @endif


        @if(isset($do_you_have_history_of_exporting_product_or_service_during_the_last_5_years))
            <li>{{__('cms.do-you-have-history-of-exporting-product-or-service-during-the-last-5-years')}} : {{  $do_you_have_history_of_exporting_product_or_service_during_the_last_5_years }}</li>
        @endif

        @if(isset($export_amount_during_the_last_5_years))
            <li>{{__('cms.export-amount-during-the-last-5-years')}} : {{  $export_amount_during_the_last_5_years }}</li>
        @endif
        @if(isset($do_you_have_the_facilities_to_transport_the_goods_to_the_place_of_the_buyer_company))
            <li>{{__('cms.do-you-have-the-facilities-to-transport-the-goods-to-the-place-of-the-buyer-company')}}  : {{  $do_you_have_the_facilities_to_transport_the_goods_to_the_place_of_the_buyer_company }}</li>
        @endif
        @if(isset($do_you_have_distribution_network_if_yes_please_name_the_distribution_locations))
            <li>{{__('cms.do-you-have-distribution-network-if-yes-please-name-the-distribution-locations')}}: {{  $do_you_have_distribution_network_if_yes_please_name_the_distribution_locations }}</li>
        @endif
        @if(isset($do_you_have_equipment_for_packing_goods))
            <li>{{__('cms.do-you-have-equipment-for-packing-goods')}} : {{  $do_you_have_equipment_for_packing_goods }}</li>
        @endif
        @if(isset($are_you_member_of_research_institute_research_associations_trade_unions_unions_etc_name_it))
            <li>{{__('cms.are-you-member-of-research-institute-research-associations-trade-unions-unions-etc-name-it')}} : {{  $are_you_member_of_research_institute_research_associations_trade_unions_unions_etc_name_it }}</li>
        @endif
        @if(isset($how_many_days_after_the_purchased_or_repaired_goods_stay_in_your_warehouse_do_you_receive_storage_costs_and_how_much))
            <li>{{__('cms.how-many-days-after-the-purchased-or-repaired-goods-stay-in-your-warehouse-do-you-receive-storage-costs-and-how-much')}} : {{  $how_many_days_after_the_purchased_or_repaired_goods_stay_in_your_warehouse_do_you_receive_storage_costs_and_how_much }}</li>
        @endif


</ul>
