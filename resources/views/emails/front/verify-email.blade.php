<h1>
Ihre Registrierung ist erfolgreich eingegangen.
</h1>

<p>


Bevor Ihr Benutzerkonto aktiviert werden kann, muss noch ein Schritt durchgeführt werden. Die folgende URL muss nur einmal aufgerufen werden.

Um die Registrierung abzuschließen

<a target="_blank" href="{{route('verify.email.job.client',['token'=>$items_email['token']])}}">klicke auf diese URL</a>

Falls Sie Probleme bei der Registrierung haben, melden Sie sich bitte bei unserem Support-Team bei support@iniaz.de.

Mit besten Grüßen,
Iniaz

</p>