<?php

namespace App\Providers\View;


use App\View\adminComposer;
use App\View\frontComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class DataServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('template.*',frontComposer::class);
        View::composer('*',adminComposer::class);

    }
}
