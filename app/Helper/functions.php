<?php

use Illuminate\Http\Request;
use Illuminate\Support\Str;

function tokenGenerateSimple(){
   return Str::random(20);
  
}
function tokenGenerate(){
    $string=Str::random(20);
    $item=Uuid::generate(5,$string, Uuid::NS_DNS);
    return $item->string;
}

function tokenResetGenerate(){
    $string="reset-".Str::random(20);
    $item=Uuid::generate(4,$string, Uuid::NS_DNS);
    return $item->string;
}

function multiRouteKey(){
    return "token";
}

function orderInfo($order){
    return is_null(empty($order)) ? 1 : $order;
}

