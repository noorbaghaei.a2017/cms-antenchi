<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class LockedOut
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $username;
    public $ip;

    /**
     * Create a new event instance.
     *
     * @param $username
     * @param $ip
     */
    public function __construct($username,$ip)
    {
        $this->username=$username;
        $this->ip=$ip;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
