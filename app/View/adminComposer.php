<?php

namespace App\View;

use Modules\Advertising\Entities\Advertising;
use Modules\Carousel\Entities\Carousel;
use Modules\Client\Entities\Client;
use Modules\Core\Entities\Area;
use Modules\Core\Entities\City;
use Modules\Core\Entities\Contact;
use Modules\Core\Entities\Country;
use Modules\Core\Entities\Setting;
use Modules\Core\Entities\State;
use Modules\Core\Entities\User;
use Illuminate\View\View;
use Modules\Core\Entities\UserServices;
use Modules\Article\Entities\Article;
use Modules\Educational\Entities\ClassRoom;
use Modules\Educational\Entities\Race;
use Modules\Event\Entities\Event;
use Modules\Comment\Entities\Comment;
use Modules\Information\Entities\Information;
use Modules\Member\Entities\Member;
use Modules\Order\Entities\Order;
use Modules\Payment\Entities\Payment;
use Modules\Portfolio\Entities\Portfolio;
use Modules\Product\Entities\Product;

class adminComposer{

    public function compose(View $view){
        $view->with('setting',Setting::with('info')->first());
        $view->with('articlescount', Article::latest()->count());
        $view->with('informationscount', Information::latest()->count());
        $view->with('advertisingscount', Advertising::latest()->count());
        $view->with('bussinesscount', UserServices::latest()->count());  
        $view->with('requestbussinesscount', UserServices::latest()->whereStatus(0)->count());   
        $view->with('requestcommentcount', Comment::latest()->whereStatus(0)->count());          
        $view->with('memberscount', Member::latest()->count());
        $view->with('eventscount', Event::latest()->count());
        $view->with('classroomscount', ClassRoom::latest()->count());
        $view->with('racescount', Race::latest()->count());
        $view->with('areas', Area::orderBy('name','desc')->get());
        $view->with('total_orders',Order::where('status',1)->sum('total_price'));
        $view->with('countries', Country::orderBy('name','desc')->get());
        $view->with('states', State::orderBy('name','desc')->get());
        $view->with('cities', City::orderBy('name','desc')->get());
        $view->with('carouselscount', Carousel::latest()->count());
        $view->with('seekerscount', Client::latest()
            ->whereHas('role',function ($query){
                $query->where('title', '=', 'seeker');
            })
            ->count());
        $view->with('employerscount', Client::latest()
            ->whereHas('role',function ($query){
                $query->where('title', '=', 'employer');
            })
            ->count());
        $view->with('clientscount', Client::latest()
            ->whereHas('role',function ($query){
                $query->where('title', '=', 'user');
            })
            ->count());
        $view->with('clients', Client::latest()
            ->whereHas('role',function ($query){
                $query->where('title', '=', 'user');
            })
            ->get());
        $view->with('portfolioscount', Portfolio::latest()->count());
        $view->with('productscount', Product::latest()->count());
        $view->with('informationscount', Information::latest()->count());
        $view->with('contact_count', Contact::latest()->count());
        $view->with('userscount', User::latest()->count());
        $view->with('lastArticles', Article::latest()->take(5)->get());
        $view->with('lastUsers', User::latest()->take(5)->get());
        if(auth('web')->check()){
            $view->with('admin', User::find(auth('web')->user()->id));
        }
        $view->with('lastMembers', Member::latest()->take(5)->get());
        $view->with('bussiness', UserServices::latest()->take(5)->get());   
        $view->with('lastClients', Client::latest()->take(5)->get());
        $view->with('lastProducts', Product::latest()->take(5)->get());
        $view->with('lastInformations', Information::latest()->take(5)->get());

    }

}
