<?php

namespace App\Http\Controllers;

use App\Events\Visit;
use Illuminate\Support\Facades\DB;
use App\Facades\Rest\Rest;
use App\Http\Requests\RegisterReceiverRequest;
use App\Http\Requests\SendChat;
use Artesaos\SEOTools\Facades\SEOMeta;
use Modules\Core\Entities\Rate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Modules\Advertising\Entities\Advertising;
use Modules\Advertising\Entities\AttributeJob;
use Modules\Advertising\Entities\ClientAttributeJob;
use Modules\Advertising\Entities\ClientMenuJob;
use Modules\Advertising\Entities\MenuJob;
use Modules\Article\Entities\Article;
use Modules\Chat\Entities\Chat;
use Modules\Client\Entities\Client;
use Modules\Client\Entities\ClientBot;
use Modules\Client\Http\Requests\AddAdvertEmployer;
use Modules\Advertising\Http\Requests\ClientMenuJobRequest;
use Modules\Client\Http\Requests\ChangePasswordRequest;
use Modules\Client\Http\Requests\ClientUpdateRequest;
use Modules\Client\Http\Requests\ForeignClientUpdateRequest;
use Modules\Client\Http\Requests\StoreBussiness;
use Modules\Client\Http\Requests\CvSendRequest;
use Modules\Client\Http\Requests\SeekerUpdateRequest;
use Modules\Client\Http\Requests\StoreGalleryBussiness;
use Modules\Client\Http\Requests\BannerBussinessRequest;
use Modules\Comment\Http\Requests\CommentRequest;
use Modules\Core\Entities\Category;
use Modules\Core\Entities\City;
use Modules\Core\Entities\Favorite;
use Modules\Core\Entities\Info;
use Modules\Core\Entities\AnalyticsClient;
use Modules\Core\Entities\Country;
use Modules\Core\Entities\Currency;
use Modules\Core\Entities\ListServices;
use Modules\Core\Entities\ServiceField;
use Modules\Core\Entities\UserServiceField;
use Modules\Core\Entities\Setting;
use Modules\Core\Entities\State;
use Modules\Core\Entities\Translate;
use Modules\Core\Entities\UserAction;
use Modules\Core\Entities\UserServices;
use Modules\Core\Http\Requests\UserServiceRequest;
use Modules\Core\Helper\CoreHelper;
use Modules\Core\Http\Requests\IdeaContactRequest;
use Modules\Educational\Entities\ClassRoom;
use Modules\Educational\Entities\Race;
use Modules\Event\Entities\Event;
use Modules\Gallery\Entities\Gallery;
use Modules\Information\Entities\Information;
use Modules\Member\Entities\Member;
use Modules\Member\Entities\MemberRole;
use Modules\Menu\Entities\ListMenu;
use Modules\Menu\Entities\Menu;
use Modules\Page\Entities\Page;
use Modules\Payment\Http\Controllers\Geteway\Melat;
use Modules\Payment\Http\Controllers\Geteway\ZarinPal;
use Modules\Plan\Entities\Plan;
use Modules\Portfolio\Entities\Portfolio;
use Modules\Product\Entities\Cart;
use Modules\Product\Entities\CartList;
use Modules\Product\Entities\Product;
use Modules\Question\Entities\Question;
use Modules\Request\Entities\listRequest;
use Modules\Request\Http\Requests\RequestRequest;
use Modules\Service\Entities\Advantage;
use Modules\Service\Entities\Property;
use Modules\Service\Entities\Service;
use Modules\Sms\Http\Controllers\Gateway\Idepardazan;
use Modules\Sms\Http\Controllers\Gateway\Kavenegar;
use Modules\Video\Entities\Video;
use Modules\Comment\Entities\Comment;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;
use Spatie\Tags\Tag;
use  Spatie\MediaLibrary\Models\Media;


class HomeController extends Controller
{
    public function resetPasswordProfile($object,$current,$pass){

        if(Hash::check($current, $object->password)){

            $this->resetPassword($object,$pass);

            return $errorStatus=true;
        }
        else{
            return $errorStatus=false;
        }
    }
    protected function resetPassword($user, $password)
    {
        $this->setUserPassword($user, $password);

        $user->save();
    }

    protected function setUserPassword($user, $password)
    {
        $user->password = Hash::make($password);
    }

    public function generateSiteMap(){
        try {
            app()->make(\Spatie\Permission\PermissionRegistrar::class)->forgetCachedPermissions();
            

            Artisan::call('cache:clear');
            Artisan::call('passport:install');

            $sitemap=Sitemap::create();

            $sitemap->add(Url::create('/')
                ->setLastModificationDate(Carbon::yesterday())
                ->setChangeFrequency(Url::CHANGE_FREQUENCY_YEARLY)
                ->setPriority(0.2));

            $sitemap->add(Url::create('/about-us')
                ->setLastModificationDate(Carbon::yesterday())
                ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                ->setPriority(0.2));
            $sitemap->add(Url::create('/contact-us')
                ->setLastModificationDate(Carbon::yesterday())
                ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                ->setPriority(0.2));

            if(CoreHelper::hasCourse()){
                $sitemap->add(Url::create('/courses')
                    ->setLastModificationDate(Carbon::yesterday())
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                    ->setPriority(0.2));
            }

            if(CoreHelper::hasEvent()){
                $sitemap->add(Url::create('/events')
                    ->setLastModificationDate(Carbon::yesterday())
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                    ->setPriority(0.2));
            }
            if(CoreHelper::hasService()){
                $sitemap->add(Url::create('/services')
                    ->setLastModificationDate(Carbon::yesterday())
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                    ->setPriority(0.2));
            }

            if(CoreHelper::hasPortfolio()){
                $sitemap->add(Url::create('/portfolios')
                    ->setLastModificationDate(Carbon::yesterday())
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                    ->setPriority(0.2));
            }
            if(CoreHelper::hasInformation()){
                $sitemap->add(Url::create('/informations')
                    ->setLastModificationDate(Carbon::yesterday())
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                    ->setPriority(0.2));
            }
            if(CoreHelper::hasAdvertising()){
                $sitemap->add(Url::create('/advertisings')
                    ->setLastModificationDate(Carbon::yesterday())
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                    ->setPriority(0.2));
            }
            if(CoreHelper::hasPortfolio()){
                $sitemap->add(Url::create('/advertisings')
                    ->setLastModificationDate(Carbon::yesterday())
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                    ->setPriority(0.2));
            }

            if(CoreHelper::hasProduct()){
                $sitemap->add(Url::create('/products')
                    ->setLastModificationDate(Carbon::yesterday())
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                    ->setPriority(0.2));
            }




            if(CoreHelper::hasRace()) {
                $sitemap->add(Url::create('/races')
                    ->setLastModificationDate(Carbon::yesterday())
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                    ->setPriority(0.2));

            }

            if(CoreHelper::hasArticle()) {
                foreach (Article::latest()->get() as $article) {
                    $sitemap->add(Url::create('/articles/' . $article->slug)
                        ->setLastModificationDate(Carbon::yesterday())
                        ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                        ->setPriority(0.2));
                }
            }
            if(CoreHelper::hasAdvertising()) {
                foreach (Advertising::latest()->get() as $advertising) {
                    $sitemap->add(Url::create('/advertisings/' . $advertising->slug)
                        ->setLastModificationDate(Carbon::yesterday())
                        ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                        ->setPriority(0.2));
                }
            }
            if(CoreHelper::hasInformation()) {
                foreach (Advertising::latest()->get() as $information) {
                    $sitemap->add(Url::create('/informations/' . $information->slug)
                        ->setLastModificationDate(Carbon::yesterday())
                        ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                        ->setPriority(0.2));
                }
            }

            if(CoreHelper::hasPortfolio()) {
                foreach (Portfolio::latest()->get() as $portfolio) {
                    $sitemap->add(Url::create('/portfolios/' . $portfolio->slug)
                        ->setLastModificationDate(Carbon::yesterday())
                        ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                        ->setPriority(0.2));
                }
            }

            if(CoreHelper::hasProduct()) {
                foreach (Product::latest()->get() as $product) {
                    $sitemap->add(Url::create('/products/' . $product->slug)
                        ->setLastModificationDate(Carbon::yesterday())
                        ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                        ->setPriority(0.2));
                }
            }

            if(CoreHelper::hasCourse()) {
                foreach (ClassRoom::latest()->get() as $classroom) {
                    $sitemap->add(Url::create('/courses/' . $classroom->slug)
                        ->setLastModificationDate(Carbon::yesterday())
                        ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                        ->setPriority(0.2));
                }
            }

            if(CoreHelper::hasRace()) {
                foreach (Race::latest()->get() as $race) {
                    $sitemap->add(Url::create('/races/' . $race->slug)
                        ->setLastModificationDate(Carbon::yesterday())
                        ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                        ->setPriority(0.2));
                }
            }

            $sitemap->writeToFile(public_path('sitemap.xml'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }

    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function website()
    {
       

        try {
            // return view('template.comingsoon');
                

                $setting=Setting::with('info','translates')->firstOrFail();
                SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
                SEOMeta::setTitle(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'title_seo',true,'title'));
                SEOMeta::setDescription(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'description_seo',true,'description'));
                SEOMeta::setCanonical(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'canonical_seo',true,'canonical'));
                return view('template.index');

        }catch (\Exception $exception){
         
            return abort('500');
        }
    }

    public function prefix()
    {

        try {
            // return view('template.comingsoon');
           
            $setting=Setting::with('info')->firstOrFail();
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'title_seo',true,'title'));
            SEOMeta::setDescription(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'description_seo',true,'description'));
            SEOMeta::setCanonical(env('APP_URL'));
            return view('template.prefix.index');
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function  verifyClientJobActive(Request $request,$token)
    {

        try {
            $item=UserServicess::whereToken($token)->firstOrFail();

            $item->update([
                'status'=>2
            ]);
          
            return view('template.verify.email');
        }catch (\Exception $exception){
          
            return abort('500');
        }
    }
   

    public function articles(){
        try {
            $items=AnalyticsClient::create([
                'ip'=> $_SERVER['REMOTE_ADDR'] ,
                'analytic'=> 1,
                'device'=>$_SERVER['HTTP_USER_AGENT'],
            ]);
            return dd($items);
            $setting=Setting::with('info')->firstOrFail();
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(__('cms.articles'));
            $items=Article::latest()->with('user_info')->get();
            $categories=Category::whereModel(Article::class)->whereStatus(1)->whereParent(0)->get();
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/articles');
            return view('template.articles.index',compact('items','categories'));

        }catch (\Exception $exception){
return dd($exception);
         
            return abort('500');
        }
    }

    public function loginWithToken($token){
        try {

            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->where('client_token',$token)->firstOrFail();
            auth('client')->loginUsingId($client->id);
            $client->update([
                'status_client_token'=>0,
                'start_client_token'=>Carbon::now(),
                'client_token'=>tokenResetGenerate()
            ]);
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.edit-profile'));
            SEOMeta::setDescription('ویرایش پنل کاربری');
            SEOMeta::setKeywords('ویرایش پنل کاربری');
            SEOMeta::setCanonical(env('APP_URL').'/user/panel/edit');
            return view('template.auth.edit',compact('client'));
        }catch (\Exception $exception){
           
            return abort('500');
        }
    }

    public function ads(){
        try {
            $setting=Setting::with('info')->firstOrFail();
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(__('cms.ads'));
            // $items=Article::latest()->with('user_info')->get();
            // $categories=Category::whereModel(Article::class)->whereStatus(1)->whereParent(0)->get();
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/ads');
            return view('template.ads.index');

        }catch (\Exception $exception){

            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function bussiness(){
        try {
            $setting=Setting::with('info')->firstOrFail();
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(__('cms.bussiness'));
            $items=UserServices::with('user_info','comments','time','attributes')->get();
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/bussiness');
            return view('template.bussiness.index',compact('items'));

        }catch (\Exception $exception){

          
            return abort('500');
        }
    }
    public function subCategoryBussiness(Request $request,$id){
        try {
            $setting=Setting::with('info')->firstOrFail();
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(__('cms.bussiness'));
            $category=ListServices::find($id);
            if($category->parent==0){
                $category_array=ListServices::whereParent($category->id)->pluck('id')->toArray();
                $items=UserServices::with('user_info','comments','time','attributes')->whereIn('service',$category_array)->get();
                $sub_categories=ListServices::whereIn('id',$category_array)->get();
            }else{
                $items=UserServices::with('user_info','comments','time','attributes')->where($category->id)->get();
                $sub_categories=[];
            }
       
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/bussiness');
            return view('template.pages.sub_category',compact('items','sub_categories'));

        }catch (\Exception $exception){

          
            return abort('500');
        }
    }
    public function members(){
        try {
            $setting=Setting::with('info')->firstOrFail();
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(__('cms.members'));
            $role_id=Member::latest()->pluck('role')->toArray();
            $items=Member::latest()->get();
            $roles=MemberRole::with('member')->whereIn('id',$role_id)->latest()->get();
            $categories=Category::whereModel(Member::class)->whereStatus(1)->whereParent(0)->get();
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/members');
            return view('template.members.index',compact('items','categories','roles'));

        }catch (\Exception $exception){

           
            return abort('500');
        }
    }
    public function services(){
        try {
            $setting=Setting::with('info')->firstOrFail();
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(__('cms.services'));
            $items=Service::latest()->with('user_info')->get();
            $categories=Category::whereModel(Service::class)->whereStatus(1)->whereParent(0)->get();
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/services');
            return view('template.services.index',compact('items','categories'));

        }catch (\Exception $exception){

          
            return abort('500');
        }
    }
    public function faqs(){
        try {
            $setting=Setting::with('info','questions')->firstOrFail();
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(__('cms.faqs'));
            $items=$setting->questions;
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/faqs');
            return view('template.faqs.index',compact('items'));

        }catch (\Exception $exception){
            
            return abort('500');
        }
    }
    public function forms(){
        try {
            $setting=Setting::with('info','questions')->firstOrFail();
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(__('cms.forms'));
            $items=$setting->questions;
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/forms');
            return view('template.forms.index',compact('items'));

        }catch (\Exception $exception){
            
            return abort('500');
        }
    }
    public function sendChat(SendChat $request){
        try {

            $chat=Chat::create([
                'client'=>auth('client')->user()->id,
                'text'=>$request->message,
                'created_at'=>now(),
            ]);
            if(!$chat){
                return redirect()->back()->with('error',__('client::clients.error'));

            }else{
                return redirect()->back()->with('message',__('client::clients.store'));
            }
        }catch (\Exception $exception){
            
            return abort('500');
        }
    }
    public function students(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.students'));
            $items=ClassRoom::orderBy('order','asc')->whereParent(0)->latest()->get();
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/students');
            return view('template.students.index',compact('items'));

        }catch (\Exception $exception){

           
            return abort('500');
        }
    }
    public function singleStudents($id){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('students');
            $class=ClassRoom::orderBy('order','asc')->findOrFail($id);
            $items=UserAction::with('selfClient')->where('actionable_type',ClassRoom::class)->where('actionable_id',$class->id)->where('status',1)->get();
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/students/single');
            return view('template.students.single',compact('items'));

        }catch (\Exception $exception){
           
            return abort('500');
        }
    }
    public function singleUserService($service){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('user service');
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/user/service');
            $item=UserServices::find($service);
            return view('template.user_services.single',compact('item'));

        }catch (\Exception $exception){
          
          
            return abort('500');
        }
    }
    public function galleries(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.galleries'));
            $categories=Category::orderBy('order','asc')->whereModel(Gallery::class)->whereStatus(1)->whereParent(0)->get();
            $items=Gallery::orderBy('order','asc')->whereParent(0)->get();
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/galleries');
            return view('template.galleries.index',compact('categories','items'));

        }catch (\Exception $exception){
            
            return abort('500');
        }
    }
    public function singleGallery($category){

        try {
            $item=Category::find($category);
            $items=Gallery::whereCategory($category)->get();
            SEOMeta::setTitleDefault(\setting('name'));
            event(new Visit($item));
            return view('template.galleries.single',compact('items'));
        }catch (\Exception $exception){
          
            return abort('500');
        }
    }
    public function videos(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.videos'));
            $categories=Category::orderBy('order','asc')->whereModel(Video::class)->whereStatus(1)->whereParent(0)->get();
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/videos');
            return view('template.videos.index',compact('categories'));

        }catch (\Exception $exception){
           
            return abort('500');
        }
    }
    public function singleVideo($category){

        try {
            $items=Video::whereCategory($category)->get();
            SEOMeta::setTitleDefault(\setting('name'));
            return view('template.videos.single',compact('items'));
        }catch (\Exception $exception){
          
            return abort('500');
        }
    }
    public function articlesTag($tag){
        try {
            $tag_id=Tag::whereJsonContains('name',['fa'=>$tag])->first()->id;
            $articles=Article::with('user_info')->get();
            $items=null;
            foreach ($articles as $article){

                foreach ($article->tags as $tag){
                    if($tag->id==$tag_id){
                        $items[]=$article;

                    }
                }

            }
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('مقالات برچسب شده');
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/articles/tag/'.$tag);
            return view('template.articles.index',compact('items'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function articlesCategory($category){
        try {
            $category=Category::whereSlug($category)->first()->id;
            $articles=Article::with('user_info')->get();
            $items=null;
            foreach ($articles as $article){
                if($article->category==$category){
                    $items[]=$article;
                }

            }
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('مقالات دسته بندی شده');
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/articles/category/'.$category);
            return view('template.articles.index',compact('items'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function courses(){
        try {
            SEOMeta::setTitle(__('cms.classrooms'));
            $items=ClassRoom::with('member','price','analyzer')->has('member')->orderBy('order','asc')->whereParent(0)->latest()->get();
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/courses');
            return view('template.courses.index',compact('items'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return  abort('500');
        }
    }
    public function races(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.races'));
            $items=Race::with('member','price','analyzer')->has('member')->orderBy('order','asc')->whereParent(0)->latest()->get();
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/races');
            return view('template.races.index',compact('items'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function singleArticle($obj){

        try {
            $setting=Setting::with('info','translates')->firstOrFail();

            if(Translate::where('lang',LaravelLocalization::getCurrentLocale())->where('slug',$obj)->first()){

                $item=Article::with('user_info','seo','translates')->findOrFail(Translate::where('lang',LaravelLocalization::getCurrentLocale())->where('slug',$obj)->first()->translateable_id);
            }
            else{
                $item=Article::with('user_info','seo','translates')->whereSlug($obj)->firstOrFail();
            }

            $categories=Category::whereModel(Article::class)->whereStatus(1)->whereParent(0)->get();
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(convert_lang($item,LaravelLocalization::getCurrentLocale(),'title_seo',true,'title'));
            SEOMeta::setDescription(convert_lang($item,LaravelLocalization::getCurrentLocale(),'description_seo',true,'description'));
            SEOMeta::setKeywords(convert_lang($item,LaravelLocalization::getCurrentLocale(),'keyword_seo',true,'description'));
            SEOMeta::setCanonical($item->seo->canonical);
            event(new Visit($item));
            return view('template.articles.single',compact('item','categories'));
        }catch (\Exception $exception){
          
            return abort('500');
        }
    }



    public function singleBussiness($obj){

        try {

           

            if(auth('client')->check()){
                $client=auth('client')->user();
                $service=UserServices::whereSlug($obj)->firstOrFail();
                if(ClientBot::where('botable_type','Modules\Core\Entities\UserServices')->where('botable_id',$service->id)->where('client',$client->id)->first() && $service->user!=$client->id){
                $bot=ClientBot::where('botable_type','Modules\Core\Entities\UserServices')->where('botable_id',$service->id)->where('client',$client->id)->first();
                    $bot->update([
                        'count'=>$bot->count + 1,
                ]);
                }else{

                    if($service->user!=$client->id){
                        $service->bot()->create([
                            'client'=>$client->id,
                    ]);
                    }
                   
                   
                }
              
              

            }
            $setting=Setting::with('info','translates')->firstOrFail();
            $menus=MenuJob::latest()->get();


             if(Translate::where('lang',LaravelLocalization::getCurrentLocale())->where('slug',$obj)->first()){

                 $item=UserServices::with('user_info','translates','comments','attributes','time')->findOrFail(Translate::where('lang',LaravelLocalization::getCurrentLocale())->where('slug',$obj)->first()->translateable_id);
                 
                 $rate=$item->rates->sum('rate')==0 ? 0 : $item->rates->sum('rate') / $item->rates->count();

                $medias= $item->getMedia(config('cms.collection-images'));

                $slides= $item->getMedia('slides');
             
                $comments=$item->comments;
                }
             else{
                 $item=UserServices::with('user_info','translates','comments','attributes','time')->whereSlug($obj)->firstOrFail();
                
                 $rate=$item->rates->sum('rate')==0 ? 0 : $item->rates->sum('rate') / $item->rates->count();


                $medias= $item->getMedia(config('cms.collection-images'));
             
                $slides= $item->getMedia('slides');

                $comments=$item->comments;
                }

            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            event(new Visit($item));
            return view('template.bussiness.single',compact('item','comments','medias','menus','slides','rate'));
        }catch (\Exception $exception){
          
          
            return abort('500');
        }
    }

    public function singleMember($obj){

        try {
            $setting=Setting::with('info','translates')->firstOrFail();

            if(Translate::where('lang',LaravelLocalization::getCurrentLocale())->where('slug',$obj)->first()){

                $item=Member::findOrFail(Translate::where('lang',LaravelLocalization::getCurrentLocale())->where('slug',$obj)->first()->translateable_id);
            }
            else{
                $item=Member::whereSlug($obj)->firstOrFail();
            }

            $categories=Category::whereModel(Article::class)->whereStatus(1)->whereParent(0)->get();
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(convert_lang($item,LaravelLocalization::getCurrentLocale(),'title_seo',true,'title'));
            SEOMeta::setDescription(convert_lang($item,LaravelLocalization::getCurrentLocale(),'description_seo',true,'description'));
            SEOMeta::setKeywords(convert_lang($item,LaravelLocalization::getCurrentLocale(),'keyword_seo',true,'description'));
            SEOMeta::setCanonical($item->seo->canonical);
            event(new Visit($item));
            return view('template.members.single',compact('item','categories'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function singleService(){

        try {
            $setting=Setting::with('info','translates')->firstOrFail();

            if(Translate::where('lang',LaravelLocalization::getCurrentLocale())->where('slug',$obj)->first()){

                $item=Service::with('user_info','seo','translates')->findOrFail(Translate::where('lang',LaravelLocalization::getCurrentLocale())->where('slug',$obj)->first()->translateable_id);
            }
            else{
                $item=Service::with('user_info','seo','translates')->whereSlug($obj)->firstOrFail();
            }

            $categories=Category::whereModel(Service::class)->whereStatus(1)->whereParent(0)->get();
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(convert_lang($item,LaravelLocalization::getCurrentLocale(),'title_seo',true,'title'));
            SEOMeta::setDescription(convert_lang($item,LaravelLocalization::getCurrentLocale(),'description_seo',true,'description'));
            SEOMeta::setKeywords(convert_lang($item,LaravelLocalization::getCurrentLocale(),'keyword_seo',true,'description'));
            SEOMeta::setCanonical($item->seo->canonical);
            event(new Visit($item));
            return view('template.services.single',compact('item','categories'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function singleEvent($obj){

        try {
            $setting=Setting::with('info','translates')->firstOrFail();
            $item=Event::whereSlug($obj)->firstOrFail();
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(convert_lang($item,LaravelLocalization::getCurrentLocale(),'title_seo',true,'title'));
            SEOMeta::setDescription(convert_lang($item,LaravelLocalization::getCurrentLocale(),'description_seo',true,'description'));
            SEOMeta::setKeywords(convert_lang($item,LaravelLocalization::getCurrentLocale(),'keyword_seo',true,'keyword'));
            SEOMeta::setCanonical($item->seo->canonical);
            event(new Visit($item));
            return view('template.events.single',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function singleCourse($obj){

        try {
            $setting=Setting::with('info','translates')->firstOrFail();
            $item=ClassRoom::with('member','price','analyzer','students')->has('member')->whereSlug($obj)->firstOrFail();
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(convert_lang($item,LaravelLocalization::getCurrentLocale(),'title_seo',true,'title'));
            SEOMeta::setDescription(convert_lang($item,LaravelLocalization::getCurrentLocale(),'description_seo',true,'description'));
            SEOMeta::setKeywords(convert_lang($item,LaravelLocalization::getCurrentLocale(),'keyword_seo',true,'keyword'));
            SEOMeta::setCanonical($item->seo->canonical);
            event(new Visit($item));
            return view('template.courses.single',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function singleRace($obj){

        try {
            $setting=Setting::with('info','translates')->firstOrFail();
            $item=Race::with('member','price','analyzer','students')->has('member')->whereSlug($obj)->firstOrFail();
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(convert_lang($item,LaravelLocalization::getCurrentLocale(),'title_seo',true,'title'));
            SEOMeta::setDescription(convert_lang($item,LaravelLocalization::getCurrentLocale(),'description_seo',true,'description'));
            SEOMeta::setKeywords(convert_lang($item,LaravelLocalization::getCurrentLocale(),'keyword_seo',true,'keyword'));
            SEOMeta::setCanonical($item->seo->canonical);
            event(new Visit($item));
            return view('template.races.single',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function informations(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.informations'));
            $items=Information::latest()->get();
            $categories=Category::whereModel(Information::class)->whereStatus(1)->whereParent(0)->get();
            SEOMeta::setCanonical(env('APP_URL').'/informations');
            return view('template.informations.index',compact('items','categories'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function companies(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.businesses'));
            $items=Client::with('company')->latest()
                ->whereHas('role',function ($query){
                    $query->where('title', '=', 'employer');
                })->get();
            SEOMeta::setCanonical(env('APP_URL').'/companies');
            return view('template.companies.index',compact('items'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function singleInformation($obj){

        try {
            $setting=Setting::with('info','translates')->firstOrFail();

            if(Translate::where('lang',LaravelLocalization::getCurrentLocale())->where('slug',$obj)->first()){

                $item=Information::with('user_info','seo','translates')->findOrFail(Translate::where('lang',LaravelLocalization::getCurrentLocale())->where('slug',$obj)->first()->translateable_id);
            }
            else{
                $item=Information::with('user_info','seo','translates')->whereSlug($obj)->firstOrFail();
            }

            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(convert_lang($item,LaravelLocalization::getCurrentLocale(),'title_seo',true,'title'));
            SEOMeta::setDescription(convert_lang($item,LaravelLocalization::getCurrentLocale(),'description_seo',true,'description'));
            SEOMeta::setKeywords(convert_lang($item,LaravelLocalization::getCurrentLocale(),'keyword_seo',true,'keyword'));
            SEOMeta::setCanonical($item->seo->canonical);
            event(new Visit($item));
            return view('template.informations.single',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function singleCompany($obj){

        try {
//                $active_adverts=!is_null(Advertising::latest()->with('info_company')->whereStatus(2)->whereClient($obj)->get()) ? Advertising::latest()->with('info_company')->whereStatus(2)->whereClient($obj)->get() : [];
//
            $item=Client::with('company','analyzer','seo')->whereId($obj)
                ->whereHas('role',function ($query){
                    $query->where('title', '=', 'employer');
                })->first();
//            SEOMeta::setTitleDefault(\setting('name'));
//            SEOMeta::setTitle($item->seo->title);
//            SEOMeta::setDescription($item->seo->description);
//            SEOMeta::setKeywords($item->seo->keyword);
//            SEOMeta::setCanonical($item->seo->canonical);
//            event(new Visit($item));
//            return view('template.companies.single',compact('item','active_adverts'));
            return view('template.companies.single',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }


    public function singleAdvertising($obj){

        try {
            $item=Advertising::with('info_company','client_info','analyzer')->whereStatus(2)->findOrFail($obj);
            $similar_advertisings=Advertising::where('category',$item->category)->where('id','!=',$obj)->get();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($item->seo->title);
            SEOMeta::setDescription($item->seo->description);
            SEOMeta::setKeywords($item->seo->keyword);
            SEOMeta::setCanonical($item->seo->canonical);
            event(new Visit($item));
            return view('template.advertisings.single',compact('item','similar_advertisings'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function addAdvert(AddAdvertEmployer $request){

        try {
            $client=Client::findOrFail(\auth('client')->user()->id);
            $category=Category::whereToken($request->input('category'))->firstOrFail();
            $currency=Currency::whereToken($request->input('currency'))->firstOrFail();
            $plan=getPlan();

            if($plan->status==2){
                return redirect()->back()->with('error','اعبار پلن شما به پایان رسیده است.');
            }
            $store=Advertising::create([
                'title'=>$request->input('title'),
                'excerpt'=>$request->input('excerpt'),
                'number_employees'=>$request->input('number_employees'),
                'category'=>$category->id,
                'salary'=>$request->input('salary'),
                'work_experience'=>$request->input('experience'),
                'job_position'=>json_encode($request->input('positions')),
                'education'=>json_encode($request->input('educations')),
                'guild'=>$request->input('guild'),
                'r'=>$request->input('kind'),
                'skill'=>json_encode($request->input('skills')),
                'kind'=>json_encode($request->input('kinds')),
                'expire'=>now()->addDays(Plan::find($plan->plan)->time_limit),
                'currency'=>$currency->id,
                'slug'=>null,
                'gender'=>$request->input('gender'),
                'plan'=>$plan->id,
                'text'=>$request->input('text'),
                'token'=>tokenGenerate(),
                'client'=>$client->id,
                'country'=>'Iran',
                'city'=>'Tehran',
                'state'=>'Tehran',
            ]);

            if($request->has('image')){
                destroyMedia($store,config('cms.collection-image'));
                $store->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            $store->seo()->create();

            $store->analyzer()->create();

            $store->syncTags([]);

            $plan->update([
                'count'=>$plan->count -1
            ]);

            if($plan->count ==0){
                    $plan->update([
                        'status'=>2
                    ]);
            }

            if(!$store){
                return redirect()->back()->with('error',__('advertising::advertisings.error'));
            }else{
                return redirect()->back()->with('message',__('advertising::advertisings.store'));
            }


        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function advertisings(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.advertisings'));
            $items=Advertising::latest()->with('client_info')->whereStatus(2)->latest()->paginate(config('cms.paginate'));
            SEOMeta::setCanonical(env('APP_URL').'/advertisings');
            return view('template.advertisings.index',compact('items'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function portfolios(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.portfolios'));
            $items=Portfolio::latest()->get();
            SEOMeta::setCanonical(env('APP_URL').'/portfolios');
            return view('template.portfolios.index',compact('items'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function singlePortfolio($obj){

        try {
            $setting=Setting::with('info','translates')->firstOrFail();
            $item=Portfolio::whereSlug($obj)->firstOrFail();
            $medias= $item->getMedia(config('cms.collection-images'));
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(convert_lang($item,LaravelLocalization::getCurrentLocale(),'title_seo',true,'title'));
            SEOMeta::setDescription(convert_lang($item,LaravelLocalization::getCurrentLocale(),'description_seo',true,'description'));
            SEOMeta::setKeywords(convert_lang($item,LaravelLocalization::getCurrentLocale(),'keyword_seo',true,'keyword'));
            SEOMeta::setCanonical($item->seo->canonical);
            event(new Visit($item));
            return view('template.portfolios.single',compact('item','medias'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function singleProperty($obj){

        try {
            $item=Property::whereSlug($obj)->firstOrFail();
            SEOMeta::setTitleDefault(\setting('name'));
            return view('template.properties.single',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function singleAdvantage($obj){

        try {
            $item=Advantage::whereSlug($obj)->firstOrFail();
            SEOMeta::setTitleDefault(\setting('name'));
            return view('template.advantages.single',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function products(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.products'));
            $items=Product::latest()->get();
            SEOMeta::setCanonical(env('APP_URL').'/products');
            return view('template.products.index',compact('items'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function shop(){
        try {
            $items=Product::with('user_info')->whereIn('status',[1,3])->paginate(config('cms.paginate'));
            $categories=Category::whereModel(Product::class)->whereStatus(1)->whereParent(0)->get();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.shop'));
            SEOMeta::setCanonical(env('APP_URL').'/shop');
            return view('template.shop.index',compact('items','categories'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function downloads(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.downloads'));
            SEOMeta::setCanonical(env('APP_URL').'/downloads');
            return view('template.downloads.index');

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function categoriesProduct(Request $request, $category){
        try {
            $item=Category::whereSlug($category)->first();

            $product_categories=Product::whereCategory($item->id)->get();

            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('categories');
            SEOMeta::setCanonical(env('APP_URL').'/categories');
            return view('template.categories.products.index',compact('product_categories'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function events(){
        try {
            $items=Event::latest()->get();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.events'));
            SEOMeta::setCanonical(env('APP_URL').'/events');
            return view('template.events.index',compact('items'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function singleProduct($obj){

        try {
            $item=Product::with('attributes','properties')->whereSlug($obj)->firstOrFail();
            $medias= $item->getMedia(config('cms.collection-images'));
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($item->seo->title);
            SEOMeta::setDescription($item->seo->description);
            SEOMeta::setKeywords($item->seo->keyword);
            SEOMeta::setCanonical($item->seo->canonical);
            event(new Visit($item));
            return view('template.products.single',compact('item','medias'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function searchProduct($obj){

        try {
            $category=Category::find($obj);
            $items=is_null(Product::with('price')->whereCategory($category->id)->get()) ? [] :Product::with('price')->whereCategory($category->id)->get();
            $count=is_null(Product::with('price')->whereCategory($category->id)) ? 0 : Product::with('price')->whereCategory($category->id)->count();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('جستجوی محصول');
            SEOMeta::setDescription('جستجوی محصول');
            SEOMeta::setKeywords('جستجوی آمحصول');
            return view('template.products.search',compact('items','count'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function searchBussiness(Request $request,$filter=1,$category=null,$postal_code=null,$city=null,$title=null,$was=null,$wo=null){

        try {
           
            
            $category=0;
            $sub_category=0;
            $queries=UserServices::query();
            $queries=$queries->with('user_info','comments','time','attributes')->whereStatus(2);
           
          
            if($request->has('was')){
                
                if(ListServices::whereTitle($request->was)->first()){

                    $was_item=ListServices::whereTitle($request->was)->first();

                   
                    $was_items=ListServices::whereParent($was_item->id)->pluck('id')->toArray();
                       
                    $queries=$queries->where('service',$was_item->id)->orWhereIn('service',$was_items);
                
                  

                }else{
                    $queries=$queries->where('title', 'like',$request->was . '%');
                   
                }

                
                    
               
               
            }
            if(isset($request->wo) && !is_null($request->wo)){
              
                
                $queries=$queries->where('city', 'like',$request->wo . '%');
                 
                    
               
            }
            
            if(isset($request->category) && !is_null($request->category)){

                if($request->category!=0){
                    if(isset($request->sub_category)){
                        $queries=$queries->where('service',$request->sub_category);
                        $category=$request->category;
                        $sub_category=$request->category;
                    }
                    else{
                        $queries=$queries->whereService($request->category);
                        $category=$request->category;
                    }
                   

                }
               
            }
            if(isset($request->city) && !is_null($request->city)){

                $queries=$queries->whereCity($request->city);
            }
            if(isset($request->title) && !is_null($request->title)){

                $queries=$queries->whereTitle(trim($request->title));
            }
            if(isset($request->postal_code) && !is_null($request->postal_code)){

                $queries=$queries->where('postal_code',$request->postal_code);
            }

            $queries=$queries->get();

            
          
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.search-bussiness'));
            SEOMeta::setDescription(__('cms.search-bussiness'));
            SEOMeta::setKeywords(__('cms.search-bussiness'));
            return view('template.bussiness.index',compact('queries','category','filter'));
        }catch (\Exception $exception){
          
         
            return abort('500');
        }
    }


    public function searchBussinessCategory(Request $request,$filter=1,$category){

        try {
            $queries=UserServices::query();
            $queries=$queries->with('user_info','comments','time','attributes')->whereStatus(2);
    
                if(ListServices::find($category)){

                    $item=ListServices::find($category)->first();

                   
                    $items=ListServices::whereParent($item->id)->pluck('id')->toArray();
                       
                    $queries=$queries->where('service',$item->id)->orWhereIn('service',$items);
            
                }

          
   
            $queries=$queries->get();

            
          
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.search-category-bussiness'));
            SEOMeta::setDescription(__('cms.search-category-bussiness'));
            SEOMeta::setKeywords(__('cms.search-category-bussiness'));
            return view('template.bussiness.index',compact('queries','category','filter'));
        }catch (\Exception $exception){
          
         
            return abort('500');
        }
    }

    
    public function searchShowCategoryBussiness(Request $request,$category){

        try {
           
            $queries=ListServices::query();
         
           if($category==0){
            $queries=$queries->where('parent', 0);
           }
           else{
            $queries=$queries->where('parent', $category);  
           }
           
            $queries=$queries->get();

            
          
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.search-show-category-bussiness'));
            SEOMeta::setDescription(__('cms.search-show-category-bussiness'));
            SEOMeta::setKeywords(__('cms.search-show-category-bussiness'));
            return view('template.bussiness.categories',compact('queries','category'));
        }catch (\Exception $exception){
          
          
            return abort('500');
        }
    }
    public function showShopping(){

        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(' تکمیل سفارش ');
            SEOMeta::setDescription('تکمیل سفارش');
            SEOMeta::setKeywords('تکمیل سفارش');
            return view('template.auth.shopping');
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function showPayment(){

        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(' پرداخت سفارش ');
            SEOMeta::setDescription('پرداخت سفارش');
            SEOMeta::setKeywords('پرداخت سفارش');
            return view('template.auth.payment');
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function showEditBussiness($token){

        try {
            $item=UserServices::whereToken($token)->firstOrFail();
            $item_attributes=$item->attributes->pluck('attribute')->toArray();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.edit-bussiness'));
            SEOMeta::setDescription(__('cms.edit-bussiness'));
            SEOMeta::setKeywords(__('cms.edit-bussiness'));
            return view('template.auth.bussiness.edit',compact('item','item_attributes'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function showComment($token){

        try {
            

             $data = UserServices::whereToken($token)->with(['comments' => function ($query) {
                $query->where('status', '=', 1);
            }])->get();
         
            $items=$data[0]->comments;
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.show-comments'));
            SEOMeta::setDescription(__('cms.show-comments'));
            SEOMeta::setKeywords(__('cms.show-comments'));
            return view('template.auth.comments',compact('items'));
        }catch (\Exception $exception){
           
            return abort('500');
        }
    }
    public function showGallery($token){

        try {
            

             $item = UserServices::whereToken($token)->firstOrFail();
            

             $medias= $item->getMedia(config('cms.collection-images'));

             $photos= $item->getMedia('gallery');

         
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.show-galleries'));
            SEOMeta::setDescription(__('cms.show-galleries'));
            SEOMeta::setKeywords(__('cms.show-galleries'));
            return view('template.auth.galleries',compact('item','medias','photos'));
        }catch (\Exception $exception){
          
            return abort('500');
        }
    }

    public function storeGalleryBussiness(StoreGalleryBussiness $request ,$token){

        try {
       
            $item=UserServices::whereToken($token)->first();

            if($request->has('gallery')){
                $item->addMedia($request->file('gallery'))->toMediaCollection('slides');
                
                
                return redirect()->back()->with('message',__('gallery::galleries.store'));
            }
            else{
                return redirect()->back()->with('error',__('gallery::galleries.error'));
            }
           
          
        }catch (\Exception $exception){
          
            return redirect()->back()->with('error',__('gallery::galleries.error'));

        }
    }
    public function storeGalleryJobBussiness(StoreGalleryBussiness $request ,$token){

        try {
       
            $item=UserServices::whereToken($token)->first();

            if($request->has('gallery')){
                $item->addMedia($request->file('gallery'))->toMediaCollection('gallery');
                
                
                return redirect()->back()->with('message',__('gallery::galleries.store'));
            }
            else{
                return redirect()->back()->with('error',__('gallery::galleries.error'));
            }
           
          
        }catch (\Exception $exception){
          
            return redirect()->back()->with('error',__('gallery::galleries.error'));

        }
    }
    public function storeGallerySlideBussiness(StoreGalleryBussiness $request ,$token){

        try {
       
            $item=UserServices::whereToken($token)->first();

            if($request->has('gallery')){
                $item->addMedia($request->file('gallery'))->toMediaCollection('slides');
                
                
                return redirect()->back()->with('message',__('gallery::galleries.store'));
            }
            else{
                return redirect()->back()->with('error',__('gallery::galleries.error'));
            }
           
          
        }catch (\Exception $exception){
          
            return redirect()->back()->with('error',__('gallery::galleries.error'));

        }
    }

    public function storeBannerBussiness(BannerBussinessRequest $request ,$token){

        try {
       
            $item=UserServices::whereToken($token)->firstOrFail();


            if($request->has('banner')){
                destroyMedia($item,'banner');
                $item->addMedia($request->file('banner'))->toMediaCollection('banner');
                
                
                return redirect()->back()->with('message',__('gallery::galleries.store'));
            }
            else{
                return redirect()->back()->with('error',__('gallery::galleries.error'));
            }
           
          
        }catch (\Exception $exception){
          
            return redirect()->back()->with('error',__('gallery::galleries.error'));

        }
    }



    public function removeBannerBussiness(Request $request ,$token){

        try {
       
            $item=UserServices::whereToken($token)->firstOrFail();


           
                destroyMedia($item,'banner');
                
                
                return redirect()->back()->with('message',__('gallery::galleries.store'));
            
          
        }catch (\Exception $exception){
          
            return redirect()->back()->with('error',__('gallery::galleries.error'));

        }
    }

    public function removeGalleryBussiness(Request $request){

        try {
       

            if($request->has('translate')){
                Media::findOrFail($request->translate)->delete();
                    return redirect()->back()->with('message',__('gallery::galleries.store'));
                
            }else{
                return redirect()->back()->with('error',__('gallery::galleries.error'));

            }
          
        }catch (\Exception $exception){
          
            return redirect()->back()->with('error',__('gallery::galleries.error'));

        }
    }
    public function removeGalleryJobBussiness(Request $request){

        try {
       

            if($request->has('translate')){
                Media::findOrFail($request->translate)->delete();
                    return redirect()->back()->with('message',__('gallery::galleries.store'));
                
            }else{
                return redirect()->back()->with('error',__('gallery::galleries.error'));

            }
          
        }catch (\Exception $exception){
          
            return redirect()->back()->with('error',__('gallery::galleries.error'));

        }
    }
    public function removeGallerySlideBussiness(Request $request){

        try {
       

            if($request->has('translate')){
                Media::findOrFail($request->translate)->delete();
                    return redirect()->back()->with('message',__('gallery::galleries.store'));
                
            }else{
                return redirect()->back()->with('error',__('gallery::galleries.error'));

            }
          
        }catch (\Exception $exception){
          
            return redirect()->back()->with('error',__('gallery::galleries.error'));

        }
    }
    public function changeStatusBannerBussiness(Request $request,$token){

        try {
            $item=UserServices::whereToken($token)->firstOrFail();

            if($request->banner_status==1){
                $item->update([
                    'banner_status'=>1
                ]);
            }elseif($request->banner_status==2){
            $item->update([
                'banner_status'=>2
                ]);
            }
       
            return redirect()->back()->with('message',__('gallery::galleries.store'));
           
          
        }catch (\Exception $exception){
          
            return redirect()->back()->with('error',__('gallery::galleries.error'));

        }
    }





    public function showTime($token){

        try {
            

             $item = UserServices::with('time')->whereToken($token)->firstOrFail();

             SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.show-times'));
            SEOMeta::setDescription(__('cms.show-times'));
            SEOMeta::setKeywords(__('cms.show-times'));
            return view('template.auth.times',compact('item'));
        }catch (\Exception $exception){
           
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function changeClientAttributeJob(Request $request,$token){

        try {
         
          
            $item=UserServices::whereToken($token)->firstOrFail();
            $attributes=ClientAttributeJob::whereClient(auth('client')->user()->id)->whereService($item->id)->get();

            foreach($attributes as $attribute){

                $attribute->delete();
            }
        
            foreach($request->request as $key=>$value){

                if($key!='_token'){

                    $item->attributes()->create([
                        'client'=>auth('client')->user()->id,
                        'attribute'=>intval($key),
                        'token'=>tokenGenerate()
                    ]);
                }

        }
        return redirect()->route('client.dashboard')->with('message',__('advertising::attribute_job.store'));

        }catch (\Exception $exception){

           
           
            return redirect()->back()->with('error',__('advertising::attribute_job.error'));
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }


    public function changeClientSocialMediasJob(Request $request,$token){

        try {
         
           
          
            $item=UserServices::whereToken($token)->firstOrFail();

            $item->info()->update([
                'facebook'=>$request->facebook,
                'instagram'=>$request->instagram,
                'whatsapp'=>$request->whatsapp,
                'website'=>$request->website,
                'twitter'=>$request->twitter,  
            ]);
        
         
        return redirect()->back()->with('message',__('advertising::attribute_job.store'));

        }catch (\Exception $exception){

           
           
            return redirect()->back()->with('error',__('advertising::attribute_job.error'));
           
            return abort('500');
        }
    }

    public function addClientMenuJob(ClientMenuJobRequest $request,$token,$menu){

        try {
         
         
            
            $item=UserServices::with('menus')->whereToken($token)->firstOrFail();
        
        
                    $data=$item->menus()->create([
                        'client'=>auth('client')->user()->id,
                        'menu'=>$menu,
                        'service'=>$item->service,
                        'title'=>$request->title,
                        'excerpt'=>$request->excerpt,
                        'price'=>$request->price,
                        'token'=>tokenGenerate()
                    ]);

                  
           
            if($request->has("image-$menu")){
               
                $data->addMedia($request->file("image-$menu"))->toMediaCollection('images');
     
            }

         
       
        return redirect()->route('client.dashboard')->with('message',__('advertising::attribute_job.store'));

        }catch (\Exception $exception){

          
           
            return redirect()->back()->with('error',__('advertising::attribute_job.error'));
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function removeClientMenuJob(Request $request,$token){

        try {
         
         
            $item=ClientMenuJob::whereToken($token)->firstOrFail();

            if($item->Hasmedia('images')){
                destroyMedia($item,config('cms.collection-image'));
            }
            $item->delete();

               
       
        return redirect()->route('client.dashboard')->with('message',__('advertising::attribute_job.store'));

        }catch (\Exception $exception){

          
           
            return redirect()->back()->with('error',__('advertising::attribute_job.error'));
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function editClientMenuJob(Request $request,$token){

        try {
         
         
            $item=ClientMenuJob::whereToken($token)->firstOrFail();
            return view('template.auth.edit-menu',compact('item'));
       

        }catch (\Exception $exception){
                    
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function updateClientMenuJob(ClientMenuJobRequest $request,$token){

        try {
         
         
            $item=ClientMenuJob::whereToken($token)->firstOrFail();

            $item->update([

                'title'=>$request->title,
                'excerpt'=>$request->excerpt,
                'price'=>$request->price,

            ]);

            if($request->has('image')){
               
                if($item->hasMedia('images')){
                    destroyMedia($item,config('images'));
                    $item->addMedia($request->file('image'))->toMediaCollection('images');
                }else{
                    $item->addMedia($request->file('image'))->toMediaCollection('images');

                }
               
     
            }
          
       
        return redirect()->route('client.dashboard')->with('message',__('advertising::attribute_job.store'));

        }catch (\Exception $exception){
                     
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function changeClientTimeJob(Request $request,$token){

        try {
         
         
            $item=UserServices::with('time')->whereToken($token)->firstOrFail();


            if(is_null($item->time)){
                $item->time()->create([
                    'client'=>auth('client')->user()->id,
                    'monday'=>$request->monday,
                    'tuesday'=>$request->tuesday,
                    'wednesday'=>$request->wednesday,
                    'thursday'=>$request->thursday,
                    'friday'=>$request->friday,
                    'saturday'=>$request->saturday,
                    'sunday'=>$request->sunday,
                    'token'=>tokenGenerate()
                ]);
            }
            else{
                $item->time()->update([
                    'monday'=>$request->monday,
                    'tuesday'=>$request->tuesday,
                    'wednesday'=>$request->wednesday,
                    'thursday'=>$request->thursday,
                    'friday'=>$request->friday,
                    'saturday'=>$request->saturday,
                    'sunday'=>$request->sunday,

                ]);
            }
        


        return redirect()->route('client.dashboard')->with('message',__('advertising::attribute_job.store'));

        }catch (\Exception $exception){

           
           
            return redirect()->back()->with('error',__('advertising::attribute_job.error'));
           
            return abort('500');
        }
    }

   
    public function showMenu($token){

        try {
            

             $item = UserServices::with('menus')->whereToken($token)->firstOrFail();
        
             $menus=MenuJob::latest()->get();
        
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.show-menus'));
            SEOMeta::setDescription(__('cms.show-menus'));
            SEOMeta::setKeywords(__('cms.show-menus'));
            return view('template.auth.menus',compact('item','menus'));
        }catch (\Exception $exception){
          
            return abort('500');
        }
    }
    public function showAttribute($token){

        try {
            

             $item = UserServices::with('attributes')->whereToken($token)->firstOrFail();
             $item_attributes=$item->attributes->pluck('attribute')->toArray();
             $attributes=AttributeJob::latest()->get();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.show-attributes'));
            SEOMeta::setDescription(__('cms.show-attributes'));
            SEOMeta::setKeywords(__('cms.show-attributes'));
            return view('template.auth.attributes',compact('item','attributes','item_attributes'));
        }catch (\Exception $exception){
          
            return abort('500');
        }
    }
    public function showSocialMedias($token){

        try {
            

             $item = UserServices::with('info')->whereToken($token)->firstOrFail();

            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.show-attributes'));
            SEOMeta::setDescription(__('cms.show-attributes'));
            SEOMeta::setKeywords(__('cms.show-attributes'));
            return view('template.auth.social_medias',compact('item'));
        }catch (\Exception $exception){
        
            return abort('500');
        }
    }
    public function showHeaderMedias($token){

        try {
            

            $item = UserServices::with('info')->whereToken($token)->firstOrFail();
            $slides= $item->getMedia('slides');
            $medias= $item->getMedia(config('cms.collection-images'));
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.show-header-mdias'));
            SEOMeta::setDescription(__('cms.show-header-medias'));
            SEOMeta::setKeywords(__('cms.show-header-medias'));
            return view('template.auth.header_medias',compact('item','medias','slides'));
        }catch (\Exception $exception){
          
            return abort('500');
        }
    }
    public function showNotification(){

        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.show-notifications'));
            SEOMeta::setDescription(__('cms.show-notifications'));
            SEOMeta::setKeywords(__('cms.show-notifications'));
            return view('template.auth.notifications');
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }


    public function showOrders(){

        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->user()->id);
            $items=null;
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(' لیست سفارشات ');
            SEOMeta::setDescription('لیست سفارشات');
            SEOMeta::setKeywords('لیست سفارشات');
            return view('template.auth.orders',compact('items','client'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function showCart(){

        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->user()->id);
            $items=null;
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.client-cart'));
            SEOMeta::setDescription(__('cms.client-cart'));
            SEOMeta::setKeywords(__('cms.client-cart'));
            return view('template.auth.cart',compact('items','client'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function showOrdersReturn(){

        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->user()->id);
            $items=null;
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(' لیست سفارشات مرجوعی ');
            SEOMeta::setDescription('لیست سفارشات مرجوعی');
            SEOMeta::setKeywords('لیست سفارشات مرجوعی');
            return view('template.auth.orders_return',compact('items','client'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function register(){

        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.register'));
            SEOMeta::setDescription('register');
            SEOMeta::setKeywords('register');
            SEOMeta::setCanonical(env('APP_URL').'/user/register');
            return view('template.auth.register.index',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function sendSupplier(Request $request){

        try {

            $data=[
                'kind'=>(__('cms.kind')!='cms.kind') ? $request->kind : __('cms.'.$request->kind),
                'company_name'=>(__('cms.company-name')!='cms.company-name') ? $request->company_name : __('cms.'.$request->company_name),
                'company_type'=>(__('cms.company-type')!='cms.company-type') ? $request->company_type : __('cms.'.$request->company_type),
                'field_of_activity'=>(__('cms.field-of-activity')!='cms.field-of-activity') ? $request->field_of_activity : __('cms.'.$request->field_of_activity),
                'type_of_ownership'=>(__('cms.type-of-ownership')!='cms.type-of-ownership') ? $request->type_of_ownership : __('cms.'.$request->type_of_ownership),
                'product_name'=>(__('cms.product-name')!='cms.product-name') ? $request->product_name : __('cms.'.$request->product_name),
                'registration_number'=>(__('cms.registration-number')!='cms.registration-number') ? $request->registration_number : __('cms.'.$request->registration_number),
                'operating_license_number'=>(__('cms.operating-license-number')!='cms.operating-license-number') ? $request->operating_license_number : __('cms.'.$request->operating_license_number),
                'economic_code'=>(__('cms.economic-code')!='cms.economic-code') ? $request->economic_code : __('cms.'.$request->economic_code),
                'national_id'=>(__('cms.national-id')!='cms.national-id') ? $request->national_id : __('cms.'.$request->national_id),
                'business_license_number'=>(__('cms.business-license-number')!='cms.business-license-number') ? $request->business_license_number : __('cms.'.$request->business_license_number),
                'date_of_establishment'=>(__('cms.date-of-establishment')!='cms.date-of-establishment') ? $request->date_of_establishment : __('cms.'.$request->date_of_establishment),
                'licensee_name'=>(__('cms.licensee-name')!='cms.licensee-name') ? $request->licensee_name : __('cms.'.$request->licensee_name),
                'national_code_of_the_licensee'=>(__('cms.national-code-of-the-licensee')!='cms.national-code-of-the-licensee') ? $request->national_code_of_the_licensee : __('cms.'.$request->national_code_of_the_licensee),
                'address_of_central_office'=>(__('cms.address-of-central-office')!='cms.address-of-central-office') ? $request->address_of_central_office : __('cms.'.$request->address_of_central_office),
                'head_office_fax'=>(__('cms.head-office-fax')!='cms.head-office-fax') ? $request->head_office_fax : __('cms.'.$request->head_office_fax),
                'factory_address'=>(__('cms.factory-address')!='cms.factory-address') ? $request->factory_address : __('cms.'.$request->factory_address),
                'factory_phone'=>(__('cms.factory-phone')!='cms.factory-phone') ? $request->factory_phone : __('cms.'.$request->factory_phone),
                'factory_fax'=>(__('cms.factory-fax')!='cms.factory-fax') ? $request->factory_fax : __('cms.'.$request->factory_fax),
                'company_email'=>(__('cms.company-email')!='cms.company-email') ? $request->company_email : __('cms.'.$request->company_email),
                'company_website'=>(__('cms.company-website')!='cms.company-website') ? $request->company_website : __('cms.'.$request->company_website),
                'company_capital'=>(__('cms.company-capital')!='cms.company-capital') ? $request->company_capital : __('cms.'.$request->company_capital),
                'names_of_board_members'=>(__('cms.names-of-board-members')!='cms.names-of-board-members') ? $request->names_of_board_members : __('cms.'.$request->names_of_board_members),
                'are_you_a_representative_if_yes_what_kind_of_representation'=>(__('cms.are-you-a-representative-if-yes-what-kind-of-representation')!='cms.are-you-a-representative-if-yes-what-kind-of-representation') ? $request->are_you_a_representative_if_yes_what_kind_of_representation : __('cms.'.$request->are_you_a_representative_if_yes_what_kind_of_representation),
                'is_there_a_variety_of_brands_in_the_goods_you_supply'=>(__('cms.is-there-a-variety-of-brands-in-the-goods-you-supply')!='cms.is-there-a-variety-of-brands-in-the-goods-you-supply') ? $request->is_there_a_variety_of_brands_in_the_goods_you_supply : __('cms.'.$request->is_there_a_variety_of_brands_in_the_goods_you_supply),
                'do_you_use_the_services_of_insurance_companies_for_employees_products_devices_and_buildings_explain'=>(__('cms.do-you-use-the-services-of-insurance-companies-for-employees-products-devices-and-buildings-explain')!='cms.do-you-use-the-services-of-insurance-companies-for-employees-products-devices-and-buildings-explain') ? $request->do_you_use_the_services_of_insurance_companies_for_employees_products_devices_and_buildings_explain : __('cms.'.$request->do_you_use_the_services_of_insurance_companies_for_employees_products_devices_and_buildings_explain),
                'do_you_have_a_stock_for_the_products_or_products_you_supply'=>(__('cms.do-you-have-a-stock-for-the-products-or-products-you-supply')!='cms.do-you-have-a-stock-for-the-products-or-products-you-supply') ? $request->do_you_have_a_stock_for_the_products_or_products_you_supply : __('cms.'.$request->do_you_have_a_stock_for_the_products_or_products_you_supply),
                'warehouse_capacity'=>(__('cms.warehouse-capacity')!='cms.warehouse-capacity') ? $request->warehouse_capacity : __('cms.'.$request->warehouse_capacity),
                'number_of_warehouses'=>(__('cms.number-of-warehouses')!='cms.number-of-warehouses ') ? $request->number_of_warehouses : __('cms.'.$request->number_of_warehouses),
                'annual_production_capacity'=>(__('cms.annual-production-capacity')!='cms.annual-production-capacity') ? $request->annual_production_capacity : __('cms.'.$request->annual_production_capacity),
                'production_volume_of_the_last_three_years'=>(__('cms.production-volume-of-the-last-three-years')!='cms.production-volume-of-the-last-three-years') ? $request->production_volume_of_the_last_three_years : __('cms.'.$request->production_volume_of_the_last_three_years),
                'the_average_annual_turnover_of_the_current_year'=>(__('cms.the-average-annual-turnover-of-the-current-year')!='cms.the-average-annual-turnover-of-the-current-year') ? $request->the_average_annual_turnover_of_the_current_year : __('cms.'.$request->the_average_annual_turnover_of_the_current_year),
                'money_in_circulation_Annual_average_of_the_last_5_years'=>(__('cms.money-in-circulation-Annual-average-of-the-last-5-years')!='cms.money-in-circulation-Annual-average-of-the-last-5-years') ? $request->money_in_circulation_Annual_average_of_the_last_5_years : __('cms.'.$request->money_in_circulation_Annual_average_of_the_last_5_years),
                'if_you_are_a_manufacturer_select_the_status_of_the_factory_or_workshop'=>(__('cms.if-you-are-a-manufacturer-select-the-status-of-the-factory-or-workshop')!='cms.if-you-are-a-manufacturer-select-the-status-of-the-factory-or-workshop') ? $request->if_you_are_a_manufacturer_select_the_status_of_the_factory_or_workshop : __('cms.'.$request->if_you_are_a_manufacturer_select_the_status_of_the_factory_or_workshop),
                'does_the_product_offered_by_you_have_a_warranty_guarantee_and_after_sales_service'=>(__('cms.does-the-product-offered-by-you-have-a-warranty-guarantee-and-after-sales-service')!='cms.does-the-product-offered-by-you-have-a-warranty-guarantee-and-after-sales-service') ? $request->does_the_product_offered_by_you_have_a_warranty_guarantee_and_after_sales_service : __('cms.'.$request->does_the_product_offered_by_you_have_a_warranty_guarantee_and_after_sales_service),
                'do_you_offer_technical_advice_in_your_field_of_work'=>(__('cms.do-you-offer-technical-advice-in-your-field-of-work')!='cms.do-you-offer-technical-advice-in-your-field-of-work') ? $request->do_you_offer_technical_advice_in_your_field_of_work : __('cms.'.$request->do_you_offer_technical_advice_in_your_field_of_work),
                'is_your_technical_advice_free_if_you_do_not_buy'=>(__('cms.is-your-technical-advice-free-if-you-do-not-buy')!='cms.is-your-technical-advice-free-if-you-do-not-buy') ? $request->is_your_technical_advice_free_if_you_do_not_buy : __('cms.'.$request->is_your_technical_advice_free_if_you_do_not_buy),
                'is_your_technical_advice_free_to_buy'=>(__('cms.is-your-technical-advice-free-to-buy')!='cms.is-your-technical-advice-free-to-buy') ? $request->is_your_technical_advice_free_to_buy : __('cms.'.$request->is_your_technical_advice_free_to_buy),
                'number_of_successful_contracts_in_the_past_year_for_the_goods_under_evaluation_with_the_cement_steel_oil_and_petrochemical_industries_mines'=>(__('cms.number-of-successful-contracts-in-the-past-year-for-the-goods-under-evaluation-with-the-cement-steel-oil-and-petrochemical-industries-mines')!='cms.number-of-successful-contracts-in-the-past-year-for-the-goods-under-evaluation-with-the-cement-steel-oil-and-petrochemical-industries-mines') ? $request->number_of_successful_contracts_in_the_past_year_for_the_goods_under_evaluation_with_the_cement_steel_oil_and_petrochemical_industries_mines : __('cms.'.$request->number_of_successful_contracts_in_the_past_year_for_the_goods_under_evaluation_with_the_cement_steel_oil_and_petrochemical_industries_mines),
                'number_of_successful_contracts_in_the_past_year_with_companies_under_espandar_for_the_product_under_evaluation'=>(__('cms.number-of-successful-contracts-in-the-past-year-with-companies-under-espandar-for-the-product-under-evaluation')!='cms.number-of-successful-contracts-in-the-past-year-with-companies-under-espandar-for-the-product-under-evaluation') ? $request->number_of_successful_contracts_in_the_past_year_with_companies_under_espandar_for_the_product_under_evaluation : __('cms.'.$request->number_of_successful_contracts_in_the_past_year_with_companies_under_espandar_for_the_product_under_evaluation),
                'does_the_product_you_produce_or_supply_have_iran_code'=>(__('cms.does-the-product-you-produce-or-supply-have-iran-code')!='cms.does-the-product-you-produce-or-supply-have-iran-code') ? $request->does_the_product_you_produce_or_supply_have_iran_code : __('cms.'.$request->does_the_product_you_produce_or_supply_have_iran_code),
                'do_you_use_the_services_of_other_companies_or_factories'=>(__('cms.do-you-use-the-services-of-other-companies-or-factories')!='cms.do-you-use-the-services-of-other-companies-or-factories') ? $request->do_you_use_the_services_of_other_companies_or_factories : __('cms.'.$request->do_you_use_the_services_of_other_companies_or_factories),
                'explain_if_you_use_the_services_of_other_companies'=>(__('cms.explain-if-you-use-the-services-of-other-companies')!='cms.explain-if-you-use-the-services-of-other-companies') ? $request->explain_if_you_use_the_services_of_other_companies : __('cms.'.$request->explain_if_you_use_the_services_of_other_companies),
                'does_your_product_have_standard_name_it'=>(__('cms.does-your-product-have-standard-name-it')!='cms.does-your-product-have-standard-name-it') ? $request->does_your_product_have_standard_name_it : __('cms.'.$request->does_your_product_have_standard_name_it),
                'is_your_production_method_below_certain_standard_name_it'=>(__('cms.is-your-production-method-below-certain-standard-name-it')!='cms.is-your-production-method-below-certain-standard-name-it') ? $request->is_your_production_method_below_certain_standard_name_it : __('cms.'.$request->is_your_production_method_below_certain_standard_name_it),
                'have_you_obtained_certificates_of_quality_management_environment_etc_explain'=>(__('cms.have-you-obtained-certificates-of-quality-management-environment-etc-explain')!='cms.have-you-obtained-certificates-of-quality-management-environment-etc-explain') ? $request->have_you_obtained_certificates_of_quality_management_environment_etc_explain : __('cms.'.$request->have_you_obtained_certificates_of_quality_management_environment_etc_explain),
                'do_you_have_unit_called_research_and_development'=>(__('cms.do-you-have-unit-called-research-and-development')!='cms.do-you-have-unit-called-research-and-development') ? $request->do_you_have_unit_called_research_and_development : __('cms.'.$request->do_you_have_unit_called_research_and_development),
                'is_the_quality_control_work_of_the_product_or_production_service_done_in_your_collection'=>(__('cms.is-the-quality-control-work-of-the-product-or-production-service-done-in-your-collection')!='cms.is-the-quality-control-work-of-the-product-or-production-service-done-in-your-collection') ? $request->is_the_quality_control_work_of_the_product_or_production_service_done_in_your_collection : __('cms.'.$request->is_the_quality_control_work_of_the_product_or_production_service_done_in_your_collection),
                'number_of_staff_below_the_diploma_set_but_with_specialization'=>(__('cms.number-of-staff-below-the-diploma-set-but-with-specialization')!='cms.number-of-staff-below-the-diploma-set-but-with-specialization') ? $request->number_of_staff_below_the_diploma_set_but_with_specialization : __('cms.'.$request->number_of_staff_below_the_diploma_set_but_with_specialization),
                'is_there_codified_training_system_for_the_unit_staff'=>(__('cms.is-there-codified-training-system-for-the-unit-staff')!='cms.is-there-codified-training-system-for-the-unit-staff') ? $request->is_there_codified_training_system_for_the_unit_staff : __('cms.'.$request->is_there_codified_training_system_for_the_unit_staff),
                'number_of_third_party_inspector_certification_during_the_past_year'=>(__('cms.number-of-third-party-inspector-certification-during-the-past-yea')!='cms.number-of-third-party-inspector-certification-during-the-past-year') ? $request->number_of_third_party_inspector_certification_during_the_past_year : __('cms.'.$request->number_of_third_party_inspector_certification_during_the_past_year),
                'do_you_have_history_of_exporting_product_or_service_during_the_last_5_years'=>(__('cms.do-you-have-history-of-exporting-product-or-service-during-the-last-5-years')!='cms.do-you-have-history-of-exporting-product-or-service-during-the-last-5-years') ? $request->do_you_have_history_of_exporting_product_or_service_during_the_last_5_years : __('cms.'.$request->do_you_have_history_of_exporting_product_or_service_during_the_last_5_years),
                'export_amount_during_the_last_5_years'=>(__('cms.export-amount-during-the-last-5-years')!='cms.export-amount-during-the-last-5-years') ? $request->export_amount_during_the_last_5_years : __('cms.'.$request->export_amount_during_the_last_5_years),
                'do_you_have_the_facilities_to_transport_the_goods_to_the_place_of_the_buyer_company'=>(__('cms.do-you-have-the-facilities-to-transport-the-goods-to-the-place-of-the-buyer-company')!='cms.do-you-have-the-facilities-to-transport-the-goods-to-the-place-of-the-buyer-company') ? $request->do_you_have_the_facilities_to_transport_the_goods_to_the_place_of_the_buyer_company : __('cms.'.$request->do_you_have_the_facilities_to_transport_the_goods_to_the_place_of_the_buyer_company),
                'do_you_have_distribution_network_if_yes_please_name_the_distribution_locations'=>(__('cms.do-you-have-distribution-network-if-yes-please-name-the-distribution-locations')!='cms.do-you-have-distribution-network-if-yes-please-name-the-distribution-locations') ? $request->do_you_have_distribution_network_if_yes_please_name_the_distribution_locations : __('cms.'.$request->do_you_have_distribution_network_if_yes_please_name_the_distribution_locations),
                'do_you_have_equipment_for_packing_goods'=>(__('cms.do-you-have-equipment-for-packing-goods')!='cms.do-you-have-equipment-for-packing-goods') ? $request->do_you_have_equipment_for_packing_goods : __('cms.'.$request->do_you_have_equipment_for_packing_goods),
                'are_you_member_of_research_institute_research_associations_trade_unions_unions_etc_name_it'=>(__('cms.are-you-member-of-research-institute-research-associations-trade-unions-unions-etc-name-it')!='cms.are-you-member-of-research-institute-research-associations-trade-unions-unions-etc-name-it') ? $request->are_you_member_of_research_institute_research_associations_trade_unions_unions_etc_name_it : __('cms.'.$request->are_you_member_of_research_institute_research_associations_trade_unions_unions_etc_name_it),
                'how_many_days_after_the_purchased_or_repaired_goods_stay_in_your_warehouse_do_you_receive_storage_costs_and_how_much'=>(__('cms.how-many-days-after-the-purchased-or-repaired-goods-stay-in-your-warehouse-do-you-receive-storage-costs-and-how-much')!='cms.how-many-days-after-the-purchased-or-repaired-goods-stay-in-your-warehouse-do-you-receive-storage-costs-and-how-much') ? $request->how_many_days_after_the_purchased_or_repaired_goods_stay_in_your_warehouse_do_you_receive_storage_costs_and_how_much : __('cms.'.$request->how_many_days_after_the_purchased_or_repaired_goods_stay_in_your_warehouse_do_you_receive_storage_costs_and_how_much),
            ];
            $this->sendForm($data);
            return redirect()->back()->with('message',__('request::requests.store'));


        }catch (\Exception $exception){
          
            return abort('500');
        }
    }
    public function login(){

        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.login'));
            SEOMeta::setDescription('login');
            SEOMeta::setKeywords('login');
            SEOMeta::setCanonical(env('APP_URL').'/user/login');
            return view('template.auth.login',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public  function analyzer(){

        try {
            $setting=Setting::with('info','translates')->firstOrFail();
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(__('cms.analyzer'));
            SEOMeta::setDescription(__('cms.analyzer'));
            SEOMeta::setKeywords(__('cms.analyzer'));
            SEOMeta::setCanonical(env('APP_URL').'/analyzer');
            return view('template.pages.analyzer',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public  function contactUs(){

        try {
            $setting=Setting::with('info','translates')->firstOrFail();
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(__('cms.contact-us'));
            SEOMeta::setDescription(__('cms.contact-us'));
            SEOMeta::setKeywords(__('cms.contact-us'));
            SEOMeta::setCanonical(env('APP_URL').'/contact-us');
            return view('template.pages.contact_us',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public  function aboutUs(){
        try {
            $setting=Setting::with('info','translates')->firstOrFail();
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(__('cms.about-us'));
            SEOMeta::setDescription(__('cms.about-us'));
            SEOMeta::setKeywords(__('cms.about-us'));
            SEOMeta::setCanonical(env('APP_URL').'/about-us');
            return view('template.pages.about_us',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public  function imprint(){

        try {
            $setting=Setting::with('info','translates')->firstOrFail();
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(__('cms.imprint'));
            SEOMeta::setDescription(__('cms.imprint'));
            SEOMeta::setKeywords(__('cms.imprint'));
            SEOMeta::setCanonical(env('APP_URL').'/imprint');
            return view('template.pages.imprint',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public  function help(){

        try {
            $setting=Setting::with('info','translates')->firstOrFail();
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(__('cms.help'));
            SEOMeta::setDescription(__('cms.help'));
            SEOMeta::setKeywords(__('cms.help'));
            SEOMeta::setCanonical(env('APP_URL').'/help');
            return view('template.pages.help',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public  function privacy(){

        try {
            $setting=Setting::with('info','translates')->firstOrFail();
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(__('cms.privacy'));
            SEOMeta::setDescription(__('cms.privacy'));
            SEOMeta::setKeywords(__('cms.privacy'));
            SEOMeta::setCanonical(env('APP_URL').'/privacy');
            return view('template.pages.privacy',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public  function createService(){
        try {
            $setting=Setting::with('info','translates')->firstOrFail();
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(__('cms.create-service'));
            SEOMeta::setDescription(__('cms.create-service'));
            SEOMeta::setKeywords(__('cms.create-service'));
            SEOMeta::setCanonical(env('APP_URL').'/create/service');
            return view('template.auth.create-service',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public  function createBussiness(){
        try {
            $setting=Setting::with('info','translates')->firstOrFail();
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(__('cms.create-bussiness'));
            SEOMeta::setDescription(__('cms.create-bussiness'));
            SEOMeta::setKeywords(__('cms.create-bussiness'));
            SEOMeta::setCanonical(env('APP_URL').'/create/bussiness');
            return view('template.auth.bussiness.create',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public  function clientBussiness(){
        try {
            $setting=Setting::with('info','translates')->firstOrFail();
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(__('cms.client-bussiness'));
            SEOMeta::setDescription(__('cms.client-bussiness'));
            SEOMeta::setKeywords(__('cms.client-bussiness'));
            SEOMeta::setCanonical(env('APP_URL').'/client/bussiness');
            return view('template.auth.bussiness.index',compact('item'));
        }catch (\Exception $exception){
          
            return abort('500');
        }
    }
    public  function clientFavoritesJob(){
        try {
            $setting=Setting::with('info','translates')->firstOrFail();
            $favorites=Favorite::whereClient(auth('client')->user()->id)->pluck('favoriteable_id')->toArray();
           $items=UserServices::whereIn('id',$favorites)->get();
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(__('cms.client-favorites'));
            SEOMeta::setDescription(__('cms.client-favorites'));
            SEOMeta::setKeywords(__('cms.client-favorites'));
            SEOMeta::setCanonical(env('APP_URL').'/client/favorites');
            return view('template.auth.favorites',compact('items'));
        }catch (\Exception $exception){
          
           
            return abort('500');
        }
    }

    public function clientDestroyFavoritesJob(Request $request,$token){
        try {
            
            $job=UserServices::whereToken($token)->firstOrFail();
            $item=Favorite::whereClient(auth('client')->user()->id)->where('favoriteable_id',$job->id)->where('favoriteable_type','Modules\Core\Entities\UserServices')->firstOrFail();
          
            $item->delete();

            return redirect()->route('client.dashboard')->with('message',__('user::users.store'));
            
            return view('template.auth.favorites',compact('items'));
        }catch (\Exception $exception){
          
            return redirect()->back()->with('error',__('user::users.error'));
           
        }
    }

    
    public  function clientMessages(){
        try {
            $chats=Chat::where('client',auth('client')->user()->id)->where('user','!=',null)->get();
           
            foreach($chats as $chat){
                $chat->update([
                    'status'=>2
                ]);
            }

            $setting=Setting::with('info','translates')->firstOrFail();
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(__('cms.client-messages'));
            SEOMeta::setDescription(__('cms.client-messages'));
            SEOMeta::setKeywords(__('cms.client-messages'));
            SEOMeta::setCanonical(env('APP_URL').'/client/messages');
            return view('template.auth.message');
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    

    


    public  function storeBussiness(StoreBussiness $request){
        try {

            DB::beginTransaction();
            

            $data=UserServices::create([
                'user'=>auth('client')->user()->id,
                'service'=>$request->has('sub_category') ?$request->sub_category : $request->category,
                'title'=>$request->input('title'),
                'text'=>$request->input('text'),
                'address_two'=>$request->input('address_two'),
                'excerpt'=>$request->input('excerpt'),
                'longitude'=>'',
                'latitude'=>'',
                'country'=>'german',
                'google_map'=>'',
                'status'=>0,
                'city'=>$request->city,
                'postal_code'=>$request->postal_code,
                'address'=>$request->input('address'),
                'email'=>$request->input('email'),
                'mobile'=>$request->input('mobile'),
                'phone'=>$request->input('phone'),
                'phone_2'=>$request->input('phone_2'),
                'token'=>tokenGenerate()
            ]);

            $data->info()->create([
                'facebook'=>$request->facebook,
                'instagram'=>$request->instagram,
                'whatsapp'=>$request->whatsapp,
                'website'=>$request->website,
                'twitter'=>$request->twitter,  
            ]);
        
            if($request->has('image')){
                $data->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if($request->has('banner')){
               
                $data->addMedia($request->file('banner'))->toMediaCollection('banner');
                                
            }

            if($request->has('slides')){
               
                foreach($request->slides as $slide){
                    $data->addMedia($slide)->toMediaCollection('slides');

                }
                                
            }

            if($request->has('photos')){
               
                foreach($request->photos as $photo){
                    $data->addMedia($photo)->toMediaCollection('gallery');

                }
                                
            }

            
           
            $data->analyzer()->create();

            if($request->has('attributes_item')){

                foreach($request->attributes_item as $value){

                        $data->attributes()->create([
                            'client'=>auth('client')->user()->id,
                            'attribute'=>intval($value),
                            'token'=>tokenGenerate()
                        ]);
    
                 }
            }

            $data->time()->create([
                'client'=>auth('client')->user()->id,
                'monday'=>isset($request->monday) ? $request->monday : null,
                'tuesday'=>isset($request->tuesday) ?  $request->tuesday : null,
                'wednesday'=>isset($request->wednesday) ?  $request->wednesday : null,
                'thursday'=>isset($request->thursday) ? $request->thursday : null,
                'friday'=>isset($request->friday) ? $request->friday : null,
                'saturday'=>isset($request->saturday) ? $request->saturday : null,
                'sunday'=>isset($request->sunday) ? $request->sunday : null,
                'token'=>tokenGenerate()
            ]);

            if($request->status_banner=='banner'){
                $data->update([
                    'banner_status'=>1
                ]);
            }elseif($request->status_banner=='slide'){
            $data->update([
                'banner_status'=>2
                ]);
            }
            elseif($request->status_banner=='video'){
                $data->update([
                    'banner_status'=>3
                    ]);
                }
           


            if($request->title_fa){
                $data->translates()->create([
                    'title'=>$request->title_fa,

                    'lang'=>'fa'
                ]);
            }
            if($request->title_en){
                $data->translates()->create([
                    'title'=>$request->title_en,

                    'lang'=>'en'
                ]);
            }

           
           DB::commit();

          
            $items_email=[
                'token'=>$data->token,
                'email'=>$data->token,
                'title'=>$data->title
            ];

                Mail::send('emails.front.verify-email', $items_email, function($message) use ($items_email)
                    {
                        $message->from('support@antenchi.ir')
                        ->to($items_email['email'])
                        ->subject($items_email['title']);        
                        
                    });

           return redirect()->route('client.dashboard')->with('message',__('advertising::jobs.store'));


        }catch (\Exception $exception){
       
            return dd($exception);
            return redirect()->back()->with('error',__('advertising::jobs.error'));
       
        }
    }


    public  function storeFavoriteJob(Request $request,$token){
        try {
          
           
            DB::beginTransaction();

           
            $job=UserServices::whereToken($token)->firstOrFail();

            if(Favorite::where('favoriteable_type','Modules\Core\Entities\UserServices')->where('favoriteable_id',$job->id)->where('client',auth('client')->user()->id)->first()){
               
                return redirect()->back()->with('error',__('comment::comments.error'));
            }


            $job->favorites()->create([
                'client'=>auth('client')->user()->id,
            ]);
         
           
           DB::commit();

           return redirect()->back()->with('message',__('comment::comments.store'));

        }catch (\Exception $exception){
           
           
            return redirect()->back()->with('error',__('comment::comments.error'));
       
        }
    }
    public  function storeRateJob(Request $request,$token,$rate){
        try {
          
           
            DB::beginTransaction();

           
            $job=UserServices::whereToken($token)->firstOrFail();

            if(Rate::where('rateable_type','Modules\Core\Entities\UserServices')->where('rateable_id',$job->id)->where('client',auth('client')->user()->id)->first()){
               
                return redirect()->back()->with('error',__('comment::comments.error'));
            }


            $job->favorites()->create([
                'client'=>auth('client')->user()->id,
                'rate'=>$rate,
            ]);
         
           
           DB::commit();

           return redirect()->back()->with('message',__('comment::comments.store'));

        }catch (\Exception $exception){
           
           
            return redirect()->back()->with('error',__('comment::comments.error'));
       
        }
    }

    public  function sendCommentBussiness(CommentRequest $request,$token){
        try {
          
            
            DB::beginTransaction();

            $data=UserServices::whereToken($token)->firstOrFail();


            if(auth('client')->check()){

             $update=$data->comments()->create([
                    'client'=>auth('client')->user()->id,
                    'email'=>auth('client')->user()->email,
                    'title'=>$request->title,
                    'rate'=>is_null($request->rate) ?  0 : $request->rate,
                    'text'=>$request->text,
                    'secret'=>($request->secret=="1") ? 1 : 0,
                    'token'=>tokenGenerate()
                ]);

                if(Rate::where('rateable_type','Modules\Core\Entities\UserServices')->where('rateable_id',$data->id)->where('client',auth('client')->user()->id)->first()){
           
                    $rate_item=Rate::where('rateable_type','Modules\Core\Entities\UserServices')->where('rateable_id',$data->id)->where('client',auth('client')->user()->id)->first();
                        
                    $rate_comment=Comment::where('commentable_type','Modules\Core\Entities\UserServices')->where('commentable_id',$data->id)->where('client',auth('client')->user()->id)->latest()->first();
                   
                    $rate_item->update([
                        'rate'=>$rate_comment->rate
                    ]);

                }
                else{
                    $data->rates()->create([
                        'client'=>auth('client')->user()->id,
                        'rate'=>$request->rate,
                    ]);
            
                }
                
                
               
            }else{
                $data->comments()->create([
                    'title'=>$request->title,
                    'email'=>$request->email,
                    'rate'=>null,
                    'text'=>$request->text,
                    'secret'=>1,
                    'token'=>tokenGenerate()
                ]);
                
                
               
            }
            
           DB::commit();

           return redirect()->back()->with('message',__('comment::comments.store'));

        }catch (\Exception $exception){
       
            return redirect()->back()->with('error',__('comment::comments.error'));
       
        }
    }



    public  function updateBussiness(StoreBussiness $request,$token){
        try {
          
            DB::beginTransaction();

            $data=UserServices::whereToken($token)->firstOrFail();
            $attributes=ClientAttributeJob::whereClient(auth('client')->user()->id)->whereService($data->id)->get();

            $data->update([
                'service'=>$request->has('sub_category') ?$request->sub_category : $request->category,
                'slug'=>null,
                'title'=>$request->input('title'),
                'excerpt'=>$request->input('excerpt'),
                'text'=>$request->input('text'),
                'address_two'=>$request->input('address_two'),
                'country'=>'german',
                'city'=>$request->city,
                'postal_code'=>$request->postal_code,
                'longitude'=>$request->input('long'),
                'latitude'=>$request->input('lat'),
                'address'=>$request->input('address'),
                'email'=>$request->input('email'),
                'phone'=>$request->input('phone'),
                'phone_2'=>$request->input('phone_2'),
                'mobile'=>$request->input('mobile')
            ]);

           
            $data->replicate();

            if($request->has('image')){
                destroyMedia($data,config('cms.collection-image'));
                $data->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

           
           DB::commit();

           return redirect()->route('client.dashboard')->with('message',__('advertising::jobs.store'));


        }catch (\Exception $exception){
        
          
            return redirect()->back()->with('error',__('advertising::jobs.error'));
       
        }
    }


    public  function storeService(UserServiceRequest $request){
        try {
          
            DB::beginTransaction();

            $data=UserServices::create([
                'user'=>auth('client')->user()->id,
                'service'=>ListServices::whereTitle('restaurant')->first()->id,
                'title'=>$request->input('title'),
                'excerpt'=>$request->input('excerpt'),
                'country'=>'german',
                'city'=>$request->input('city'),
                'google_map'=>$request->input('google_map'),
                'address'=>$request->input('address'),
                'email'=>$request->input('email'),
                'phone'=>$request->input('phone')
            ]);
        
            if($request->has('image')){
                $data->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

           
            $food=UserServiceField::create([
                'user'=>auth('client')->user()->id,
                'user_service'=>$data->id,
                'field'=>ServiceField::whereTitle('food')->first()->id,
                'title'=>$request->input('title-food'),
                'excerpt'=>$request->input('excerpt-food'),
                'price'=>$request->input('price-food'),
            ]);
        
            if($request->has('image-food')){
                $food->addMedia($request->file('image-food'))->toMediaCollection(config('cms.collection-image'));
            }

         
            $drink=UserServiceField::create([
                'user'=>auth('client')->user()->id,
                'user_service'=>$data->id,
                'field'=>ServiceField::whereTitle('drink')->first()->id,
                'title'=>$request->input('title-drink'),
                'excerpt'=>$request->input('excerpt-drink'),
                'price'=>$request->input('price-drink'),
            ]);
        
            if($request->has('image-drink')){
                $drink->addMedia($request->file('image-drink'))->toMediaCollection(config('cms.collection-image'));
            }
           
           DB::commit();

            return redirect(route("front.website"));


        }catch (\Exception $exception){
        
            
            return abort('500');
        }
    }



    public  function editService($token,$service){
        try {
        
            $setting=Setting::with('info','translates')->firstOrFail();
            SEOMeta::setTitleDefault(convert_lang($setting,LaravelLocalization::getCurrentLocale(),'name'));
            SEOMeta::setTitle(__('cms.edit_service'));
            SEOMeta::setDescription(__('cms.edit_service'));
            SEOMeta::setKeywords(__('cms.edit_service'));
            SEOMeta::setCanonical(env('APP_URL').'/edit/service');
            $user=Client::whereToken($token)->first();
            $data=UserServices::whereUser($user->id)->whereId($service)->first();
            $food=UserServiceField::where('user_service',$data->id)->where('field',1)->first();
            $drink=UserServiceField::where('user_service',$data->id)->where('field',2)->first();
            return view('template.auth.edit-service',compact('data','food','drink'));
        }catch (\Exception $exception){
           
            return abort('500');
        }
    }

    public  function updateService(UserServiceRequest $request,$service){
        try {
          
            // DB::beginTransaction();
            $data=UserServices::find($service);
        
            $data->update([
                'user'=>auth('client')->user()->id,
                'service'=>1,
                'title'=>$request->input('title'),
                'excerpt'=>$request->input('excerpt'),
                'country'=>'german',
                'city'=>$request->input('city'),
                'google_map'=>$request->input('google_map'),
                'address'=>$request->input('address'),
                'email'=>$request->input('email'),
                'phone'=>$request->input('phone')
            ]);
        
            if($request->has('image')){
                destroyMedia($data,config('cms.collection-image'));
                $data->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }


            $drink=UserServiceField::where('user_service',$data->id)->where('field',2)->first();
            $drink->update([
                'user'=>auth('client')->user()->id,
                'user_service'=>$data->id,
                'field'=>ServiceField::whereTitle('drink')->first()->id,
                'title'=>$request->input('title-drink'),
                'excerpt'=>$request->input('excerpt-drink'),
                'price'=>$request->input('price-drink'),
            ]);
        
            if($request->has('image-drink')){
                destroyMedia($drink,config('cms.collection-image'));
                $drink->addMedia($request->file('image-drink'))->toMediaCollection(config('cms.collection-image'));
            }


            $food=UserServiceField::where('user_service',$data->id)->where('field',1)->first();
            $food->update([
                'user'=>auth('client')->user()->id,
                'user_service'=>$data->id,
                'field'=>ServiceField::whereTitle('food')->first()->id,
                'title'=>$request->input('title-food'),
                'excerpt'=>$request->input('excerpt-food'),
                'price'=>$request->input('price-food'),
            ]);
        
            if($request->has('image-food')){
                destroyMedia($food,config('cms.collection-image'));
                $food->addMedia($request->file('image-food'))->toMediaCollection(config('cms.collection-image'));
            }
        

            return redirect(route("front.website"));

            
            // if(!$saved){
            //     DB::rollBack();
            //     return redirect()->back()->with('error','Invalid data');
            // }else{
            //     DB::commit();
            //     return redirect(route("front.website"));
            // }

        }catch (\Exception $exception){
        
           
            return abort('500');
        }
    }

    public  function categoryAdvertising(){
        try {
            $items=Category::with('advertisings')->orderBy('order','desc')->where('model',Advertising::class)->whereParent(0)->get();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('دسته بندی آگهی ها');
            SEOMeta::setDescription('دسته بندی آگهی ها');
            SEOMeta::setKeywords('دسته بندی آگهی ها');
            SEOMeta::setCanonical(env('APP_URL').'/category-advertising');
            return view('template.pages.category_advertising',compact('items'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public  function page($page){
        try {
            $page = Page::whereSlug($page)->firstOrFail();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($page->seo->title);
            SEOMeta::setDescription($page->seo->description);
            SEOMeta::setKeywords($page->seo->keyword);
            SEOMeta::setCanonical($page->seo->canonical);
            return view('template.pages.empty_page', compact('page'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public  function logout(Request $request){

        if(auth('client')->check()){
            $data=[
                'auth'=>\auth('client')->user()
            ];
        //    sendCustomEmail($data,'emails.front.logout');
            auth('client')->logout();
        }
        return redirect(route('front.website'));
    }

    public function sendRequestMale(RequestRequest $request){

        try {



            $saved=\Modules\Request\Entities\Request::create([
                'email'=>$request->input('email'),
                'name'=>$request->input('name'),
                'phone'=>$request->input('phone'),
                'country'=>'german',
                'gender'=>'male',
                'loss'=>$request->input('ihr'),
                'color'=>$request->input('blond'),
                'fall_time'=>$request->input('Jahr'),
                'transplantation'=>$request->input('eine'),
                'feeling'=>$request->input('schlimm'),
                'execution_time'=>$request->input('behandlung'),
                'list_requests'=>listRequest::whereName('hair-transplantation')->firstOrFail()->id,
                'token'=>tokenGenerate(),
            ]);
            if($request->has('Vorne')){
                $saved->addMedia($request->file('Vorne'))->toMediaCollection('Vorne');

            }
            if($request->has('Hinten')){
                $saved->addMedia($request->file('Hinten'))->toMediaCollection('Hinten');

            }
            if($request->has('Oben')){
                $saved->addMedia($request->file('Oben'))->toMediaCollection('Oben');

            }


            if(!$saved){
                return redirect(route('request.hair'))->with('error',__('request::requests.error'));
            }else{
                $data=[
                    'gender'=>'Männlich',
                    'name'=>$request->input('name'),
                    'email'=>$request->input('email'),
                    'phone'=>$request->input('phone'),
                    'ihr'=>$request->input('ihr'),
                    'blond'=>$request->input('blond'),
                    'Jahr'=>$request->input('Jahr'),
                    'eine'=>$request->input('eine'),
                    'schlimm'=>$request->input('schlimm'),
                    'behandlung'=>$request->input('behandlung'),
                ];
                $files=[
                    'Vorne'=>$saved->getFirstMediaUrl('Vorne'),
                    'Hinten'=>$saved->getFirstMediaUrl('Hinten'),
                    'Oben'=>$saved->getFirstMediaUrl('Oben')

                ];
                $this->sendEmail($data,$files);
                return redirect(route("request.hair"))->with('message',__('request::requests.store'));

            }
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function sendRequestFemale(RequestRequest $request){

        try {


            $saved=\Modules\Request\Entities\Request::create([
                'email'=>$request->input('email'),
                'name'=>$request->input('name'),
                'phone'=>$request->input('phone'),
                'country'=>'german',
                'gender'=>'female',
                'loss'=>$request->input('ihr'),
                'color'=>$request->input('blond'),
                'fall_time'=>$request->input('Jahr'),
                'transplantation'=>$request->input('eine'),
                'feeling'=>$request->input('schlimm'),
                'execution_time'=>$request->input('behandlung'),
                'list_requests'=>listRequest::whereName('hair-transplantation')->firstOrFail()->id,
                'token'=>tokenGenerate(),
            ]);

            if($request->has('Vorne')){
                $saved->addMedia($request->file('Vorne'))->toMediaCollection('Vorne');
            }
            if($request->has('Hinten')){
                $saved->addMedia($request->file('Hinten'))->toMediaCollection('Hinten');
            }
            if($request->has('Oben')){
                $saved->addMedia($request->file('Oben'))->toMediaCollection('Oben');
            }

            if(!$saved){
                return redirect(route('request.hair'))->with('error',__('request::requests.error'));
            }else{


                $data=[
                    'gender'=>'Weiblich',
                    'name'=>$request->input('name'),
                    'email'=>$request->input('email'),
                    'phone'=>$request->input('phone'),
                    'ihr'=>$request->input('ihr'),
                    'blond'=>$request->input('blond'),
                    'Jahr'=>$request->input('Jahr'),
                    'eine'=>$request->input('eine'),
                    'schlimm'=>$request->input('schlimm'),
                    'behandlung'=>$request->input('behandlung'),
                ];
                $files=[
                    'Vorne'=>$saved->getFirstMediaUrl('Vorne'),
                    'Hinten'=>$saved->getFirstMediaUrl('Hinten'),
                    'Oben'=>$saved->getFirstMediaUrl('Oben')

                ];
                $this->sendEmail($data,$files);
                return redirect(route("request.hair"))->with('message',__('request::requests.store'));
            }
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function maleHair(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('male hair');
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/hair/male');
            return view('template.forms.male_hair');

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function resultHair(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('result hair');
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/hair/result');
            return view('template.forms.result');

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function panel(){

        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet','cart')->find(\auth('client')->user()->id);
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.user-panel'));
            SEOMeta::setDescription(__('cms.user-panel'));
            SEOMeta::setKeywords(__('cms.user-panel'));
            SEOMeta::setCanonical(env('APP_URL').'/user/panel');
            return view('template.auth.panel',compact('client'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');

        }
    }
    public function welcome(){

        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet','cart')->find(\auth('client')->user()->id);
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.welcome'));
            SEOMeta::setDescription(__('cms.welcome'));
            SEOMeta::setKeywords(__('cms.welcome'));
            SEOMeta::setCanonical(env('APP_URL').'/user/panel/welcome');
            return view('template.auth.welcome',compact('client'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');

        }
    }

    public function showChangePassword(){

        try {

            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->user()->id);
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.change-password'));
            SEOMeta::setDescription(__('cms.change-password'));
            SEOMeta::setKeywords(__('cms.change-password'));
            SEOMeta::setCanonical(env('APP_URL').'/user/panel/change/password/secret');
            return view('template.auth.change-password',compact('client'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');

        }
    }
    public function changePassword(ChangePasswordRequest $request){

        try {


            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->user()->id);

            if(!empty(trim($request->input('current_password'))) && !empty(trim($request->input('new_password')))  ){

                if(!$this->resetPasswordProfile($client,$request->input('current_password'),$request->input('new_password'))){
                    return redirect()->back()->with('error',__('user::users.error'));
                }

            }
            return redirect()->route('client.dashboard')->with('message',__('user::users.update'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');

        }
    }

    public function favorite(){
        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->user()->id);
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.favorites-list'));
            SEOMeta::setDescription(__('cms.favorites-list'));
            SEOMeta::setKeywords(__('cms.favorites-list'));
            SEOMeta::setCanonical(env('APP_URL').'/user/panel/favorite');
            return view('template.auth.favorite',compact('client'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function cvs(){
        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->user()->id);
            $advertisings=Advertising::whereClient(auth('client')->id())->orderBy('created_at','desc')->whereStatus(2)->pluck('id')->toArray();
            $actions=UserAction::with('selfClient')->whereIn('actionable_id',$advertisings)->where('actionable_type',Advertising::class)->get();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.cvs-list'));
            SEOMeta::setDescription('لیست رزومه ها');
            SEOMeta::setKeywords('لیست رزومه ها');
            SEOMeta::setCanonical(env('APP_URL').'/user/panel/cvs');
            return view('template.auth.cvs',compact('client','cvs','actions'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function plans(){
        try {
            $items=Plan::orderBy('order','desc')->get();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.plans'));
            SEOMeta::setDescription('   پلن ها');
            SEOMeta::setKeywords(' پلن ها');
            SEOMeta::setCanonical(env('APP_URL').'/plans');
            return view('template.plans.index',compact('items'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function clientPlans(){
        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->user()->id);
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.plans-list'));
            SEOMeta::setDescription('لیست پلن ها');
            SEOMeta::setKeywords('لیست پلن ها');
            SEOMeta::setCanonical(env('APP_URL').'/user/panel/cvs');
            return view('template.auth.plans',compact('client'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function adverts(){
        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->id());
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.adverts-list'));
            SEOMeta::setDescription('لیست آگهی ها');
            SEOMeta::setKeywords('لیست آگهی ها');
            SEOMeta::setCanonical(env('APP_URL').'/user/panel/adverts/cvs');
            return view('template.auth.advertisings',compact('client'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function advertCvs(){
        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->id());
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.advertcvs-list'));
            SEOMeta::setDescription('لیست درخواست  ها');
            SEOMeta::setKeywords('لیست درخواست  ها');
            SEOMeta::setCanonical(env('APP_URL').'/user/panel/adverts/cvs');
            return view('template.auth.advert',compact('client'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function seekers(){
        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->user()->id);
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.seekers-list'));
            SEOMeta::setDescription('لیست کارجویان');
            SEOMeta::setKeywords('لیست کارجویان');
            SEOMeta::setCanonical(env('APP_URL').'/user/panel/seekers');
            return view('template.auth.seekers',compact('client'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function wallet(){
        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->user()->id);
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.wallets-list'));
            SEOMeta::setDescription('کیف پول');
            SEOMeta::setKeywords('کیف پول');
            SEOMeta::setCanonical(env('APP_URL').'/user/panel/wallet');
            return view('template.auth.wallet',compact('client'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function edit(){
        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->user()->id);
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.edit-profile'));
            SEOMeta::setDescription('ویرایش پنل کاربری');
            SEOMeta::setKeywords('ویرایش پنل کاربری');
            SEOMeta::setCanonical(env('APP_URL').'/user/panel/edit');
            return view('template.auth.edit',compact('client'));
        }catch (\Exception $exception){
            
            return abort('500');
        }
    }
    public function registerReceiver(RegisterReceiverRequest $request){
        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet','cart')->find(\auth('client')->user()->id);

            $client->cart()->update([
                'mobile'=>$request->mobile,
                'address'=>$request->address,
                'first_name'=>$request->first_name,
                'last_name'=>$request->last_name,
                'postal_code'=>$request->postal_code,
            ]);
            return redirect(route('client.show.payment'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function selfInformation(){
        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->user()->id);
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.information-profile'));
            SEOMeta::setDescription('اطلاعات شخصی');
            SEOMeta::setKeywords('اطلاعات شخصی');
            SEOMeta::setCanonical(env('APP_URL').'/user/panel/self-information');
            return view('template.auth.edit',compact('client'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function update(ClientUpdateRequest $request,$client){
        try {
            $user=Client::findOrFail($client)->update([
                'mobile'=>$request->mobile,
                'phone'=>$request->phone,
                'email'=>$request->email,
                'first_name'=>$request->first_name,
                'address'=>$request->address,
                'postal_code'=>$request->postal_code,
                'identity_card'=>$request->identity_card,
                'last_name'=>$request->last_name,
            ]);

            if($request->has('username')){
                $user=Client::findOrFail($client)->update([
                    'username'=>$request->username,
                ]);
            }

            $item=Client::with('info')->findOrFail($client);

            if($request->has('image')){
                if($item->Hasmedia(config('cms.collection-image'))){
                    destroyMedia($item,config('cms.collection-image'));
                }
                $item->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!empty(trim($request->input('current_password'))) && !empty(trim($request->input('new_password')))  ){

                if(!$this->resetPasswordProfile($item,$request->input('current_password'),$request->input('new_password'))){
                    return redirect()->back()->with('error',__('user::users.error'));
                }

            }

            $item->info()->update([
                'website'=>$request->website,
                'youtube'=>$request->youtube,
                'github'=>$request->github,
                'facebook'=>$request->facebook,
                'twitter'=>$request->twitter,
                'telegram'=>$request->telegram,
                'instagram'=>$request->instagram,
                'whatsapp'=>$request->whatsapp,
                'pintrest'=>$request->pintrest,
                'about'=>$request->about,
                'address'=>$request->info_address,
                'country'=>$request->info_country,
            ]);

            if(!$user){
                return redirect()->back()->with('error',__('client::clients.error'));


            }else{
                return redirect()->route('client.dashboard')->with('message',__('client::clients.update'));

            }

        }catch (\Exception $exception){
            
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }



    public function foreignUpdate(ForeignClientUpdateRequest $request,$client){
        try {

            $user=Client::findOrFail($client)->update([
                'mobile'=>$request->mobile,
                'phone'=>$request->phone,
                'email'=>$request->email,
                'first_name'=>$request->first_name,
                'address'=>$request->address,
                'last_name'=>$request->last_name,
                'postal_code'=>$request->postal_code,
                'city'=>$request->city,
            ]);

            if( env("SET_POSTAL_CODE")=='german'){
                $user=Client::findOrFail($client)->update([
                    'postal_code'=>$request->postal_code,
                    'city'=>$request->city,
                    'country'=>'german',
                ]);
            }
        

            if($request->has('username')){
                $user=Client::findOrFail($client)->update([
                    'username'=>$request->username,
                ]);
            }

            $item=Client::with('info')->findOrFail($client);

            if($request->has('image')){
                if($item->Hasmedia(config('cms.collection-image'))){
                    destroyMedia($item,config('cms.collection-image'));
                }
                $item->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!empty(trim($request->input('current_password'))) && !empty(trim($request->input('new_password')))  ){

                if(!$this->resetPasswordProfile($item,$request->input('current_password'),$request->input('new_password'))){
                    return redirect()->back()->with('error',__('user::users.error'));
                }

            }

            $item->info()->update([
                'website'=>$request->website,
                'youtube'=>$request->youtube,
                'github'=>$request->github,
                'facebook'=>$request->facebook,
                'twitter'=>$request->twitter,
                'telegram'=>$request->telegram,
                'instagram'=>$request->instagram,
                'whatsapp'=>$request->whatsapp,
                'pintrest'=>$request->pintrest,
                'about'=>$request->about,
                'address'=>$request->info_address,
                'country'=>$request->info_country,
            ]);

            if(!$user){
                return redirect()->back()->with('error',__('client::clients.error'));


            }else{
                return redirect()->route('client.dashboard')->with('message',__('client::clients.update'));

            }

        }catch (\Exception $exception){
            return redirect()->back()->with('error',__('client::clients.error'));
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }


    public function updateSeeker(SeekerUpdateRequest $request,$seeker){
        try {
            $user=Client::whereToken($seeker)->update([
                'email'=>$request->email,
                'username'=>$request->username,
                'identity_card'=>$request->identity_card,
                'first_name'=>$request->first_name,
                'last_name'=>$request->last_name,
            ]);

            $item=Client::with('info')->whereToken($seeker)->firstOrFail();

            if(!empty(trim($request->input('current_password'))) && !empty(trim($request->input('new_password')))  ){

                if(!$this->resetPasswordProfile($item,$request->input('current_password'),$request->input('new_password'))){
                    return redirect()->back()->with('error',__('user::users.error'));
                }
            }

            $item->info()->update([
                'website'=>$request->website,
                'youtube'=>$request->youtube,
                'github'=>$request->github,
                'facebook'=>$request->facebook,
                'twitter'=>$request->twitter,
                'telegram'=>$request->telegram,
                'instagram'=>$request->instagram,
                'whatsapp'=>$request->whatsapp,
                'pintrest'=>$request->pintrest,
                'about'=>$request->about,
                'address'=>$request->info_address,
                'country'=>$request->info_country,
            ]);

            if(!$user){
                return redirect()->back()->with('error',__('client::clients.error'));


            }else{
                return redirect()->back()->with('message',__('client::clients.update'));

            }


        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function femaleHair(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('female hair');
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/hair/female');
            return view('template.forms.female_hair');

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function sendEmail($data,$files){

        Mail::send('mail', $data, function($message) use($data,$files )
        {
            $message
                ->to('noorbaghaei.a2017@gmail.com')
                ->from('noorbaghaei.a2017@gmail.com')
                ->subject('amin nourbaghaei');
            foreach ($files as $key=>$value) {
                if(!empty(trim($value))){
                    $message->attach(public_path() . $value);
                }
            }

        });

    }
  
    
    public function sendForm($data){

        Mail::send('form', $data, function($message) use($data )
        {
            $message
                ->to(env('FORM_EMAIL'))
                ->from(env('MAIL_USERNAME'))
                ->subject('supplier form');
        });

    }
    public function specialProducts(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('محصولات شگفت انگیز');
            $items=Product::with('attributes','properties','price')->whereSpecial(2)->get();
            SEOMeta::setCanonical(env('APP_URL').'/products');
            return view('template.products.special',compact('items'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function searchCompany(Request $request,$category){
        try {

            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(__('cms.search'));
            $items= Client::with('company','info')->latest()
                ->whereHas('role',function ($query){
                    $query->where('title', '=', 'employer');
                })->where('category',$category)->get();
            SEOMeta::setCanonical(env('APP_URL').'/products');
            return view('template.companies.search',compact('items'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function shopFilter(Request $request){

        try {
            $array_category=[];
            $order="";
            $query=Product::query();
            $query=$query->with('info_company')->whereStatus(1);
            if($request->order){

                if($request->order=='new')
                {
                    $order=$request->order;
                    $query=$query->latest();
                }
                elseif(($request->order=='view')){
                    $order=$request->order;
                    $query=$query->whereHas('analyzer', function ($q) {
                        return $q->orderBy('view','desc');
                    });
                }

            }
            if($request->category){
                $query=$query->whereIn('category',$request->category);
                $array_category=$request->category;
            }
            $query=$query->paginate(config('cms.paginate'));
            $categories=Category::whereModel(Product::class)->whereIn('status',[1,3])->whereParent(0)->get();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('فروشگاه');
            SEOMeta::setCanonical(env('APP_URL').'/shop');
            return view('template.shop.index',compact('query','categories','array_category','order'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }


    }
    public function companyFilter(Request $request){

        try {

            $query=Client::query();

            $query=$query->with('company','info')->latest()
                ->whereHas('role',function ($query){
                    $query->where('title', '=', 'employer');
                });
            if($request->category){
                $query=$query->where('category',$request->category);
            }
            $items=$query->paginate(config('cms.paginate'));
            $categories=Category::whereModel(Client::class)->whereIn('status',[1,3])->whereParent(0)->get();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('کسب و کار ها');
            SEOMeta::setCanonical(env('APP_URL').'/companies');
            return view('template.companies.search',compact('items','categories'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }


    }
    public function advertFilter(Request $request){

        try {
            $array_category=[];
            $array_company=[];
            $array_skill=[];
            $item_salary="";
            $order="";
            $query=Advertising::query();
            $query=$query->with('info_company')->whereStatus(2);
            if($request->order){

                if($request->order=='new')
                {
                    $order=$request->order;
                    $query=$query->latest();
                }
                elseif(($request->order=='view')){
                    $order=$request->order;
                   $query=$query->whereHas('analyzer', function ($q) {
                       return $q->orderBy('view','desc');
                   });
                }

            }
            if($request->category){
                $query=$query->whereIn('category',$request->category);
                $array_category=$request->category;
            }
            if($request->company){
                $company=$request->company;
                $query=$query->whereHas('info_company', function ($q) use ($company) {
                    return $q->whereIn('id', $company);
                });
                $array_company=$request->company;
            }
            if($request->skill){
                $array_skill=$request->skill;
                $query=$query->whereJsonContains('skill',$array_skill);
            }
            if($request->salary){
                $item_salary=$request->salary;
                $query=$query->where('salary',$item_salary);
            }

             $query=$query->paginate(config('cms.paginate'));
            $categories=Category::whereModel(Product::class)->whereIn('status',[1,3])->whereParent(0)->get();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('جستجو آگهی ها');
            SEOMeta::setCanonical(env('APP_URL').'/advertising');
            return view('template.advertisings.index',compact('query','categories','array_category','array_company','item_salary','array_skill','order'));
        }catch (\Exception $exception){

            Rest::errorLog($exception->getMessage());
            return abort('500');
        }


    }

    public function RegisterClassroom(Request $request,$classroom){
        try {
            $classroom=ClassRoom::whereToken($classroom)->with('price')->firstOrFail();
            if($classroom->price->amount!=0 || !auth('client')->check() || hasAction($classroom,auth('client')->user(),'classroom')){
                return abort('500');
            }
            if(hasChild($classroom,'classroom')){
                UserAction::create([
                    'actionable_id'=>$classroom->id,
                    'actionable_type'=>ClassRoom::class,
                    'client'=>auth('client')->user()->id,
                    'status'=>1,
                    'title'=>"ثبت نام",
                    'text'=>"ثبت نام رایگان",
                ]);

                foreach (getChild($classroom,'classroom') as $child){
                    UserAction::create([
                        'actionable_id'=>$child->id,
                        'actionable_type'=>ClassRoom::class,
                        'client'=>auth('client')->user()->id,
                        'status'=>1,
                        'title'=>"ثبت نام",
                        'text'=>"ثبت نام رایگان",
                    ]);
                }


            }else{
                UserAction::create([
                    'actionable_id'=>$classroom->id,
                    'actionable_type'=>ClassRoom::class,
                    'client'=>auth('client')->user()->id,
                    'status'=>1,
                    'title'=>"ثبت نام",
                    'text'=>"ثبت نام رایگان",
                ]);
            }

            return redirect()->back()->with('message',__('educational::classrooms.store'));



        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }

    }
    public  function RegisterRace(Request $request,$race){
        try {
            $race=Race::whereToken($race)->with('price')->whereParent(0)->firstOrFail();
            if($race->price->amount!=0 || !auth('client')->check() || hasAction($race,auth('client')->user(),'race')){
                return abort('500');
            }
            if(hasChild($race,'race')){
                UserAction::create([
                    'actionable_id'=>$race->id,
                    'actionable_type'=>Race::class,
                    'client'=>auth('client')->user()->id,
                    'status'=>1,
                    'title'=>"ثبت نام",
                    'text'=>"ثبت نام رایگان",
                ]);

                foreach (getChild($race,'race') as $child){
                    UserAction::create([
                        'actionable_id'=>$child->id,
                        'actionable_type'=>Race::class,
                        'client'=>auth('client')->user()->id,
                        'status'=>1,
                        'title'=>"ثبت نام",
                        'text'=>"ثبت نام رایگان",
                    ]);
                }


            }else{
                UserAction::create([
                    'actionable_id'=>$race->id,
                    'actionable_type'=>ClassRoom::class,
                    'client'=>auth('client')->user()->id,
                    'status'=>1,
                    'title'=>"ثبت نام",
                    'text'=>"ثبت نام رایگان",
                ]);
            }

            return redirect()->back()->with('message',__('educational::races.store'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function sendCv(CvSendRequest $request,$token){
        try {
            $data=Advertising::whereToken($token)->whereStatus(2)->firstOrFail();

            $action=UserAction::create([
                'actionable_id'=>$data->id,
                'actionable_type'=>Advertising::class,
                'client'=>auth('client')->user()->id,
                'status'=>1,
                'title'=>"درخواست کار",
                'text'=>"ارسال رزومه برای کارفرما",
            ]);

            if($request->file('document')){
                $action->addMedia($request->file('document'))->toMediaCollection('cv');
            }

            if(!$action){
                return redirect()->back()->with('error',__('advertising::advertisings.error'));

            }else{
                return redirect()->back()->with('message',__('advertising::advertisings.store_cv'));
            }


        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function sendIdea(IdeaContactRequest $request){
        try {

            $data=[
                'name'=>$request->name,
                'email'=>$request->email,
                'subject'=>$request->subject,
                'message'=>$request->message,
                'ip'=>$_SERVER['REMOTE_ADDR'],
            ];

            sendCustomEmail($data,'emails.front.contact');

            return redirect()->back()->with('message',__('client::clients.store'));


        }catch (\Exception $exception){
           
            return redirect()->back()->with('error',__('client::clients.error'));
        }
    }

    public function allCountry(){

            return Country::OrderBy('name','desc')->get();
    }
    public function getStates($country){


            return State::where('country',$country)->OrderBy('name','desc')->get();
    }
    public function addToCart(Request $request,$product){
        try {

            $product=Product::with('price')->whereToken($product)->firstOrFail();

            $client=Client::findOrFail(\auth('client')->user()->id);



            $cart=$client->cart()->updateOrCreate([
                'client'=>$client->id,
                'mobile'=>$client->mobile
            ]);

            $cart=Cart::findOrFail($cart->id);

            if(CartList::where('product',$product->id)->where('cart',$cart->id)->first()){
                $cartlist=CartList::where('product',$product->id)->where('cart',$cart->id)->first();
                $cartlist->update([
                    'count'=>$cartlist->count + 1,
                    'price'=>$cartlist->price + intval($product->price->amount)
                ]);
            }else{
                CartList::create([
                    'cart'=>$cart->id,
                    'product'=>$product->id,
                    'price'=>intval($product->price->amount)
                ]);

            }

            return redirect(route('client.show.cart'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function getCities($state){



        return City::where('state',$state)->OrderBy('name','desc')->get();
    }

    public function sendTransactionRequest($name,$item=null){

        try {
            $user=Client::with('cart')->find(auth('client')->user()->id);
            switch ($name){
                case 'classRoom':
                    $result=ClassRoom::with('price')->whereToken($item)->firstOrFail();
                    break;

                case 'race':
                    $result=Race::with('price')->whereToken($item)->firstOrFail();
                    break;
                case 'product':
                   $result=$user->cart;
                    break;
                default:
                    break;
            }
           if(count($user->cart->items) > 0){
               $data=[
                   'amount'=>$user->cart->items->sum('price'),
                   'title'=>'پرداخت الکترونیکی',
                   'description'=>'پرداخت مرسوله',
                   'email'=>'noorbaghaei.a2017@gmail.com',
                   'mobile'=>'09195995044 ',
               ];
           }else{
               $data=[
                   'amount'=>$result->price->amount,
                   'title'=>'پرداخت الکترونیکی',
                   'description'=>'پرداخت شهریه',
                   'email'=>'noorbaghaei.a2017@gmail.com',
                   'mobile'=>'09195995044 ',
               ];
           }

               ZarinPal::request($data,$result);
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function callbackTransactionMessage($error,$payment){
        $message="";

        if($error==1){
            $message="عملیات پرداخت ناموفق بود";

        }elseif($error==2){
            $message="عملیات پرداخت با موفقیت انجام شد";
        }
        elseif($error==0){
            $message="شما از عملیات درگاه بانک انصراف دادید";
        }
        return view('template.transactions.message',compact('message','error','payment'));
    }
    public function callbackTransaction(){

        $zarinpal=new ZarinPal();
        return $zarinpal->response();
    }

    public function showClientLanguageJob(Request $request,$lang,$token){

        $item=UserServices::with('translates')->where('token',$token)->first();
        return view('template.auth.bussiness.language',compact('item','lang'));
    }
    public function updateClientLanguageJob(Request $request,$lang,$token){

        DB::beginTransaction();
        $item=UserServices::with('translates')->where('token',$token)->first();
        if( $item->translates->where('lang',$lang)->first()){
            $changed=$item->translates->where('lang',$lang)->first()->update([
                'title'=>$request->title,
                'text'=>$request->text,
                'excerpt'=>$request->excerpt
            ]);
        }else{
            $changed=$item->translates()->create([
                'title'=>$request->title,
                'text'=>$request->text,
                'excerpt'=>$request->excerpt,
                'lang'=>$lang
            ]);
        }


        if(!$changed){
            DB::rollBack();
            return redirect()->back()->with('error',__('user::users.error'));
        }else{
            DB::commit();
            return redirect()->route('client.dashboard')->with('message',__('user::users.store'));

        }
    }



    public function showClientLanguageMenuJob(Request $request,$lang,$token){
        
        $item=ClientMenuJob::with('translates')->where('token',$token)->first();
        return view('template.auth.bussiness.menus.language',compact('item','lang'));
    }
    public function updateClientLanguageMenuJob(Request $request,$lang,$token){

        DB::beginTransaction();
        $item=ClientMenuJob::with('translates')->where('token',$token)->first();
        if( $item->translates->where('lang',$lang)->first()){
            $changed=$item->translates->where('lang',$lang)->first()->update([
                'title'=>$request->title,
                'excerpt'=>$request->excerpt,

            ]);
        }else{
            $changed=$item->translates()->create([
                'title'=>$request->title,
                'excerpt'=>$request->excerpt,
                'lang'=>$lang
            ]);
        }


        if(!$changed){
            DB::rollBack();
            return redirect()->back()->with('error',__('user::users.error'));
        }else{
            DB::commit();
            return redirect()->route('client.dashboard')->with('message',__('user::users.store'));

        }
    }




}
