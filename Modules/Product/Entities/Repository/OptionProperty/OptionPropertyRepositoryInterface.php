<?php


namespace Modules\Product\Entities\Repository\OptionProperty;


interface OptionPropertyRepositoryInterface
{
    public function getAll();
    public function getOption($token);
}
