<?php


namespace Modules\Product\Entities\Repository\OptionAttribute;


use Modules\Core\Entities\Option;
use Modules\Product\Entities\OptionAttribute;

class OptionAttributeRepository implements OptionAttributeRepositoryInterface
{

    public function getAll()
    {
        return OptionAttribute::latest()->get();
    }

    public function getOption($token)
    {
        $product=OptionAttribute::whereToken($token)->firstOrFail();
        return Option::latest()->where('optionable_type',OptionAttribute::class)->where('optionable_id',$product->id)->get();
    }
}
