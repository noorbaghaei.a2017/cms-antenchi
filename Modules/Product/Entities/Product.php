<?php

namespace Modules\Product\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Analyzer;
use Modules\Core\Entities\Discount;
use Modules\Core\Entities\Price;
use Modules\Core\Entities\Translate;
use Modules\Core\Entities\User;
use Modules\Core\Entities\UserAction;
use Modules\Core\Entities\UserCompany;
use Modules\Core\Helper\Trades\TimeAttribute;
use Modules\Order\Entities\OrderList;
use Modules\Product\Helper\ProductHelper;
use Modules\Question\Entities\Question;
use Modules\Seo\Entities\Seo;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Tags\HasTags;

class Product extends Model implements HasMedia
{
    use Sluggable,HasMediaTrait,HasTags,TimeAttribute;

    protected $fillable = ['title','special','category','excerpt','currency','inventory','code','level','parent','text','store_category','token','slug','user','option','ability','status','special'];

    public function getRouteKeyName()
    {
        return multiRouteKey();
    }
    public function analyzer()
    {
        return $this->morphOne(Analyzer::class, 'analyzerable');
    }
    public function translates()
    {
        return $this->morphMany(Translate::class, 'translateable');
    }
    public function orderList()
    {
        return $this->morphOne(OrderList::class, 'orderable');
    }
    public function userAction()
    {
        return $this->morphMany(UserAction::class, 'actionable');
    }
    public function options()
    {
        return $this->morphToMany(ProductOption::class, 'optionable');
    }

    public function seo()
    {
        return $this->morphOne(Seo::class, 'seoable');
    }
    public function info_company()
    {
        return $this->hasOne(UserCompany::class, 'id','client');
    }
    public function user_info()
    {
        return $this->belongsTo(User::class,'user','id');
    }
    public function discount()
    {
        return $this->morphOne(Discount::class, 'discountable');
    }
    public function price()
    {
        return $this->morphOne(Price::class, 'priceable');
    }
    public function questions()
    {
        return $this->morphMany(Question::class, 'questionable');
    }
    public function attributes()
    {
        return $this->morphMany(ProductAttribute::class, 'attributeable')->with('option_item','attribute_item');
    }
    public function properties()
    {
        return $this->morphMany(ProductProperty::class, 'propertyable')->with('option_item','property_item');
    }

    public  function getQuestionAttribute(){

        return $this->questions()->count();
    }

    public  function getViewAttribute(){

        return $this->analyzer->view;
    }
    public  function getNameCategoryAttribute(){

        return get_category($this->category);
    }




    public  function getLikeAttribute(){

        return $this->analyzer->like;
    }
    public  function getShowStatusAttribute(){

        return ProductHelper::publish( ($this->status));
    }
    public  function getShowSpecialAttribute(){

        return ProductHelper::special( ($this->special));
    }
    public  function getPriceFormatAttribute(){

        return isset($this->price->amount) ? number_format($this->price->amount):"رایگان" ;
    }


    public function syncAttribute( $items){

        $this->attributes()->delete();
        foreach ($items as $item){
            $this->attributes()->create([
                'option'=>$item
            ]);
        }

    }

    public function syncProperty( $items){

        $this->properties()->delete();
        foreach ($items as $item){
            $this->properties()->create([
                'option'=>$item
            ]);
        }

    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(250)
            ->height(250)
            ->keepOriginalImageFormat()
            ->performOnCollections(config('cms.collection-image'));

        $this->addMediaConversion('list')
            ->width(200)
            ->height(200)
            ->keepOriginalImageFormat()
            ->performOnCollections(config('cms.collection-image'));
    }

    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug'=>[
                'source'=>'title'
            ]
        ];
    }


}
