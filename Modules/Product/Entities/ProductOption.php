<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class ProductOption extends Model implements HasMedia
{
    use HasMediaTrait;
    protected $fillable = ['count','item'];

    public function products()
    {
        return $this->morphToMany(Product::class,'optionable');
    }
}
