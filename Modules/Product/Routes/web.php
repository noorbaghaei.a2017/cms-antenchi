<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(["prefix"=>config('cms.prefix-admin'),"middleware"=>["auth:web"]],function() {
    Route::resource('/products', 'ProductController')->only('create','store','destroy','update','index','edit');
    Route::group(["prefix"=>'attribute'], function () {
        Route::get('/list', 'OptionAttributeController@index')->name('attributes.index');
        Route::get('/create', 'OptionAttributeController@create')->name('product.create.attribute');
        Route::get('/edit/{attribute}', 'OptionAttributeController@edit')->name('product.edit.attribute');
        Route::post('/store', 'OptionAttributeController@store')->name('product.store.attribute');
        Route::post('/update', 'OptionAttributeController@update')->name('product.update.attribute');
        Route::get('/orders/{product}', 'ProductController@orders')->name('product.orders');
    });
    Route::group(["prefix"=>'property'], function () {
        Route::get('/list', 'OptionPropertyController@index')->name('properties.index');
        Route::get('/create', 'OptionPropertyController@create')->name('product.create.property');
        Route::get('/edit/{property}', 'OptionPropertyController@edit')->name('product.edit.property');
        Route::post('/store', 'OptionPropertyController@store')->name('product.store.property');
        Route::patch('/update', 'OptionPropertyController@create')->name('product.update.property');
    });



    Route::group(["prefix"=>'products'], function () {
        Route::get('/gallery/{product}', 'ProductController@gallery')->name('product.gallery');
        Route::post('/gallery/store/{product}', 'ProductController@galleryStore')->name('product.gallery.store');
        Route::get('/gallery/destroy/{media}', 'ProductController@galleryDestroy')->name('product.gallery.destroy');
    });
    Route::group(["prefix"=>'search'], function () {
        Route::post('/products', 'ProductController@search')->name('search.product');
        Route::post('/product/attribute', 'OptionAttributeController@search')->name('search.product.attribute');
        Route::post('/product/property', 'OptionPropertyController@search')->name('search.product.property');
        Route::post('/order/products', 'ProductController@orderSearch')->name('search.product.orders');
    });

    Route::group(["prefix"=>'product/questions'], function () {
        Route::get('/{product}', 'ProductController@question')->name('product.questions');
        Route::get('/create/{product}', 'ProductController@questionCreate')->name('product.question.create');
        Route::post('/store/{product}', 'ProductController@questionStore')->name('product.question.store');
        Route::delete('/destroy/{question}', 'ProductController@questionDestroy')->name('product.question.destroy');
        Route::get('/edit/{product}/{question}', 'ProductController@questionEdit')->name('product.question.edit');
        Route::patch('/update/{question}', 'ProductController@questionUpdate')->name('product.question.update');
    });

    Route::group(["prefix"=>'product/categories'], function () {
        Route::get('/', 'ProductController@categories')->name('product.categories');
        Route::get('/create', 'ProductController@categoryCreate')->name('product.category.create');
        Route::post('/store', 'ProductController@categoryStore')->name('product.category.store');
        Route::get('/edit/{category}', 'ProductController@categoryEdit')->name('product.category.edit');
        Route::patch('/update/{category}', 'ProductController@categoryUpdate')->name('product.category.update');

    });

    Route::group(["prefix"=>'product_attribute/options'], function () {
        Route::get('/attribute/{token}', 'OptionAttributeController@options')->name('attributes.options');
        Route::get('/attribute/add/{token}', 'OptionAttributeController@optionCreate')->name('attribute.option.create');
        Route::post('/store/{data}', 'OptionAttributeController@optionStore')->name('attribute.option.store');
        Route::get('/edit/{option}', 'OptionAttributeController@optionEdit')->name('attribute.option.edit');
        Route::patch('/update/{option}', 'OptionAttributeController@optionUpdate')->name('attribute.option.update');

    });
    Route::group(["prefix"=>'product_property/options'], function () {
        Route::get('/property/{token}', 'OptionPropertyController@options')->name('properties.options');
        Route::get('/property/add/{token}', 'OptionPropertyController@optionCreate')->name('property.option.create');
        Route::post('/store/{data}', 'OptionPropertyController@optionStore')->name('property.option.store');
        Route::get('/edit/{option}', 'OptionPropertyController@optionEdit')->name('property.option.edit');
        Route::patch('/update/{option}', 'OptionPropertyController@optionUpdate')->name('property.option.update');

    });

});
