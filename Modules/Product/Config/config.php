<?php

return [
    'name' => 'Product',
    'icons'=>[
        'product'=>'shopping_basket ',
        'attribute'=>'fa fa-cube ',
        'property'=>'fa fa-cube ',
    ]
];
