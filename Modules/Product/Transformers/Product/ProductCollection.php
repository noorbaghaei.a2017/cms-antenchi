<?php

namespace Modules\Product\Transformers\Product;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Modules\Product\Entities\Product;

class ProductCollection extends ResourceCollection
{
    public $collects = Product::class;

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(
                function ( $item ) {
                    return new ProductResource($item);
                }
            ),
            'filed' => [
                'thumbnail','title','code','price','status','special','category','view','like','questions','update_date','create_date',
            ],
            'public_route'=>[

                [
                    'name'=>'products.create',
                    'param'=>[null],
                    'icon'=>config('cms.icon.add'),
                    'title'=>__('cms.add'),
                    'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                    'method'=>'GET',
                ],
                [
                    'name'=>'product.categories',
                    'param'=>[null],
                    'icon'=>config('cms.icon.categories'),
                    'title'=>__('cms.categories'),
                    'class'=>'btn btn-sm text-sm text-sm-center btn-warning pull-right',
                    'method'=>'GET',
                ]

            ],
            'private_route'=>
                [
                    [
                        'name'=>'products.edit',
                        'param'=>[
                            'product'=>'token'
                        ],
                        'icon'=>config('cms.icon.edit'),
                        'title'=>__('cms.edit'),
                        'class'=>'btn btn-warning btn-sm text-sm',
                        'modal'=>false,
                        'method'=>'GET',
                    ],
                    [
                        'name'=>'products.destroy',
                        'param'=>[


                            'product'=>'token'

                        ],
                        'icon'=>config('cms.icon.delete'),
                        'title'=>__('cms.delete'),
                        'class'=>'btn btn-danger btn-sm text-sm text-white',
                        'modal'=>true,
                        'modal_type'=>'destroy',
                        'method'=>'DELETE',
                    ],
                    [
                        'name'=>'product.orders',
                        'param'=>[


                            'product'=>'token'

                        ],
                        'icon'=>' fa fa-shopping-cart',
                        'title'=>__('cms.orders'),
                        'class'=>'btn btn-success btn-sm text-sm text-white',
                        'modal'=>false,
                        'method'=>'GET',
                    ],
                    [
                        'name'=>'products.detail',
                        'param'=>[null],
                        'icon'=>config('cms.icon.detail'),
                        'title'=>__('cms.detail'),
                        'class'=>'btn btn-primary btn-sm text-sm text-white',
                        'modal'=>true,
                        'modal_type'=>'detail',
                        'method'=>'GET',
                    ],
                    [
                        'name'=>'product.gallery',
                        'param'=>[
                            'product'=>'token'
                        ],
                        'icon'=>config('cms.icon.gallery'),
                        'title'=>__('cms.gallery'),
                        'class'=>'btn btn-warning btn-sm text-sm text-white',
                        'modal'=>false,
                        'method'=>'GET',
                    ],
                    [
                        'name'=>'product.questions',
                        'param'=>[
                            'product'=>'token'
                        ],
                        'icon'=>config('cms.icon.questions'),
                        'title'=>__('cms.question'),
                        'class'=>'btn btn-info btn-sm text-sm text-white',
                        'modal'=>false,
                        'method'=>'GET',
                    ]

                ],
            'search_route'=>[

                'name'=>'search.product',
                'filter'=>[
                    'title','code'
                ],
                'icon'=>config('cms.icon.add'),
                'title'=>__('cms.add'),
                'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                'method'=>'POST',

            ],
            'count' => $this->collection->count(),
            'links' => [
                'self' => 'link-value',
            ],
            'collect'=>__('product::products.collect'),
            'title'=>__('product::products.index'),
        ];
    }
}
