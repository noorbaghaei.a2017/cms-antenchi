 <li>
        <a href="{{route('carousels.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="material-icons">{{config('carousel.icons.carousel')}} </i>
                              </span>
            <span class="nav-text">{{__('carousel::carousels.collect')}}</span>
        </a>
    </li>
