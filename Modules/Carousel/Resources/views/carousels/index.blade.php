@include('core::layout.modules.index',[

    'title'=>__('carousel::carousels.index'),
    'items'=>$items,
    'parent'=>'carousel',
    'model'=>'carousel',
    'directory'=>'carousels',
    'collect'=>__('carousel::carousels.collect'),
    'singular'=>__('carousel::carousels.singular'),
    'create_route'=>['name'=>'carousels.create'],
    'edit_route'=>['name'=>'carousels.edit','name_param'=>'carousel'],
    'destroy_route'=>['name'=>'carousels.destroy','name_param'=>'carousel'],
     'search_route'=>true,
     'setting_route'=>true,
    'datatable'=>[ __('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',
    __('cms.slug')=>'slug',
    __('cms.order')=>'order',
    __('cms.update_date')=>'AgoTimeUpdate',
     __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[__('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',
               __('cms.order')=>'order',
           __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
        __('cms.slug')=>'slug',

    ],


])

