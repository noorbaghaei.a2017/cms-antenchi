<?php

namespace Modules\Carousel\Transformers;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Modules\Carousel\Entities\Carousel;

class CarouselCollection extends ResourceCollection
{
    public $collects = Carousel::class;

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(
                function ( $item ) {
                    return new CarouselResource($item);
                }
            ),
            'filed' => [
                'thumbnail','title','slug','order','update_date','create_date'
            ],
            'public_route'=>[

                [
                    'name'=>'carousels.create',
                    'param'=>[null],
                    'icon'=>config('cms.icon.add'),
                    'title'=>__('cms.add'),
                    'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                    'method'=>'GET',
                ]
            ],
            'private_route'=>
                [
                    [
                        'name'=>'carousels.edit',
                        'param'=>[
                            'carousel'=>'token'
                        ],
                        'icon'=>config('cms.icon.edit'),
                        'title'=>__('cms.edit'),
                        'class'=>'btn btn-warning btn-sm text-sm',
                        'modal'=>false,
                        'method'=>'GET',
                    ],
                    [
                        'name'=>'carousels.destroy',
                        'param'=>[
                            'carousel'=>'token'
                        ],
                        'icon'=>config('cms.icon.delete'),
                        'title'=>__('cms.delete'),
                        'class'=>'btn btn-danger btn-sm text-sm text-white',
                        'modal'=>true,
                        'modal_type'=>'destroy',
                        'method'=>'DELETE',
                    ],
                    [
                        'name'=>'carousels.detail',
                        'param'=>[null],
                        'icon'=>config('cms.icon.detail'),
                        'title'=>__('cms.detail'),
                        'class'=>'btn btn-primary btn-sm text-sm text-white',
                        'modal'=>true,
                        'modal_type'=>'detail',
                        'method'=>'GET',
                    ]
                ],
            'search_route'=>[

                'name'=>'search.carousel',
                'filter'=>[
                    'title','slug','order'
                ],
                'icon'=>config('cms.icon.add'),
                'title'=>__('cms.add'),
                'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                'method'=>'POST',

            ],
            'language_route'=>true,
            'class_model'=> 'carousel',
            'count' => $this->collection->count(),
            'links' => [
                'self' => 'link-value',
            ],
            'collect'=>__('carousel::carousels.collect'),
            'title'=>__('carousel::carousels.index'),
        ];
    }
}
