<?php


namespace Modules\Download\Entities\Repository;


use Modules\Download\Entities\Download;

class DownloadRepository implements DownloadRepositoryInterface
{

    public function getAll()
    {
        return Download::latest()->get();
    }
}
