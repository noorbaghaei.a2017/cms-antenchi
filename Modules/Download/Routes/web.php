<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(["prefix"=>config('cms.prefix-admin'),"middleware"=>["auth:web"]],function() {
    Route::resource('/downloads', 'DownloadController')->only('create', 'store', 'destroy', 'update', 'index', 'edit');

    Route::group(["prefix"=>'search'], function () {
        Route::post('/downloads', 'DownloadController@search')->name('search.download');
    });


    Route::group(["prefix"=>'download/categories'], function () {
        Route::get('/', 'DownloadController@categories')->name('download.categories');
        Route::get('/create', 'DownloadController@categoryCreate')->name('download.category.create');
        Route::post('/store', 'DownloadController@categoryStore')->name('download.category.store');
        Route::get('/edit/{category}', 'DownloadController@categoryEdit')->name('download.category.edit');
        Route::patch('/update/{category}', 'DownloadController@categoryUpdate')->name('download.category.update');

    });

    Route::group(["prefix"=>'download/questions'], function () {
        Route::get('/{download}', 'DownloadController@question')->name('download.questions');
        Route::get('/create/{download}', 'DownloadController@questionCreate')->name('download.question.create');
        Route::post('/store/{download}', 'DownloadController@questionStore')->name('download.question.store');
        Route::delete('/destroy/{question}', 'DownloadController@questionDestroy')->name('download.question.destroy');
        Route::get('/edit/{download}/{question}', 'DownloadController@questionEdit')->name('download.question.edit');
        Route::patch('/update/{question}', 'DownloadController@questionUpdate')->name('download.question.update');
    });


});
