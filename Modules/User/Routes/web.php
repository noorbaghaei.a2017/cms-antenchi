<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(["prefix"=>config('cms.prefix-admin'),"middleware"=>["auth:web"]],function() {
    Route::resource('/users', 'UserController')->only('create','store','destroy','update','index','edit');
    Route::get('/profile/{user}', 'UserController@profile')->name('user.profile');
    Route::patch('/profile/update/{user}', 'UserController@updateProfile')->name('profile.update');
    Route::patch('/user/update/{user}', 'UserController@updateUser')->name('user.update');

    Route::group(["prefix"=>'search', "middleware" => ["auth:web"]], function () {
        Route::post('/users', 'UserController@search')->name('search.user');
    });
});
