@extends('core::layout.panel')
@section('pageTitle', __('cms.create'))
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h2>{{__('cms.create')}}  </h2>
                        <small>
                            {{__('user::users.text-create')}}
                        </small>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
@include('core::layout.alert-danger')
                        <form id="signupForm" role="form" method="post" action="{{route('users.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="firstname" class="form-control-label">{{__('cms.first_name')}}</label>
                                    <input type="text" name="firstname" value="{{old('firstname')}}" class="form-control" id="firstname"  required>
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="lastname" class="form-control-label">{{__('cms.last_name')}}</label>
                                    <input type="text" name="lastname" value="{{old('lastname')}}" class="form-control" id="lastname" required>
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="mobile" class="form-control-label">{{__('cms.mobile')}}</label>
                                    <input  type="number" name="mobile" value="{{old('mobile')}}" class="form-control" id="mobile" required>
                                    <small class="help-block text-warning">9195995044</small>
                                </div>
                                <div class="col-sm-3">
                                    <label for="images" class="form-control-label">{{__('cms.thumbnail')}} </label>
                                    @include('core::layout.load-single-image')
                                </div>

                            </div>
                            <div class="row form-group">

                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="roles"  class="form-control-label">{{__('cms.role')}}  </label>
                                    <select dir="rtl" class="form-control" id="roles" name="roles" required>

                                            @foreach($roles as $role)
                                                <option value="{{$role}}">{{$role}}</option>
                                            @endforeach


                                    </select>

                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="username" class="form-control-label">{{__('cms.username')}}</label>
                                    <input type="text" name="username"  value="{{old('username')}}" class="form-control" id="username" required>
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="email" class="form-control-label">{{__('cms.email')}} </label>
                                    <input type="text" name="email" value="{{old('email')}}" class="form-control" id="email" required>
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="password" class="form-control-label">{{__('cms.password')}} </label>
                                    <input type="password" name="password" class="form-control" id="password" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <span class="text-danger">*</span>
                                    <label for="co" class="form-control-label">{{__('cms.countries')}} </label>
                                    <select dir="rtl" class="form-control"  id="co" name="country" required >

                                        @foreach($countries as $key=>$value)

                                            <option value="{{$value->id}}">{{__('countries.'.$value->name)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            @include('core::layout.modules.info-box',['info'=>null])

                            @include('core::layout.create-button')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>


        $().ready(function() {
            // validate the comment form when it is submitted
            $("#commentForm").validate();
            $.validator.addMethod("regex",
                function (value, element, regexp) {
                    var re = new RegExp(regexp);
                    return this.optional(element) || re.test(value);
                },"Please check your input."
            );
            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    firstname: {
                        required: true
                    },
                    lastname: {
                        required: true
                    },
                    mobile: {
                        number:true,
                        required: true
                    },
                    email: {
                        required: true
                    },
                    password: {
                        required: true
                    },
                    role: {
                        required: true
                    },
                    username: {
                        required: true
                    },


                },
                messages: {
                    firstname:"نام الزامی است",
                    lastname: "نام خانوادگی  لزامی است",
                    mobile: "موبایل  الزامی است",
                    email: "ایمیل  الزامی است",
                    password: "کلمه عبور  الزامی است",
                    role: "نقش  الزامی است",
                    username: "نام کاربری  الزامی است",
                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

@endsection



