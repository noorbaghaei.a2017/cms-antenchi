@extends('core::layout.panel')
@section('pageTitle', 'جزییات')
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <div class="pull-left">
                            <h2>نام {{$item->name}}</h2>
                            <small>توضیحات </small>
                        </div>
                        <a onclick="window.print()" class="btn btn-primary btn-sm text-sm text-white pull-right">چاپ </a>
                    </div>
                    <br>
                    <br>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label for="title" class="form-control-label">عنوان :  </label>
                                <span>{{$item->title}}</span>
                            </div>

                            <div class="col-sm-4">
                                <label for="excerpt" class="form-control-label">توضیح مختصر : </label>
                                <span>{{$item->excerpt}}</span>
                            </div>
                            <div class="col-sm-4">
                                <label for="time" class="form-control-label">زمان انتشار  : </label>
                                <span>{{$item->created_at->ago()}}</span>
                            </div>
                        </div>
                        <div class="form-group row">

                            <div class="col-sm-12">
                                <label for="text" class="form-control-label">متن :  </label>
                                <br>
                                <span>{!! $item->text !!}</span>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
