<?php
return [
    "text-create"=>"you can create your student",
    "score-register"=>"you can register your score",
    "text-edit"=>"you can edit your student",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"students list",
    "singular"=>"student",
    "collect"=>"students",
    "permission"=>[
        "student-full-access"=>"student full access",
        "student-list"=>"students list",
        "student-delete"=>"student delete",
        "student-create"=>"student create",
        "student-edit"=>"student race",
    ]
];
