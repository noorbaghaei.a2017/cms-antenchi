<?php
return [
    "text-create"=>"you can create your classrooms",
    "text-edit"=>"you can edit your classrooms",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"customers list",
    "singular"=>"educational",
    "collect"=>"classrooms",
    "permission"=>[
        "classroom-full-access"=>"classroom full access",
        "classroom-list"=>"classrooms list",
        "classroom-delete"=>"classroom delete",
        "classroom-create"=>"classroom create",
        "classroom-edit"=>"edit classroom",
    ]
];
