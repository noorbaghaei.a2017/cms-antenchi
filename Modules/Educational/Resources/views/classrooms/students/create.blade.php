@extends('core::layout.panel')
@section('pageTitle', 'ثبت نام دانش آموزان')
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box p-a-xs">
                    <div class="row">
                        <div class="col-md-5">
                            <a href="#">
                                @if(!$item->Hasmedia('images'))
                                    <img style="width: 400px;height: auto" src="{{asset('img/no-img.gif')}}" alt="" class="img-responsive">


                                @else
                                    <img style="width: 400px;height: auto" src="{{$item->getFirstMediaUrl('images')}}" alt="" class="img-responsive">


                                @endif


                            </a>
                        </div>
                        <div class="col-md-7">
                            <div style="padding-top: 35px">
                                <h6 style="padding-top: 35px"> {{__('cms.title')}} : </h6>
                                <h4 style="padding-top: 35px">    {{$item->title}}</h4>
                            </div>
                            <div style="padding-top: 35px">
                                <h6 style="padding-top: 35px"> {{__('cms.excerpt')}} : </h6>
                                <h4 style="padding-top: 35px">    {{$item->excerpt}}</h4>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    @include('core::layout.alert-danger')
                    @include('core::layout.alert-success')
                    <div class="box-header">
                        <div class="pull-left">

                            <small>
                                {{__('educational::students.score-register')}}
                            </small>
                        </div>
                        <a onclick="window.print()" class="btn btn-primary btn-sm text-sm text-white pull-right">{{__('cms.print')}} </a>
                    </div>
                    <br>
                    <br>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <form id="signupForm" action="{{route('register.classroom.client.force',['classroom'=>$classroom])}}" method="POST" enctype="multipart/form-data">
                            @csrf



                                    <div class="box" style="padding: 5px">
                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <label for="student" class="form-control-label">{{__('cms.students')}} </label>
                                                    <select dir="rtl" class="form-control"  id="student" name="student"  >
                                                        @foreach($clients as $client)
                                                            <option value="{{$client->token}}">{{$client->full_name}}</option>

                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                    </div>






                            @include('core::layout.create-button')
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>


        $().ready(function() {

            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    client: {
                        required: true
                    },
                },
                messages: {
                    client:"عنوان الزامی است",

                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

@endsection
