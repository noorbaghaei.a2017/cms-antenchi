<?php

namespace Modules\Page\Transformers;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Modules\Page\Entities\Page;

class PageCollection extends ResourceCollection
{
    public $collects = Page::class;
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(
                function ( $item ) {
                    return new PageResource($item);
                }
            ),
            'filed' => [
                'thumbnail','title','slug','href','update_date','create_date'
            ],
            'public_route'=>[

                [
                    'name'=>'pages.create',
                    'param'=>[null],
                    'icon'=>config('cms.icon.add'),
                    'title'=>__('cms.add'),
                    'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                    'method'=>'GET',
                ]
            ],
            'private_route'=>
                [
                    [
                        'name'=>'pages.edit',
                        'param'=>[
                            'page'=>'token'
                        ],
                        'icon'=>config('cms.icon.edit'),
                        'title'=>__('cms.edit'),
                        'class'=>'btn btn-warning btn-sm text-sm',
                        'modal'=>false,
                        'method'=>'GET',
                    ],
                    [
                        'name'=>'pages.destroy',
                        'param'=>[
                            'page'=>'token'
                        ],
                        'icon'=>config('cms.icon.delete'),
                        'title'=>__('cms.delete'),
                        'class'=>'btn btn-danger btn-sm text-sm text-white',
                        'modal'=>true,
                        'modal_type'=>'destroy',
                        'method'=>'DELETE',
                    ],
                    [
                        'name'=>'pages.detail',
                        'param'=>[null],
                        'icon'=>config('cms.icon.detail'),
                        'title'=>__('cms.detail'),
                        'class'=>'btn btn-primary btn-sm text-sm text-white',
                        'modal'=>true,
                        'modal_type'=>'detail',
                        'method'=>'GET',
                    ]
                ],
            'links' => [
                'self' => 'link-value',
            ],
            'collect'=>__('page::pages.collect'),
            'title'=>__('page::pages.index'),
        ];
    }
}
