


@include('core::layout.modules.index',[

    'title'=>__('order::orders.index'),
    'items'=>$items,
    'parent'=>'order',
    'model'=>'order',
    'directory'=>'orders',
    'collect'=>__('order::orders.collect'),
    'singular'=>__('order::orders.singular'),
     'search_route'=>true,
     'setting_route'=>true,
    'datatable'=>[
    __('cms.order_id')=>'order_id',
    __('cms.title')=>['payment','title'],
    __('cms.status')=>['payment','status'],
    __('cms.first_name')=>['payment','info_client','first_name'],
    __('cms.last_name')=>['payment','info_client','last_name'],
    __('cms.price')=>'price',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
               __('cms.order_id')=>'order_id',
        __('cms.price')=>'price',

        __('cms.create_date')=>'created_at'

    ],
])

{{--__('cms.title')=>'titlePayment',--}}
{{--__('cms.full_name')=>'fullNameUser',--}}
{{--__('cms.mobile')=>'mobileUser',--}}
{{--__('cms.email')=>'emailUser',--}}


{{--__('cms.title')=>'titlePayment',--}}
{{--__('cms.full_name')=>'fullNameUser',--}}
{{--__('cms.mobile')=>'mobileUser',--}}
{{--__('cms.email')=>'emailUser',--}}
{{--__('cms.price')=>'price',--}}
{{--__('cms.status')=>'showStatus',--}}

{{--'edit_route'=>['name'=>'orders.edit','name_param'=>'order'],--}}
{{--'destroy_route'=>['name'=>'orders.destroy','name_param'=>'order'],--}}
{{--'create_route'=>['name'=>'orders.create'],--}}
