<?php


namespace Modules\Order\Entities\Repository;


interface OrderRepositoryInterface
{

    public function getAll();
}
