<?php

namespace Modules\Menu\Entities;

use Illuminate\Database\Eloquent\Model;

class ListMenu extends Model
{
    protected $table="list_menus";

    protected $fillable = ['token','label','name'];

    public function getRouteKeyName()
    {
        return multiRouteKey();
    }
}
