<?php

namespace Modules\Menu\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Translate;
use Modules\Core\Helper\Trades\TimeAttribute;
use Spatie\Permission\Models\Role;

class Menu extends Model
{
    use TimeAttribute;

    protected $fillable = ['token','title','pattern','symbol','href','level','parent','icon','order','status','rel','target','user','list_menus','columns','column'];


    public function getRouteKeyName()
    {
        return multiRouteKey();
    }

    public function translates()
    {
        return $this->morphMany(Translate::class, 'translateable');
    }
    public function childs()
    {
        return $this->hasMany(Menu::class, 'parent','id');
    }

}
