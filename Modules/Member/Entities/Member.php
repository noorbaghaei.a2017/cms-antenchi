<?php

namespace Modules\Member\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticate;
use Laravel\Passport\HasApiTokens;
use Modules\Core\Entities\Info;
use Modules\Core\Entities\Wallet;
use Modules\Core\Helper\Trades\TimeAttribute;
use Modules\Seo\Entities\Seo;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Tags\HasTags;

class Member extends  Authenticate implements HasMedia
{
    use HasApiTokens,HasTags,HasMediaTrait,Sluggable,TimeAttribute,Notifiable;

    protected $guard='member';

    protected  $table='members';

    protected $fillable = [
        'first_name',
        'last_name',
        'status',
        'password',
        'country',
        'order',
        'is_active',
        'role',
        'token',
        'slug',
        'mobile',
        'email',
        'user',
        'role',
        'branch'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function seo()
    {
        return $this->morphOne(Seo::class, 'seoable');
    }
    public function wallet()
    {
        return $this->morphOne(Wallet::class, 'walletable');
    }

    public  function info(){
        return $this->morphOne(Info::class,'infoable');
    }

    public  function role(){
        return $this->hasOne(MemberRole::class,'id','role');
    }

    public  function professor(){
        return $this->hasOne(MemberRole::class,'id','role')->where('title','=','professor');
    }


    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'last_name'
            ]
        ];
    }


    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('medium')
            ->width(418)
            ->height(200)
            ->performOnCollections(config('cms.collection-image'));

    }
    public  function getRoleNameAttribute(){
        return __('cms.'.$this->role()->first()->title);
    }

    public  function getFullNameAttribute(){
        return $this->first_name." ".$this->last_name;
    }




}
