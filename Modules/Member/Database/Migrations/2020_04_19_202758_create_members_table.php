<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user');
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('branch');
            $table->foreign('branch')->references('id')->on('branches')->onDelete('cascade');
            $table->string('token')->unique();
            $table->string('username', 45)->unique()->nullable();
            $table->string('avatar')->nullable();
            $table->string('direction', 10)->default('rtl');
            $table->string('lang', 10)->default('fa');
            $table->string('slug')->nullable();
            $table->string('phone')->nullable();
            $table->string('postal_code',10)->nullable();
            $table->text('address')->nullable();
            $table->tinyInteger('is_private')->default(1);
            $table->tinyInteger('is_active')->default(0);
            $table->string('mobile',11)->unique()->nullable();;
            $table->string('email')->nullable()->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('country', 10)->nullable();
            $table->string('city')->nullable();
            $table->string('code', 6)->unique()->nullable();
            $table->string('password');
            $table->unsignedBigInteger('role');
            $table->foreign('role')->references('id')->on('member_roles')->onDelete('cascade');
            $table->integer('order')->default(1);
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->timestamp('code_expire')->nullable();
            $table->timestamp('last_connection')->nullable();
            $table->string('two_step')->nullable();
            $table->string('name')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->rememberToken();
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
