@include('core::layout.modules.index',[

    'title'=>__('member::members.index'),
    'items'=>$items,
    'parent'=>'member',
    'model'=>'member',
    'directory'=>'members',
    'collect'=>__('member::members.collect'),
    'singular'=>__('member::members.singular'),
    'create_route'=>['name'=>'members.create'],
    'edit_route'=>['name'=>'members.edit','name_param'=>'member'],
    'destroy_route'=>['name'=>'members.destroy','name_param'=>'member'],
    'search_route'=>true,
     'pagination'=>true,
    'datatable'=>[__('cms.thumbnail')=>'thumbnail',
    __('cms.first_name')=>'first_name',
    __('cms.last_name')=>'last_name',
    __('cms.email')=>'email',
    __('cms.mobile')=>'mobile',
    __('cms.role')=>'RoleName',
    __('cms.order')=>'order',
      __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
            __('cms.thumbnail')=>'thumbnail',
    __('cms.first_name')=>'first_name',
    __('cms.last_name')=>'last_name',
    __('cms.email')=>'email',
    __('cms.role')=>'RoleName',
    __('cms.order')=>'order',
    __('cms.slug')=>'slug',
        __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])





