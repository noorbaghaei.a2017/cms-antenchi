<?php

namespace Modules\Member\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MemberRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password'=>'required|min:8',
            'mobile'=>'nullable|unique:members,mobile,'.$this->token.',token',
            'email'=>'required|unique:members,email,'.$this->token.',token',
            'order'=>'required|numeric|min:1',
            'branch'=>'required',
            'role'=>'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
