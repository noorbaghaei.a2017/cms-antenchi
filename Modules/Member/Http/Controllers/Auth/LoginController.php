<?php

namespace Modules\Member\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/desk/panel';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:member')->except('logout');
        $this->loginRoute = route('member.login');
    }

    protected function guard()
    {
        return Auth::guard('member');
    }

    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|min:8',
        ]);
    }

    public function username()
    {
        $value=\request()->input('identify');

        if(filter_var($value,FILTER_VALIDATE_EMAIL)){
            $field='email';
        }
        elseif (preg_match('/(9)[0-9]{9}/',$value)){
            $field='mobile';
        }
        else{
            $field='username';
        }

        \request()->merge([$field=>$value]);

        return $field;
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('member::admin.auth.login');
    }

    public  function login(Request $request)
    {
        $this->validateLogin($request);

        if (Auth::guard('member')->attempt([$this->username() => $request->input('identify'), 'password' => $request->password,'is_active'=>2], $request->remember)) {

            $data=[
                'auth'=>\auth('member')->user()
            ];
            sendCustomEmail($data,'emails.front.welcome');

            return redirect()->intended(route('member.dashboard'));

        }
        return $this->sendFailedLoginResponse($request);


    }

    public function logout(Request $request){

        $data=[
            'auth'=>\auth('member')->user()
        ];

        $this->guard()->logout();

        $request->session()->invalidate();

        sendCustomEmail($data,'emails.admin.logout');

        return $this->loggedOut($request) ?: redirect(route('front.website'));

    }

    protected function loggedOut(Request $request)
    {
        //
    }



}
