<?php

namespace Modules\Sms\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Helper\Trades\TimeAttribute;

class Sms extends Model
{
    use TimeAttribute;

    protected  $table="smses";

    protected $fillable = ['provider','key','token','cash','status'];

    public  function getRouteKeyName()
    {
       return multiRouteKey();
    }


}
