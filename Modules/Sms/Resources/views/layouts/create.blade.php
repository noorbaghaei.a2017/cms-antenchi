@extends('core::layout.panel')
@section('pageTitle', 'ایجاد پیام رسان')
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h2>ایجاد پلن</h2>
                        <small>
                            با استفاده از فرم زیر میتوانید پیام رسان جدید اضافه کنید.
                        </small>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form role="form" method="post" action="{{route('smses.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">

                                <div class="col-sm-3">
                                    <label for="provider"  class="form-control-label">شرکت  </label>
                                    <select dir="rtl" class="form-control" name="provider" >

                                        @foreach(\Modules\Sms\library\SmsHelper::SmsProvider() as $key=>$value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach

                                    </select>

                                </div>
                                <div class="col-sm-4">
                                    <label for="provider"  class="form-control-label">وضعیت  </label>
                                    <label class="ui-switch data-ui-switch-md m-t-xs m-r">
                                        <input type="checkbox" checked name="status">
                                        <i></i>
                                    </label>
                                </div>

                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <label for="token" class="form-control-label">توکن  </label>
                                    <input type="text" name="token" class="form-control" id="token">
                                </div>
                            </div>
                            <div class="form-group row m-t-md">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-primary pull-right">ایجاد </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
