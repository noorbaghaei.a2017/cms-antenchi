<?php

namespace Modules\SpecialDiscount\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\SpecialDiscount\Entities\SpecialDiscount;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(SpecialDiscount::class)->delete();

        Permission::create(['name'=>'specialdiscount-list','model'=>SpecialDiscount::class,'created_at'=>now()]);
        Permission::create(['name'=>'specialdiscount-create','model'=>SpecialDiscount::class,'created_at'=>now()]);
        Permission::create(['name'=>'specialdiscount-edit','model'=>SpecialDiscount::class,'created_at'=>now()]);
        Permission::create(['name'=>'specialdiscount-delete','model'=>SpecialDiscount::class,'created_at'=>now()]);

    }
}
