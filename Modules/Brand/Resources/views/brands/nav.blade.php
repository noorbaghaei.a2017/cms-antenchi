

    <li>
        <a href="{{route('brands.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('brand.icons.brand')}}"></i>
                              </span>
            <span class="nav-text"> {{__('brand::brands.collect')}}</span>
        </a>
    </li>

