<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;



Route::group(["prefix"=>config('cms.prefix-admin'), "middleware" => ["auth:web"]], function () {
    Route::resource('/agents', 'AgentController')->only('create','store','destroy','update','index','edit');
    Route::get('agents/{lang}/{token}', 'AgentController@languageShow')->name('agent.language.show');
    Route::patch('agents/lang/update/{lang}/{token}', 'AgentController@languageUpdate')->name('agent.language.update');

    Route::group(["prefix"=>'search', "middleware" => ["auth:web"]], function () {
        Route::post('/agents', 'AgentController@search')->name('search.agent');
    });

});

