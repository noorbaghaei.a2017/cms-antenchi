@extends('core::layout.panel')
@section('pageTitle', __('cms.create'))
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h2>{{__('cms.create')}} </h2>
                        <small>
                           {{__('agent::agents.text-create')}}
                        </small>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
@include('core::layout.alert-danger')
                        <form id="signupForm" role="form" method="post" action="{{route('agents.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <span class="text-danger">*</span>
                                    <label for="name" class="form-control-label">{{__('cms.name')}}  </label>
                                    <input type="text" name="name" class="form-control" value="{{old('name')}}" id="name" required>
                                </div>
                                    <div class="col-sm-6">
                                        <label for="title" class="form-control-label">{{__('cms.thumbnail')}} </label>
                                        @include('core::layout.load-single-image')
                                    </div>

                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="firstname" class="form-control-label">{{__('cms.first_name')}}  </label>
                                    <input type="text" name="firstname" class="form-control" value="{{old('firstname')}}" id="firstname" required>
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="lastname" class="form-control-label">{{__('cms.last_name')}} </label>
                                    <input type="text" name="lastname" class="form-control" value="{{old('lastname')}}" id="lastname" required>
                                </div>



                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="order" class="form-control-label">{{__('cms.order')}} </label>
                                    <input type="number" name="order" value="{{old('order')}}" class="form-control" id="order">
                                </div>

                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="branch" class="form-control-label">{{__('cms.branch')}} </label>
                                    <input type="text" name="branch" value="{{old('branch')}}" class="form-control" id="branch">
                                </div>


                            </div>

                            <div class="form-group row">
                                <span class="text-danger">*</span>
                                <label for="text" class="form-control-label">{{__('cms.text')}} </label>
                                <div class="box m-b-md">
                                    <div class="box m-b-md">
                                        @include('core::layout.text-editor',['item'=>null])
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">

                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="mobile" class="form-control-label">{{__('cms.mobile')}} </label>
                                    <input type="text" name="mobile" value="{{old('mobile')}}" class="form-control" id="mobile" required>
                                    <small class="help-block text-warning">09195995044</small>
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="email" class="form-control-label">{{__('cms.email')}} </label>
                                    <input type="email" name="email" value="{{old('email')}}" class="form-control" id="email">
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="password" class="form-control-label">{{__('cms.password')}} </label>
                                    <input type="text" name="password"  class="form-control" id="password" >
                                </div>
                            </div>

                            @include('core::layout.modules.info-box',['info'=>null])

                            @include('core::layout.modules.seo-box',['seo'=>null])


                            <div class="form-group row m-t-md">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-primary btn-sm text-sm">{{__('cms.add')}} </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>

        $().ready(function() {
            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    firstname: {
                        required: true
                    },
                    lastname: {
                        required: true
                    },
                    mobile: {
                        required: true
                    },
                    email: {
                        required: true,
                        email:true
                    },
                    role: {
                        required: true
                    },
                    order: {
                        number: true,
                        required:true
                    },


                },
                messages: {
                    firstname:"نام الزامی است",
                    lastname: "نام خانوادگی  لزامی است",
                    mobile: "موبایل  الزامی است",
                    email: "ایمیل  الزامی است",
                    role: "نقش  الزامی است",
                    order: "فرمت نادرست",
                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

@endsection
