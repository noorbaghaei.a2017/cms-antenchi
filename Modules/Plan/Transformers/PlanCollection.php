<?php

namespace Modules\Plan\Transformers;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Modules\Plan\Entities\Plan;

class PlanCollection extends ResourceCollection
{
    public $collects = Plan::class;
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(
                function ( $item ) {
                    return new PlanResource($item);
                }
            ),
            'filed' => [
                'thumbnail','title','slug','status','price','update_date','create_date'
            ],
            'public_route'=>[

                [
                    'name'=>'plans.create',
                    'param'=>[null],
                    'icon'=>config('cms.icon.add'),
                    'title'=>__('cms.add'),
                    'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                    'method'=>'GET',
                ],
                [
                    'name'=>'plan.categories',
                    'param'=>[null],
                    'icon'=>config('cms.icon.categories'),
                    'title'=>__('cms.categories'),
                    'class'=>'btn btn-sm text-sm text-sm-center btn-warning pull-right',
                    'method'=>'GET',
                ]
            ],
            'private_route'=>
                [
                    [
                        'name'=>'plans.edit',
                        'param'=>[
                            'plan'=>'token'
                        ],
                        'icon'=>config('cms.icon.edit'),
                        'title'=>__('cms.edit'),
                        'class'=>'btn btn-warning btn-sm text-sm',
                        'modal'=>false,
                        'method'=>'GET',
                    ],
                    [
                        'name'=>'plans.destroy',
                        'param'=>[
                            'plan'=>'token'
                        ],
                        'icon'=>config('cms.icon.delete'),
                        'title'=>__('cms.delete'),
                        'class'=>'btn btn-danger btn-sm text-sm text-white',
                        'modal'=>true,
                        'modal_type'=>'destroy',
                        'method'=>'DELETE',
                    ],
                    [
                        'name'=>'plans.detail',
                        'param'=>[null],
                        'icon'=>config('cms.icon.detail'),
                        'title'=>__('cms.detail'),
                        'class'=>'btn btn-primary btn-sm text-sm text-white',
                        'modal'=>true,
                        'modal_type'=>'detail',
                        'method'=>'GET',
                    ]
                ],
            'search_route'=>[

                'name'=>'search.plan',
                'filter'=>[
                    'title','slug'
                ],
                'icon'=>config('cms.icon.add'),
                'title'=>__('cms.add'),
                'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                'method'=>'POST',

            ],
            'count' => $this->collection->count(),
            'links' => [
                'self' => 'link-value',
            ],
            'collect'=>__('plan::plans.collect'),
            'title'=>__('plan::plans.index'),
        ];
    }
}
