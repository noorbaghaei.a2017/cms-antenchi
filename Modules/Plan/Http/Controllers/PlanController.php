<?php

namespace Modules\Plan\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Mockery\Exception;
use Modules\Advertising\Entities\Period;
use Modules\Core\Entities\Category;
use Modules\Core\Entities\Currency;
use Modules\Core\Http\Controllers\HasCategory;
use Modules\Core\Http\Controllers\HasQuestion;
use Modules\Core\Http\Requests\CategoryRequest;
use Modules\Plan\Entities\Plan;
use Modules\Plan\Entities\Repository\PlanRepositoryInterface;
use Modules\Plan\Http\Requests\PlanRequest;
use Modules\Plan\Transformers\PlanCollection;
use Modules\Question\Entities\Question;
use Modules\Question\Http\Requests\QuestionRequest;


class PlanController extends Controller
{
    use HasQuestion,HasCategory;
    protected $entity;

    protected $class;

    private $repository;

    //category

    protected $route_categories_index='plan::categories.index';
    protected $route_categories_create='plan::categories.create';
    protected $route_categories_edit='plan::categories.edit';
    protected $route_categories='plan.categories';


//question

    protected $route_questions_index='plan::questions.index';
    protected $route_questions_create='plan::questions.create';
    protected $route_questions_edit='plan::questions.edit';
    protected $route_questions='plans.index';


//notification

    protected $notification_store='plan::plans.store';
    protected $notification_update='plan::plans.update';
    protected $notification_delete='plan::plans.delete';
    protected $notification_error='plan::plans.error';

    public function __construct(PlanRepositoryInterface $repository)
    {
        $this->entity=new Plan();

        $this->class=Plan::class;

        $this->repository=$repository;

        $this->middleware('permission:plan-list')->only('index');
        $this->middleware('permission:plan-create')->only(['create','store']);
        $this->middleware('permission:plan-edit' )->only(['edit','update']);
        $this->middleware('permission:plan-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->repository->getAll();

            $plans = new PlanCollection($items);

            $data= collect($plans->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $currencies=Currency::latest()->get();
            $periods=Period::latest()->get();
            $categories=Category::latest()->where('model',Plan::class)->get();
            return view('plan::plans.create',compact('categories','currencies','periods'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }

    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
            !isset($request->title) &&
            !isset($request->slug)

            ){

                $items=$this->repository->getAll();

                $result = new PlanCollection($items);

                $data= collect($result->response()->getData())->toArray();

                return view('core::response.index',compact('data'));
            }
            $items=$this->entity
                ->where("title",trim($request->title))
                ->where("slug",trim($request->slug))
                ->paginate(config('cms.paginate'));

            $result = new PlanCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param PlanRequest $request
     * @return Response
     */
    public function store(PlanRequest $request)
    {
        try {
            DB::beginTransaction();
            $attributes=($request->has('attributes')) ? json_encode($request->input('attributes')) : null ;
            $notifications=($request->has('notifications')) ? $request->input('notifications') : [] ;

            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->status=$request->input('status');
            $this->entity->special=$request->input('special');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->attributes=$attributes;
            $this->entity->text=$request->input('text');
            $this->entity->time_limit=$request->input('time_limit');
            $this->entity->number_limit=$request->input('number_limit');
            $this->entity->number_limit_special=$request->input('number_limit_special');
            $this->entity->order=orderInfo($request->input('order'));
            $this->entity->period=Period::whereToken($request->input('period'))->first()->id;
            $this->entity->icon=$request->input('icon');
            $this->entity->token=tokenGenerate();

            $saved=$this->entity->save();

            $this->entity->price()->create([
                'amount'=>$request->input('price'),
            ]);

            $this->entity->discount()->create([
                'title'=>$request->input('title'),
                'amount'=>$request->input('amount'),
                'percentage'=>$request->input('percentage'),
                'start_at'=>now()
            ]);

            $this->entity->currency()->create([
                'currency'=>Currency::whereToken($request->input('currency'))->first()->id,
            ]);
           if($request->notifications){
               $this->entity->notification()->create([
                   'sms'=>in_array('sms',$notifications) ? 1: 0,
                   'call'=>in_array('call',$notifications) ? 1: 0,
                   'email'=>in_array('email',$notifications) ? 1: 0,
               ]);
           }else{
               $this->entity->notification()->create([
                   'sms'=>0,
                   'call'=> 0,
                   'email'=> 0,
               ]);

           }

            if(!$saved){
                DB::rollBack();
                return redirect()->back()->with('error',__('plan::plans.error'));
            }else{
                DB::commit();
                return redirect(route("plans.index"))->with('message',__('plan::plans.store'));
            }


        }catch (Exception $exception){
            DB::rollBack();
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the specified resource.
     * @param $token
     * @return Response
     */
    public function show($token)
    {
        try {
            $item=$this->entity->whereToken($token)->first();
            return view('plan::plans.show',compact('item'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return Response
     */
    public function edit($token)
    {
        try {
            $periods=Period::latest()->get();
            $categories=Category::latest()->where('model',Plan::class)->get();
            $currencies=Currency::latest()->get();

             $item=$this->entity->with('discount','notification','price','currency')->whereToken($token)->firstOrFail();
            return view('plan::plans.edit',compact('item','categories','currencies','periods'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param PlanRequest $request
     * @param $token
     * @return void
     */
    public function update(Request $request, $token)
    {
        try {
            DB::beginTransaction();
            $validator=Validator::make($request->all(),[
                'title'=>'required|unique:plans,title,'.$token.',token',
                'excerpt'=>'required',
                'time_limit'=>'required|numeric|max:30',
                'number_limit'=>'required',
                'price'=>'required',
                'period'=>'required',
                'category'=>'required'
            ]);


             $this->entity=$this->entity->whereToken($token)->firstOrFail();

            $attributes=($request->has('attributes')) ? json_encode($request->input('attributes')) : null ;
            $notifications=($request->has('notifications')) ? $request->input('notifications') : [] ;

            $updated=$this->entity->update([
                "user"=>auth('web')->user()->id,
                "title"=>$request->input('title'),
                "slug"=>null,
                "excerpt"=>$request->input('excerpt'),
                "status"=>$request->input('status'),
                "special"=>$request->input('special'),
                "time_limit"=>$request->input('time_limit'),
                "number_limit"=>$request->input('number_limit'),
                "number_limit_special"=>$request->input('number_limit_special'),
                "attributes"=>$attributes,
                "text"=>$request->input('text'),
                "period"=>Period::whereToken($request->input('period'))->first()->id,
                "order"=>orderInfo($request->input('order')),
                "icon"=>$request->input('icon'),
            ]);

            $this->entity->price()->update([
                'amount'=>$request->input('price'),
            ]);

            $this->entity->discount()->create([
                'title'=>$request->input('title-discount'),
                'amount'=>$request->input('amount-discount'),
                'percentage'=>$request->input('percentage-discount'),
                'start_at'=>now()
            ]);

            $this->entity->currency()->update([
                'currency'=>Currency::whereToken($request->input('currency'))->first()->id,
            ]);
            $this->entity->notification()->update([
                'sms'=>in_array('sms',$notifications) ? 1: 0,
                'call'=>in_array('call',$notifications) ? 1: 0,
                'email'=>in_array('email',$notifications) ? 1: 0,
            ]);
            $this->entity->replicate();

            if(!$updated){
                DB::rollBack();
                return redirect()->back()->with('error',__('plan::plans.error'));
            }else{
                DB::commit();
                return redirect(route("plans.index"))->with('message',__('plan::plans.update'));
            }


        }catch (\Exception $exception){
            DB::rollBack();
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param $token
     * @return void
     */
    public function destroy($token)
    {
        try {
            DB::beginTransaction();
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('plan::plans.error'));
            }else{
                return redirect(route("plans.index"))->with('message',__('plan::plans.delete'));
            }


        }catch (\Exception $exception){
            DB::rollBack();
            sendMailErrorController($exception);
            return abort('500');
        }
    }


}
