@include('core::layout.modules.category.edit',[

    'title'=>__('core::categories.create'),
    'item'=>$item,
    'parent'=>'gallery',
    'model'=>'gallery',
    'directory'=>'galleries',
    'collect'=>__('core::categories.collect'),
    'singular'=>__('core::categories.singular'),
   'update_route'=>['name'=>'gallery.category.update','param'=>'category'],

])








