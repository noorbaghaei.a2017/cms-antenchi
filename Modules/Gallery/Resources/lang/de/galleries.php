<?php
return [
    "text-create"=>"you can create your galleries",
    "text-edit"=>"you can edit your galleries",
    "store"=>"Store Success",
    "error"=>"Store UnSuccess",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"galleries list",
    "singular"=>"gallery",
    "collect"=>"galleries",
    "permission"=>[
        "galleries-full-access"=>"galleries full access",
        "galleries-list"=>"galleries list",
        "galleries-delete"=>"galleries delete",
        "galleries-create"=>"galleries create",
        "galleries-edit"=>"edit galleries",
    ]
];
