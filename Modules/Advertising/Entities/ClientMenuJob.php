<?php

namespace Modules\Advertising\Entities;


use Modules\Core\Helper\Trades\TimeAttribute;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Translate;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class ClientMenuJob extends Model implements HasMedia
{
    use HasMediaTrait,TimeAttribute;
   
    protected $table = 'client_menu_job';
    protected $fillable = ['id','client','menu','service','attribute','title','text','price','excerpt','token'];


    public function user_service()
    {
        return $this->belongsTo(UserServices::class, 'id');
    }

    public function translates()
    {
        return $this->morphMany(Translate::class, 'translateable');
    }
    

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('medium')
            ->width(418)
            ->height(200)
            ->performOnCollections(config('cms.collection-image'));

        $this->addMediaConversion('thumb')
            ->width(70)
            ->height(60)
            ->performOnCollections(config('cms.collection-image'));
    }
  
}
