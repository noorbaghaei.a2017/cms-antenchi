<?php


namespace Modules\Advertising\Entities\Repository;


use Modules\Advertising\Entities\TypeAdvertising;

class TypeAdvertisingRepository implements TypeAdvertisingRepositoryInterface
{

    public function getAll()
    {
       return TypeAdvertising::latest()->get();
    }
}
