<?php

namespace Modules\Advertising\Entities\Repository;


use Modules\Core\Entities\ListServices;


class CategoryJobRepository  implements CategoryJobRepositoryInterface
{
    public function getAll()
    {
        return ListServices::latest()->get();
    }
}
