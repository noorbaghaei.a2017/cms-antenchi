<?php

namespace Modules\Advertising\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Helper\Trades\TimeAttribute;

class Skill extends Model
{

    use Sluggable,TimeAttribute;

    protected $fillable = ['parent','level','icon','slug','excerpt','token','order','user','title','symbol','status'];

    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

}
