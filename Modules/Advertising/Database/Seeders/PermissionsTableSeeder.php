<?php

namespace Modules\Advertising\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Advertising\Entities\Advertising;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(Advertising::class)->delete();

        Permission::create(['name'=>'advertising-list','model'=>Advertising::class,'created_at'=>now()]);
        Permission::create(['name'=>'advertising-create','model'=>Advertising::class,'created_at'=>now()]);
        Permission::create(['name'=>'advertising-edit','model'=>Advertising::class,'created_at'=>now()]);
        Permission::create(['name'=>'advertising-delete','model'=>Advertising::class,'created_at'=>now()]);
    }
}
