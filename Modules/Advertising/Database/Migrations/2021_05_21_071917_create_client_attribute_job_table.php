<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientAttributeJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_attribute_job', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('client');
            $table->foreign('client')->references('id')->on('clients')->onDelete('cascade');
            $table->unsignedBigInteger('service');
            $table->foreign('service')->references('id')->on('user_services')->onDelete('cascade');
            $table->unsignedBigInteger('attribute');
            $table->foreign('attribute')->references('id')->on('attribute_job')->onDelete('cascade');
            $table->string('title')->nullable();
            $table->string('excerpt')->nullable();
            $table->text('text')->nullable();
            $table->string('token')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_attribute_job');
    }
}
