<?php

namespace Modules\Advertising\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JobStoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required',
            'client'=>'required',
            'text'=>'required',
            'excerpt'=>'required',
            'email'=>'required',
            'address'=>'required',
            'mobile'=>'required',
            'phone'=>'required',
            'postal_code'=>'required',
            'city'=>'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
