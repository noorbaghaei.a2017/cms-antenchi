<?php

namespace Modules\Advertising\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryJobRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|unique:list_services,title,'.$this->token.',token',
            'image'=>'mimes:jpeg,png,jpg|max:2000',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
