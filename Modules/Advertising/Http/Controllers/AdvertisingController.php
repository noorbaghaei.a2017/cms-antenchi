<?php

namespace Modules\Advertising\Http\Controllers;

use App\Events\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Modules\Advertising\Entities\Advertising;
use Modules\Advertising\Entities\Education;
use Modules\Advertising\Entities\Experience;
use Modules\Advertising\Entities\Guild;
use Modules\Advertising\Entities\Period;
use Modules\Advertising\Entities\Position;
use Modules\Advertising\Entities\Repository\AdvertisingRepositoryInterface;
use Modules\Advertising\Entities\Salary;
use Modules\Advertising\Entities\Skill;
use Modules\Advertising\Entities\TypeAdvertising;
use Modules\Advertising\Http\Requests\AdvertisingRequest;
use Modules\Advertising\Transformers\AdvertisingCollection;
use Modules\Client\Entities\Client;
use Modules\Core\Entities\Category;
use Modules\Core\Entities\City;
use Modules\Core\Entities\Country;
use Modules\Core\Entities\Currency;
use Modules\Core\Entities\State;
use Modules\Core\Http\Controllers\HasCategory;
use Modules\Core\Http\Controllers\HasGallery;
use Modules\Core\Http\Controllers\HasQuestion;
use Modules\Plan\Entities\Plan;
use Modules\Plan\Helper\PlanHelper;
use Modules\Service\Entities\Advantage;

class AdvertisingController extends Controller
{
    use HasQuestion,HasCategory,HasGallery;

    protected $entity;
    protected $class;
    protected $query;
    private $repository;


//category

    protected $route_categories_index='advertising::categories.index';
    protected $route_categories_create='advertising::categories.create';
    protected $route_categories_edit='advertising::categories.edit';
    protected $route_categories='advertising.categories';


//question

    protected $route_questions_index='advertising::questions.index';
    protected $route_questions_create='advertising::questions.create';
    protected $route_questions_edit='advertising::questions.edit';
    protected $route_questions='advertising.index';


//gallery

    protected $route_gallery_index='advertising::advertisings.gallery';
    protected $route_gallery='advertisings.index';

//notification

    protected $notification_store='advertising::advertisings.store';
    protected $notification_update='advertising::advertisings.update';
    protected $notification_delete='advertising::advertisings.delete';
    protected $notification_error='advertising::advertisings.error';





    public function __construct(AdvertisingRepositoryInterface $repository)
    {
      
        $this->entity=new Advertising();
        $this->class=Advertising::class;
        $this->query= Advertising::query();
        $this->middleware('permission:advertising-list');
        $this->middleware('permission:advertising-create')->only(['create','store']);
        $this->middleware('permission:advertising-edit' )->only(['edit','update']);
        $this->middleware('permission:advertising-delete')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        try {

            $items=$this->repository->getAll();

            $result = new AdvertisingCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $employers=Client::with('role')->whereHas('role',function ($query) {
                $query->where('title', '=', 'employer');

            });
            $categories=Category::latest()->where('model',Advertising::class)->get();
            $currencies=Currency::latest()->get();
            $positions=Position::latest()->get();
            $educations=Education::latest()->get();
            $types=TypeAdvertising::latest()->get();
            $skills=Skill::latest()->get();
            $salaries=Salary::latest()->get();
            $experiences=Experience::latest()->get();
            $guilds=Guild::latest()->get();
            $plans=Plan::latest()->get();
            return view('advertising::advertisings.create',compact('categories','employers','plans','currencies','positions','types','educations','salaries','experiences','guilds','skills'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
            !isset($request->title) &&
            !isset($request->slug)
            ){
                $items=$this->repository->getAll();

                $result = new AdvertisingCollection($items);

                $data= collect($result->response()->getData())->toArray();

                return view('core::response.index',compact('data'));
            }
            $items=$this->entity
                ->where("title",'LIKE','%'.trim($request->title).'%')
                ->where("slug",'LIKE','%'.trim($request->slug).'%')
                ->paginate(config('cms.paginate'));
            $result = new AdvertisingCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param AdvertisingRequest $request
     * @return Response
     */
    public function store(AdvertisingRequest $request)
    {
        try {
            DB::beginTransaction();
            $client=Client::whereToken($request->input('employer'))->firstOrFail();
            $category=Category::whereToken($request->input('category'))->firstOrFail();
            $currency=Currency::whereToken($request->input('currency'))->firstOrFail();
            $plan=Plan::whereToken($request->input('plan'))->first();

            $this->entity->title=$request->input('title');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->number_employees=$request->input('number_employees');
            $this->entity->salary=$request->input('salary');
            $this->entity->work_experience=$request->input('experience');
            $this->entity->job_position=json_encode($request->input('positions'));
            $this->entity->status=$request->input('status');
            $this->entity->country=Country::find($request->input('country'))->name;
            $this->entity->city=City::find($request->input('city'))->name;
            $this->entity->state=State::find($request->input('state'))->name;
            $this->entity->education=json_encode($request->input('educations'));
            $this->entity->guild=$request->input('guild');
            $this->entity->kind=json_encode($request->input('kinds'));
            $this->entity->skill=json_encode($request->input('skills'));
            $this->entity->gender=$request->input('gender');
            $this->entity->text=$request->input('text');
            $this->entity->currency=$currency->id;
            $this->entity->plan=$plan->id;
            $this->entity->expire=now()->addDays($request->input('time_limit'));
            $this->entity->category=$category->id;
            $this->entity->client=$client->id;

            $this->entity->token=tokenGenerate();

            $this->entity->save();


            $this->entity->seo()->create([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>(is_null(json_encode($request->input('robots'))) ? [] : json_encode($request->input('robots'))),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);

             if(!is_null($plan->price->amount) || $plan->price->amount!=0)
             {
                 $client->plan()->create([
                     'count'=>$plan->number_limit - 1,
                     'special_count'=>($plan->number_limit_special==0) ? 0 :  $plan->number_limit_special - 1,
                     'plan'=>$plan->id,
                     'start'=>now(),
                     'expire'=>PlanHelper::periodCalc($plan->period),
                 ]);

                 $order=\Modules\Order\Entities\Order::create([
                     'order_id'=>Carbon::now()->timestamp + 323 ,
                     'total_price'=>$plan->price->amount ,
                     'status'=> 1
                 ]);
             }
             else{
                 $client->plan()->create([
                     'count'=>$plan->number_limit - 1,
                     'special_count'=>($plan->number_limit_special==0) ? 0 :  $plan->number_limit_special - 1,
                     'plan'=>$plan->id,
                     'start'=>now(),
                     'expire'=>PlanHelper::periodCalc($plan->period),
                 ]);

                 $order=\Modules\Order\Entities\Order::create([
                     'order_id'=>Carbon::now()->timestamp + 323 ,
                     'total_price'=>0 ,
                     'status'=> 1
                 ]);
             }


            $client->payment()->create([
                'title'=> "به صورت دستی",
                'token'=> tokenGenerate(),
                'order'=> $order->id,
                'status'=> 1
            ]);


            $this->entity->analyzer()->create();

            $this->entity->attachTags($request->input('tags'));

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if(!$this->entity){
                DB::rollBack();
                return redirect()->back()->with('error',__('advertising::advertisings.error'));
            }else{
                DB::commit();
                return redirect(route("advertisings.index"))->with('message',__('advertising::advertisings.store'));
            }
        }catch (\Exception $exception){
            DB::rollBack();
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return Response
     */
    public function edit($token)
    {
        try {
            $categories=Category::latest()->where('model',Advertising::class)->get();
            $employers=Client::latest()->whereHas('role',function ($query){
                    $query->where('title', '=', 'employer');
                });
            $item=$this->entity->with('tags')->whereToken($token)->first();
            $plans=Plan::latest()->where('status',1)->get();
            $positions=Position::latest()->get();
            $currencies=Currency::latest()->get();
            $educations=Education::latest()->get();
            $types=TypeAdvertising::latest()->get();
            $skills=Skill::latest()->get();
            $salaries=Salary::latest()->get();
            $guilds=Guild::latest()->get();
            $experiences=Experience::latest()->get();
            return view('advertising::advertisings.edit',compact('item','categories','employers','plans','positions','educations','types','salaries','experiences','guilds','currencies','skills'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $token
     * @return Response
     */
    public function update(Request $request, $token)
    {
        try {
            DB::beginTransaction();
            $validator=Validator::make($request->all(),[
                'title'=>'required|string',
                'text'=>'required|string',
                'excerpt'=>'required|string',
                'image'=>'mimes:jpeg,png,jpg|max:2000',
                'kinds'=>'required',
                'skills'=>'required',
                'number_employees'=>'required|numeric',
                'category'=>'required',
                'salary'=>'required',
                'experience'=>'required',
                'positions'=>'required',
                'educations'=>'required',
                'guild'=>'required',
                'currency'=>'required',
                'gender'=>'required',
            ]);
            if($validator->fails()){
                return  redirect()->back()->withErrors($validator);
            }



            $category=Category::whereToken($request->input('category'))->firstOrFail();
            $currency=Currency::whereToken($request->input('currency'))->firstOrFail();

            $this->entity=$this->entity->whereToken($token)->firstOrFail();

            $updated=$this->entity->update([
                'user'=>auth('web')->user()->id,
                "slug"=>null,
                "title"=>$request->input('title'),
                "category"=>$category->id,
                "excerpt"=>$request->input('excerpt'),
                "text"=>$request->input('text'),
                "number_employees"=>$request->input('number_employees'),
                "salary"=>$request->input('salary'),
                "city"=>City::find($request->input('city'))->name,
                "state"=>State::find($request->input('state'))->name,
                "work_experience"=>$request->input('experience'),
                "job_position"=>json_encode($request->input('positions')),
                "status"=>$request->input('status'),
                "country"=>Country::find($request->input('country'))->name,
                "education"=>json_encode($request->input('educations')),
                "guild"=>$request->input('guild'),
                "skill"=>json_encode($request->input('skills')),
                "gender"=>$request->input('gender'),
                "currency"=>$currency->id,
            ]);



            $this->entity->seo()->update([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>(is_null(json_encode($request->input('robots'))) ? [] : json_encode($request->input('robots'))),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);



            $this->entity->attachTags($request->input('tags'));

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if(!$updated){
                DB::rollBack();
                return redirect()->back()->with('error',__('advertising::advertisings.error'));
            }else{
                DB::commit();
                return redirect(route("advertisings.index"))->with('message',__('advertising::advertisings.update'));
            }
        }catch (\Exception $exception){
            DB::rollBack();
            sendMailErrorController($exception);
            return abort('500');
        }
    }

}
