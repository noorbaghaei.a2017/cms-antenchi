<?php

namespace Modules\Advertising\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Advertising\Http\Requests\JobStoreRequest;
use Modules\Advertising\Http\Requests\JobUpdateRequest;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\UserServices;
use Modules\Core\Entities\ListServices;
use Modules\Client\Entities\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use \Maatwebsite\Excel\Facades\Excel;
use App\Imports\JobImport;

class JobController extends Controller
{

    protected $entity;
    protected $class;

    public function __construct()
    {
        $this->entity=new UserServices();
        $this->class=UserServices::class;
      
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {

            $items=UserServices::with('user_info')->paginate(config('cms.paginate'));

            return view('advertising::jobs.index',compact('items'));

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    
    public function approved($id)
    {
        try {

            $item=UserServices::findOrFail($id);

            $item->update([
                'status'=>1
            ]);

            $items=UserServices::with('user_info')->paginate(config('cms.paginate'));

            return view('advertising::jobs.index',compact('items'));

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            $clients=Client::latest()->get();
            $list_services=ListServices::latest()->get();
            return view('advertising::jobs.create',compact('clients','list_services'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(JobStoreRequest $request)
    {
        try {
            DB::beginTransaction();
            
            $this->entity->title=$request->input('title');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->address=$request->input('address');
            $this->entity->mobile=$request->input('mobile');
            $this->entity->phone=$request->input('phone');
            $this->entity->postal_code=$request->input('postal_code');
            $this->entity->city=$request->input('city');
            $this->entity->service=$request->input('list_services');
            $this->entity->status=1;
            $this->entity->country='german';
            $this->entity->google_map='';
            $this->entity->email=$request->input('email');
            $this->entity->user=Client::whereToken($request->input('client'))->first()->id;
            $this->entity->text=$request->input('text');
            $this->entity->token=tokenGenerate();

            $saved=$this->entity->save();

            $saved->info()->create();

            $this->entity->analyzer()->create();
           

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if(!$saved){
                DB::rollBack();
                return redirect()->back()->with('error',__('advertising::jobs.error'));
            }else{
                DB::commit();
                return redirect(route("jobs.index"))->with('message',__('advertising::jobs.store'));
            }
        }catch (Exception $exception){
            DB::rollBack();
            sendMailErrorController($exception);
            return abort('500');
        }

    }

    

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('advertising::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($token)
    {
        try {
           
            $item=UserServices::whereToken($token)->first();
            $list_services=ListServices::latest()->get();
            return view('advertising::jobs.edit',compact('item','list_services'));
        }catch (\Exception $exception){
          
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(JobUpdateRequest $request, $token)
    {
        try {
            DB::beginTransaction();
            $this->entity=UserServices::whereToken($token)->firstOrFail();
            $updated=$this->entity->update([
               
                "slug"=>null,
                "title"=>$request->input('title'),
                "excerpt"=>$request->input('excerpt'),
                "text"=>$request->input('text'),
                "service"=>$request->input('list_services'),
                "email"=>$request->input('email'),
                "mobile"=>$request->input('mobile'),
                "phone"=>$request->input('phone'),
                "address"=>$request->input('address'),
                "postal_code"=>$request->input('postal_code'),
                "city"=>$request->input('city'),
            ]);
            $this->entity->replicate();

            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

           

            
            if(!$updated){
                DB::rollBack();
                return redirect()->back()->with('error',__('advertising::jobs.error'));
            }else{
                DB::commit();
                return redirect(route("jobs.index"))->with('message',__('advertising::jobs.update'));
            }


        }catch (\Exception $exception){
            return dd($exception);
            DB::rollBack();
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
    public  function languageShow(Request $request,$lang,$token){
        $item=UserServices::with('translates')->where('token',$token)->first();
        return view('advertising::jobs.language',compact('item','lang'));

    }
    public  function languageUpdate(Request $request,$lang,$token){

        DB::beginTransaction();
        $item=UserServices::with('translates')->where('token',$token)->first();
        if( $item->translates->where('lang',$lang)->first()){
            $changed=$item->translates->where('lang',$lang)->first()->update([
                'title'=>$request->title,
                'text'=>$request->text,
                'excerpt'=>$request->excerpt
            ]);
        }else{
            $changed=$item->translates()->create([
                'title'=>$request->title,
                'text'=>$request->text,
                'excerpt'=>$request->excerpt,
                'lang'=>$lang
            ]);
        }


        if(!$changed){
            DB::rollBack();
            return redirect()->back()->with('error',__('advertising::job.error'));
        }else{
            DB::commit();
            return redirect(route("jobs.index"))->with('message',__('advertising::jobs.update'));
        }

    }

    public function importFile(Request $request){

        try{
            Excel::import(new JobImport,$request->file);
            return redirect(route("jobs.index"))->with('message',__('advertising::jobs.update'));
        }catch(\Exception $exception){
            return redirect()->back()->with('error',__('advertising::job.error'));
        }
       
       
    }
}
