<?php

namespace Modules\Advertising\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\ListServices;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Modules\Advertising\Transformers\CategoryJobCollection;
use Modules\Advertising\Entities\Repository\CategoryJobRepositoryInterface;
use Modules\Advertising\Http\Requests\CategoryJobRequest;
use \Maatwebsite\Excel\Facades\Excel;
use Modules\Core\Http\Controllers\HasGallery;
use App\Imports\CategoryJobImport;

class CategoryJobController extends Controller
{
    use HasGallery;

    protected $entity;

    protected $class;

    protected $query;

    private  $repository;

    //gallery

    protected $route_gallery_index='advertising::category_jobs.gallery';
    protected $route_gallery='categoryjobs.index';

    //notification

    protected $notification_store='advertising::categoryjobs.store';
    protected $notification_update='advertising::categoryjobs.update';
    protected $notification_delete='advertising::categoryjobs.delete';
    protected $notification_error='advertising::categoryjobs.error';

    public function __construct()
    {
        $this->entity=new ListServices();
        $this->class=ListServices::class;
        $this->query= ListServices::query();
    
      
        $this->middleware('permission:advertising-list');
        $this->middleware('permission:advertising-create')->only(['create','store']);
        $this->middleware('permission:advertising-edit' )->only(['edit','update']);
        $this->middleware('permission:advertising-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
  
        try {

            $items=ListServices::paginate(config('cms.paginate'));

            return view('advertising::category_jobs.index',compact('items'));

        }catch (\Exception $exception){
            
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
           
            $listcategories=ListServices::whereParent(0)->get();
            return view('advertising::category_jobs.create',compact('listcategories'));
        }catch (\Exception $exception){
            // sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(CategoryJobRequest $request)
    {
        try {
           
            DB::beginTransaction();
        
           
            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=$this->entity->whereToken($request->input('parent'))->first();
            }

            $this->entity->title=$request->input('title');

            $this->entity->icon=$request->input('icon');

            $this->entity->parent=($request->input('parent')==-1) ? 0: $parent->id;

            $this->entity->pattern=$request->input('pattern');

            $this->entity->status=$request->input('status');

            $this->entity->status=$request->input('order');
           
            $this->entity->token=tokenGenerate();

            $this->entity->save();

           
            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if(!$this->entity){
                DB::rollBack();
                return redirect()->back()->with('error',__('advertising::category_jobs.error'));
            }else{
                DB::commit();
                return redirect(route("categoryjobs.index"))->with('message',__('advertising::category_job.store'));
            }
        }catch (\Exception $exception){
            DB::rollBack();
          return dd($exception);
            return abort('500');
        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($token)
    {
        try {
            $item=ListServices::whereToken($token)->firstOrFail();
            $listcategories=ListServices::whereParent(0)->where('token','!=',$token)->get();
            return view('advertising::category_jobs.edit',compact('item','listcategories'));
        }catch (\Exception $exception){
       
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $token)
    {
        try {
            DB::beginTransaction();

            $validator=Validator::make($request->all(),[
                'title'=>'required|string',
                'image'=>'mimes:jpeg,png,jpg|max:2000',
            ]);
            if($validator->fails()){
                return  redirect()->back()->withErrors($validator);
            }
            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=$this->entity->whereToken($request->input('parent'))->first();
            }


            $this->entity=$this->entity->whereToken($token)->firstOrFail();

            $updated=$this->entity->update([
                "title"=>$request->input('title'),
                "icon"=>$request->input('icon'),
                "parent"=>($request->input('parent')==-1) ? 0: $parent->id,
                "pattern"=>$request->input('pattern'),
                "order"=>$request->input('order'),
                "status"=>$request->input('status'),
                "slug"=>null,
            ]);

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if(!$updated){
                DB::rollBack();
                return redirect()->back()->with('error',__('advertising::category_jobs.error'));
            }else{
                DB::commit();
                return redirect(route("categoryjobs.index"))->with('message',__('advertising::category_job.update'));
            }
        }catch (\Exception $exception){
            DB::rollBack();
           
            return abort('500');
        }
    }

     /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
                !isset($request->title) 
            ){

                $items=$this->repository->getAll();

                $result = new CategoryJobCollection($items);

                $data= collect($result->response()->getData())->toArray();

                return view('core::response.index',compact('data'));
            }
             $items=$this->entity
                ->where("title",'LIKE','%'.trim($request->title).'%')
                ->paginate(config('cms.paginate'));
            $result = new CategoryJobCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }


    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public  function languageShow(Request $request,$lang,$token){
        $item=ListServices::with('translates')->where('token',$token)->first();
        return view('advertising::category_jobs.language',compact('item','lang'));

    }
    public  function languageUpdate(Request $request,$lang,$token){

        DB::beginTransaction();
        $item=ListServices::with('translates')->where('token',$token)->first();
        if( $item->translates->where('lang',$lang)->first()){
            $changed=$item->translates->where('lang',$lang)->first()->update([
                'title'=>$request->title
            ]);
        }else{
            $changed=$item->translates()->create([
                'title'=>$request->title,
                'lang'=>$lang
            ]);
        }


        if(!$changed){
            DB::rollBack();
            return redirect()->back()->with('error',__('advertising::category_job.error'));
        }else{
            DB::commit();
            return redirect(route("categoryjobs.index"))->with('message',__('advertising::category_job.update'));
        }

    }
    public function importFile(Request $request){

        try{
            Excel::import(new CategoryJobImport,$request->file);
            return redirect(route("categoryjobs.index"))->with('message',__('advertising::category_job.update'));
        }catch(\Exception $exception){
         
            
            return redirect()->back()->with('error',__('advertising::category_job.error'));
        }
       
       
    }
}
