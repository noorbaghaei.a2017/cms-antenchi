<?php

namespace Modules\Advertising\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Advertising\Entities\MenuJob;
use Modules\Advertising\Http\Requests\MenuJobRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MenuJobController extends Controller
{
    public function __construct()
    {
        $this->entity=new MenuJob();
        $this->class=MenuJob::class;
      
        $this->middleware('permission:advertising-list');
        $this->middleware('permission:advertising-create')->only(['create','store']);
        $this->middleware('permission:advertising-edit' )->only(['edit','update']);
        $this->middleware('permission:advertising-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {

            $items=MenuJob::paginate(config('cms.paginate'));

            return view('advertising::menu_jobs.index',compact('items'));

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
           
            return view('advertising::menu_jobs.create');
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(MenuJobRequest $request)
    {
        try {
            DB::beginTransaction();
        

            $this->entity->title=$request->input('title');

            $this->entity->icon=$request->input('icon');
           
            $this->entity->token=tokenGenerate();

            $this->entity->save();

           
            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if(!$this->entity){
                DB::rollBack();
                return redirect()->back()->with('error',__('advertising::menu_jobs.error'));
            }else{
                DB::commit();
                return redirect(route("menujobs.index"))->with('message',__('advertising::menu_job.store'));
            }
        }catch (\Exception $exception){
          
            DB::rollBack();
            sendMailErrorController($exception);
            return abort('500');
        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($token)
    {
        try {
            $item=MenuJob::whereToken($token)->firstOrFail();
            return view('advertising::menu_jobs.edit',compact('item'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $token)
    {
        try {
            DB::beginTransaction();
            $this->entity=MenuJob::whereToken($token)->firstOrFail();
            $updated=$this->entity->update([
               
                "slug"=>null,
                "title"=>$request->input('title'),
                "icon"=>$request->input('icon'),
              
            ]);
            $this->entity->replicate();

            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            
            if(!$updated){
                DB::rollBack();
                return redirect()->back()->with('error',__('advertising::menujobs.error'));
            }else{
                DB::commit();
                return redirect(route("menujobs.index"))->with('message',__('advertising::menujobs.update'));
            }


        }catch (\Exception $exception){
            return dd($exception);
            DB::rollBack();
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
    public  function languageShow(Request $request,$lang,$token){
        $item=MenuJob::with('translates')->where('token',$token)->first();
        return view('advertising::menu_jobs.language',compact('item','lang'));

    }
    public  function languageUpdate(Request $request,$lang,$token){

        DB::beginTransaction();
        $item=MenuJob::with('translates')->where('token',$token)->first();
        if( $item->translates->where('lang',$lang)->first()){
            $changed=$item->translates->where('lang',$lang)->first()->update([
                'title'=>$request->title
            ]);
        }else{
            $changed=$item->translates()->create([
                'title'=>$request->title,
                'lang'=>$lang
            ]);
        }


        if(!$changed){
            DB::rollBack();
            return redirect()->back()->with('error',__('advertising::menu_job.error'));
        }else{
            DB::commit();
            return redirect(route("menujobs.index"))->with('message',__('advertising::menu_job.update'));
        }

    }
    public function importFile(Request $request){

        try{
            Excel::import(new MenuJobImport,$request->file);
            return redirect(route("menujobs.index"))->with('message',__('advertising::jobs.update'));
        }catch(\Exception $exception){
            return redirect()->back()->with('error',__('advertising::job.error'));
        }
       
       
    }
}
