<?php
return [
    "text-create"=>"you can create your advertising",
    "text-edit"=>"you can edit your advertising",
    "store"=>"Store Success",
    "store_cv"=>"Send Cv",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"articles list",
    "error"=>"error",
    "singular"=>"advertising",
    "collect"=>"advertisings",
    "permission"=>[
        "advertising-full-access"=>"advertising full access",
        "advertising-list"=>"advertisings list",
        "advertising-delete"=>"advertising delete",
        "advertising-create"=>"advertising create",
        "advertising-edit"=>"edit advertising",
    ]


];
