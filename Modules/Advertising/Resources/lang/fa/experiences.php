<?php

return [
    "text-create"=>' با استفاده از فرم زیر میتوانید سابقه جدید اضافه کنید.',
    "text-edit"=>' با استفاده از فرم زیر میتوانید سابقه خود را ویرایش کنید.',
    "store"=>"ثبت با موفقیت انجام شد.",
    "delete"=>"حذف با موفقیت انجام شد",
    "update"=>"بروز رسانی با موفقیت انجام شد",
    "index"=>"لیست سوابق",
    "error"=>"خطا",
    "singular"=>"سابقه",
    "collect"=>"سوابق",
    "permission"=>[
        "experience-full-access"=>"دسترسی کامل به موقعیت ها",
        "experience-list"=>"لیست سوابق",
        "experience-delete"=>"حذف سابقه",
        "experience-create"=>"ایجاد سابقه",
        "experience-edit"=>"ویرایش سابقه",
    ]

];
