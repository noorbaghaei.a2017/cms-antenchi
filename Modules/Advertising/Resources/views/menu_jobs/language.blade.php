@extends('core::layout.panel')
@section('pageTitle', __('cms.language'))
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <div class="pull-left">
                            <img src="{{asset(config('cms.flag.'.$lang))}}" width="30">

                        </div>
                        <h2>    {{$item->title}}</h2>
                    </div>
                    <br>
                    <br>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        @include('core::layout.alert-danger')
                        <form id="signupForm" role="form" method="POST" action="{{route('menujob.language.update',['lang'=>$lang,'token'=>$item->token])}}" enctype="multipart/form-data">
                            @csrf
                            @method('PATCH')
                            <input type="hidden" value="{{$item->token}}" name="token">
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="title" class="form-control-label">{{__('cms.title')}} </label>
                                    <input type="text" value="{{isset($item->translates->where('lang',$lang)->first()->title) ? $item->translates->where('lang',$lang)->first()->title : ""}}" name="title" class="form-control" id="symbol" required>
                                </div>

                            </div>


                            @include('core::layout.update-button')
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>
        $().ready(function() {
            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

@endsection
