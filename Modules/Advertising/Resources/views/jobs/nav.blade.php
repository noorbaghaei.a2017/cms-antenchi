
    <li>
        <a href="{{route('jobs.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('advertising.icons.job')}}"></i>
                              </span>

            <span class="nav-text">{{__('advertising::jobs.collect')}}</span>
        </a>
    </li>
    <li>
        <a href="{{route('categoryjobs.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('advertising.icons.category')}}"></i>
                              </span>

            <span class="nav-text">{{__('advertising::category_job.collect')}}</span>
        </a>
    </li>
    <li>
        <a href="{{route('menujobs.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('advertising.icons.menu')}}"></i>
                              </span>

            <span class="nav-text">{{__('advertising::menu_job.collect')}}</span>
        </a>
    </li>

    <li>
        <a href="{{route('attributejobs.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('advertising.icons.attribute')}}"></i>
                              </span>

            <span class="nav-text">{{__('advertising::attribute_job.collect')}}</span>
        </a>
    </li>
   

