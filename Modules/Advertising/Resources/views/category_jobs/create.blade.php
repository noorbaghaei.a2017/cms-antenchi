@extends('core::layout.panel')
@section('pageTitle',__('cms.create'))
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h2>{{__('cms.create')}} </h2>
                        <small>
                            {{__('advertising::category_job.text-create')}}
                        </small>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        @include('core::layout.alert-danger')
                        @include('core::layout.alert-success')
                        <form id="signupForm" role="form" method="post" action="{{route('categoryjobs.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="title" class="form-control-label">{{__('cms.title')}} </label>
                                    <input type="text" name="title" value="{{old('title')}}" class="form-control" id="title" required autocomplete="off">
                                </div>
                                <div class="col-sm-3">
                                  
                                    <label for="icon" class="form-control-label">{{__('cms.icon')}} </label>
                                    <input type="text" name="icon" value="{{old('icon')}}" class="form-control" id="icon"  autocomplete="off">
                                </div>
                                <div class="col-sm-3">
                                    <label for="title" class="form-control-label">{{__('cms.thumbnail')}} </label>
                                    @include('core::layout.load-single-image')
                                </div>
                                <div class="col-sm-3">
                                  
                                  <label for="pattern" class="form-control-label">{{__('cms.pattern')}} </label>
                                  <input type="text" name="pattern" value="{{old('pattern')}}" class="form-control" id="pattern" >
                              </div>
                              
                              
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="parent" class="form-control-label">{{__('cms.sub-category')}} </label>
                                    <select class="form-control" id="parent" name="parent" required>
                                    <option selected value="-1">{{__('cms.self')}}</option>    
                                    @foreach($listcategories as $list)
                                            <option value="{{$list->token}}">{!! convert_lang($list,LaravelLocalization::getCurrentLocale(),'title') !!}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="status" class="form-control-label">{{__('cms.status')}}  </label>
                                    <select dir="rtl" class="form-control" id="status" name="status" required>

                                        <option  value="1" selected>{{__('cms.active')}}</option>
                                        <option  value="0">{{__('cms.inactive')}}</option>


                                    </select>
                                </div>
                                <div class="col-sm-3">
                                  
                                  <label for="order" class="form-control-label">{{__('cms.order')}} </label>
                                  <input type="text" name="order" value="{{old('order')}}" class="form-control" id="order" >
                              </div>

                            </div>
         

                            @include('core::layout.create-button')


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>
        $().ready(function() {

            loadCountries();

            $('#co').change(function(){

                loadStates($(this));


            });

            $('#state').change(function(){

                loadCities($(this));

            });




            function loadCountries(){
                $.ajax({
                    type:'GET',
                    url:'/ajax/load/countries',
                    success:function(response) {
                        $.each(response.data, function(val, text) {
                            $('#co').append( $('<option></option>').val(text.id).html(text.name) )
                        });

                    },
                    error:function (jqXHR, textStatus, errorThrown) {
                        console.log(errorThrown);
                    }
                });
            }

            function loadStates(item){
                $.ajax({
                    type:'GET',
                    url:'/ajax/load/country/states/'+item.val(),
                    success:function(response) {
                        $('#state').empty()
                        $('#city').empty()
                        $.each(response.data, function(val, text) {
                            $('#state')
                                .append( $('<option></option>').val(text.id).html(text.name) )
                        });

                    },
                    error:function (jqXHR, textStatus, errorThrown) {
                        console.log(errorThrown);
                    }
                });
            }


            function loadCities(item){
                $.ajax({
                    type:'GET',
                    url:'/ajax/cities/'+item.val(),
                    success:function(response) {
                        $('#city').empty()
                        $.each(response.data, function(val, text) {
                            $('#city')
                                .append( $('<option></option>').val(text.id).html(text.name) )
                        });

                    },
                    error:function (jqXHR, textStatus, errorThrown) {
                        console.log(errorThrown);
                    }
                });
            }



            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    title: {
                        required: true
                    },
                    text: {
                        required: true
                    },
                    excerpt: {
                        required: true
                    },
                    category: {
                        required: true
                    },


                },
                messages: {
                    title:"{{__('cms.title.required')}}",
                    text: "{{__('cms.text.required')}}",
                    excerpt: "{{__('cms.excerpt.required')}}",
                    category: "{{__('cms.category.required')}}",
                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });




        });
    </script>

@endsection
