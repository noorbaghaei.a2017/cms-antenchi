<?php

namespace Modules\Advertising\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class ExperienceResource extends Resource
{
    public $preserveKeys = true;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title'=>$this->title,
            'symbol'=>$this->symbol,
            'slug'=>$this->slug,
            'order'=>$this->order,
            'token'=>$this->token,
            'update_date'=>$this->AgoTimeUpdate,
            'create_date'=>$this->TimeCreate,
        ];
    }
}
