<?php

namespace Modules\Core\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\UserServices;
use Modules\Core\Entities\ListServices;
use Modules\Core\Helper\Trades\TimeAttribute;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class ListServices extends Model implements HasMedia
{
    use Sluggable,HasMediaTrait,TimeAttribute;
    protected $table = 'list_services';
    protected $fillable = ['id','title','token','slug','order','status','level','parent','pattern','excerpt','text','icon'];


    public function user_services(){

        return $this->hasMany(UserServices::class,'service','id');
    }
    public function childs(){

        return $this->hasMany(ListServices::class,'parent','id');
    }

    public function translates()
    {
        return $this->morphMany(Translate::class, 'translateable');
    }

 

    public function getTranslationAttribute()
    {

        $lang=LaravelLocalization::getCurrentLocale();
        
        return $this->translates()->where('lang',$lang)->count() > 0 ? $this->translates()->where('lang',$lang)->first()->title : $this->title ;
    }

    public function getLangItemAttribute()
    {
        $lang=LaravelLocalization::getCurrentLocale();
        return $lang;
    }
    

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('medium')
            ->width(418)
            ->height(200)
            ->performOnCollections(config('cms.collection-image'));

        $this->addMediaConversion('thumb')
            ->width(70)
            ->height(60)
            ->performOnCollections(config('cms.collection-image'));
    }

    
  

    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
