<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class ServiceField extends Model
{
    protected $table = 'service_field';

    protected $fillable = ['id','service','title','text','excerpt'];

}
