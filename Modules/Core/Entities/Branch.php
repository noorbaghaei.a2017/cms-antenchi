<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Cviebrock\EloquentSluggable\Sluggable;
use Modules\Core\Helper\Trades\TimeAttribute;

class Branch extends Model implements HasMedia
{
    use Sluggable,HasMediaTrait,TimeAttribute;

    protected $fillable = ['user','token','title','symbol','slug','text','excerpt','address','google_map','latitude','longitude'];


    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('medium')
            ->width(418)
            ->height(200)
            ->performOnCollections(config('cms.collection-image'));

    }

        /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

}
