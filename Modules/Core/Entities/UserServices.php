<?php

namespace Modules\Core\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Modules\Core\Entities\Translate;
use Modules\Core\Entities\ListServices;
use Modules\Core\Entities\Info;
use Modules\Advertising\Entities\ClientAttributeJob;
use Modules\Advertising\Entities\ClientMenuJob;
use Modules\Advertising\Entities\TimeJob;
use Modules\Core\Entities\Analyzer;
use Modules\Core\Entities\Favorite;
use Modules\Core\Entities\Rate;
use Modules\Comment\Entities\Comment;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Helper\Trades\TimeAttribute;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Modules\Client\Entities\Client;
use Modules\Client\Entities\ClientBot;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Modules\Core\Helper\JobHelper;

class UserServices extends Model implements HasMedia
{
    use Sluggable,HasMediaTrait,TimeAttribute;

    protected $table = 'user_services';

    protected $fillable = ['id','status','address_two','banner_status','longitude','latitude','user','service','mobile','postal_code','title','text','token','slug','excerpt','country','phone_2','city','google_map','email','address','phone'];


    public function getShowStatusAttribute(){

        return JobHelper::ShowStatusStyle($this->status);
    }

    public function getRouteKeyName()
    {
       return multiRouteKey();
    }
    public function analyzer()
    {
        return $this->morphOne(Analyzer::class, 'analyzerable');
    }

   

    public function favorites()
    {
        return $this->morphMany(Favorite::class, 'favoriteable');
    }
   

    public function rates()
    {
        return $this->morphMany(Rate::class, 'rateable');
    }



    public function attributes()
    {
        return $this->hasMany(ClientAttributeJob::class, 'service','id');
    }

    public function menus()
    {
        return $this->hasMany(ClientMenuJob::class, 'service','id')->with('translates');
    }

    public function bot()
    {
        return $this->morphOne(ClientBot::class, 'botable');
    }

    public function time()
    {
        return $this->hasOne(TimeJob::class, 'service','id');
    }


    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function translates()
    {
        return $this->morphMany(Translate::class, 'translateable');
    }

    public function user_info()
    {
        return $this->belongsTo(Client::class,'user','id');
    }
    public  function info(){
        return $this->morphOne(Info::class,'infoable');
    }


    public function list_service(){

        return $this->belongsTo(ListServices::class,'service','id');

    }




    public  function getViewAttribute(){

        return $this->analyzer->view;
    }

    public  function getLikeAttribute(){

        return $this->analyzer->like;
    }

    public  function getAuthorAttribute(){

        return User::find($this->user_info)->first()->last_name;
    }

    public  function getRateCountAttribute(){

        return $this->rates->sum('rate')==0 ? 0 : $this->rates->sum('rate') / $this->rates->count();

    }





    // public function field()
    // {
    //     return $this->hasMany(UserServiceField::class,'service','id');
    // }

    // public function drink()
    // {
    //     return $this->hasMany(UserServiceField::class,'service','id');
    // }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('medium')
            ->width(418)
            ->height(200)
            ->performOnCollections(config('cms.collection-image'));

        $this->addMediaConversion('thumb')
            ->width(70)
            ->height(60)
            ->performOnCollections(config('cms.collection-image'));
    }


      /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

}
