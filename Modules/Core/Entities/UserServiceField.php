<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Helper\Trades\TimeAttribute;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class UserServiceField extends Model implements HasMedia
{
    use HasMediaTrait,TimeAttribute;

    protected $table = 'user_service_field';

    protected $fillable = ['id','user','user_service','field','title','excerpt','price'];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('medium')
            ->width(418)
            ->height(200)
            ->performOnCollections(config('cms.collection-image'));

        $this->addMediaConversion('thumb')
            ->width(70)
            ->height(60)
            ->performOnCollections(config('cms.collection-image'));
    }


}
