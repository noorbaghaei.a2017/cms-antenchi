<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class UserCurrency extends Model
{
    protected $table="user_currencies";
    protected $fillable=['currency','userable'];
}
