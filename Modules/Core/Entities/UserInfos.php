<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class UserInfos extends Model
{
    protected $table="user_infos";

    protected $fillable = ['phone','postal_code','address','mobile','country','city'];

}
