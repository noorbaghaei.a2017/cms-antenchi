<?php

namespace Modules\Core\Entities;


use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Modules\Article\Entities\Article;
use Modules\Core\Helper\Trades\TimeAttribute;
use Modules\Seo\Entities\Seo;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements HasMedia
{
    use Notifiable,HasMediaTrait,HasRoles,TimeAttribute;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','token','mobile','username','lang','direction','first_name','last_name','identity_card'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function wallet()
    {
        return $this->morphOne(Wallet::class, 'walletable');
    }

    public  function info(){
        return $this->morphOne(Info::class,'infoable');
    }

    public  function company(){

        return $this->morphOne(UserCompany::class,'companyable');
    }

    public function user()
    {
        return $this->hasMany(Article::class);
    }




    public  function getRoleNameAttribute(){
        return !isset($this->getRoleNames()[0]) ? __('cms.no-role'): $this->getRoleNames()[0];
    }

    public  function getFullNameAttribute(){
        return fullName($this->first_name,$this->last_name);
    }
}
