<?php
return [
    "text-create"=>"you can create your option",
    "text-edit"=>"you can edit your option",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"options list",
    "singular"=>"option",
    "collect"=>"options",
    "permission"=>[
        "option-full-access"=>"options full access",
        "option-list"=>"options list",
        "option-delete"=>"option delete",
        "option-create"=>"option create",
        "option-edit"=>"edit option",
    ]
];
