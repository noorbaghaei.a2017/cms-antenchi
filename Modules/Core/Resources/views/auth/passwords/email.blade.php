

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <title>Reset netroba</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--===============================================================================================-->
        <link rel="icon" type="image/png"  href="{{asset('img/cms/netroba.png')}}" />
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('assets/login/user/vendor/bootstrap/css/bootstrap.min.css')}}">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('assets/login/user/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('assets/login/user/fonts/Linearicons-Free-v1.0.0/icon-font.min.css')}}">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('assets/login/user/vendor/animate/animate.css')}}">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('assets/login/user/vendor/css-hamburgers/hamburgers.min.css')}}">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('assets/login/user/vendor/animsition/css/animsition.min.css')}}">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('assets/login/user/vendor/select2/select2.min.css')}}">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('assets/login/user/vendor/daterangepicker/daterangepicker.css')}}">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('assets/login/user/css/util.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/login/user/css/main.css')}}">
        <!--===============================================================================================-->
    </head>
    <body>

    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <div class="login100-form-title" style="background-image: url({{asset('assets/login/user/images/bg.jpg')}});">
					<span class="login100-form-title-1">
						Reset Password
					</span>
                </div>

                <form method="POST" action="{{ route('password.email') }}" class="login100-form validate-form">
                    @csrf
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @error('email')
                    <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                    <div class="wrap-input100 validate-input m-b-26" data-validate="Email is required">
                        <span class="label-input100">Username</span>
                        <input class="input100" type="text" name="email" placeholder="Email">
                        <span class="focus-input100"></span>
                    </div>

                    <div class="container-login100-form-btn">
                        <button type="submit" class="login100-form-btn">
                            {{ __('Send Password Reset Link') }}
                        </button>

                    </div>
                </form>

            </div>
        </div>
    </div>

    <!--===============================================================================================-->
    <script src="{{asset('assets/login/user/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
    <!--===============================================================================================-->
    <script src="{{asset('assets/login/user/vendor/animsition/js/animsition.min.js')}}"></script>
    <!--===============================================================================================-->
    <script src="{{asset('assets/login/user/vendor/bootstrap/js/popper.js')}}"></script>
    <script src="{{asset('assets/login/user/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    <!--===============================================================================================-->
    <script src="{{asset('assets/login/user/vendor/select2/select2.min.js')}}"></script>
    <!--===============================================================================================-->
    <script src="{{asset('assets/login/user/vendor/daterangepicker/moment.min.js')}}"></script>
    <script src="{{asset('assets/login/user/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <!--===============================================================================================-->
    <script src="{{asset('assets/login/user/vendor/countdowntime/countdowntime.js')}}"></script>
    <!--===============================================================================================-->
    <script src="{{asset('assets/login/user/js/main.js')}}"></script>

    </body>
    </html>


{{--    <div class="b-t">--}}
{{--        <div class="center-block w-xxl w-auto-xs p-y-md text-center">--}}
{{--            <div class="p-a-md">--}}
{{--                <div class="card-header">{{ __('Reset Password') }}</div>--}}
{{--                @if (session('status'))--}}
{{--                    <div class="alert alert-success" role="alert">--}}
{{--                        {{ session('status') }}--}}
{{--                    </div>--}}
{{--                @endif--}}

{{--                <div>--}}
{{--                    <h4>رمز عبور را فراموش کرده اید؟</h4>--}}
{{--                    <p class="text-muted m-y">--}}
{{--                        ایمیل خود را وارد کنید و ما شما را به دستورالعمل در مورد نحوه تغییر رمز عبور خود را ارسال کنید.          </p>--}}
{{--                </div>--}}
{{--                <form method="POST" action="{{ route('password.email') }}">--}}
{{--                    @csrf--}}
{{--                    <div class="form-group">--}}
{{--                        <input type="email" name="email" placeholder="ایمیل" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" required>--}}
{{--                        @error('email')--}}
{{--                        <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                        @enderror--}}
{{--                    </div>--}}
{{--                    <button type="submit" class="btn black btn-block p-x-md" >--}}
{{--                        {{ __('Send Password Reset Link') }}--}}
{{--                    </button>--}}
{{--                </form>--}}

{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}





