@extends('core::layout.panel')
@section('pageTitle', __('cms.language'))
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <div class="pull-left">
                            <img src="{{asset(config('cms.flag.'.$lang))}}" width="30">

                        </div>
                        <h2>    {{$item->symbol}}</h2>
                    </div>
                    <br>
                    <br>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif
                        @include('core::layout.alert-danger')
                        <form id="signupForm" role="form" method="POST" action="{{route('setting.language.update',['lang'=>$lang,'id'=>$item->id])}}" enctype="multipart/form-data">
                            @csrf
                            @method('PATCH')
                            <input type="hidden" value="{{$item->token}}" name="token">
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <label for="copy_right" class="form-control-label">{{__('cms.copy-right')}} </label>
                                    <input type="text" value="{{isset($item->translates->where('lang',$lang)->first()->copy_right) ? $item->translates->where('lang',$lang)->first()->copy_right : ""}}" name="copy_right" class="form-control" id="copy_right" >
                                </div>
                                <div class="col-sm-12">
                                    <label for="excerpt" class="form-control-label">{{__('cms.excerpt')}} </label>
                                    <input type="text" value="{{isset($item->translates->where('lang',$lang)->first()->excerpt) ? $item->translates->where('lang',$lang)->first()->excerpt : ""}}" name="excerpt" class="form-control" id="excerpt" >
                                </div>
                                <div class="col-sm-12">
                                    <label for="slogan" class="form-control-label">{{__('cms.slogan')}} </label>
                                    <input type="text" value="{{isset($item->translates->where('lang',$lang)->first()->slogan) ? $item->translates->where('lang',$lang)->first()->slogan : ""}}" name="slogan" class="form-control" id="slogan" >
                                </div>
                                <div class="col-sm-12">
                                    <label for="address" class="form-control-label">{{__('cms.address')}} </label>
                                    <input type="text" value="{{isset($item->translates->where('lang',$lang)->first()->address) ? $item->translates->where('lang',$lang)->first()->address : ""}}" name="address" class="form-control" id="address" >
                                </div>
                                <div class="col-sm-12">
                                    <label for="title" class="form-control-label">{{__('cms.title')}} </label>
                                    <input type="text" value="{{isset($item->translates->where('lang',$lang)->first()->title_seo) ? $item->translates->where('lang',$lang)->first()->title_seo : ""}}" name="title_seo" class="form-control" id="title" >
                                </div>
                                <div class="col-sm-12">
                                    <label for="description" class="form-control-label">{{__('cms.description')}} </label>
                                    <input type="text" value="{{isset($item->translates->where('lang',$lang)->first()->description_seo) ? $item->translates->where('lang',$lang)->first()->description_seo : ""}}" name="description_seo" class="form-control" id="description" >
                                </div>
                                <div class="col-sm-12">
                                    <label for="keyword" class="form-control-label">{{__('cms.keyword')}} </label>
                                    <input type="text" value="{{isset($item->translates->where('lang',$lang)->first()->keyword_seo) ? $item->translates->where('lang',$lang)->first()->keyword_seo : ""}}" name="keyword_seo" class="form-control" id="keyword" >
                                </div>
                                <div class="col-sm-12">
                                    <label for="canonical" class="form-control-label">{{__('cms.canonical')}} </label>
                                    <input type="text" value="{{isset($item->translates->where('lang',$lang)->first()->canonical_seo) ? $item->translates->where('lang',$lang)->first()->canonical_seo : ""}}" name="canonical_seo" class="form-control" id="canonical" >
                                </div>

                            </div>


                            @include('core::layout.update-button')
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>
        $().ready(function() {
            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

@endsection
