<div class="col-md-12 sub-menu_content">
    @if(!is_null(hasTitleMegaMenu($menu->id,1)))
    <span class="sub-menu-title"> <h4> {{hasTitleMegaMenu($menu->id,1)->symbol}} </h4> </span>
    @endif
    <ul class="nav flex-column">
        @foreach(childColumn($menu->id,1) as $child)
            <li class="sub-item">
                <a class="sub-link" href="{{$child->href}}"><i class="fas fa-caret-right"></i>
                    {{$child->symbol}}</a>
            </li>
        @endforeach
    </ul>
</div>
<!-- /.col-md-4  -->
