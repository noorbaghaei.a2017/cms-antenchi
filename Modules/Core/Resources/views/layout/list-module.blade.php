<div data-flex class="hide-scroll">
    <nav class="scroll nav-stacked nav-stacked-rounded nav-color">
        <ul class="nav" data-ui-nav>
<li class="nav-header hidden-folded">
    <span class="text-xs">{{__('cms.fast_access')}}</span>
</li>

<li>
    <a href="{{route('dashboard.website')}}" class="b-danger">
                                  <span class="nav-icon text-white no-fade">
                                    <i class="{{config('core.icons.dashboard')}}"></i>
                                  </span>
        <span class="nav-text">{{__('cms.dashboard')}}</span>
    </a>
</li>
            @can('menu-list')
            @if(hasModule('Menu'))
@include('menu::menus.nav')
            @endif
            @endcan

            @can(['article-list','information-list'])

                <li>
                    <a  class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('user.icons.user')}}"></i>
                              </span>
                        <span class="nav-caret">
                        <i class="fa fa-caret-down"></i>
                      </span>
                        <span class="nav-text">  {{__('cms.content')}}</span>
                    </a>
                    <ul class="nav-sub">
                        @can('article-list')
                            @if(hasModule('Article'))
                                @include('article::articles.nav')
                            @endif
                        @endcan

                            <!-- @can('information-list')
                                @if(hasModule('Information'))
                                    @include('information::informations.nav')
                                @endif
                            @endcan -->
                    </ul>
                </li>

            @endcan

<!-- 

{{---------start advertising-------}}


            @can(['advertising-list'])

                <li>
                    <a  class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('user.icons.user')}}"></i>
                              </span>
                        <span class="nav-caret">
                        <i class="fa fa-caret-down"></i>
                      </span>
                        <span class="nav-text">  {{__('cms.advertisings')}}</span>
                    </a>
                    <ul class="nav-sub">

            @can('advertising-list')
                @if(hasModule('Advertising'))
                    @include('advertising::advertisings.nav')
                @endif
            @endcan

                    </ul>

                </li>

            @endcan


{{---------end advertising-------}}



 -->





{{---------start job-------}}




                @can(['advertising-list'])

                <li>
                    <a  class="b-success">

                    @if($requestbussinesscount > 0)
                    <span class="nav-label">
                    <b class="label label-xs rounded danger" style="width:20px;padding:3px;height:16px">{{$requestbussinesscount}}</b>
                  </span>
                    @endif
                            <span class="nav-icon text-white no-fade">
                                <i class="{{config('user.icons.user')}}"></i>
                            </span>
                        <span class="nav-caret">
                        <i class="fa fa-caret-down"></i>
                    </span>
                        <span class="nav-text">  {{__('cms.jobs')}}</span>
                    </a>
                    <ul class="nav-sub">

                    @can('advertising-list')
                        @if(hasModule('Advertising'))
                            @include('advertising::jobs.nav')
                        @endif
                    @endcan

                    </ul>

                </li>

                @endcan




{{---------end job-------}}




{{---------start comment-------}}




                @can(['comment-list'])

                @include('comment::comments.nav')

                @endcan




{{---------end comment-------}}










<!-- {{---------start toturial-------}} -->
<!-- 

            @can(['product-list','store-list'])

                <li>
                    <a  class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('user.icons.user')}}"></i>
                              </span>
                        <span class="nav-caret">
                        <i class="fa fa-caret-down"></i>
                      </span>
                        <span class="nav-text">{{__('cms.educations')}}</span>
                    </a>
                    <ul class="nav-sub">


                        @can('race-list')
                            @if(hasModule('Educational'))
                                @include('educational::races.nav')
                            @endif
                        @endcan

                        @can('classroom-list')
                            @if(hasModule('Educational'))
                                @include('educational::classrooms.nav')
                            @endif
                        @endcan



                    </ul>


                </li>


                        @endcan






{{---------end toturial-------}} -->




<!-- 
{{---------start product-------}}


            @can(['product-list','store-list'])

                <li>
                    <a  class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('user.icons.user')}}"></i>
                              </span>
                        <span class="nav-caret">
                        <i class="fa fa-caret-down"></i>
                      </span>
                        <span class="nav-text">  {{__('cms.products')}}</span>
                    </a>
                    <ul class="nav-sub">

            @can('product-list')

                    @if(hasModule('Product'))
                        @include('product::products.nav')
                        @include('product::attributes.nav')
                        @include('product::properties.nav')
                    @endif

            @endcan

                @can('store-list')
                    @if(hasModule('Store'))
                        @include('store::stores.nav')
                    @endif
                @endcan


                    </ul>

                </li>

            @endcan

{{---------end product-------}} -->



            <!-- @can('event-list')
                @if(hasModule('Event'))
                    @include('event::events.nav')
                @endif
            @endcan -->

            <!-- @can('download-list')
                @if(hasModule('Download'))
                    @include('download::downloads.nav')
                @endif
            @endcan -->


            <!-- @can('ad-list')
                @if(hasModule('Ad'))
                    @include('ad::ads.nav')
                @endif
            @endcan -->

            <!-- @can('specialdiscount-list')
                @if(hasModule('SpecialDiscount'))
                    @include('specialdiscount::specialdiscounts.nav')
                @endif
            @endcan -->

            <!-- @can('lottery-list')
                @if(hasModule('Lottery'))
                    @include('lottery::lotteries.nav')
                @endif
            @endcan -->


        @can('carousel-list')
            @if(hasModule('Carousel'))
@include('carousel::carousels.nav')
            @endif
            @endcan




            <!-- @can('brand-list')
                @if(hasModule('Brand'))
                    @include('brand::brands.nav')
                @endif
            @endcan -->



            @can('service-list')
            @if(hasModule('Service'))
@include('service::services.nav')
            @endif
            @endcan



<!-- 

{{------start portfolio-------}}


            @can(['customer-list','portfolio-list'])

                <li>
                    <a  class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('user.icons.user')}}"></i>
                              </span>
                        <span class="nav-caret">
                        <i class="fa fa-caret-down"></i>
                      </span>
                        <span class="nav-text">  {{__('cms.portfolios')}}</span>
                    </a>
                    <ul class="nav-sub">
            @can('customer-list')
                @if(hasModule('Customer'))
                    @include('customer::customers.nav')
                @endif
            @endcan


            @can('portfolio-list')
            @if(hasModule('Portfolio'))
                  @include('portfolio::portfolios.nav')
            @endif
            @endcan

                    </ul>

                </li>

            @endcan

{{----------end portfolio---------}}
 -->




{{----------start user---------}}

            @can(['user-list','member-list','client-list'])
                <li>
                    <a  class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('user.icons.user')}}"></i>
                              </span>
                        <span class="nav-caret">
                        <i class="fa fa-caret-down"></i>
                      </span>
                        <span class="nav-text">  {{__('cms.clients')}}</span>
                    </a>
                    <ul class="nav-sub">

            @can('user-list')
                @if(hasModule('User'))
                            @include('user::users.nav')
                @endif
            @endif



                @can('member-list')
                    @if(hasModule('Member'))
                        @include('member::members.nav')
                    @endif
                @endcan


                @can('client-list')
                    @if(hasModule('Client'))
                        @include('client::clients.nav')
                    @endif
                    <!-- @if(hasModule('Client'))
                        @include('client::employers.nav')
                    @endif
                    @if(hasModule('Client'))
                        @include('client::seekers.nav')
                    @endif -->
                @endcan
                    </ul>

                </li>


            @endcan

{{----------end user---------}}




            <!-- @can('order-list')
                @if(hasModule('Order'))
                    @include('order::orders.nav')
                @endif
            @endcan -->



            <!-- @can('page-list')
            @if(hasModule('Page'))
                    @include('page::pages.nav')
            @endif
            @endcan -->


<!-- 
            @can('agent-list')
                @if(hasModule('Agent'))
                    @include('agent::agents.nav')
                @endif
            @endcan -->









<!-- 
{{---------start message-------}}





            @can(['request-list','comment-list','user-list'])

                <li>
                    <a  class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('user.icons.user')}}"></i>
                              </span>
                        <span class="nav-caret">
                        <i class="fa fa-caret-down"></i>
                      </span>
                        <span class="nav-text">   {{__('cms.messages')}}</span>
                    </a>
                    <ul class="nav-sub"> -->


                        <!-- @can('request-list')
                            @if(hasModule('Request'))
                                @include('request::requests.nav')
                            @endif
                        @endcan -->


                            <!-- @can('comment-list')
                                @if(hasModule('Comment'))
                                    @include('comment::comments.nav')
                                @endif
                            @endcan -->

                            <!-- @can('user-list')
                                @if(hasModule('Core'))
                                    @include('core::layout.nav.message')
                                @endif
                            @endif -->


<!-- 
                    </ul>

                </li>


                            @endcan

{{--------end message-------}} -->






            <!-- @can('plan-list')
            @if(hasModule('Plan'))
@include('plan::plans.nav')
            @endif
            @endcan -->

            @can('user-list')
                @if(hasModule('Netroba'))
                    @include('netroba::oauths.nav')
                @endif
            @endif


<!-- 

            {{---------start access-------}}



            @can(['user-list','side-list'])

                <li>
                    <a  class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('core.icons.access')}}"></i>
                              </span>
                        <span class="nav-caret">
                        <i class="fa fa-caret-down"></i>
                      </span>
                        <span class="nav-text"> {{__('cms.permissions')}}</span>
                    </a>
                    <ul class="nav-sub">



                        @can('user-list')
                            @if(hasModule('Core'))
                                @include('core::layout.roles.nav')
                            @endif
                        @endif



                        @can('side-list')
                            @if(hasModule('Member'))
                                @include('member::sides.nav')
                            @endif
                        @endcan




                    </ul>


                </li>


            @endcan


            {{---------end access-------}} -->


{{---------start setting-------}}


            @can(['user-list'])


                <li>
                    <a  class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('user.icons.user')}}"></i>
                              </span>
                        <span class="nav-caret">
                        <i class="fa fa-caret-down"></i>
                      </span>
                        <span class="nav-text">{{__('cms.setting')}}</span>

                    </a>
                    <ul class="nav-sub">

            @can('user-list')
                @if(hasModule('Core'))
                        @include('core::layout.nav.template')
                        @include('core::layout.nav.awards')
                        @include('core::layout.nav.branches')
                        <!-- @include('core::layout.nav.message') -->
                        @include('core::layout.nav.footer')
                        @include('core::layout.nav')
                    @endif
                @endif

                @can('questions-list')
                    @if(hasModule('Question'))
                        @include('questions::questions.nav')
                    @endif
                @endcan

                @can('gallery-list')
                    @if(hasModule('Gallery'))
                        @include('gallery::galleries.nav')
                    @endif
                @endcan

                @can('widget-list')
                    @if(hasModule('Widget'))
                        @include('widget::widgets.nav')
                    @endif
                @endcan

                @can('video-list')
                    @if(hasModule('Video'))
                        @include('video::videos.nav')
                    @endif
                @endcan


                @can('service-list')
                    @if(hasModule('Service'))
                        @include('service::properties.nav')
                    @endif
                @endcan





                @can('service-list')
                    @if(hasModule('Service'))
                        @include('service::advantages.nav')
                    @endif
                @endcan



                <!-- @can('currency-list')
                    @if(hasModule('Core'))
                        @include('core::currencies.nav')
                    @endif
                @endcan -->



                    </ul>


                </li>


            @endcan


{{---------end setting-------}}



        </ul>
    </nav>
</div>
