@if(is_null($item))


    <div class="box-body p-v-md">
        <div class="row row-sm">
            <div class="form-group row">
                <div class="col-sm-12">
                    <span class="text-danger">*</span>
                    <label for="experience" class="form-control-label">{{__('cms.work_experience')}} </label>
                    <select dir="rtl" class="form-control"  id="experience" name="experience" required >
                        @foreach($experiences as $key=>$value)

                            <option value="{{$value->title}}">{{$value->symbol}}</option>

                        @endforeach
                    </select>
                </div>
            </div>

        </div>
    </div>
@else

    <div class="box-body p-v-md">
        <div class="row row-sm">

            <div class="form-group row">
                <div class="col-sm-12">
                    <span class="text-danger">*</span>
                    <label for="experience" class="form-control-label">{{__('cms.work_experience')}} </label>

                    <select dir="rtl" class="form-control"  id="experiences" name="experience" required >
                        @foreach($experiences as $key=>$value)


                                <option value="{{$value->title}}" {{$item==$value ? "selected": ""}}>{{$value->symbol}}</option>

                        @endforeach
                    </select>

                </div>
            </div>

        </div>
    </div>
@endif





