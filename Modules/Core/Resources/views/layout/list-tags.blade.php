<div class="form-group row">
@if(is_null($item))


        <div class="col-sm-12">
            <label for="tags"  class="form-control-label">{{__('cms.tags')}} </label>
            <select dir="rtl" class="form-control" multiple="multiple" id="tags" name="tags[]">

            </select>

        </div>


@else


        <div class="col-sm-12">
            <label for="tags"  class="form-control-label">{{__('cms.tags')}}  </label>
            <select dir="rtl" class="form-control" multiple="multiple" id="tags" name="tags[]">
                @foreach($item->tags as $tag)
                    <option selected>{{$tag->name}}</option>
                @endforeach
            </select>

        </div>


@endif

</div>
