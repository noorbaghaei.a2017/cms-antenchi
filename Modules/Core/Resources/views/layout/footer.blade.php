<script src="{{asset('assets/libs/jquery/dist/jquery.js')}}"></script>
<script src="{{asset('assets/libs/tether/dist/js/tether.min.js')}}"></script>
<script src="{{asset('assets/libs/bootstrap/dist/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/libs/jQuery-Storage-API/jquery.storageapi.min.js')}}"></script>
<script src="{{asset('assets/libs/PACE/pace.min.js')}}"></script>
<script src="{{asset('assets/libs/jquery-pjax/jquery.pjax.js')}}"></script>
<script src="{{asset('assets/libs/blockUI/jquery.blockUI.js')}}"></script>
<script src="{{asset('assets/libs/jscroll/jquery.jscroll.min.js')}}"></script>
<script src="{{asset('assets/scripts/config.lazyload.js')}}"></script>
<script src="{{asset('assets/scripts/ui-load.js')}}"></script>
<script src="{{asset('assets/scripts/ui-jp.js')}}"></script>
<script src="{{asset('assets/scripts/ui-load.js')}}"></script>
<script src="{{asset('assets/scripts/ui-include.js')}}"></script>
<script src="{{asset('assets/scripts/ui-device.js')}}"></script>
<script src="{{asset('assets/scripts/ui-form.js')}}"></script>
<script src="{{asset('assets/scripts/ui-modal.js')}}"></script>
<script src="{{asset('assets/scripts/ui-nav.js')}}"></script>
<script src="{{asset('assets/scripts/ui-list.js')}}"></script>
<script src="{{asset('assets/scripts/summernote.js')}}"></script>
<script src="{{asset('assets/scripts/rtl.js')}}"></script>
<script src="{{asset('assets/scripts/ui-scroll-to.js')}}"></script>
<script src="{{asset('assets/scripts/ui-toggle-class.js')}}"></script>
<script src="{{asset('assets/scripts/ui-taburl.js')}}"></script>
<script src="{{asset('assets/scripts/app.js')}}"></script>
<script src="{{asset('assets/scripts/ajax.js')}}"></script>
<script src="{{asset('assets/scripts/Chart.js')}}"></script>
<script src="{{asset('assets/scripts/Chart.bundle.js')}}"></script>
<script src="{{asset('assets/scripts/jquery.chart.js')}}"></script>
<script src="{{asset('assets/scripts/jquery.easypiechart.fill.js')}}"></script>
<script src="{{asset('assets/scripts/persian-date.js')}}"></script>
<script src="{{asset('assets/scripts/kamadatepicker.min.js')}}"></script>
<script src="{{asset('assets/scripts/jquery.lazy.min.js')}}"></script>
<script src="{{asset('assets/scripts/plugins/jquery.lazy.plugins.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jscroll/2.4.1/jquery.jscroll.min.js"></script>



<script src="{{asset('assets/scripts/select2.min.js')}}"></script>
<script type="text/javascript">

$('#href').select2({
    placeholder: 'انتخاب آدرس',
    allowClear: true
});
$('#permission').select2({
    placeholder: 'انتخاب نقش',
    allowClear: true
});
$('#robots-seo').select2({
    placeholder: 'انتخاب وضعیت',
    allowClear: true
});
// $('#country').select2({
//     placeholder: 'انتخاب کشور',
//     allowClear: true
// });
$('#tags').select2({
    tags: true
});
$('#kinds').select2({
    tags: true
});
$('#educations').select2({
    tags: true
});
$('#skills').select2({
    tags: true
});
$('#kinds').select2({
    tags: true
});
$('#positions').select2({
    tags: true
});
$('#notifications').select2({
    tags: true
});
$('#mobiles').select2({
    tags: true
});
$('#phones').select2({
    tags: true
});
$('#faxes').select2({
    tags: true
});
$('#options').select2({
    tags: true
});
$('#abilities').select2({
    tags: true
});
$('#attributes').select2({
    tags: true
});
$('#properties').select2({
    tags: true
});
$('#prerequisites').select2({
});

</script>
<script>
    $(document).on('click', '#close-preview', function(){
        $('.image-preview').popover('hide');
        // Hover befor close the preview
        $('.image-preview').hover(
            function () {
                $('.image-preview').popover('show');
            },
            function () {
                $('.image-preview').popover('hide');
            }
        );
    });

    $(function() {
        // Create the close button
        var closebtn = $('<button/>', {
            type:"button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class","close pull-right");
        // Set the popover default content
        $('.image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>پیش نمایش</strong>"+$(closebtn)[0].outerHTML,
            content: "There's no image",
            placement:'bottom'
        });
        // Clear event
        $('.image-preview-clear').click(function(){
            $('.image-preview').attr("data-content","").popover('hide');
            $('.image-preview-filename').val("");
            $('.image-preview-clear').hide();
            $('.image-preview-input input:file').val("");
            $(".image-preview-input-title").text("انتخاب");
        });
        // Create the preview image
        $(".image-preview-input input:file").change(function (){
            var img = $('<img/>', {
                id: 'dynamic',
                width:250,
                height:200
            });
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
                $(".image-preview-input-title").text("تغییر عکس");
                $(".image-preview-clear").show();
                $(".image-preview-filename").val(file.name);
                img.attr('src', e.target.result);
                $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
            }
            reader.readAsDataURL(file);
        });

    });
</script>

<script>

    $(window).load(function() {
        $('#detail').hide;
        $('#profile').hide;
        // Animate loader off screen
        $(".se-pre-con").fadeOut("slow");
    });
</script>

<!-- Paste this code after body tag -->
<div style="opacity: .8" class="se-pre-con"></div>
<!-- Ends -->


<script>

    $('#search-contact').keypress(function() {
        var dInput = this.value;


                  console.log(this.value)
                  $(".list-item").hide()
                  $(".list-item .item-title a:contains("+this.value+")")
                      .parent()
                      .parent()
                      .parent()
                      .show()

    });


    $('.list-item.chat').click('function',function () {

        $user=$(this).data('user');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type:'POST',
            contentType:'application/json; charset=utf-8',
            url:'/ajax/admin/get/chats/'+$user,
            data: { field1:$user} ,
            beforeSend: function(){
            },
            success: function (response) {
                console.log(response)
                $('.list-item.chat.active').find('.list-body').find('.unread').hide()
                $('#chat-title').text(response.info.full_name)
                $('#profile-name').text(response.info.full_name)
                $('#profile-mobile').text(response.info.mobile)
                $('#profile-email').text(response.info.email)
                $('#profile-about').text(response.info.about)
                $('#profile-image').attr('src',response.info.image)
                $('#chat-list').empty()
                $('#detail').css('opacity',1)
                $('#profile').css('opacity',1)
                $.each(response.result, function(index,value){
                    $('#profile').show()
                    $('#detail').show()
                    if (value.user==null){
                        $('#chat-list').append(
                            '<div class="m-b">'+
                            '<a href="#" class="pull-right w-40 m-r-sm">'+
                            '<img src="'+value.clientImage+'" alt="." class="w-full img-circle">'+
                            '</a>'+
                            '<div class="clear text-right">'+
                            '<div>'+
                            '<div class="p-a p-y-sm success inline rounded">'+
                            value.text+
                            '</div>'+
                            '</div>'+
                            '<div class="text-muted text-xs m-t-xs"><i class="fa fa-ok text-success"></i>'+
                            value.created_at+
                            ' </div>'+
                            '</div>'+
                            '</div>'
                        );
                    }else {
                        $('#chat-list').append(
                            '<div class="m-b">'+
                            '<a href="#" class="pull-left w-40 m-r-sm">'+
                            '<img src="'+value.userImage+'" alt="." class="w-full img-circle">'+
                            '</a>'+
                            '<div class="clear">'+
                            '<div>'+
                            '<div class="p-a p-y-sm success inline rounded">'+
                            value.text+
                            '</div>'+
                            '</div>'+
                            '<div class="text-muted text-xs m-t-xs"><i class="fa fa-ok text-success"></i>'+
                            value.created_at+
                            ' </div>'+
                            '</div>'+
                            '</div>'
                        );
                    }

                });

                // $('#chat-list').html(response)
            },
            error: function (response) {
                console.log('error')
            },
            complete:function(data){

            }

        });

    });



    $('#sendMessage').click('function',function () {

        $user=$('.list-item.chat.active').data('user');
        $message=$(this).siblings('#message').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'POST',
                contentType:'application/json; charset=utf-8',
                url:'/ajax/admin/send/message/'+$user+'/'+$message,
                data: { field1:$user} ,
                beforeSend: function(){
                },
                success: function (response) {
                    console.log(response)
                    $('#message').val("")
                    $('#chat-title').text(response.info.full_name)
                    $('#profile-name').text(response.info.full_name)
                    $('#profile-mobile').text(response.info.mobile)
                    $('#profile-email').text(response.info.email)
                    $('#profile-about').text(response.info.about)
                    $('#profile-image').attr('src',response.info.image)
                    $('#chat-list').empty()
                    $.each(response.result, function(index,value){
                        $('#profile').show()
                        $('#detail').show()
                        if (value.user==null){
                            $('#chat-list').append(
                                '<div class="m-b">'+
                                '<a href="#" class="pull-right w-40 m-r-sm">'+
                                '<img src="'+value.clientImage+'" alt="." class="w-full img-circle">'+
                                '</a>'+
                                '<div class="clear text-right">'+
                                '<div>'+
                                '<div class="p-a p-y-sm success inline rounded">'+
                                value.text+
                                '</div>'+
                                '</div>'+
                                '<div class="text-muted text-xs m-t-xs"><i class="fa fa-ok text-success"></i>'+
                                value.created_at+
                                ' </div>'+
                                '</div>'+
                                '</div>'
                            );
                        }else {
                            $('#chat-list').append(
                            '<div class="m-b">'+
                                '<a href="#" class="pull-left w-40 m-r-sm">'+
                                '<img src="'+value.userImage+'" alt="." class="w-full img-circle">'+
                                '</a>'+
                                '<div class="clear">'+
                                '<div>'+
                                '<div class="p-a p-y-sm success inline rounded">'+
                                value.text+
                                '</div>'+
                                '</div>'+
                                '<div class="text-muted text-xs m-t-xs"><i class="fa fa-ok text-success"></i>'+
                                value.created_at+
                            ' </div>'+
                            '</div>'+
                            '</div>'
                            );
                        }

                    });

                   // $('#chat-list').html(response)
                },
                error: function (response) {
                   console.log(response)
                },
                complete:function(data){

                }

            });

    });
</script>

<script>
    var address1="{{asset('assets/images/ads/shahre-farsh.jpeg')}}";
    var address2="{{asset('assets/images/ads/mahd-farsh.jpg')}}";

    var video = document.getElementById('video'),
        userAgent=navigator.userAgent || navigator.vendor || navigator.opera;
    if(userAgent.match(/iPod/i) || userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || userAgent.match(/Android/i)){
        document.getElementById('video').setAttribute('poster',address1);
    }
    else{

        if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
                video.srcObject = stream;
                video.play();
            });
        }
        else{
            document.getElementById('video').setAttribute('poster',address2);
        }


    }





</script>



@yield('scripts')
</body>
</html>
