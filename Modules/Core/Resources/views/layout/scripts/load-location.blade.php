<script>

    loadCountries();

    $('#info_country').change(function(){

        loadStates($(this));


    });

    $('#info_state').change(function(){

        loadCities($(this));

    });

    $('#info_city').change(function(){

        loadAreas($(this));

    });





    function loadCountries(){
        $.ajax({
            type:'GET',
            url:'/ajax/load/countries',
            success:function(response) {
                $.each(response.data, function(val, text) {
                    $('#info_country').append( $('<option></option>').val(text.id).html(text.name))
                });

            },
            error:function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    }

    function loadStates(item){
        $.ajax({
            type:'GET',
            url:'/ajax/load/country/states/'+item.val(),
            success:function(response) {
                $('#info_city').empty()
                $('#info_area').empty()
                $.each(response.data, function(val, text) {
                    $('#info_state')
                        .append( $('<option></option>').val(text.id).html(text.name) )
                });

            },
            error:function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    }



    function loadCities(item){
        $.ajax({
            type:'GET',
            url:'/ajax/cities/'+item.val(),
            success:function(response) {
                $('#info_city').empty()
                $.each(response.data, function(val, text) {
                    $('#info_city')
                        .append( $('<option></option>').val(text.id).html(text.name) )
                });

            },
            error:function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    }

    function loadAreas(item){
        $.ajax({
            type:'GET',
            url:'/ajax/areas/'+item.val(),
            success:function(response) {
                $('#info_area').empty()
                $.each(response.data, function(val, text) {
                    $('#info_area')
                        .append( $('<option></option>').val(text.id).html(text.name) )

                });

            },
            error:function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    }


</script>
