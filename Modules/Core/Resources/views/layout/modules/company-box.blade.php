@if(is_null($company))

    <div class="box-header">
        <h2>{{__('cms.company')}}</h2>
        <small>{{__('cms.company-another')}}</small>
    </div>
    <div class="box-divider m-a-0"></div>
    <div class="box-body p-v-md">
        <div class="row row-sm">

        <div  class="form-group row">


            <div class="col-sm-3">
                <label for="company_name" class="form-control-label">{{__('cms.name')}} </label>
                <input type="text" name="company_name" value="{{old('company_name')}}" class="form-control" id="company_name">
            </div>
            <div class="col-sm-3">
                <label for="company_mobile" class="form-control-label">{{__('cms.mobile')}} </label>
                <input type="text" name="company_mobile" value="{{old('mobile')}}" class="form-control" id="company_mobile">
            </div>
            <div class="col-sm-3">
                <label for="company_phone" class="form-control-label">{{__('cms.phone')}} </label>
                <input type="text" name="company_phone" value="{{old('phone')}}" class="form-control" id="company_phone">
            </div>
            <div class="col-sm-3">
                <label for="company_address" class="form-control-label">{{__('cms.address')}} </label>
                <input type="text" name="company_address" value="{{old('company_address')}}" class="form-control" id="company_address">
            </div>

        </div>
            <div class="form-group row">
                <div class="col-sm-3">
                    <label for="company_website" class="form-control-label">{{__('cms.website')}} </label>
                    <input type="text" name="company_website" value="{{old('website')}}" class="form-control" id="company_website"  autocomplete="off">
                </div>
                <div class="col-sm-3">
                    <label for="count_member" class="form-control-label">{{__('cms.count_member')}} </label>
                    <input type="text" name="count_member" value="{{old('count_member')}}" class="form-control" id="count_member"  autocomplete="off">
                </div>

            </div>

           





        </div>
    </div>
@else

    <div class="box-header">
        <h2>{{__('cms.company')}}</h2>
        <small>{{__('cms.company-another')}}</small>
    </div>
    <div class="box-divider m-a-0"></div>
    <div class="box-body p-v-md">
        <div class="row row-sm">


        <div  class="form-group row">


        <div class="col-sm-3">
            <label for="company_name" class="form-control-label">{{__('cms.name')}} </label>
            <input type="text" value="{{is_null($company->name) ? "" :$company->name}}" name="company_name" class="form-control" id="company_name">
        </div>
        <div class="col-sm-3">
            <label for="company_mobile" class="form-control-label">{{__('cms.mobile')}} </label>
            <input type="text" value="{{is_null($company->mobile) ? "" :$company->mobile}}" name="company_mobile" class="form-control" id="company_mobile">
        </div>
        <div class="col-sm-3">
            <label for="company_phone" class="form-control-label">{{__('cms.phone')}} </label>
            <input type="text" value="{{is_null($company->phone) ? "" :$company->phone}}" name="company_phone" class="form-control" id="company_phone">
        </div>
        <div class="col-sm-3">
            <label for="company_address" class="form-control-label">{{__('cms.address')}} </label>
            <input type="text" value="{{is_null($company->address) ? "" :$company->address}}" name="company_address" class="form-control" id="company_address">
        </div>

    </div>
            <div class="form-group row">
                <div class="col-sm-3">
                    <label for="company_website" class="form-control-label">{{__('cms.website')}} </label>
                    <input type="text" name="company_website" value="{{is_null($company->website) ? "" :$company->website}}" class="form-control" id="company_website"  autocomplete="off">
                </div>
                <div class="col-sm-3">
                    <label for="count_member" class="form-control-label">{{__('cms.count_member')}} </label>
                    <input type="text" name="count_member" value="{{is_null($company->count_member) ? "" :$company->count_member}}" class="form-control" id="count_member"  autocomplete="off">
                </div>
            </div>


          
        </div>





    </div>
    </div>



@endif
