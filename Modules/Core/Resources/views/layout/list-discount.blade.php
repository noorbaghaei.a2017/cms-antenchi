@if(is_null($item))
    <div class="box-header">
        <h2>{{__('cms.discount')}}</h2>
        <small>{{__('cms.discount-services')}}</small>
    </div>
    <div class="box-body p-v-md">
        <div class="row row-sm">
            <div class="form-group row">
                <div class="col-sm-3">
                    <label for="title-discount" class="form-control-label">{{__('cms.title')}} </label>
                    <input dir="rtl" value="{{old('title-discount')}}" class="form-control"  id="title-discount" name="title-discount" >
                </div>
                <div class="col-sm-3">
                    <label for="code-discount" class="form-control-label">{{__('cms.code')}} </label>
                    <input dir="rtl" value="{{old('code-discount')}}" class="form-control"  id="code-discount" name="code-discount" >
                </div>
                <div class="col-sm-3">
                    <label for="amount-discount" class="form-control-label">{{__('cms.amount')}} </label>
                    <input dir="rtl" value="{{old('amount-discount')}}" class="form-control"  id="amount-discount" name="amount-discount" >
                </div>
                <div class="col-sm-3">
                    <label for="percentage-discount" class="form-control-label">{{__('cms.percentage')}} </label>
                    <input dir="rtl" value="{{old('percentage-discount')}}" class="form-control"  id="percentage-discount" name="percentage-discount" >
                </div>

            </div>

        </div>
    </div>
@else
    <div class="box-header">
        <h2>{{__('cms.discount')}}</h2>
        <small>{{__('cms.discount-services')}}</small>
    </div>
    <div class="box-body p-v-md">
        <div class="row row-sm">

            <div class="form-group row">
                <div class="col-sm-3">
                    <label for="title-discount" class="form-control-label">{{__('cms.title')}} </label>

                    <input dir="rtl" value="{{$item->discount->title}}" class="form-control"  id="title-discount" name="title-discount" >


                </div>
                <div class="col-sm-3">

                    <label for="code-discount" class="form-control-label">{{__('cms.code')}} </label>

                    <input dir="rtl" value="{{$item->discount->code}}" class="form-control"  id="code-discount" name="code-discount" >


                </div>
                <div class="col-sm-3">

                    <label for="amount-discount" class="form-control-label">{{__('cms.amount')}} </label>

                    <input dir="rtl" value="{{$item->discount->amount}}" class="form-control"  id="amount-discount" name="amount-discount" >


                </div>
                <div class="col-sm-3">

                    <label for="percentage-discount" class="form-control-label">{{__('cms.percentage')}} </label>

                    <input dir="rtl" value="{{$item->discount->percentage}}" class="form-control"  id="percentage-discount" name="percentage-discount" >


                </div>




            </div>

        </div>
    </div>
@endif





