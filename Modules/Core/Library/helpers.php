<?php

use App\Facades\Rest\Rest;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Modules\Core\Entities\Translate;
use Modules\Ad\Entities\Ad;
use Modules\Advertising\Entities\Advertising;
use Modules\Advertising\Entities\Experience;
use Modules\Advertising\Entities\Salary;
use Modules\Advertising\Entities\AttributeJob;
use Modules\Advertising\Entities\MenuJob;
use Modules\Article\Entities\Article;
use Modules\Brand\Entities\Brand;
use Modules\Carousel\Entities\Carousel;
use Modules\Chat\Entities\Chat;
use Modules\Client\Entities\Client;
use Modules\Client\Entities\ClientBot;
use Modules\Client\Entities\ClientRole;
use Modules\Core\Entities\Category;
use Modules\Core\Entities\ListServices;
use Modules\Core\Entities\UserServices;
use Modules\Core\Entities\Code;
use Modules\Core\Entities\IdentifierCode;
use Modules\Core\Entities\Favorite;
use Modules\Core\Entities\UserServiceField;
use Modules\Core\Entities\ServiceField;
use Modules\Core\Entities\Info;
use Modules\Core\Entities\Option;
use Modules\Core\Entities\User;
use Illuminate\Support\Arr;
use Modules\Core\Entities\Setting;
use Modules\Core\Entities\UserAction;
use Modules\Educational\Entities\ClassRoom;
use Modules\Educational\Entities\Race;
use Modules\Information\Entities\Information;
use Modules\Menu\Entities\Menu;
use Modules\Portfolio\Entities\Portfolio;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\OptionAttribute;
use Modules\Product\Entities\OptionProperty;
use Modules\Question\Entities\Question;
use Modules\Service\Entities\Service;
use Modules\Widget\Entities\Widget;
use Morilog\Jalali\Jalalian;
use Nwidart\Modules\Facades\Module;

if (!function_exists("Address_Project")) {

    function Address_Project(string $url)
    {

        return Module::asset($url);

    }
}


if(! function_exists('Address_Module')){

    function Address_Module(string $name){

        return Module::assetPath($name) ;
    };

}

if(! function_exists('convert_lang')){

    function convert_lang($model,$lang,$item,$seo=false,$item_seo=null){
       if($seo){
           if($model->translates->where('lang',$lang)->first()){

               return $model->translates->where('lang',$lang)->first()->$item;
           }
           else{
               return $model->seo->$item_seo ;
           }
       }
       else{

           if($model->translates->where('lang',$lang)->first()){
               return $model->translates->where('lang',$lang)->first()->$item;
           }
           else{
               return $model->$item ;
           }

       }

    };

}

if(! function_exists('convert_lang_with_id')){

    function convert_lang_with_id($id,$lang,$item="title",$model="categoty-job"){

        switch ($model){
            case 'category_job':
                $data=ListServices::find($id)->first();
                if($data->translates->where('lang',$lang)->first()){
                    return $data->translates->where('lang',$lang)->first()->$item;
                }
                else{
                    return $data->$item ;
                }
              break;
    
              default:
            break;
        }
          

    }

}


if(! function_exists('convert_slug')){

    function convert_slug($model,$lang,$item){

            if($model->translates->where('lang',$lang)->first()){

                return $model->translates->where('lang',$lang)->first()->$item;
            }
            else{
                return $model->seo->$item ;
            }

    };

}

if(! function_exists('get_language')){

    function get_language($data,$model){

        switch ($model){

            case 'article':
                $item=Article::with('translates')->where('token',$data)->first();
                break;
            case 'information':
                $item=Information::with('translates')->where('token',$data)->first();
                break;
            case 'menu':
                $item=Menu::with('translates')->where('token',$data)->first();
                break;
            case 'product':
                $item=Product::with('translates')->where('token',$data)->first();
                break;
            case 'carousel':
                $item=Carousel::with('translates')->where('token',$data)->first();
                break;
            case 'portfolio':
                $item=Portfolio::with('translates')->where('token',$data)->first();
                break;
            case 'brand':
                $item=Brand::with('translates')->where('token',$data)->first();
                break;
            case 'service':
                $item=Service::with('translates')->where('token',$data)->first();
                break;
                case 'job':
                    $item=UserServices::with('translates')->where('token',$data)->first();
                    break;
            case 'question':
                $item=Question::with('translates')->where('token',$data)->first();
                break;
                case 'categoryjob':
                    $item=ListServices::with('translates')->where('token',$data)->first();
                    break;
                    case 'menujob':
                        $item=MenuJob::with('translates')->where('token',$data)->first();
                        break;
                        case 'attributejob':
                            $item=AttributeJob::with('translates')->where('token',$data)->first();
                            break;
                    
            default:
                break;
        }
        if($item->translates){

            return $item->translates;
        }
        else{
            return "" ;
        }

    };

}
if(! function_exists('convert_date')){

    function convert_date($model,$lang,$item){

        if($model->translates->where('lang',$lang)->first()){
            return $model->$item->format('l , j F Y');
        }
        else{
            return Morilog\Jalali\Jalalian::forge($model->$item)->format('%A, %d %B %y');
        }

    };

}
if(! function_exists('checkAddress')){

    function checkAddress($cart){

        return (is_null($cart->address) || is_null($cart->postal_code) || is_null($cart->mobile) || is_null($cart->first_name) || is_null($cart->last_name)) ? false :true;
    };

}
if(! function_exists('numberCode')){

    function numberCode(){

        return  Str::random(6);;
    };

}

if(! function_exists('calcSend')){



    function calcSend($items)
    {
        return ($items->sum('price') > 300000) ? 0 : 25000;
    }

}

if(! function_exists('totalPrice')){



    function totalPrice($items)
    {
        return calcSend($items) + intval($items->sum('price'));
    }

}




if(! function_exists('hasModule')){

    function hasModule(string $module){

        return array_key_exists($module,Module::getByStatus(1)) ?true :false ;
    };

}
if(! function_exists('showStatus')){

    function showStatus($status){

            switch ($status) {

                case '1' :
                    return "<span class='danger'>غیر فعال</span>";
                case '2' :
                    return "<span class='success'> فعال</span>";
            }
    }

}


if(! function_exists('destroyMedia')){

    function destroyMedia($item,$type){


        if($item->Hasmedia($type)){
            $item->clearMediaCollection($type);
        }
    };

}

if(! function_exists('findAvatar')){

    function findAvatar($email,$size=200){


        $default = "https://www.somewhere.com/homestar.jpg";
        $size = 200;
         return $url = "https://www.gravatar.com/avatar/"
           . md5( strtolower( trim( $email ) ) ) .
           "?d=" . urlencode( $default ) .
           "&s=" . $size;


    };

}
if(! function_exists('statusUrl')){

    function statusUrl($url,$method='GET',$param=null){

        $curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_HEADER, true);
        curl_setopt($curlHandle, CURLOPT_NOBODY  , true);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_exec($curlHandle);
        return curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);


    };

}

if(! function_exists('fullName')){

    function fullName($fname,$lname){

        return (trim($fname). " ".trim($lname));

    };

}

if(! function_exists('checkZeroFirst')){

    function checkZeroFirst($string){
        if($string[0]==='0')
        {
            return substr($string,1);
        }
        return $string;
    }

}


if(! function_exists('setting')){

    function setting($string){
        if(is_string($string))
        {
            return Setting::with('info')->first()->$string;
        }
        return "";
    }

}

if(! function_exists('checkboxStatus')){

    function checkboxStatus($status){
        return ($status=="on") ? 1 : 0;
    }

}


if(! function_exists('findImageAuthor')){

    function findImageAuthor($user,$type,$avatar=null){

        try {
            $model=User::whereId($user)->firstOrFail();
            if(!$model->Hasmedia($type))
            {
                if(isset($avatar) && $avatar && statusUrl(findAvatar($model->email,200))===200){
                    return findAvatar($model->email,200);
                }

                else{

                    return asset('img/no-img.gif');
                }
            }
            else{
                return $model->getFirstMediaUrl($type);
            }
        }catch (Exception $exception){
            return "";
        }


    }

}
if(! function_exists('findNameAuthor')){

    function findNameAuthor($user){

        try {
            $model=User::whereId($user)->firstOrFail();
            return fullName($model->first_name,$model->last_name);
        }catch (Exception $exception){
            return "";
        }


    }

}

if(! function_exists('calcPrePhone')){

    function calcPrePhone($country){

        try {
           switch ($country){
               case 'ir':
                   return "+98";
                   break;
               case 'ar':
                   return "+34";
                   break;
               case 'de':
                   return "+49";
                   break;
               default :
                   return " ";
                   break;
           }
        }catch (Exception $exception){
            return "";
        }


    }

}




if(! function_exists('numPages')){

    function numPages($file) {
        $pdftext = file_get_contents($file);
        $count = preg_match_all("/\/Page\W/", $pdftext, $dummy);
        if(!is_null( $count)){
            return $count;
        }
        else{
            return null;
        }
    }

}

if(! function_exists('generateCode')){

    function generateCode() {
        do{
            $code=Str::random(10);
        }while(!is_null(Code::whereCode($code)->first()));

      return $code;
    }

}

if(! function_exists('array_get')){



    function array_get($array, $key, $default = null)
    {
        return Arr::get($array, $key, $default);
    }

}

if(! function_exists('get_category')){



    function get_category($id)
    {
        return Category::find($id)->symbol;
    }

}


if(! function_exists('child')){



    function child($id)
    {
        return Menu::where('parent',$id)->get();
    }

}
if(! function_exists('childCategory')){



    function childCategory($id)
    {
        return ListServices::where('parent',$id)->get();
    }

}
if(! function_exists('childCategoryTop')){



    function childCategoryTop($id,$category=nul)
    {
        if(is_null($category)){
            return ListServices::where('parent',$id)->take(6)->get();

        }
        else{
            return ListServices::where('parent',$id)->whereIn('id',$category)->take(6)->get();

        }
    }

}

if(! function_exists('childCount')){



    function childCount($id)
    {
        return Menu::where('parent',$id)->count();
    }

}
if(! function_exists('setLangFront')){



    function setLangFront($current,$lang)
    {

        
        if($current=='de'){
            return  str_replace("antenchi.ir", "antenchi.ir/$lang", Request::url());

        }else{
        return  str_replace("antenchi.ir/$current", "antenchi.ir/$lang", Request::url());
        }
       
      
    }

}

if(! function_exists('setLangJob')){



    function setLangJob($current,$lang,$url)
    {

       
        if($current=='de'){
            return  str_replace("antenchi.ir/", "antenchi.ir/$lang/", $url);

        }else{
        return  str_replace("antenchi.ir/$current", "antenchi.ir/$lang",$url);
        }
       
      
    }

}



if(! function_exists('childCountCategory')){



    function childCountCategory($id)
    {
        return ListServices::where('parent',$id)->count();
    }

}

if(! function_exists('childColumn')){



    function childColumn($id,$column)
    {
        return Menu::where('parent',$id)
            ->where('column',$column)->where('href','<>','@')->orderBy('order','desc')->get();
    }

}

if(! function_exists('hasTitleMegaMenu')){



    function hasTitleMegaMenu($id,$column)
    {
        return Menu::where('parent',$id)->where('column',$column)->where('href','@')->orderBy('order','desc')->first();
    }

}

if(! function_exists('showIp')){



    function showIp()
    {
        return $_SERVER['REMOTE_ADDR'];
    }

}

if(! function_exists('showChildCategoryJob')){



    function showChildCategoryJob($obj)
    {
       $category=ListServices::find($obj);
        return ListServices::find($category->parent);
       
    }

}
if(! function_exists('getCategoryWithTitle')){



    function getCategoryWithTitle($obj)
    {
       
        return ListServices::where('title',$obj)->first() ?  ListServices::where('title',$obj)->first() : null ;
       
    }

}

if(! function_exists('showChildCategoryJob')){



    function showChildCategoryJob($obj)
    {
       $category=ListServices::find($obj);
        return ListServices::find($category->parent);
       
    }

}


if(! function_exists('mostJobBot')){



    function mostJobBot($client)
    {
      
        if(count(ClientBot::where('botable_type','Modules\Core\Entities\UserServices')->where('client',$client)->get())){

          return  $bot=ClientBot::where('botable_type','Modules\Core\Entities\UserServices')->where('client',$client)->orderBy('count','DESC')->first();
        }
        else{
            return null;
        }
       
    }

}


if(! function_exists('getAttribute')){



    function getAttribute($id)
    {
        if(AttributeJob::find($id)){
            return AttributeJob::find($id);
        }
        else{
            return " ";
        }
        
    }

}







if(! function_exists('convertNumber')){



    function convertNumber($string,$lang='en') {

        if($lang=='en'){
            return strtr($string, array('۰'=>'0', '۱'=>'1', '۲'=>'2', '۳'=>'3', '۴'=>'4', '۵'=>'5', '۶'=>'6', '۷'=>'7', '۸'=>'8', '۹'=>'9', '٠'=>'0', '١'=>'1', '٢'=>'2', '٣'=>'3', '٤'=>'4', '٥'=>'5', '٦'=>'6', '٧'=>'7', '٨'=>'8', '٩'=>'9'));

        }else{
            return strtr($string, array('0'=>'۰', '1'=>'۱', '2'=>'۲', '3'=>'۳', '4'=>'۴', '5'=>'۵', '6'=>'۶', '7'=>'۷', '8'=>'۸', '9'=>'۹'));

        }
           }

}

if(! function_exists('convertJalali')){



    function convertJalali($date,$time) {

        $date=explode('/',convertNumber($date));
        $time=explode(':',$time);

        $date = (new Jalalian($date[0], $date[1], $date[2],  $time[0], $time[1], $time[2]))->toCarbon()->toDateTimeString();
        return $date;
    }

}


if(! function_exists('loadCountry')){



    function loadCountry()
    {
        $client = new GuzzleHttp\Client();
        $res=$client->request('GET','https://restcountries.eu/rest/v2/all');
        if($res->getStatusCode()==200){
            $res = $res->getBody();
            $res = json_decode($res);
            $result= response()->json($res);
            $array=(array)$result;
            return $array['original'];
        }else{
            return [];
        }

    }

}

if(! function_exists('callingCodeCountry')){



    function callingCodeCountry($country)
    {
        $client = new GuzzleHttp\Client();
        $res=$client->request('GET',"https://restcountries.eu/rest/v2/alpha/$country");
        if($res->getStatusCode()==200){
            $res = $res->getBody();
            $res = json_decode($res);
            $result= response()->json($res);
            $array=(array)$result;
            return "+".$array['original']->callingCodes[0];

        }else{
            return " ";
        }

    }

}

if(! function_exists('callingNameCountry')){



    function callingNameCountry($country)
    {
        $client = new GuzzleHttp\Client();
        $res=$client->request('GET',"https://restcountries.eu/rest/v2/alpha/$country");
        if($res->getStatusCode()==200){
            $res = $res->getBody();
            $res = json_decode($res);
            $result= response()->json($res);
            $array=(array)$result;
            return "+".$array['original']->name;

        }else{
            return " ";
        }

    }

}

if(! function_exists('callingTranslateCountry')){



    function callingTranslateCountry($country,$lang)
    {
        $client = new GuzzleHttp\Client();
        $res=$client->request('GET',"https://restcountries.eu/rest/v2/alpha/$country");
        if($res->getStatusCode()==200){
            $res = $res->getBody();
            $res = json_decode($res);
            $result= response()->json($res);
            $array=(array)$result;
            return "+".$array['original']->translations->$lang;

        }else{
            return " ";
        }

    }

}
if(! function_exists('getTimeZone')){



    function getTimeZone()
    {
        $client = new GuzzleHttp\Client();
        $res=$client->request('GET',"https://timezone.abstractapi.com/v1/current_time?api_key=a2a413a836524e30942975153635c6ab&location=Tehran, Iran");
        if($res->getStatusCode()==200){
            return $res->getBody();

        }else{
            return " ";
        }

    }

}


if(! function_exists('apiPostGerman')){



    function apiPostGerman($code)
    {
        $client = new GuzzleHttp\Client();
        $res=$client->request('GET',"https://public.opendatasoft.com/api/records/1.0/search/?dataset=georef-germany-postleitzahl&q=&facet=plz_name&facet=lan_name&facet=lan_code&facet=plz_code&refine.plz_code=$code");
        if($res->getStatusCode()==200){
           
            $data=json_decode($res->getBody()->getContents());
           
            if(!empty($data->records)){
                    return $data->records;
            }else{
                return false;
            }
           
       
        }else{
            return false;
        }

    }

}

if(! function_exists('apiInfoPostGerman')){



    function apiInfoPostGerman($info)
    {
        $client = new GuzzleHttp\Client();
        $res=$client->request('GET',"https://public.opendatasoft.com/api/records/1.0/search/?dataset=georef-germany-postleitzahl&q=&facet=plz_name&facet=lan_name&facet=lan_code&refine.plz_name=$info");
        if($res->getStatusCode()==200){
           
            $data=json_decode($res->getBody()->getContents());
           
            if(!empty($data->records)){
                    return $data->records;
            }else{
                return false;
            }
           
       
        }else{
            return false;
        }

    }

}




if(! function_exists('apiALLPostGerman')){



    function apiALLPostGerman()
    {
        $client = new GuzzleHttp\Client();
        $res=$client->request('GET',"https://public.opendatasoft.com/api/datasets/1.0/search/?q=");
        if($res->getStatusCode()==200){
           
            $data=json_decode($res->getBody()->getContents());
           
            if(!empty($data->records)){
                    return $data->records;
            }else{
                return false;
            }
           
       
        }else{
            return false;
        }

    }

}


function checkIp($ip)
{
    $client = new GuzzleHttp\Client();
    $res=$client->request('GET',"http://api.ipstack.com/$ip?access_key=e06bdc68c9e1e3af73752ed531046f52");
    if($res->getStatusCode()==200){
       
        $data=json_decode($res->getBody()->getContents());
       
        if(!empty($data)){
                return $data->country_name;
        }else{
            return false;
        }
       
   
    }else{
        return false;
    }

}




if(! function_exists('get_info_today')){



    function get_info_today()
    {
        $client = new \GuzzleHttp\Client();
        $res=$client->get('https://pholiday.herokuapp.com/today/holiday', [
            'headers'         => ['Content-Type'=> 'application/json'],
            'body'            =>  '',
        ]);
        return json_decode($res->getBody()->getContents());

    }

}
if(! function_exists('TranslateCountry')){



    function TranslateCountry($country)
    {
       return __('cms.country_name.'.$country);

    }

}

if(! function_exists('checkJob')){



    function checkJob($job,$client)
    {
       return UserServices::where('user',$client)->where('id',$job)->first() ? true : false;

    }

}
if(! function_exists('getJob')){



    function getJob($job)
    {
       return UserServices::find($job);

    }

}

if(! function_exists('getMenuJob')){



    function getMenuJob($menu)
    {

       return MenuJob::find($menu);

    }

}
if(! function_exists('checkLangTranslate')){



    function checkLangTranslate($model,$lang,$item)
    {

       switch ($model){

        case 'job':

            return Translate::where('translateable_type','Modules\Core\Entities\ListServices')->where('translateable_id',$item)->where('lang',$lang)->first() ? true :false;
        break;
        case 'menujob':

            return Translate::where('translateable_type','Modules\Advertising\Entities\MenuJob')->where('translateable_id',$item)->where('lang',$lang)->first() ? true :false;
        break;

         default:
            break;
       }

    }

}
if(! function_exists('checkFavorite')){



    function checkFavorite($job,$client)
    {
       return Favorite::where('client',$client)->where('favoriteable_type','Modules\Core\Entities\UserServices')->where('favoriteable_id',$job)->first() ? true : false;

    }

}
if(! function_exists('TranslateCity')){



    function TranslateCity($city)
    {
        return __('cms.city_name.'.$city);

    }

}
if(! function_exists('TranslateState')){



    function TranslateState($state)
    {
        return __('cms.state_name.'.$state);

    }

}

if(! function_exists('isNot')){



    function isNot($data)
    {
        return (empty(trim($data)) || is_null($data)) ? true : false ;

    }

}

if(! function_exists('hasPlan')){



    function hasPlan($client)
    {


        return(is_null(Client::with('lastPlan')->whereHas('lastPlan',function ($query){
            $query->where('expire', '>=',date("Y-m-d H:i:s"));
            $query->where('count', '>',0);
        })->whereToken($client)->first())) ? false : true ;


    }

}

if(! function_exists('userChatUnreadCount')){



    function userChatUnreadCount($client)
    {

        return Chat::where('client',$client)->where('user',null)->where('status',1)->count();


    }

}

if(! function_exists('clientChatUnreadCount')){



    function clientChatUnreadCount($client)
    {


        return Chat::where('client',$client)->where('user','!=',null)->where('status',1)->count();


    }

}

if(! function_exists('chatText')){



    function chatText($client)
    {

        return Chat::where('client',$client)->get();


    }

}

if(! function_exists('checkPlan')){



    function checkPlan($client)
    {

        if(is_null($client->lastPlan)){
            return true;
         }
        elseif ($client->lastPlan->count == 0 ){
            return true;
        }
        elseif($client->lastPlan->expire < date("Y-m-d H:i:s")){
            return true;
        }
        else{
            return false;
        }

    }

}


if(! function_exists('getInfoClient')){



   function getInfoClient($client)
   {


      return Client::find($client);

   }

}

if(! function_exists('showSalary')){



    function showSalary($salary)
    {

        return $item=Salary::whereTitle($salary)->first()->symbol;



    }

}
if(! function_exists('sendCustomEmail')){



    function sendCustomEmail($data,$view)
    {

        Mail::send($view,['data'=>$data], function($message)
        {
            $message
                ->to(env('NOTIFICATION_EMAIL'))
                ->from(env('MAIL_USERNAME'))
                ->subject(env('APP_NAME'));

        });



    }

}

if(! function_exists('sendOtpEmail')){



    function sendOtpEmail($data)
    {

        Mail::send('emails.front.otp-email',['data'=>$data], function($message)
        {
            $message
                ->to(env('NOTIFICATION_EMAIL'))
                ->from(env('MAIL_USERNAME'))
                ->subject(env('APP_NAME'));

        });



    }

}




if(! function_exists('showExperience')){



    function showExperience($experience)
    {

        return $item=Experience::whereTitle($experience)->first()->symbol;



    }

}
if(! function_exists('sendMailErrorController')){



    function sendMailErrorController($exception)
    {

        sendCustomEmail(Rest::MailError($exception,\Illuminate\Support\Facades\Request::method(),\Illuminate\Support\Facades\Request::url()),'emails.admin.error');




    }

}
//if(! function_exists('sendMailErrorController')){
//
//
//
//    function sendMailErrorController()
//    {
//
//        sendCustomEmail(Rest::MailError('hello',\Illuminate\Support\Facades\Request::method(),\Illuminate\Support\Facades\Request::url()),'emails.admin.error');
//
//
//
//
//    }
//
//}
if(! function_exists('showCategory')){



    function showCategory($id)
    {

        return Category::whereId($id)->first();



    }

}

if(! function_exists('hasEmployer')){



    function hasEmployer()
    {

        $client=Client::with('role')->find(auth('client')->user()->id);
        $role=ClientRole::whereTitle('employer')->first()->id;

        return $client->role!=$role ? false : true;



    }

}

if(! function_exists('hasSeeker')){



    function hasSeeker()
    {

        $client=Client::with('role')->find(auth('client')->user()->id);
        $role=ClientRole::whereTitle('seeker')->first()->id;

        return $client->role!=$role ? false : true;



    }

}

if(! function_exists('countCategory')){



    function countCategory($model,$category)
    {

       switch ($model){

           case 'product':
               return count(Product::latest()->whereCategory($category)->whereIn('status',[1,3])->get());
               break;
           case 'advertising':
               return count(Advertising::latest()->whereCategory($category)->where('status',2)->get());
               break;
           default:
               break;

       }


    }

}
if(! function_exists('countSalary')){



    function countSalary($model,$item)
    {

        switch ($model){

            case 'advertising':
                return count(Advertising::whereSalary($item)->whereIn('status',[2])->get());
                break;
            default:
                break;

        }


    }

}

if(! function_exists('countSkill')){



    function countSkill($model,$item)
    {

        switch ($model){

            case 'advertising':
                return count(Advertising::whereJsonContains('skill',$item)->whereIn('status',[2])->get());
                break;
            default:
                break;

        }


    }

}
if(! function_exists('countCompany')){



    function countCompany($model,$item)
    {

        switch ($model){

            case 'advertising':
                return count(Advertising::whereHas('info_company',function ($q) use($item){
                    return $q->where('id',$item);
                })->get());
                break;
            default:
                break;

        }


    }

}
if(! function_exists('countTotalCategory')){



    function countTotalCategory($model,$category)
    {

        $list=returnParent($category,'category');

        foreach ($list as $item)
        {
            $result=returnParent($item,'category');
            $total=array_merge($list,$result);
            $list=$total;
        }
        switch ($model){

            case 'product':
                return count(Product::latest()->whereIn('category',$list)->whereIn('status',[1,3])->get());
                break;
            default:
                break;

        }


    }

}

if(! function_exists('hasChildCategory')){



    function hasChildCategory($category)
    {
                return count(Category::latest()->whereParent($category)->whereStatus(1)->get()) == 0 ? false : true ;

    }

}

if(! function_exists('hasChild')){



    function hasChild($token,$model)
    {
        switch ($model){
            case 'classroom':
                return count(ClassRoom::latest()->whereParent($token->id)->whereStatus(1)->get()) == 0 ? false : true ;
                break;
            case 'race' :
                return count(Race::latest()->whereParent($token->id)->whereStatus(1)->get()) == 0 ? false : true ;

                break;
            default:
                break;

        }

    }

}
if(! function_exists('getChild')){



    function getChild($data,$model)
    {
        switch ($model){
            case 'classroom':
                return ClassRoom::latest()->whereParent($data->id)->whereStatus(1)->get();
                break;
            case 'race' :
                return Race::latest()->whereParent($data->id)->whereStatus(1)->get();

                break;
            default:
                break;

        }

    }

}

if(! function_exists('showChildCategory')){



    function showChildCategory($category)
    {
        return Category::latest()->whereParent($category)->whereStatus(1)->get();

    }

}
if(! function_exists('showServiceCategory')){



    function showServiceCategory($category)
    {
        return ListServices::find($category);

    }

}

if(! function_exists('returnParent')){



    function returnParent($id,$model)
    {
        switch ($model){

            case 'category':

               return Category::whereParent($id)->pluck('id')->toArray();

                break;
            default:
                break;

        }

    }

}


if(! function_exists('hasCode')){



    function hasCode($item)
    {
                return Code::where('codeable_type',Client::class)->where('codeable_id',$item->id)->first() ?  true : false;
    }

}
if(! function_exists('checkNationalCode')){



    function checkNationalCode($code)
    {
        if(!preg_match('/^[0-9]{10}$/',$code))
            return false;
        for($i=0;$i<10;$i++)
            if(preg_match('/^'.$i.'{10}$/',$code))
                return false;
        for($i=0,$sum=0;$i<9;$i++)
            $sum+=((10-$i)*intval(substr($code, $i,1)));
        $ret=$sum%11;
        $parity=intval(substr($code, 9,1));
        if(($ret<2 && $ret==$parity) || ($ret>=2 && $ret==11-$parity))
            return true;
        return false;
    }

}

if(! function_exists('hasInfo')){



    function hasInfo($item,$model)
    {
        switch ($model){

            case 'client':

                return Info::where('infoable_type',Client::class)->where('infoable_id',$item->id)->first() ? true : false;

                break;
            default:
                break;

        }
    }

}

if(! function_exists('findAction')){



    function findAction($client,$data,$model)
    {
        switch ($model){

            case 'classroom':

                return UserAction::whereClient($client->id)->where('actionable_id',$data->id)->where('actionable_type',ClassRoom::class)->first();

                break;
            case 'race':

                return UserAction::whereClient($client->id)->where('actionable_id',$data->id)->where('actionable_type',Race::class)->first();

                break;
            default:
                break;

        }

    }

}
if(! function_exists('showAdvertising')){



    function showAdvertising($id)
    {

        return Advertising::with('info_company','creator')->where('id',$id)->get();

    }

}
if(! function_exists('showAdvertisingName')){



    function showAdvertisingName($id)
    {

        return Advertising::with('info_company','creator')->where('id',$id)->first()->title;

    }

}
if(! function_exists('hasCv')){



    function hasCv($item)
    {
        return UserAction::where('actionable_id',$item->id)->where('actionable_type',Advertising::class)->where('client',auth('client')->id())->first() ? true : false;

    }

}
if(! function_exists('loadTranslate')){



    function loadTranslate($word)
    {
        return __('states.'.$word);

    }

}
if(! function_exists('showWidget')){



    function showWidget($name)
    {
       $category=Category::whereTitle($name)->first();
       return Widget::whereCategory($category->id)->orderBy('order','desc')->get();
    }

}

if(! function_exists('dataWidget')){



    function dataWidget($name)
    {
        return $category=Category::whereTitle($name)->first();
    }

}

if(! function_exists('calPercentProfile')){



    function calPercentProfile($client)
    {
        $sum=0;

        if(!is_null(empty($client->first_name))){
            $sum+=10;
        }
        if(!is_null(empty($client->last_name))){
            $sum+=10;
        }
        if(!is_null(empty($client->mobile))){
            $sum+=10;
        }
        if(!is_null(empty($client->identity_card))){
            $sum+=10;
        }
        if($client->hasMedia('images')){
            $sum+=10;
        }
        if(!is_null(empty($client->email))){
            $sum+=10;
        }

        return $sum;
    }

}

if(! function_exists('countDown')){

    function countDown($date)
    {
        return (new Jalalian(intval(Jalalian::forge($date)->format(' %Y')), intval(Jalalian::forge($date)->format(' %m')), intval(Jalalian::forge($date)->format(' %d'))))->toCarbon()->format('Y/m/d');
    }

}

if(! function_exists('getAd')){

    function getAd($token)
    {
        return Ad::whereToken($token)->first();
    }

}
if(! function_exists('expireTime')){

    function expireTime($start,$end)
    {
        return (now() > Carbon::parse($start) && now() < Carbon::parse($end) ) ? true : false;
    }

}
if(! function_exists('expirePlan')){

    function expirePlan($client=null)
    {
        $item=!is_null($client) ? $client : auth('client')->user()->id;
        return !is_null(\Modules\Core\Entities\UserPlan::where('userable_type',Client::class)->where('userable_id',$item)->where('status',1)->first()) ? false :true;
    }

}

if(! function_exists('accessCountAdvert')){

    function accessCountAdvert($client=null)
    {
        $item=!is_null($client) ? $client : auth('client')->user()->id;
        return \Modules\Core\Entities\UserPlan::where('userable_type',Client::class)->where('userable_id',$item)->where('status',1)->first()->count;
    }

}
if(! function_exists('getPlan')){

    function getPlan()
    {
        return \Modules\Core\Entities\UserPlan::where('userable_type',Client::class)->where('userable_id',auth('client')->user()->id)->where('status',1)->first();
    }

}
if(! function_exists('convertArrayParam')){

    function convertArrayParam($array,$value=null)
    {
        if(!is_null($value)){
            return $result=array_map(function ($item) use ($value){
                return $value->$item;
            },$array);
        }
        return $array;


    }

}
if(! function_exists('stringToProperty')){

    function stringToProperty($string)
    {

        return '$values->'.$string;

    }

}
if(! function_exists('hasAction')){

    function hasAction($item,$user,$name)
    {
      switch ($name){
          case 'classroom':
              return (is_null(UserAction::whereClient($user->id)->where('actionable_type',ClassRoom::class)->where('actionable_id',$item->id)->whereStatus(1)->first())) ? false : true;
              break;
          case 'race':
              return (is_null(UserAction::whereClient($user->id)->where('actionable_type',Race::class)->where('actionable_id',$item->id)->whereStatus(1)->first())) ? false : true;
              break;
          case 'advertising':
              return (is_null(UserAction::whereClient($user->id)->where('actionable_type',Advertising::class)->where('actionable_id',$item->id)->whereStatus(1)->first())) ? false : true;
              break;
          default:
              break;
      }
    }

}


if(! function_exists('arrayIp')){

    function arrayIp()
    {
       return explode('.',showIp());
    }

}

if(! function_exists('showClient')){

    function showClient($client)
    {
       return Client::find($client);
    }

}

if(! function_exists('showDay')){

    function showDay()
    {
       return date("l");
    }

}




if(! function_exists('Iranian')){

    function Iranian($ip)
    {

        if(true){

        }

        else{

            return false;
        }
    }

}

if(! function_exists('getField')){

    function getField($item,$title)
    {

        if(ServiceField::whereTitle($title)->first()){

            $field=ServiceField::whereTitle($title)->first()->id;
            return UserServiceField::whereField($field)->whereUser($item->user)->where('user_service',$item->id)->get();
        }

        else{

            return [];
        }
    }

}









































