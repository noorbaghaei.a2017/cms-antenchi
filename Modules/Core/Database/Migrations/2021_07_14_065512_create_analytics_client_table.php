<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnalyticsClientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analytics_client', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ip')->unique()->nullable();
            $table->string('analytic')->nullable();
            $table->string('device')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analytics_client');
    }
}
