<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Auth::routes(['register'=>false]);


Route::group(["prefix"=>'password'],function (){

    Route::post('/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('/final/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
});


Route::feeds();

    Route::group(["prefix"=>config('cms.prefix-admin'), "middleware" => ["auth:web"]], function () {
        Route::resource('/branches', 'BranchController')->only('create','store','destroy','update','index','edit');
        Route::resource('/awards', 'AwardController')->only('create','store','destroy','update','index','edit');
        Route::get('/', 'CoreController@index')->name('dashboard.website');
        Route::get('/meeting', 'CoreController@meeting')->name('meeting.website');
        Route::get('/change/lang/admin/{lang}', 'CoreController@changeLangAdmin')->name('dashboard.change.lang.admin');
        Route::get('/setting', 'SettingController@setting')->name('dashboard.setting.index');
        Route::get('/footer', 'SettingController@footer')->name('dashboard.footer.index');
        Route::get('/template', 'SettingController@template')->name('dashboard.template.index');
        Route::patch('/setting/update', 'SettingController@settingUpdate')->name('dashboard.setting.update');
        Route::get('setting/{lang}/{id}', 'SettingController@languageShow')->name('setting.language.show');
        Route::patch('setting/lang/update/{lang}/{id}', 'SettingController@languageUpdate')->name('setting.language.update');
        Route::patch('/footer/update', 'SettingController@footerUpdate')->name('dashboard.footer.update');
        Route::resource('/roles','RoleController');
        Route::resource('/currencies','CurrencyController');
        Route::get('/contacts', 'ContactController@index')->name('contacts.index');
        Route::get('/contacts/{contact}/edit', 'ContactController@edit')->name('contacts.edit');
        Route::delete('/contacts/{contact}', 'ContactController@destroy')->name('contacts.destroy');

        Route::group(["prefix"=>'search'], function () {
            Route::post('/contact', 'ContactController@search')->name('search.contact');
            Route::post('/currency', 'CurrencyController@search')->name('search.currency');
            Route::post('/awards', 'AwardController@search')->name('search.award');
        });

    });

    Route::post('/contacts', 'ContactController@store')->name('contacts.store');


Route::group(["prefix"=>"ajax/admin"],function() {
    Route::post('/get/chats/{client}', 'AjaxController@getChats')->name('client.get.chats');
    Route::post('/send/message/{client}/{message}', 'AjaxController@sendMessage')->name('client.send.message');
});







