@include('core::layout.modules.index',[

    'title'=>__('questions::questions.index'),
    'items'=>$items,
    'parent'=>'widget',
    'model'=>'widget',
    'directory'=>'widgets',
    'collect'=>__('question::questions.collect'),
    'singular'=>__('question::questions.singular'),
    'create_route'=>['name'=>'widget.question.create','param'=>$token],
    'edit_route'=>['name'=>'widget.question.edit','name_param'=>'widget','param'=>'question'],
    'destroy_route'=>['name'=>'widget.question.destroy','name_param'=>'question'],
    'pagination'=>false,
    'parent_route'=>true,
    'datatable'=>[
    __('cms.title')=>'title',
     __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
    __('cms.title')=>'title',
   __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])
