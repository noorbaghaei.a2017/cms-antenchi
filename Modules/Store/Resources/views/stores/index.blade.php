@include('core::layout.modules.index',[

    'title'=>__('store::stores.index'),
    'items'=>$items,
    'parent'=>'store',
    'model'=>'store',
    'directory'=>'stores',
    'collect'=>__('store::stores.collect'),
    'singular'=>__('store::stores.singular'),
    'create_route'=>['name'=>'stores.create'],
    'edit_route'=>['name'=>'stores.edit','name_param'=>'store'],
    'destroy_route'=>['name'=>'stores.destroy','name_param'=>'store'],
     'search_route'=>true,
      'setting_route'=>true,
     'question_route'=>true,
      'category_route'=>true,
    'datatable'=>[__('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',
    __('cms.slug')=>'slug',
   __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
    __('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',
    __('cms.slug')=>'slug',
    __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])
