@extends('core::layout.panel')
@section('pageTitle', __('cms.create'))
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h2>{{__('cms.create')}} </h2>
                        <small>
                           {{__('store::stores.text-create')}}
                        </small>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
@include('core::layout.alert-danger')
                        <form id="signupForm" role="form" method="post" action="{{route('stores.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="title" class="form-control-label">{{__('cms.title')}} </label>
                                    <input type="text" name="title" value="{{old('title')}}" class="form-control" id="title" required>
                                </div>
                                <div class="col-sm-3">
                                    <label for="image" class="form-control-label">{{__('cms.thumbnail')}}</label>
                                    @include('core::layout.load-single-image')
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="order" class="form-control-label">{{__('cms.order')}} </label>
                                    <input type="number" name="order" value="{{old('order')}}" class="form-control" id="order">
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="status" class="form-control-label">{{__('cms.status')}}  </label>
                                    <select dir="rtl" class="form-control" id="status" name="status" required>

                                        <option  value="1" selected>{{__('cms.active')}}</option>
                                        <option  value="0">{{__('cms.inactive')}}</option>


                                    </select>
                                </div>

                            </div>

                            <div class="form-group row">
                                <span class="text-danger">*</span>
                                <label for="text" class="form-control-label">{{__('cms.text')}} </label>
                                <div class="box m-b-md">
                                    <div class="box m-b-md">
                                        @include('core::layout.text-editor',['item'=>null])
                                    </div>
                                </div>
                            </div>
                            @include('core::layout.list-categories',['item'=>null])
                            <div class="form-group row">

                                <div class="col-sm-12">
                                    <span class="text-danger">*</span>
                                    <label for="excerpt" class="form-control-label">{{__('cms.excerpt')}} </label>
                                    <input type="text" value="{{old('excerpt')}}" name="excerpt" class="form-control" id="excerpt" autocomplete="off" required>
                                </div>

                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <label for="longitude" class="form-control-label">{{__('cms.google-map-longitude')}} </label>
                                    <input type="text" value="{{old('longitude')}}" name="longitude" class="form-control" id="longitude" autocomplete="off">
                                </div>
                                <div class="col-sm-6">
                                    <label for="latitude" class="form-control-label">{{__('cms.google-map-latitude')}} </label>
                                    <input type="text" value="{{old('latitude')}}" name="copy" class="form-control" id="latitude" autocomplete="off">
                                </div>


                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <label for="map" class="form-control-label">{{__('cms.google-map-link')}} </label>
                                    <input type="text" value="{{old('map')}}" name="map" class="form-control" id="map" autocomplete="off">
                                </div>


                            </div>

                            <div class="form-group row m-t-md">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-primary pull-right">{{__('cms.add')}} </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>


        $().ready(function() {
            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    title: {
                        required: true,
                    },
                    text: {
                        required: true,
                    },
                    excerpt: {
                        required: true
                    },
                    order: {
                        number: true,
                        required:true
                    },
                    category: {
                        required: true
                    },


                },
                messages: {
                    title:"عنوان الزامی است",
                    text:"متن الزامی است",
                    excerpt: "خلاصه  الزامی است",
                    category: "دسته بندی  الزامی است",
                    order: "فرمت نادرست",

                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

@endsection
