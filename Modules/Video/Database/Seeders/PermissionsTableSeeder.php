<?php

namespace Modules\Video\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Gallery\Entities\Gallery;
use Modules\Video\Entities\Video;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(Video::class)->delete();

        Permission::create(['name'=>'video-list','model'=>Video::class,'created_at'=>now()]);
        Permission::create(['name'=>'video-create','model'=>Video::class,'created_at'=>now()]);
        Permission::create(['name'=>'video-edit','model'=>Video::class,'created_at'=>now()]);
        Permission::create(['name'=>'video-delete','model'=>Video::class,'created_at'=>now()]);
    }
}
