@include('core::layout.modules.index',[

    'title'=>__('core::categories.index'),
    'items'=>$items,
    'parent'=>'video',
    'model'=>'video',
    'directory'=>'videos',
    'collect'=>__('core::categories.collect'),
    'singular'=>__('core::categories.singular'),
   'create_route'=>['name'=>'video.category.create'],
    'edit_route'=>['name'=>'video.category.edit','name_param'=>'category'],
    'pagination'=>false,
    'parent_route'=>true,
    'datatable'=>[
         __('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',
       __('cms.symbol')=>'symbol',
        __('cms.status')=>'showStatus',
    __('cms.order')=>'order',
           __('cms.view')=>'View',
    __('cms.like')=>'Like',
   __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
             __('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',
       __('cms.symbol')=>'symbol',
        __('cms.status')=>'showStatus',
    __('cms.order')=>'order',
           __('cms.view')=>'View',
    __('cms.like')=>'Like',
   __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])



