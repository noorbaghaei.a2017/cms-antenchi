<?php

namespace Modules\Service\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Translate;
use Modules\Core\Helper\Trades\CommonAttribute;
use Modules\Core\Helper\Trades\TimeAttribute;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Permission\Models\Role;

class Property extends Model implements HasMedia
{
    use TimeAttribute,CommonAttribute,Sluggable,HasMediaTrait;

    protected $fillable = ['title','text','title','icon','user','token','icon','order','slug','excerpt'];

    public function translates()
    {
        return $this->morphMany(Translate::class, 'translateable');
    }
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(70)
            ->height(70)
            ->performOnCollections(config('cms.collection-image'));
    }

    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

}
