@include('core::layout.modules.index',[

    'title'=>__('service::advantages.index'),
    'items'=>$items,
    'parent'=>'service',
    'model'=>'advantage',
    'language_route'=>true,
    'class_model'=> 'advantage',
    'directory'=>'advantages',
    'collect'=>__('service::advantages.collect'),
    'singular'=>__('service::advantages.singular'),
    'create_route'=>['name'=>'advantages.create'],
    'edit_route'=>['name'=>'advantages.edit','name_param'=>'advantage'],
    'destroy_route'=>['name'=>'advantages.destroy','name_param'=>'advantage'],
     'search_route'=>true,
     'setting_route'=>true,
     'pagination'=>true,
    'datatable'=>[
        __('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',
    __('cms.status')=>'GetStatus',
    __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
            __('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',
     __('cms.status')=>'GetStatus',
     __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])
