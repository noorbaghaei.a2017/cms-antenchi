<?php

namespace Modules\Client\Entities;

use Illuminate\Foundation\Auth\User as Authenticate;
use Laravel\Passport\HasApiTokens;
use Modules\Advertising\Entities\Advertising;
use Modules\Core\Entities\Analyzer;
use Modules\Core\Entities\Code;
use Modules\Core\Entities\IdentifierCode;
use Modules\Core\Entities\Info;
use Modules\Core\Entities\UserAction;
use Modules\Core\Entities\UserServices;
use Modules\Core\Entities\UserCompany;
use Modules\Core\Entities\UserPlan;
use Modules\Core\Entities\Wallet;
use Modules\Core\Helper\Trades\TimeAttribute;
use Modules\Educational\Entities\ClassRoom;
use Modules\Educational\Entities\Race;
use Modules\Order\Entities\Order;
use Modules\Payment\Entities\Payment;
use Modules\Product\Entities\Cart;
use Modules\Seo\Entities\Seo;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Client extends Authenticate implements HasMedia
{
    use HasApiTokens,HasMediaTrait,TimeAttribute;

    protected $guard='client';

    protected  $table='clients';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','latitude','longitude','device','notification_email', 'email','code','law','impressum','client_token','start_client_token','status_client_token','ip','mac_address','code_expire','city', 'password','token','mobile','is_active','username','role','first_name','category','last_name','identity_card','phone','phone-2','address','country','postal_code','city_birthday'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public  function info(){
        return $this->morphOne(Info::class,'infoable');
    }

    public function analyzer()
    {
        return $this->morphOne(Analyzer::class, 'analyzerable');
    }

    public  function company(){

        return $this->morphOne(UserCompany::class,'companyable');
    }

    public function seo()
    {
        return $this->morphOne(Seo::class, 'seoable');
    }

    

    public function plan()
    {
        return $this->morphMany(UserPlan::class, 'userable');
    }
    public function cart()
    {
        return $this->belongsTo(Cart::class, 'id','client')->with('items');
    }


    public function myCode()
    {
        return $this->morphOne(Code::class, 'codeable');
    }

    public function payment()
    {
        return $this->hasOne(Payment::class, 'client','id');
    }

    public function order()
    {
        return $this->morphMany(Order::class, 'client');
    }


    public function lastPlan()
    {
        return $this->morphOne(UserPlan::class, 'userable')->orderBy('id','desc')->limit(1);
    }
    public function classrooms()
    {
        return $this->hasMany(UserAction::class, 'client','id')->where('actionable_type','=',ClassRoom::class);
    }

    public function jobs()
    {
        return $this->hasMany(UserServices::class, 'user','id')->orderBy('created_at','desc');
    }

    public function adverts()
    {
        return $this->hasMany(Advertising::class, 'client','id')->with('analyzer');
    }
    public function sendCV()
    {
        return $this->hasMany(UserAction::class, 'client','id')->where('actionable_type','=',Advertising::class);
    }
    public function cvs()
    {
        return $this->hasMany(UserAction::class, 'client','id')->where('actionable_type','=',Advertising::class);
    }
    public function races()
    {
        return $this->hasMany(UserAction::class, 'client','id')->where('actionable_type','=',Race::class);
    }
    public function advertisings()
    {
        return $this->hasMany(UserAction::class, 'client','id')->where('actionable_type','=',Advertising::class);
    }
    public function expirePlan()
    {
        return $this->morphOne(UserPlan::class, 'userable')->orderBy('id','desc')->limit(1)->where('count','=',0);
    }

    public function hasPlan()
    {
        return is_null($this->plan) ?  false:true;
    }

    public function wallet()
    {
        return $this->morphOne(Wallet::class, 'walletable');
    }


    public  function role(){
        return $this->hasOne(ClientRole::class,'id','role');
    }

    public  function getFullNameAttribute(){
        return fullName($this->first_name,$this->last_name);
    }

    public  function getIdentCodeAttribute(){
        return isset($this->myCode->code) ? $this->myCode->code : " ";
    }

    public  function getIsAccessAttribute(){
        return ($this->info->private==0) ? "عمومی" : "خصوصی ";
    }
}
