<?php

namespace Modules\Client\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\ForiegnPostalCode;
use Illuminate\Http\Request;

class StoreBussiness extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
       
        if(isset($request->mobile)){
            return [
                'title'=>'required',
                'excerpt'=>'required',
                'mobile'=>'required',
                'email'=>'required|email',
                'address'=>'required',
                'city'=>'required',
                'postal_code'=>['required','numeric','digits:5',new ForiegnPostalCode()],
                'image'=>'mimes:jpeg,png,jpg|max:3000',
            ];
        }
        elseif(isset($request->phone)){
            return [
                'title'=>'required',
                'excerpt'=>'required',
                'phone'=>'required',
                'email'=>'required|email',
                'address'=>'required',
                'city'=>'required',
                'postal_code'=>['required','numeric','digits:5',new ForiegnPostalCode()],
                'image'=>'mimes:jpeg,png,jpg|max:1000',
            ];
        }
        else{
            return [
                'title'=>'required',
                'excerpt'=>'required',
                'mobile'=>'required',
                'email'=>'required|email',
                'address'=>'required',
                'city'=>'required',
                'postal_code'=>['required','numeric','digits:5',new ForiegnPostalCode()],
                'image'=>'mimes:jpeg,png,jpg|max:1000',
            ];
        }
       
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
