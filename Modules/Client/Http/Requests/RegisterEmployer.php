<?php

namespace Modules\Client\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterEmployer extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'=>'required|string',
            'last_name'=>'required|string',
            'mobile'=>'required|regex:/(09)[0-9]{9}/|unique:clients,mobile,'.$this->token.',token',
            'email'=>'email|unique:clients,email,'.$this->token.',token',
            'code'=>'nullable|exists:codes,code',
            'company'=>'required|string',
            'password'=>'required|min:8',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
