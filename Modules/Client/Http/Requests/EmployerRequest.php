<?php

namespace Modules\Client\Http\Requests;

use App\Rules\Nationalcode;
use Illuminate\Foundation\Http\FormRequest;

class EmployerRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname'=>'required',
            'lastname'=>'required',
            'password'=>'min:8',
            'category'=>'required',
            'email'=>'required|unique:clients,email,'.$this->token.',token',
            'mobile'=>'required|regex:/(09)[0-9]{9}/|unique:clients,mobile,'.$this->token.',token',
            'identity_card' => ['nullable',new Nationalcode()]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
