<?php

namespace Modules\Client\Http\Controllers;

use App\Facades\Rest\Rest;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Str;
use Modules\Client\Entities\Client;
use Modules\Client\Entities\ClientRole;

class RestController extends Controller
{
    private $entity;

    public function __construct()
    {
        $this->entity = new Client();
    }
    public function sign(Request $request){
        try {
            $msg='User Signed';
            $data = [];
            $mobile = $request->input('mobile');
            $code = random_int(1000, 9999);
            $code = substr($mobile, 7, 4);
            $existingUser = Client::where('mobile', $mobile)->first();
            if ($existingUser === null){
                $client=Client::create([
                    'mobile' => $mobile,
                    'code' => $code,
                    'email' => Str::random(8)."@gmail.com",
                    'password' => Str::random(8),
                    'name' => $request->input('name'),
                    'role' => ClientRole::where('title','user')->first()->id,
                    'code_expire'=>now()->addMinutes(2),
                    'token'=>tokenGenerate()
                ]);

                if($client){
                    $data=[
                        'mobile'=>$mobile
                    ];
                    sendCustomEmail($data,'emails.front.register');
                }


                $client->myCode()->create([
                    'code'=>generateCode()
                ]);

                $client->info()->create();

                $client->analyzer()->create();

                $client->company()->create();

                $client->seo()->create();


                $client->wallet()->create([
                    'score'=>500
                ]);

                $client->cart()->create([
                    'client'=>$client->id,
                    'mobile'=>$client->mobile
                ]);
                $data['name'] = $request->input('name');
            }else{
                $data['name'] = $existingUser['name'];
                Client::where([['mobile','=', $mobile]])->update(['code' => $code, 'code_expire'=>now()->addMinutes(2)]);
            }
            return Rest::success($msg,$data);
        } catch (\Exception $exception) {
            return Rest::error($exception);
        }
    }

    public function verify(Request $request){

        try {
            $msg='User Verify.';
            $client=Client::where([['mobile','=',$request->mobile],['code','=',$request->code]])->firstOrFail();
            $data = [
                'token' => $client->createToken('Hirbod Access Token')->accessToken,
                'mobile' => $client->mobile,
                'firstname' => $client->first_name,
                'lastname' => $client->last_name,
                'username' => $client->username
            ];
            return Rest::success($msg,$data);
        }catch (\Exception $exception){

            return Rest::error($exception);
        }
    }
    public function show(){

        try {
            $msg='Profile Fetched';
            $user = $this->entity->whereId(auth('api')->id())->first();
            if ($user === null){
                return Rest::notFound();
            }
            $data=[
                "name"=> $user->name,
                "mobile"=> $user->country.$user->mobile,
                "username"=> $user->username,
                "email"=> $user->email,
                "createdAt"=>$user->jCreated,
                "updatedAt"=>$user->jUpdated,
            ];
            return Rest::success($msg,$data);
        }catch (\Exception $exception){
            return Rest::error($exception);
        }
    }

    public function otp(Request $request)
    {
        try {
            $msg='OTP Submitted.';
            $mobile = checkZeroFirst($request->input('mobile'));
            $country =trim($request->input('country'));
            $type = ($request->has("type")) ?  $request->input('type') : 'sms';
            $user = Client::where([['mobile','=',$mobile],['country','=',$country]])->firstOrFail();
            $code = random_int(1000, 9999);
            $code = substr($mobile, 6, 4);
            $user->update(['code'=>$code,'code_expire'=>now()->addMinutes(2)]);


            return Rest::success($msg,null);
        } catch (\Exception $exception) {
            return Rest::error($exception);
        }
    }
    public function revoke()
    {
        try {
            $msg='All access tokens revoked.';
            $user = \auth('api')->user()->tokens;
            foreach ($user as $item) {
                $item->revoke();
            }
            return  Rest::success($msg,null);
        } catch (\Exception $exception) {
            return  Rest::error($exception);
        }
    }



}
