<?php

namespace Modules\Client\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Modules\Client\Entities\Client;
use Modules\Client\Entities\ClientRole;
use Modules\Client\Http\Requests\ClientRequest;
use Modules\Client\Http\Requests\RegisterClientRequest;
use Modules\Core\Entities\Country;
use Modules\Sms\Http\Controllers\Gateway\FarazSms;

class ClientController extends Controller
{
    protected $entity;

    public function resetPasswordProfile($object,$current,$pass){

        if(Hash::check($current, $object->password)){

            $this->resetPassword($object,$pass);

            return $errorStatus=true;
        }
        else{
            return $errorStatus=false;
        }
    }
    protected function resetPassword($user, $password)
    {
        $this->setUserPassword($user, $password);

        $user->save();
    }

    protected function setUserPassword($user, $password)
    {
        $user->password = Hash::make($password);
    }

    public function __construct()
    {
        $this->entity=new Client();

        $this->middleware('permission:client-list')->only('index');
        $this->middleware('permission:client-create')->only(['create','store']);
        $this->middleware('permission:client-edit' )->only(['edit','update']);
        $this->middleware('permission:client-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity
                ->latest()
                ->whereHas('role',function ($query){
                    $query->where('title', '=', 'user');
                })
                ->paginate(50);
            return view('client::clients.index',compact('items'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            return view('client::clients.create');
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }

    }
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
                !isset($request->first_name) &&
                !isset($request->last_name)  &&
                !isset($request->email) &&
                !isset($request->username) &&
                !isset($request->mobile)

            ){
                $items=$this->entity->latest()->paginate(50);
                return view('client::clients.index',compact('items'));
            }
            $items=$this->entity
                ->where("first_name",trim($request->first_name))
                ->orwhere("last_name",trim($request->last_name))
                ->orwhere("email",trim($request->email))
                ->orwhere("username",trim($request->username))
                ->orwhere("mobile",trim($request->mobile))
                ->paginate(50);
            return view('client::clients.index',compact('items','request'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(RegisterClientRequest $request)
    {
        try {
            $this->entity->first_name=$request->input('firstname');
            $this->entity->last_name=$request->input('lastname');
            $this->entity->username=$request->input('username');
            $this->entity->mobile=$request->input('mobile');
            $this->entity->role=ClientRole::whereTitle('user')->firstOrFail()->id;
            $this->entity->name=$request->input('name');
            $this->entity->two_step=$request->input('two_step');
            $this->entity->email=$request->input('email');
            $this->entity->username=$request->input('username');
            $this->entity->country="German";
            $this->entity->is_active=2;
            $this->entity->phone=$request->input('phone');
            $this->entity->postal_code=$request->input('postal_code');
            $this->entity->city_birthday=$request->input('city_birthday');
            $this->entity->address=$request->input('address');
            $this->entity->password=Hash::make($request->password);
            $this->entity->token=tokenGenerate();
            $this->entity->law=1;
            $this->entity->impressum=1;
            $this->entity->notification_email=1;
            $this->entity->client_token=tokenResetGenerate();
            $this->entity->save();

            $this->entity->analyzer()->create();

            $this->entity->myCode()->create([
                'code'=>generateCode()
            ]);


            $this->entity->info()->create([
                'website'=>$request->website,
                'youtube'=>$request->youtube,
                'github'=>$request->github,
                'facebook'=>$request->facebook,
                'twitter'=>$request->twitter,
                'telegram'=>$request->telegram,
                'private'=>$request->access,
                'instagram'=>$request->instagram,
                'whatsapp'=>$request->whatsapp,
                'pintrest'=>$request->pintrest,
                'about'=>$request->about,
                'address'=>$request->info_address,
                'country'=>$request->info_country,
                'state'=>$request->info_state,
                'city'=>$request->info_city,
                'area'=>$request->info_area,

            ]);

            $this->entity->company()->create([
                'name'=>$request->company_name,
                'count_member'=>$request->count_member,
                'mobile'=>$request->company_mobile,
                'phone'=>$request->company_phone,
                'address'=>$request->company_address,
                'country'=>$request->company_country,
                'state'=>$request->company_state,
                'city'=>$request->company_city,
                'area'=>$request->company_area,


            ]);

            $this->entity->seo()->create([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>(is_null(json_encode($request->input('robots'))) ? [] : json_encode($request->input('robots'))),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);

            $this->entity->wallet()->create([
                'score'=>500
            ]);

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$this->entity->save()){
                return redirect()->back()->with('error',__('client::clients.error'));
            }
            else{
                return redirect(route("clients.index"))->with('message',__('client::clients.store'));
            }


        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return Response
     */
    public function edit($token)
    {
        try {
            $item=$this->entity->whereToken($token)->first();
            return view('client::clients.edit',compact('item'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $token
     * @return Response
     */
    public function update(Request $request, $token)
    {
        try {

            $validator=Validator::make($request->all(),[
                'firstname'=>'required',
                'lastname'=>'required',
                'new_password'=>'nullable|min:8',
                'current_password'=>'nullable|min:8',
                'mobile'=>'required|regex:/(9)[0-9]{9}/|unique:clients,mobile,'.$token.',token',
                'username'=>'required|unique:clients,username,'.$token.',token',
                'email'=>'required|unique:clients,email,'.$token.',token'
            ]);
            if($validator->fails()){
                return  redirect()->back()->withErrors($validator);
            }


            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $updated=$this->entity->update([
                "first_name"=>$request->input('firstname'),
                "last_name"=>$request->input('lastname'),
                "mobile"=>$request->input('mobile'),
                "email"=>$request->input('email'),
                "phone"=>$request->input('phone'),
                "is_active"=>2,
                "username"=>$request->input('username'),
                "postal_code"=>$request->input('postal_code'),
                "city_birthday"=>$request->input('city_birthday'),
                "address"=>$request->input('address'),
                "country"=>$request->input('country'),
                "role"=>ClientRole::whereTitle('user')->firstOrFail()->id,
                "name"=>$request->input('name'),
                "password"=>Hash::make($request->new_password),
                "two_step"=>$request->input('two_step'),

            ]);

            $this->entity->info()->update([
                'website'=>$request->website,
                'youtube'=>$request->youtube,
                'github'=>$request->github,
                'facebook'=>$request->facebook,
                'twitter'=>$request->twitter,
                'telegram'=>$request->telegram,
                'private'=>$request->access,
                'instagram'=>$request->instagram,
                'whatsapp'=>$request->whatsapp,
                'pintrest'=>$request->pintrest,
                'about'=>$request->about,
                'address'=>$request->info_address,
                'country'=>$request->info_country,
                'state'=>$request->info_state,
                'city'=>$request->info_city,
                'area'=>$request->info_area,
            ]);

            $this->entity->company()->update([
                'name'=>$request->company_name,
                'count_member'=>$request->count_member,
                'mobile'=>$request->company_mobile,
                'phone'=>$request->company_phone,
                'address'=>$request->company_address,
                'country'=>$request->company_country,
                'state'=>$request->company_state,
                'city'=>$request->company_city,
                'area'=>$request->company_area,
            ]);

            $this->entity->seo()->update([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>(is_null(json_encode($request->input('robots'))) ? [] : json_encode($request->input('robots'))),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);



            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }



            if(!empty($request->input('current_password')) && !empty($request->input('new_password'))  ){
                if(!$this->resetPasswordProfile($this->entity,$request->input('current_password'),$request->input('new_password'))){
                    return redirect()->back()->with('error',__('client::clients.error'));
                }

            }


            if(!$updated){
                return redirect()->back()->with('error',__('client::clients.error-password'));
            }else{
                return redirect(route("clients.index"))->with('message',__('client::clients.update'));

            }
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            if($this->entity->Hasmedia(config('cms.collection-image'))){
                destroyMedia($this->entity,config('cms.collection-image'));
            }
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('client::clients.error'));
            }else{
                return redirect(route("clients.index"))->with('message',__('client::clients.delete'));
            }
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    public  function cart(){
        try {

            return view('template.auth.cart');
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
}
