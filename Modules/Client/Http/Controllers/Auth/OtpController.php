<?php

namespace Modules\Client\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Modules\Client\Entities\Client;
use Modules\Client\Entities\ClientRole;
use Modules\Client\Http\Requests\OtpRequest;
use Modules\Client\Http\Requests\OtpSendRequest;
use Modules\Payment\Http\Controllers\Facade\MelatFacade;
use Modules\Payment\Http\Controllers\Geteway\Melat;
use Modules\Payment\Http\Controllers\Geteway\ZarinPal;
use Modules\Sms\Http\Controllers\Gateway\FarazSms;
use Modules\Sms\Http\Controllers\Gateway\Idepardazan;

class OtpController extends Controller
{

    public  function otpClient(OtpRequest $request)
    {
//        $code=strval(rand(1000,9999));
        $code=substr($request->mobile,7);
        if(is_null(Client::whereMobile($request->mobile)->first())){
            $client=Client::create([
                'mobile'=>$request->mobile,
                'code'=>$code,
                'code_expire'=>now()->addMinutes(10),
                'role'=>ClientRole::whereTitle('user')->firstOrFail()->id,
                'password'=>Hash::make(Str::random(10)),
                'token'=>tokenGenerate(),
            ]);

            $client->myCode()->create([
                'code'=>generateCode()
            ]);

            $client->info()->create();

            $client->analyzer()->create();

            $client->company()->create();

            $client->seo()->create();


            $client->wallet()->create([
                'score'=>500
            ]);

            $client->cart()->create([
                'client'=>$client->id,
                'mobile'=>$client->mobile
            ]);
            $data=[
                'mobile'=>$request->mobile,
                'client'=>$client
            ];
        }
        else{
            Client::whereMobile($request->mobile)->update([
                'code'=>$code,
                'code_expire'=>now()->addMinutes(10),
            ]);
            $data=[
                'mobile'=>$request->mobile,
                'client'=>Client::whereMobile($request->mobile)->first()
            ];

        }

//        $response=Idepardazan::sendMessage([
//            'mobile'=>$data['mobile'],
//            'code'=>$data['client']->code
//        ]);

//        if(!$response){
//            return redirect()->back()->with('error',__('در ارسال سامانه پیامکی مشکلی پیدا شده است لطفا چند دقیقه دیگه امتحان کنید'));
//        }



        return view('template.auth.verifies.otp-form', compact('data'));

    }

    public  function submitOtp(OtpSendRequest $request)
    {
        try {

            $code=strval(rand(1000,9999));
            if(filter_var($request->identify,FILTER_VALIDATE_EMAIL)){
                $client=Client::whereEmail($request->identify)->firstOrFail();

                $client->update([
                    'code'=>$code,
                    'code_expire'=>now()->addMinutes(10),
                ]);

                $token=$client->token;

                $data=[
                    'code'=>$client->code
                ];
                sendOtpEmail($data);

                return view('template.auth.verifies.otp',compact('token'));

            }else{
                $client=Client::whereMobile($request->identify)->firstOrFail();

                $client->update([
                    'code'=>$code,
                    'code_expire'=>now()->addMinutes(10),
                ]);

                $token=$client->token;

                $kavengar=new FarazSms();

                $message=$client->code;

                $sms=[
                    'sender'=>['+9850009227180825'],
                    'mobile'=>[$client->mobile],
                    'message'=>$message
                ];
                $kavengar->sendMessage($sms);

                return view('template.auth.verifies.otp',compact('token'));
            }

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }


    }
}
