@extends('core::layout.panel')
@section('pageTitle', __('cms.edit'))
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box p-a-xs">
                    <div class="row">
                        <div class="col-md-5">
                            <a href="#">
                                @if(!$item->Hasmedia('images') && statusUrl(findAvatar($item->email,200))===200)
                                    <img style="height: auto" src="{{findAvatar($item->email,200)}}" alt="" >

                                @elseif(statusUrl(findAvatar($item->email,200))!==200)

                                    <img style="width: 300px;height: auto" src="{{asset('img/no-img.gif')}}">
                                @else

                                    <img style="width: 400px;height: auto" src="{{$item->getFirstMediaUrl('images')}}" alt="" class="img-responsive">


                                @endif


                            </a>
                        </div>
                        <div class="col-md-7">
                            <div style="padding-top: 35px">
                                <h6 style="padding-top: 35px"> {{__('cms.full_name')}}  : </h6>
                                <h4 style="padding-top: 35px">    {{fullName($item->firstname,$item->lastname)}}</h4>
                            </div>
                            <div>
                                <h6 style="padding-top: 35px"> {{__('cms.email')}} : </h6>
                                <p>    {{$item->email}}</p>
                            </div>

                            <div>

                                <a target="_blank" href="{{route('login.with.token.client',['token'=>$item->client_token])}}">{{route('login.with.token.client',['token'=>$item->client_token])}}</a>

                            </div>

                            <div>
                                <h6 style="padding-top: 35px"> {{__('cms.mobile')}}  : </h6>
                                <p>    {{$item->mobile}}</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    @include('core::layout.alert-danger')
                    <div class="box-header">
                        <div class="pull-left">
                            <small>
                            {{__('client::clients.text-edit')}}
                            </small>
                        </div>
                        <a onclick="window.print()" class="btn btn-primary btn-sm text-sm text-white pull-right">{{__('cms.print')}} </a>
                    </div>
                    <br>
                    <br>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <form action="{{route('clients.update', ['client' => $item->token])}}" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" value="{{$item->token}}" name="token">
                            {{method_field('PATCH')}}
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <label for="title" class="form-control-label">{{__('cms.thumbnail')}} </label>
                                    @include('core::layout.load-single-image')
                                </div>

                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="firstname" class="form-control-label">{{__('cms.first_name')}}  </label>
                                    <input type="text" name="firstname" value="{{$item->first_name}}" class="form-control" id="firstname" required>
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="lastname" class="form-control-label">{{__('cms.last_name')}} </label>
                                    <input type="text" name="lastname" value="{{$item->last_name}}" class="form-control" id="lastname" required>
                                </div>

                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="mobile" class="form-control-label">{{__('cms.mobile')}} </label>
                                    <input type="text" name="mobile" value="{{$item->mobile}}" class="form-control" id="mobile" required>
                                   
                                </div>
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="email" class="form-control-label">{{__('cms.email')}} </label>
                                    <input type="email" name="email" value="{{$item->email}}" class="form-control" id="email">
                                </div>

                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">

                                    <label for="current_password" class="form-control-label">{{__('cms.current-password')}} </label>
                                    <input type="password" name="current_password" class="form-control" id="current_password">
                                    <small class="help-block text-warning">{{__('cms.empty_password')}}</small>

                                </div>
                                <div class="col-sm-3">

                                    <label for="new_password" class="form-control-label">{{__('cms.new-password')}} </label>
                                    <input type="password" name="new_password" class="form-control" id="new_password">
                                    <small class="help-block text-warning">{{__('cms.empty_password')}}</small>

                                </div>
                               
                                <div class="col-sm-3">
                                    <span class="text-danger">*</span>
                                    <label for="username" class="form-control-label">{{__('cms.username')}} </label>
                                    <input type="text" name="username" value="{{$item->username}}" class="form-control" id="username" >
                                </div>


                            </div>


                            @include('core::layout.modules.info-box',['info'=>$item->info])

                            @include('core::layout.modules.seo-box',['seo'=>$item->seo])

                            <div class="form-group row m-t-md">
                                <div class="col-sm-12">
                                    <input type="submit"  class="btn btn-success btn-sm text-sm" value="{{__('cms.update')}}">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('heads')

    <link href="{{asset('assets/css/validate/screen.css')}}" rel="stylesheet"/>

@endsection()

@section('scripts')


    <script src="{{asset('assets/scripts/validate/jquery.validate.js')}}"></script>


    <script>


        $().ready(function() {


            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    firstname: {
                        required: true
                    },
                    lastname: {
                        required: true
                    },
                    mobile: {
                        required: true,
                        number:true
                    },
                    role: {
                        required: true
                    },


                },
                messages: {
                    firstname:"نام الزامی است",
                    lastname: "نام خانوادگی  لزامی است",
                    mobile: "موبایل  الزامی است",
                    role: "نقش  الزامی است",
                }
            });


            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

    @include('core::layout.scripts.load-location')
    @include('core::layout.scripts.load-location-company')

@endsection
