<?php
return [
    "text-create"=>"you can create your Client",
    "text-edit"=>"you can edit your Clients",
    "store"=>"Registreringen lyckades",
    "error"=>"Problem med att spela in information",
    "error-password"=>"error password",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"members list",
    "singular"=>"client",
    "collect"=>"clients",
    "permission"=>[
        "client-full-access"=>"client full access",
        "client-list"=>"client list",
        "client-delete"=>"client delete",
        "client-create"=>"client create",
        "client-edit"=>"edit client",
    ]
];
