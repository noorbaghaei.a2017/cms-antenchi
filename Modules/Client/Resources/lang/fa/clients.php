<?php

return [
    "text-create"=>"you can create your Client",
    "text-edit"=>"you can edit your Clients",
    "store"=>"ثبت با موفقیت انجام شد.",
    "error-password"=>"پسورد اشتباه است",
    "delete"=>"حذف با موفقیت انجام شد",
    "update"=>"بروز رسانی با موفقیت انجام شد",
    "index"=>"لیست کاربران ",
    "singular"=>"کاربران",
    "collect"=>"کاربران",
    "permission"=>[
        "client-full-access"=>"دسترسی کامل به کاربران",
        "client-list"=>"لیست کاربران",
        "client-delete"=>"حذف کاربر",
        "client-create"=>"ایجاد کاربر",
        "client-edit"=>"ویرایش کاربران",
    ]
];
