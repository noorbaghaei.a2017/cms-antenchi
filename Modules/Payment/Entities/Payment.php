<?php

namespace Modules\Payment\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Client\Entities\Client;
use Modules\Core\Helper\Trades\TimeAttribute;
use Modules\Order\Entities\Order;
use Modules\Order\Helper\OrderHelper;

class Payment extends Model
{

    use TimeAttribute;

    protected $fillable = ['id','client','authority','terminal_id','order','status','pay','title','status','token'];

    public function info_client(){
        return $this->belongsTo(Client::class,'client','id');
    }

    public function order_info(){

        return $this->belongsTo(Order::class,'order','id');
    }






}
