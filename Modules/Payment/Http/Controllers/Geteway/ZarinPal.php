<?php


namespace Modules\Payment\Http\Controllers\Geteway;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Facade;
use Modules\Client\Entities\Client;
use Modules\Core\Entities\UserAction;
use Modules\Order\Entities\Order;
use Modules\Payment\Contract\PaymentGatewayInterface;
use Modules\Payment\Entities\Payment;
use Modules\Product\Entities\Product;

class ZarinPal implements PaymentGatewayInterface
{

      public static  $MerchantID="";

    public static function request($data,Model $model)
    {
        self::$MerchantID = env('ZARINPAL_MERCHANT_ID');
        $user=Client::with('cart')->find(auth('client')->user()->id);
        $Amount = $data['amount'];
        $Description = $data['description'];
        $Email = $data['email'];
        $Mobile = $data['mobile'];
        $CallbackURL = env('APP_URL').'/transaction/callback/request';

        $client = new \SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);

        $result = $client->PaymentRequest(
            [
                'MerchantID' =>  self::$MerchantID,
                'Amount' => $Amount,
                'Description' => $Description,
                'Email' => $Email,
                'Mobile' => $Mobile,
                'CallbackURL' => $CallbackURL,
            ]
        );

                $order=Order::create([
            'order_id'=>123+now()->timestamp,
            'total_price'=>$data['amount']
        ]);




        if(count($user->cart->items) > 0 ){

            foreach ($user->cart->items as $key=>$value){
                $list=Product::find($value->product);
                $list->orderList()->create([
                    'order'=>$order->id,
                    'count'=>$value->count,
                    'price'=>$value->price,
                    'discount'=>0,
                ]);
            }

        }
        else{
            $model->orderList()->create([
                'order'=>$order->id,
                'count'=>1,
                'price'=>$Amount,
                'discount'=>0,
            ]);
        }



       $payment=Payment::create([
            'client'=>$user->id,
            'order'=>$order->id,
            'title'=>$data['title'],
            'authority'=>$result->Authority,
            'token'=>tokenGenerate(),
        ]);

        if(count($user->cart->items) > 0 ){

            foreach ($user->cart->items as $key=>$value){
                $list=Product::find($value->product);
                $list->userAction()->create([
                    'client'=>$user->id,
                    'status'=>0,
                    'payment'=>$payment->id,
                    'start_at'=>now(),
                ]);
            }

        }
        else{
            $model->userAction()->create([
                'client'=>$user->id,
                'status'=>0,
                'payment'=>$payment->id,
                'start_at'=>now(),
            ]);
        }




        if ($result->Status == 100) {
            Header('Location: https://www.zarinpal.com/pg/StartPay/'.$result->Authority);

        } else {
            echo'ERR: '.$result->Status;
        }

    }

    public static function response()
    {

        try {
            DB::beginTransaction();
            self::$MerchantID = env('ZARINPAL_MERCHANT_ID');

            $Authority = $_GET['Authority'];

            $payment=Payment::whereAuthority($Authority)->firstOrFail();


            if ($_GET['Status'] == 'OK') {

                $client = new \SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);

                $result = $client->PaymentVerification([
                    'MerchantID'     => self::$MerchantID,
                    'Authority'      => $Authority
                ]);

                if ($result->Status == 100) {
                    $status=0;
                    $payment->update([
                        'ref_id'=>$result->RefID,
                        'status'=>1
                    ]);
                    $order=Order::findOrFail($payment->order);
                    $order->update([
                        'status'=>1
                    ]);
                    $user_action=UserAction::wherePayment($payment->order);
                    $user_action->update([
                        'status'=>1
                    ]);
                    return redirect(route('transaction.message.callback',['error'=>$status,'payment'=>$payment->token]));
                } else {

                    $status=1;
                    return redirect(route('transaction.message.callback',['error'=>$status,'payment'=>$payment->token]));
                }
            } else {
                $status=0;
                return redirect(route('transaction.message.callback',['error'=>$status,'payment'=>$payment->token]));
            }
        }catch (\Exception $exception){
            DB::rollBack();
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    public static function discount($amount)
    {
        // TODO: Implement discount() method.
    }

}
