@include('core::layout.modules.index',[

    'title'=>__('customer::customers.index'),
    'items'=>$items,
    'parent'=>'customer',
    'model'=>'customer',
    'directory'=>'customers',
    'collect'=>__('customer::customers.collect'),
    'singular'=>__('customer::customers.singular'),
    'create_route'=>['name'=>'customers.create'],
    'edit_route'=>['name'=>'customers.edit','name_param'=>'customer'],
    'destroy_route'=>['name'=>'customers.destroy','name_param'=>'customer'],
     'search_route'=>true,
     'setting_route'=>true,
     'pagination'=>true,
    'datatable'=>[__('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',
    __('cms.slug')=>'slug',
    __('cms.update_date')=>'AgoTimeUpdate',
     __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
    __('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',

      __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    __('cms.slug')=>'slug',
    ],


])
