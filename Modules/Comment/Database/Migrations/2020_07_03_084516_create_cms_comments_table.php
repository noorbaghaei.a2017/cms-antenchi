<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user')->nullable();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('client')->nullable();
            $table->foreign('client')->references('id')->on('clients')->onDelete('cascade');
            $table->text('text')->nullable();
            $table->text('description')->nullable();
            $table->string('email')->nullable();
            $table->string('title')->nullable();
            $table->string('rate')->nullable();
            $table->boolean('secret')->default(1);
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('seen')->default(0);
            $table->morphs('commentable');
            $table->string('token')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_comments');
    }
}
