<?php

namespace Modules\Comment\Entities\Repository;


use Modules\Comment\Entities\Comment;


class CommentRepository  implements CommentRepositoryInterface
{
    public function getAll()
    {
        return Comment::latest()->get();
    }
}
