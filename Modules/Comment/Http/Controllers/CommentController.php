<?php

namespace Modules\Comment\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Modules\Comment\Transformers\CommentCollection;
use Modules\Comment\Entities\Repository\CommentRepositoryInterface;
use Modules\Comment\Entities\Comment;

class CommentController extends Controller
{
   
    public function __construct()
    {
       
        $this->entity=new Comment();
        $this->middleware('permission:comment-list')->only('index');
        $this->middleware('permission:comment-edit' )->only(['edit','update']);
        $this->middleware('permission:comment-delete')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {

            $items=Comment::paginate(config('cms.paginate'));

            return view('comment::comments.index',compact('items'));

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    public function approved($id)
    {
        try {

            $item=Comment::findOrFail($id);

            $item->update([
                'status'=>1
            ]);

            $items=Comment::paginate(config('cms.paginate'));

            return view('comment::comments.index',compact('items'));

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }


    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('comment::comments.create');
    }


    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('comment::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('carousel::carousels.error'));
            }else{
                return redirect(route("carousels.index"))->with('message',__('carousel::carousels.delete'));
            }


        }catch (\Exception $exception){
            // sendMailErrorController($exception);
            return abort('500');
        }
    }
}
